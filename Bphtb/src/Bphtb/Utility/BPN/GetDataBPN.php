<?php

namespace Bphtb\Utility\BPN;

use Bphtb\Model\BPN\BPNTable;
use Bphtb\Model\BPN\BPNBase;
use Bphtb\Model\BPN\BPNAkunTable;
use Bphtb\Model\BPN\BPNAkunBase;

class GetDataBPN
{

    private $BPNTable;
    private $BPNAkunTable;
    //private $URL = "https://services.atrbpn.go.id/BPNApiService/api/BPHTB/getDataATRBPN";
	private $URL = "https://services.atrbpn.go.id/BpnApiService/api/BPHTB/getDataBPN";
    private $URL_CHANGE_PASS = "https://services.atrbpn.go.id/BPNApiService/Api/BPHTB/ChangePassword";

    public function __construct(BPNTable $BPNTable, BPNAkunTable $BPNAkunTable)
    {
        $this->BPNTable = $BPNTable;
        $this->BPNAkunTable = $BPNAkunTable;
    }

    private function fetch($tglTransaksi)
    {
        $json = curl_init($this->URL);
        $akun_bpn = $this->BPNAkunTable->getdata();

        $postData = array(
            'USERNAME' => $akun_bpn->username,
            'PASSWORD' => $akun_bpn->password,
            'TANGGAL' => $tglTransaksi
        );
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Content-type: application/json'),
            CURLOPT_POSTFIELDS => json_encode($postData)
        );
        curl_setopt_array($json, $options);
        $result = curl_exec($json);
        @$decode = json_decode($result, true);
        $data = array();
        if (!empty($decode['result'])) {
            $data = $decode['result'];
        }
		//var_dump($postData); var_dump($decode);
        return $data;
    }

    public function changePassword($old_pass, $new_pass)
    {
        $akun_bpn = $this->BPNAkunTable->getdata();

        if ($akun_bpn->password == $old_pass) {
            $url = curl_init($this->URL_CHANGE_PASS);
            $postData = [
                "username" => $akun_bpn->username,
                "password" => $old_pass,
                "newpassword" => $new_pass
            ];
            $options = [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array('Content-type: application/json'),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ];
            curl_setopt_array($url, $options);
            $result = curl_exec($url);
            $decode = json_decode($result, true); 
            
            if ($decode['respon_code'] == 'OK') {
                $base = new BPNAkunBase();
                $base->akunbpn_id = (int) 1;
                $base->username = $akun_bpn->username;
                $base->password = $new_pass;
                $this->BPNAkunTable->savedata($base);

                return true;
            }
            else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function clearPreviousData($tglTransaksi)
    {
        return $this->BPNTable->clearByDate($tglTransaksi);
    }

    public function sync($tglTransaksi)
    {
        try {
            $data = $this->fetch(date('d/m/Y', strtotime($tglTransaksi)));
			//var_dump($data); die;
            if (count($data) > 0) {
                $this->clearPreviousData($tglTransaksi);

                $base = new BPNBase();
                foreach ($data as $col => $row) {
                    $base->exchangeArray(array_change_key_case($row, CASE_LOWER));
                    $base->tgl_transaksi = date('Y-m-d', strtotime($tglTransaksi));
                    $date = str_replace('/', '-', $base->tgl_akta);
                    $base->tgl_akta = date('Y-m-d', strtotime($date));
                    // var_dump($date);
                    // var_dump($base->tgl_akta); die;
                    $this->BPNTable->save($base);
                }
                return true;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }
}
