<?php

namespace Bphtb\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MenuHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $tbl;

    public function __invoke()
    {
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function cekUserNotaris($id)
    {
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function GetDataSyarat($id)
    {
        $data = $this->gettbl("PersyaratanTable")->getpersyaratan($id);

        return $data;
    }

    public function getfilesyarat($t_idjenistransaksi, $t_idspt)
    {
        $data = $this->gettbl("PersyaratanTable")->syaratfileupload($t_idjenistransaksi, $t_idspt);

        return $data;
    }


    public function SimpanFileUpload($idjenistransaksi, $id_detailspt, $fileupload, $keterangan_file, $id_syarat, $letak_dir, $folderidspt, $folderiddetailspt)
    {
        $data = $this->gettbl("UploadTable")->savedata_syarat($idjenistransaksi, $id_detailspt, $fileupload, $keterangan_file, $id_syarat, $letak_dir, $folderidspt, $folderiddetailspt);

        return $data;
    }


    public function gettbl($tbl_service)
    {
        $this->tbl = $this->getServiceLocator()->getServiceLocator()->get($tbl_service);
        return $this->tbl;
    }

    public function kekata($x)
    {
        $x = abs($x);
        $angka = array(
            "", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }
    public function terbilang($x, $style = 4)
    {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    # $date:date string to be formatted
    # $type:format output
    # $type: 0 (dd-mm-yyyy) ex: 20-04-2016
    #        1 (dd MM yyyy) ex: 20 April 2016
    #        2 (MM)         ex: April
    public function dateFormat($date, $type)
    {

        $bulan = [null, "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        $tgl = explode('-', $date);
        switch ($type) {
            case 0:
                $hasil = date('d-m-Y', strtotime($date));
                break;
            case 1:
                $hasil = $tgl[2] . " " . $bulan[(int)$tgl[1]] . " " . $tgl[0];
                break;
            case 2:
                $hasil = $bulan[(int)$tgl[1]];
                break;
            default:
                $hasil = $date;
                break;
        }

        return $hasil;
    }

    public function DateToIndo($date)
    {
        $BulanIndo = array(
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"
        );
        $bulan = substr($date, 5, 2);
        $result = $BulanIndo[(int) $bulan - 1];
        return ($result);
    }

    public function MonthToIndo($bulan)
    {
        $BulanIndo = array(
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"
        );
        $bulan = $bulan;
        $result = $BulanIndo[(int) $bulan - 1];
        return ($result);
    }

    public function getperingatanvalidasi($idnotaris)
    {
        $data = $this->gettbl('VerifikasiSPTTable')->getperingatanvalidasi($idnotaris);

        return $data;
    }

    public function getperingatanvalidasilimit($idnotaris)
    {
        $data = $this->gettbl('VerifikasiSPTTable')->getperingatanvalidasilimit($idnotaris);

        return $data;
    }

    public function getpesanmasuk($idnotaris, $session)
    {
        $data = $this->gettbl('VerifikasiSPTTable')->getpesanmasuk($idnotaris, $session);

        return $data;
    }

    public function getpesanmasuklimit($idnotaris, $session)
    {
        $data = $this->gettbl('VerifikasiSPTTable')->getpesanmasuklimit($idnotaris, $session);

        return $data;
    }

    public function getpendaftaranbelumdiperiksa()
    {
        $data = $this->gettbl('VerifikasiSPTTable')->jml_data_blm_validasiberkas();

        return $data;
    }


    public function datasemuapesanvaidasi($idspt)
    {
        $data = $this->gettbl('SSPDBphtbTable')->datasemuapesanvaidasi($idspt);

        return $data;
    }

    public function CekIDpersyaratan($idspt)
    {

        $data = $this->gettbl('PersyaratanTable')->cekidpersyaratan($idspt);

        return $data;
    }

    public function persyaratantidaklengkap($idjenistransaksi, $idsyarat)
    {

        $data = $this->gettbl('PersyaratanTable')->persyaratantidaklengkap($idjenistransaksi, $idsyarat);

        return $data;
    }
}
