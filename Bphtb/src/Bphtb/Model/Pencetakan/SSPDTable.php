<?php

namespace Bphtb\Model\Pencetakan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Debug\Debug;

class SSPDTable extends AbstractTableGateway
{

    public $table = "view_sspd";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SSPDBase());
        $this->initialize();
    }

    public function getDataId($id)
    {
        $rowset = $this->select(function (Select $select) use ($id) {
            $select->where(array('t_spt.t_idspt' => $id));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
        });
        $row = $rowset->current();
        return $row;
    }

    public function getDataId_cek($id)
    {
        $rowset = $this->select(array('t_idspt' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getdatasspd(SSPDBase $db, $id)
    {
        if ($id == 2) {
            $where = " ";
        } else {
            $where = " AND t_idnotarisspt::text = '" . $id . "' ";
        }

        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " " . $where . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatasspdbuktipenerimaan(SSPDBase $db, $id, $s_tipe_pejabat)
    {
        if (($s_tipe_pejabat == 1) || ($s_tipe_pejabat == 0)) {
            $where = " ";
        } else {
            $where = " AND t_idnotarisspt::text = '" . $id . "' ";
        }

        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " " . $where . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function cetakbuktipenerimaanvalidasi(SSPDBase $db)
    {


        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $db->no_spt1 . " and a.t_periodespt=" . $db->periode_spt . " ";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatapenelitian($data_get)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt = " . $data_get->no_spt_penelitian . " and a.t_periodespt=" . $data_get->periode_spt_penelitian . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatasurathasilpenelitian($data_get)
    {
        $sql = "select * from view_sspd_pembayaran where
                t_kohirspt = " . $data_get->no_sptpenelitian . " and t_periodespt=" . $data_get->periode_sptpenelitian . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatassspd($t_idspt)
    {
        $sql = "select a.*, b.*, d.s_namanotaris , e.s_namapejabat
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                LEFT JOIN s_users c ON c.s_iduser = a.t_idnotarisspt
		LEFT JOIN s_notaris d ON d.s_idnotaris::text = c.s_idpejabat_idnotaris::text
		left join s_pejabat e on e.s_idpejabat = b.t_pejabatverifikasispt
                where a.t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->buffer();
    }

    public function getdataidsptsebelumnya($t_idspt)
    {
        $sql = "select t_idsptsebelumnya
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getdatassspdsebelumnya($t_idspt)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getdata(SSPDBase $db)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_kohirspt >=" . $db->no_spt1 . " and a.t_kohirspt <=" . $db->no_spt2 . " and a.t_periodespt=" . $db->periode_spt . " and b.t_ketetapanspt=1";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdatas($t_idspt)
    {
        $sql = "select *
                from view_sspd a
                LEFT JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where a.t_idspt =" . $t_idspt . " and b.t_ketetapanspt=1";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataPencatatanSetoran($tgl_cetak)
    {
        $sql = "select *
                from view_sspd a
                INNER JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where b.t_tanggalpembayaran <= '" . date('Y-m-d', strtotime($tgl_cetak)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataPencatatanSetoranBulanan($tgl_setor1, $tgl_setor2)
    {
        $sql = "select *
                from view_sspd a
                INNER JOIN t_pembayaranspt b ON b.t_idspt = a.t_idspt
                where b.t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_setor1)) . "' and '" . date('Y-m-d', strtotime($tgl_setor2)) . "' order by t_tanggalpembayaran asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataPencatatanSetoranBulanan_kpppratama($tgl_setor1, $tgl_setor2)
    {
        $fr_pembayaran_v5 = $this->fr_pembayaran_v5(1);
        //        $sql = "select *
        //                from fr_pembayaran_v5 
        //                where t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_setor1)) . "' and '" . date('Y-m-d', strtotime($tgl_setor2)) . "' order by t_tanggalpembayaran asc";
        $sql = "select *
                from (" . $fr_pembayaran_v5 . ") as z
                where t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_setor1)) . "' and '" . date('Y-m-d', strtotime($tgl_setor2)) . "' order by t_tanggalpembayaran asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataVerifikasi($periode_spt, $tgl_verifikasi1, $tgl_verifikasi2)
    {
        $sql = "select *
                from view_sspd_pembayaran
                where t_periodespt=$periode_spt and t_tglverifikasispt between '" . date('Y-m-d', strtotime($tgl_verifikasi1)) . "' and '" . date('Y-m-d', strtotime($tgl_verifikasi2)) . "' order by t_idspt asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapHarian($tgl_cetak)
    {
        $sql = "select *
                from view_sspd_pembayaran
                where t_tglverifikasispt ='" . date('Y-m-d', strtotime($tgl_cetak)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapBphtb($tgl_cetak1, $tgl_cetak2)
    {
        $sql = "select *
                from view_sspd_pembayaran
                where t_tglverifikasispt between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapHarianSetor($tgl_cetak, $idnotaris)
    {

        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }

        $sql = "select *
                from view_sspd_pembayaran
                where t_tanggalpembayaran ='" . date('Y-m-d', strtotime($tgl_cetak)) . "'  " . $where . "  order by t_idspt asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapBphtbBulanan_v2($tgl_cetak1, $tgl_cetak2, $idnotaris)
    {

        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }

        $sql = "select *
                from view_sspd_pembayaran
                where t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "' AND t_nilaipembayaranspt IS NOT NULL AND t_nilaipembayaranspt != 0 " . $where . "  order by t_idspt asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapBphtbBulanan($tgl_cetak1, $tgl_cetak2, $idnotaris, $order, $nihiltidaktampil)
    {

        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris . " " . $nihiltidaktampil . "";
        } else {
            $where = " " . $nihiltidaktampil;
        }

        $sql = "select *
                from view_sspd_pembayaran
                where t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "' " . $where . "  order by " . $order . " asc";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapBphtbBulanan_perdesa(
        $tgl_cetak1,
        $tgl_cetak2,
        $idnotaris,
        $kodekecamatan,
        $bln_kpp1,
        $bln_kpp2,
        $periode_kpp,
        $nihiltidaktampil
    ) {



        if (!empty($kodekecamatan)) {
            $wherekec = " and substring(t_nopbphtbsppt from 7 for 3) = '" . $kodekecamatan . "' ";
        } else {
            $wherekec = " ";
        }

        $tgl1 = date('Y-m-d', strtotime('01-' . date('m', strtotime($bln_kpp1)) . '-' . $periode_kpp));
        $tgl2 = date('Y-m-t', strtotime('01-' . date('m', strtotime($bln_kpp2)) . '-' . $periode_kpp));

        $sql = "select *
                from view_sspd_pembayaran
                where extract(year from t_tanggalpembayaran) =" . $periode_kpp . " " . $nihiltidaktampil . "  " . $wherekec . "  order by s_kodekecamatan, s_kodekelurahan asc";
        // die($sql);
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataLapBulananBphtb($bulanpelaporan, $iduser)
    {
        //        $sql = "select * from view_data_terbit_ajb 
        //                where date_part('month',t_tglajbbaru) ='" . $bulanpelaporan . "' ";
        //        if (!empty($iduser)) {
        //            $sql .= "and t_idnotaris = $iduser";
        //        }
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt",
            "t_tglprosesspt",
            "t_periodespt",
            "t_nopbphtbsppt",
            "t_totalspt",
            "t_nilaitransaksispt",
            "t_kohirspt",
            "t_potonganspt",
            "t_idjenistransaksi",
            "t_idjenishaktanah",
            "t_thnsppt",
            "t_persyaratan",
            "t_input_sismiop",
            "t_idnotarisspt"
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
            array(
                "t_namawppembeli",
                "t_nikwppembeli",
                "t_alamatwppembeli",
                "t_kecamatanwppembeli",
                "t_kelurahanwppembeli",
                "t_kabkotawppembeli",
                "t_telponwppembeli",
                "t_kodeposwppembeli",
                "t_npwpwppembeli",
                "t_namawppenjual",
                "t_nikwppenjual",
                "t_alamatwppenjual",
                "t_kecamatanwppenjual",
                "t_kelurahanwppenjual",
                "t_kabkotawppenjual",
                "t_telponwppenjual",
                "t_kodeposwppenjual",
                "t_npwpwppenjual",
                "t_luastanah",
                "t_njoptanah",
                "t_luasbangunan",
                "t_njopbangunan",
                "t_totalnjoptanah",
                "t_totalnjopbangunan",
                "t_grandtotalnjop",
                "t_nosertifikathaktanah",
                "t_iddetailsptbphtb",
                "t_kelurahanop",
                "t_kecamatanop",
                "t_kabupatenop",
                "t_rtwppembeli",
                "t_rwwppembeli",
                "t_alamatop",
                "t_rtop",
                "t_rwop",
                "t_tglajbbaru",
                "t_noajbbaru",
                "t_inputbpn",
                "t_statuskonfirmasinotaris",
                "t_tglkonfirmasinotaris",
                "t_rtwppenjual",
                "t_rwwppenjual"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_jenistransaksi"),
            new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"),
            array(
                "s_namajenistransaksi"
            ),
            "LEFT"
        );
        $select->join(array("d" => "s_jenishaktanah"), new \Zend\Db\Sql\Expression("d.s_idhaktanah = a.t_idjenishaktanah"), array(
            "s_namahaktanah"
        ), "LEFT");
        $select->join(
            array("e" => "t_pembayaranspt"),
            new \Zend\Db\Sql\Expression("e.t_idspt = a.t_idspt"),
            array(
                "t_idpembayaranspt",
                "t_kohirpembayaran",
                "t_periodepembayaran",
                "t_tanggalpembayaran",
                "t_idnotaris",
                "t_nilaipembayaranspt",
                "t_kodebayarspt",
                "t_verifikasispt",
                "t_tglverifikasispt",
                "t_pejabatverifikasispt",
                "t_statusbayarspt",
                "t_ketetapanspt",
                "t_kodebayarbanksppt"
            ),
            "LEFT"
        );
        $select->join(array("g" => "s_users"), new \Zend\Db\Sql\Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new \Zend\Db\Sql\Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(
            "s_namanotaris",
            "s_alamatnotaris"
        ), "LEFT");
        $where = new Where();
        $where->literal("((b.t_tglajbbaru IS NOT NULL) AND (b.t_noajbbaru IS NOT NULL))");
        $where->literal("date_part('month',b.t_tglajbbaru) ='" . $bulanpelaporan . "'");
        if (!empty($iduser)) {
            $where->literal("a.t_idnotarisspt = $iduser");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataLapBulananBphtbNotaris($bulanpelaporan, $periode_spt, $idnotarisdipilih, $idnotaris, $s_tipe_pejabat)
    {

        //        if (!empty($idnotarisdipilih)) {
        //            $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotarisdipilih . '';
        //        } else {
        //            if (!empty($idnotaris)) {
        //                if ($s_tipe_pejabat == 2) {
        //                    $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotaris . '';
        //                } else {
        //                    $whereidnotaris = '';
        //                }
        //            }
        //        }
        //
        //        $sql = "select * from view_data_terbit_ajb 
        //                where date_part('month',t_tglajbbaru) ='" . $bulanpelaporan . "' AND date_part('year',t_tglajbbaru) ='" . $periode_spt . "'  " . $whereidnotaris . " ";
        //
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt",
            "t_tglprosesspt",
            "t_periodespt",
            "t_nopbphtbsppt",
            "t_totalspt",
            "t_nilaitransaksispt",
            "t_kohirspt",
            "t_potonganspt",
            "t_idjenistransaksi",
            "t_idjenishaktanah",
            "t_thnsppt",
            "t_persyaratan",
            "t_input_sismiop",
            "t_idnotarisspt"
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
            array(
                "t_namawppembeli",
                "t_nikwppembeli",
                "t_alamatwppembeli",
                "t_kecamatanwppembeli",
                "t_kelurahanwppembeli",
                "t_kabkotawppembeli",
                "t_telponwppembeli",
                "t_kodeposwppembeli",
                "t_npwpwppembeli",
                "t_namawppenjual",
                "t_nikwppenjual",
                "t_alamatwppenjual",
                "t_kecamatanwppenjual",
                "t_kelurahanwppenjual",
                "t_kabkotawppenjual",
                "t_telponwppenjual",
                "t_kodeposwppenjual",
                "t_npwpwppenjual",
                "t_luastanah",
                "t_njoptanah",
                "t_luasbangunan",
                "t_njopbangunan",
                "t_totalnjoptanah",
                "t_totalnjopbangunan",
                "t_grandtotalnjop",
                "t_nosertifikathaktanah",
                "t_iddetailsptbphtb",
                "t_kelurahanop",
                "t_kecamatanop",
                "t_kabupatenop",
                "t_rtwppembeli",
                "t_rwwppembeli",
                "t_alamatop",
                "t_rtop",
                "t_rwop",
                "t_tglajbbaru",
                "t_noajbbaru",
                "t_inputbpn",
                "t_statuskonfirmasinotaris",
                "t_tglkonfirmasinotaris",
                "t_rtwppenjual",
                "t_rwwppenjual"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_jenistransaksi"),
            new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"),
            array(
                "s_namajenistransaksi"
            ),
            "LEFT"
        );
        $select->join(array("d" => "s_jenishaktanah"), new \Zend\Db\Sql\Expression("d.s_idhaktanah = a.t_idjenishaktanah"), array(
            "s_namahaktanah"
        ), "LEFT");
        $select->join(
            array("e" => "t_pembayaranspt"),
            new \Zend\Db\Sql\Expression("e.t_idspt = a.t_idspt"),
            array(
                "t_idpembayaranspt",
                "t_kohirpembayaran",
                "t_periodepembayaran",
                "t_tanggalpembayaran",
                "t_idnotaris",
                "t_nilaipembayaranspt",
                "t_kodebayarspt",
                "t_verifikasispt",
                "t_tglverifikasispt",
                "t_pejabatverifikasispt",
                "t_statusbayarspt",
                "t_ketetapanspt",
                "t_kodebayarbanksppt"
            ),
            "LEFT"
        );
        $select->join(array("g" => "s_users"), new \Zend\Db\Sql\Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new \Zend\Db\Sql\Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(
            "s_namanotaris",
            "s_alamatnotaris"
        ), "LEFT");
        $where = new Where();
        $where->literal("((b.t_tglajbbaru IS NOT NULL) AND (b.t_noajbbaru IS NOT NULL))");
        $where->literal("date_part('month',b.t_tglajbbaru) ='" . $bulanpelaporan . "' AND date_part('year',b.t_tglajbbaru) ='" . $periode_spt . "'");
        if (!empty($idnotarisdipilih)) {
            $where->literal("a.t_idnotarisspt = '$idnotarisdipilih'");
        } else {
            if (!empty($idnotaris)) {
                if ($s_tipe_pejabat == 2) {
                    $where->literal("a.t_idnotarisspt = '$idnotaris'");
                }
            }
        }
        $select->where($where);
        // echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataKonfBulananBphtb($bulanpelaporankonf, $iduser)
    {
        //        $sql = "select * from view_data_terbit_ajb 
        //                where date_part('month',t_tglajbbaru) ='" . $bulanpelaporankonf . "' and t_statuskonfirmasinotaris = true and t_tglkonfirmasinotaris is not null ";
        //        if (!empty($iduser)) {
        //            $sql .= "and t_idnotaris = $iduser";
        //        }
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt",
            "t_tglprosesspt",
            "t_periodespt",
            "t_nopbphtbsppt",
            "t_totalspt",
            "t_nilaitransaksispt",
            "t_kohirspt",
            "t_potonganspt",
            "t_idjenistransaksi",
            "t_idjenishaktanah",
            "t_thnsppt",
            "t_persyaratan",
            "t_input_sismiop",
            "t_idnotarisspt"
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
            array(
                "t_namawppembeli",
                "t_nikwppembeli",
                "t_alamatwppembeli",
                "t_kecamatanwppembeli",
                "t_kelurahanwppembeli",
                "t_kabkotawppembeli",
                "t_telponwppembeli",
                "t_kodeposwppembeli",
                "t_npwpwppembeli",
                "t_namawppenjual",
                "t_nikwppenjual",
                "t_alamatwppenjual",
                "t_kecamatanwppenjual",
                "t_kelurahanwppenjual",
                "t_kabkotawppenjual",
                "t_telponwppenjual",
                "t_kodeposwppenjual",
                "t_npwpwppenjual",
                "t_luastanah",
                "t_njoptanah",
                "t_luasbangunan",
                "t_njopbangunan",
                "t_totalnjoptanah",
                "t_totalnjopbangunan",
                "t_grandtotalnjop",
                "t_nosertifikathaktanah",
                "t_iddetailsptbphtb",
                "t_kelurahanop",
                "t_kecamatanop",
                "t_kabupatenop",
                "t_rtwppembeli",
                "t_rwwppembeli",
                "t_alamatop",
                "t_rtop",
                "t_rwop",
                "t_tglajbbaru",
                "t_noajbbaru",
                "t_inputbpn",
                "t_statuskonfirmasinotaris",
                "t_tglkonfirmasinotaris",
                "t_rtwppenjual",
                "t_rwwppenjual"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_jenistransaksi"),
            new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"),
            array(
                "s_namajenistransaksi"
            ),
            "LEFT"
        );
        $select->join(array("d" => "s_jenishaktanah"), new \Zend\Db\Sql\Expression("d.s_idhaktanah = a.t_idjenishaktanah"), array(
            "s_namahaktanah"
        ), "LEFT");
        $select->join(
            array("e" => "t_pembayaranspt"),
            new \Zend\Db\Sql\Expression("e.t_idspt = a.t_idspt"),
            array(
                "t_idpembayaranspt",
                "t_kohirpembayaran",
                "t_periodepembayaran",
                "t_tanggalpembayaran",
                "t_idnotaris",
                "t_nilaipembayaranspt",
                "t_kodebayarspt",
                "t_verifikasispt",
                "t_tglverifikasispt",
                "t_pejabatverifikasispt",
                "t_statusbayarspt",
                "t_ketetapanspt",
                "t_kodebayarbanksppt"
            ),
            "LEFT"
        );
        $select->join(array("g" => "s_users"), new \Zend\Db\Sql\Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new \Zend\Db\Sql\Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(
            "s_namanotaris",
            "s_alamatnotaris"
        ), "LEFT");
        $where = new Where();
        $where->literal("((b.t_tglajbbaru IS NOT NULL) AND (b.t_noajbbaru IS NOT NULL))");
        $where->literal("date_part('month',b.t_tglajbbaru) ='" . $bulanpelaporankonf . "' "
            . "and b.t_statuskonfirmasinotaris = true and b.t_tglkonfirmasinotaris is not null ");
        if (!empty($iduser)) {
            $where->literal("e.t_idnotaris = $iduser");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataHasilBpn($tgl_hasilbpn1, $tgl_hasilbpn2)
    {
        //        $sql = "select * from view_hasilbpn
        //                where t_tglprosesspt between '" . date('Y-m-d', strtotime($tgl_hasilbpn1)) . "' and '" . date('Y-m-d', strtotime($tgl_hasilbpn2)) . "'";
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_hasilbpn = $this->view_hasilbpn();
        $select->from(array("a" => $view_hasilbpn));
        $where = new Where();
        $where->literal("t_tglprosesspt between '" . date('Y-m-d', strtotime($tgl_hasilbpn1)) . "' and '" . date('Y-m-d', strtotime($tgl_hasilbpn2)) . "'");
        $select->where($where);
        // echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select)->execute();
        return $state;
    }

    public function view_hasilbpn()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt",
            "t_kohirspt",
            "t_kohirketetapanspt",
            "t_tglprosesspt",
            "t_periodespt",
            "t_idnotarisspt",
            "t_objekspt",
            "t_idtarifspt",
            "t_ketetapanspt",
            "t_tglketetapanspt",
            "t_tgljatuhtempospt",
            "t_nopbphtbsppt",
            "t_kodebayarbanksppt",
            "t_idjenistransaksi",
            "t_idjenishaktanah",
            "t_idrefspt",
            "t_pejabatverifikasispt",
            "t_dasarspt",
            "t_totalspt",
            "t_nilaitransaksispt",
            "t_potonganspt",
            "t_thnsppt",
            "t_persyaratan",
            "t_idjenisdoktanah"
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
            array(
                "t_inputbpn",
                "t_iddetailsptbphtb",
                "t_namawppembeli",
                "t_nikwppembeli",
                "t_alamatwppembeli",
                "t_kecamatanwppembeli",
                "t_kelurahanwppembeli",
                "t_kabkotawppembeli",
                "t_telponwppembeli",
                "t_kodeposwppembeli",
                "t_npwpwppembeli",
                "t_namawppenjual",
                "t_nikwppenjual",
                "t_alamatwppenjual",
                "t_kecamatanwppenjual",
                "t_kelurahanwppenjual",
                "t_kabkotawppenjual",
                "t_telponwppenjual",
                "t_kodeposwppenjual",
                "t_npwpwppenjual",
                "t_luastanah",
                "t_njoptanah",
                "t_luasbangunan",
                "t_njopbangunan",
                "t_totalnjoptanah",
                "t_totalnjopbangunan",
                "t_grandtotalnjop",
                "t_nosertifikathaktanah",
                "t_kelurahanop",
                "t_kecamatanop",
                "t_ketwaris",
                "t_terbukti",
                "t_rtwppembeli",
                "t_rwwppembeli",
                "t_alamatop",
                "t_rtop",
                "t_rwop",
                "t_dokpersyaratan",
                "t_namasppt",
                "t_tglajb",
                "t_luastanahbpn",
                "t_luasbangunanbpn",
                "t_tglajbbaru",
                "t_noajbbaru",
                "t_statuspelaporannotaris",
                "t_tglpelaporannotaris",
                "t_kabupatenop",
                "t_rtwppenjual",
                "t_rwwppenjual",
                "t_nosertifikatbaru",
                "t_tglsertifikatbaru"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_jenistransaksi"),
            new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"),
            array(
                "s_idjenistransaksi",
                "s_kodejenistransaksi",
                "s_namajenistransaksi"
            ),
            "LEFT"
        );
        $select->join(
            array("d" => "s_jenishaktanah"),
            new \Zend\Db\Sql\Expression("d.s_idhaktanah = a.t_idjenishaktanah"),
            array(
                "s_idhaktanah",
                "s_kodehaktanah",
                "s_namahaktanah"
            ),
            "LEFT"
        );
        $select->join(
            array("g" => "s_users"),
            new \Zend\Db\Sql\Expression("g.s_iduser = a.t_idnotarisspt"),
            array(
                "s_iduser",
                "s_username",
                "s_password",
                "s_jabatan",
                "s_akses",
                "s_idpejabat_idnotaris",
                "s_tipe_pejabat"
            ),
            "LEFT"
        );
        $select->join(
            array("h" => "s_notaris"),
            new \Zend\Db\Sql\Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"),
            array(
                "s_idnotaris",
                "s_namanotaris",
                "s_alamatnotaris",
                "s_kodenotaris",
                "s_sknotaris",
                "s_tgl1notaris",
                "s_tgl2notaris",
                "s_statusnotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("e" => "t_pembayaranspt"),
            new \Zend\Db\Sql\Expression("e.t_idspt = a.t_idspt"),
            array(
                "t_idpembayaranspt",
                "t_kohirpembayaran",
                "t_periodepembayaran",
                "t_tanggalpembayaran",
                "t_objekspt_pembayaran" => "t_objekspt",
                "t_idnotaris",
                "t_ketetapanspt_pembayaran" => "t_ketetapanspt",
                "t_nilaipembayaranspt",
                "t_idkorekspt",
                "t_kodebayarspt",
                "t_verifikasispt",
                "t_tglverifikasispt",
                "t_pejabatverifikasispt_pembayaran" => "t_pejabatverifikasispt",
                "t_statusbayarspt",
                "t_kodebayarbanksppt_pembayaran" => "t_kodebayarbanksppt",
                "t_dendabulan"
            ),
            "LEFT"
        );
        $select->join(
            array("f" => "t_pemeriksaan"),
            new \Zend\Db\Sql\Expression("f.p_idpembayaranspt = e.t_kohirpembayaran"),
            array(
                "p_idpemeriksaan",
                "p_idpembayaranspt",
                "p_luastanah",
                "p_luasbangunan",
                "p_njoptanah",
                "p_njopbangunan",
                "p_totalnjoptanah",
                "p_totalnjopbangunan",
                "p_grandtotalnjop",
                "p_nilaitransaksispt",
                "p_potonganspt",
                "p_ketwaris",
                "p_terbukti",
                "p_idjenistransaksi",
                "p_idjenishaktanah",
                "p_totalspt",
                "p_nilaipembayaranspt",
                "p_nilaikurangbayar",
                "p_kohirskpdkb",
                "p_pembayaranskpdkb"
            ),
            "LEFT"
        );
        $where = new Where();
        $where->literal("(b.t_inputbpn = true)");
        $select->where($where);

        return $select;
    }

    public function getNotaris($iduser)
    {
        $sql = "select s_namanotaris, s_alamatnotaris from view_data_notaris where s_iduser = $iduser";
        $statement = $this->adapter->query($sql);
        $state = $statement->execute();
        return $state->current();
    }

    // public function getDataRealisasi($periode_spt, $tgl_cetakrealisasi) {
    public function getDataRealisasi($periode_spt, $bulan1, $bulan2)
    {
        // $kalender = explode('-', $tgl_cetakrealisasi);
        // $bulan = $kalender[1];
        $sql = "select s_namajenistransaksi, 
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) < '$bulan1'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as real_sd_bln_lalu,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) >= '$bulan1' and  date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as real_bln_ini,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as real_sd_bln_ini
                from s_jenistransaksi ";
        // die($sql);
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataRealisasiPerbulan($periode_spt, $bulan1, $bulan2)
    {
        // $bln_name = $this->MenuHelper()->MonthToIndo($bulan);
        $sql = "select s_namajenistransaksi, 
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 1 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Januari,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 2 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Pebruari,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 3 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Maret,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 4 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as April,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 5 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Mei,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 6 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Juni,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 7 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Juli,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 8 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Agustus,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 9 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as September,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 10 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Oktober,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 11 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Nopember,
                    (select coalesce(sum(a.t_nilaipembayaranspt))
                        from view_sspd_pembayaran a
                        left join s_jenistransaksi b 
                        on b.s_idjenistransaksi = a.t_idjenistransaksi
                        where a.t_periodespt=$periode_spt 
                        and date_part('month',a.t_tanggalpembayaran) = 12 and date_part('month',a.t_tanggalpembayaran) <= '$bulan2'
                        and  s_jenistransaksi.s_idjenistransaksi = b.s_idjenistransaksi
                        ) as Desember
                from s_jenistransaksi ";
        // die($sql);
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataValidasiSkpdkb($no_sspd1, $no_sspd2)
    {
        $sql = "select *
                from view_sspd_pembayaran a
                left join t_pemeriksaan b on b.p_idpembayaranspt = a.t_idpembayaranspt
                where a.t_verifikasispt = true and a.t_kohirspt between " . $no_sspd1 . " and " . $no_sspd2 . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    //===================== menu cetak data pendaftaran
    public function getDataPendaftaran($periode_spt, $tgl_verifikasi1, $tgl_verifikasi2, $idnotaris, $idnotarisdipilih, $s_tipe_pejabat)
    {
        if (!empty($idnotarisdipilih)) {
            $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotarisdipilih . '';
        } else {
            if (!empty($idnotaris)) {
                if ($s_tipe_pejabat == 2) {
                    $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotaris . '';
                } else {
                    $whereidnotaris = '';
                }
            }
        }

        $arr_tgl = explode("-", $tgl_verifikasi1);
        $tgl_awal = $arr_tgl[2] . "-" . $arr_tgl[1] . "-" . $arr_tgl[0];
        $arr_tgl2 = explode("-", $tgl_verifikasi2);
        $tgl_akhir = $arr_tgl2[2] . "-" . $arr_tgl2[1] . "-" . $arr_tgl2[0];
        $sql = "select *
                from view_pendaftaran
                where t_periodespt=$periode_spt and t_tglprosesspt between '" . $tgl_awal . "' and '" . $tgl_akhir . "' " . @$whereidnotaris . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    //===================== end menu cetak data pendaftaran
    //===================== menu cetak data status berkas
    public function getDataStatusBerkas($periode_spt, $tgl_verifikasi1, $tgl_verifikasi2, $idnotaris, $idnotarisdipilih, $s_tipe_pejabat)
    {
        if (!empty($idnotarisdipilih)) {
            $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotarisdipilih . '';
        } else {
            if (!empty($idnotaris)) {
                if ($s_tipe_pejabat == 2) {
                    $whereidnotaris = ' AND t_idnotarisspt = ' . $idnotaris . '';
                } else {
                    $whereidnotaris = '';
                    //$whereidnotaris = ' AND t_idnotarisspt = ' . $idnotaris . '';
                }
            }
        }
        $arr_tgl = explode("-", $tgl_verifikasi1);
        $tgl_awal = $arr_tgl[2] . "-" . $arr_tgl[1] . "-" . $arr_tgl[0];
        $arr_tgl2 = explode("-", $tgl_verifikasi2);
        $tgl_akhir = $arr_tgl2[2] . "-" . $arr_tgl2[1] . "-" . $arr_tgl2[0];
        $sql = "select *
                from view_pendaftaran
                where t_periodespt=$periode_spt and t_tglprosesspt between '" . $tgl_awal . "' and '" . $tgl_akhir . "' " . @$whereidnotaris . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    //===================== end menu cetak data status berkas
    //===================== menu cetak kode bayar
    public function ambilsemuadatasptvalidasi($idnotaris, $no_spt1, $no_spt2, $periode_spt)
    {
        //        if (!empty($idnotaris)) {
        //            $whereidnotaris = " WHERE t_idnotarisspt = " . $idnotaris . " and t_kohirspt >=" . $no_spt1 . " and t_kohirspt <=" . $no_spt2 . " and t_periodespt=" . $periode_spt . "";
        //        } else {
        //            $whereidnotaris = " Where t_kohirspt >=" . $no_spt1 . " and t_kohirspt <=" . $no_spt2 . " and t_periodespt=" . $periode_spt . "";
        //        }
        //        $sql = "select *
        //                from view_data_verifikasi_isi
        //                " . @$whereidnotaris . "";
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_data_verifikasi_isi = $this->view_data_verifikasi_isi();
        $select->from(array("a" => $view_data_verifikasi_isi));
        $where = new Where();
        if (!empty($idnotaris)) {
            $where->literal("t_idnotarisspt = " . $idnotaris . " and t_kohirspt >=" . $no_spt1 . " and t_kohirspt <=" . $no_spt2 . " and t_periodespt=" . $periode_spt . "");
        } else {
            $where->literal("t_kohirspt >=" . $no_spt1 . " and t_kohirspt <=" . $no_spt2 . " and t_periodespt=" . $periode_spt . "");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select)->execute();
        return $state;
    }

    public function ambildatainsptvalidasi($data_tidspt)
    {
        //        if (@$data_tidspt) {
        //            if (!is_array($data_tidspt)) {
        //                if (!is_numeric($data_tidspt))
        //                    die('format salah salah');
        //                $where = "where t_idspt='{$data_tidspt}' ";
        //            }
        //            else {
        //                foreach ($data_tidspt as $val) {
        //                    if (strlen($val) && !is_numeric($val)) {
        //                        return false;
        //                    }
        //                }
        //
        //                $data_tidspt = implode(',', $data_tidspt);
        //                $where = "where t_idspt in ({$data_tidspt})";
        //            }
        //        } else {
        //            $where = "where t_idspt::text = 'x'";
        //        }
        //        $where = htmlspecialchars_decode($where);
        //        $sql = "SELECT * FROM view_data_verifikasi_isi $where ";
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_data_verifikasi_isi = $this->view_data_verifikasi_isi();
        $select->from(array("a" => $view_data_verifikasi_isi));
        $where = new Where();
        if (@$data_tidspt) {
            if (!is_array($data_tidspt)) {
                if (!is_numeric($data_tidspt))
                    die('format salah salah');
                $where->literal("t_idspt='{$data_tidspt}'");
            } else {
                foreach ($data_tidspt as $val) {
                    if (strlen($val) && !is_numeric($val)) {
                        return false;
                    }
                }

                $data_tidspt = implode(',', $data_tidspt);
                $where->literal("t_idspt in ({$data_tidspt})");
            }
        } else {
            $where->literal("t_idspt::text = 'x'");
        }
        $select->where($where);
        // die($select->getSqlString());
        $state = $sql->prepareStatementForSqlObject($select)->execute();
        return $state;
    }

    public function ambildatakodebayar($data_get)
    {
        //        var_dump($data_get->no_spt1);
        //        exit();
        //        $sql = "SELECT * FROM view_data_verifikasi_isi where t_kohirspt between $data_get->no_spt1 and $data_get->no_spt2 and t_kodebayarbanksppt_pembayaran is not null";
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_data_verifikasi_isi = $this->view_data_verifikasi_isi();
        $select->from(array("a" => $view_data_verifikasi_isi));
        $where = new Where();
        $where->literal("t_kohirspt between $data_get->no_spt1 and $data_get->no_spt2 and t_kodebayarbanksppt_pembayaran is not null");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select)->execute();
        return $state;
    }

    public function view_data_verifikasi_isi($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt",
            "t_kohirspt",
            "t_kohirketetapanspt",
            "t_tglprosesspt",
            "t_periodespt",
            "t_idnotarisspt",
            "t_objekspt",
            "t_idtarifspt",
            "t_ketetapanspt",
            "t_tglketetapanspt",
            "t_tgljatuhtempospt",
            "t_nopbphtbsppt",
            "t_kodebayarbanksppt",
            "t_idjenistransaksi",
            "t_idjenishaktanah",
            "t_idrefspt",
            "t_pejabatverifikasispt",
            "t_dasarspt",
            "t_totalspt",
            "t_nilaitransaksispt",
            "t_potonganspt",
            "t_thnsppt",
            "t_persyaratan",
            "t_idjenisdoktanah",
            "ntpd"
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
            array(
                "t_inputbpn",
                "t_iddetailsptbphtb",
                "t_namawppembeli",
                "t_nikwppembeli",
                "t_alamatwppembeli",
                "t_kecamatanwppembeli",
                "t_kelurahanwppembeli",
                "t_kabkotawppembeli",
                "t_telponwppembeli",
                "t_kodeposwppembeli",
                "t_npwpwppembeli",
                "t_namawppenjual",
                "t_nikwppenjual",
                "t_alamatwppenjual",
                "t_kecamatanwppenjual",
                "t_kelurahanwppenjual",
                "t_kabkotawppenjual",
                "t_telponwppenjual",
                "t_kodeposwppenjual",
                "t_npwpwppenjual",
                "t_luastanah",
                "t_njoptanah",
                "t_luasbangunan",
                "t_njopbangunan",
                "t_totalnjoptanah",
                "t_totalnjopbangunan",
                "t_grandtotalnjop",
                "t_nosertifikathaktanah",
                "t_kelurahanop",
                "t_kecamatanop",
                "t_ketwaris",
                "t_terbukti",
                "t_rtwppembeli",
                "t_rwwppembeli",
                "t_alamatop",
                "t_rtop",
                "t_rwop",
                "t_dokpersyaratan",
                "t_namasppt",
                "t_tglajb",
                "t_luastanahbpn",
                "t_luasbangunanbpn",
                "t_tglajbbaru",
                "t_noajbbaru",
                "t_statuspelaporannotaris",
                "t_tglpelaporannotaris",
                "t_kabupatenop",
                "t_rtwppenjual",
                "t_rwwppenjual",
                "t_nosertifikatbaru",
                "t_tglsertifikatbaru"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_jenistransaksi"),
            new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"),
            array(
                "s_idjenistransaksi",
                "s_kodejenistransaksi",
                "s_namajenistransaksi"
            ),
            "LEFT"
        );
        $select->join(
            array("d" => "s_jenishaktanah"),
            new \Zend\Db\Sql\Expression("d.s_idhaktanah = a.t_idjenishaktanah"),
            array(
                "s_idhaktanah",
                "s_kodehaktanah",
                "s_namahaktanah"
            ),
            "LEFT"
        );
        $select->join(
            array("g" => "s_users"),
            new \Zend\Db\Sql\Expression("g.s_iduser = a.t_idnotarisspt"),
            array(
                "s_iduser",
                "s_username",
                "s_password",
                "s_jabatan",
                "s_akses",
                "s_idpejabat_idnotaris",
                "s_tipe_pejabat"
            ),
            "LEFT"
        );
        $select->join(
            array("h" => "s_notaris"),
            new \Zend\Db\Sql\Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"),
            array(
                "s_idnotaris",
                "s_namanotaris",
                "s_alamatnotaris",
                "s_kodenotaris",
                "s_sknotaris",
                "s_tgl1notaris",
                "s_tgl2notaris",
                "s_statusnotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("e" => "t_pembayaranspt"),
            new \Zend\Db\Sql\Expression("e.t_idspt = a.t_idspt"),
            array(
                "t_idpembayaranspt",
                "t_kohirpembayaran",
                "t_periodepembayaran",
                "t_tanggalpembayaran",
                "t_objekspt_pembayaran" => "t_objekspt",
                "t_idnotaris",
                "t_ketetapanspt_pembayaran" => "t_ketetapanspt",
                "t_nilaipembayaranspt",
                "t_idkorekspt",
                "t_kodebayarspt",
                "t_verifikasispt",
                "t_tglverifikasispt",
                "t_pejabatverifikasispt_pembayaran" => "t_pejabatverifikasispt",
                "t_statusbayarspt",
                "t_bankbayar",
                "t_kodebayarbanksppt_pembayaran" => "t_kodebayarbanksppt",
                "t_dendabulan"
            ),
            "LEFT"
        );
        $select->join(
            array("f" => "t_pemeriksaan"),
            new \Zend\Db\Sql\Expression("f.p_idpembayaranspt = e.t_kohirpembayaran"),
            array(
                "p_idpemeriksaan",
                "p_idpembayaranspt",
                "p_luastanah",
                "p_luasbangunan",
                "p_njoptanah",
                "p_njopbangunan",
                "p_totalnjoptanah",
                "p_totalnjopbangunan",
                "p_grandtotalnjop",
                "p_nilaitransaksispt",
                "p_potonganspt",
                "p_ketwaris",
                "p_terbukti",
                "p_idjenistransaksi",
                "p_idjenishaktanah",
                "p_totalspt",
                "p_nilaipembayaranspt",
                "p_nilaikurangbayar",
                "p_kohirskpdkb",
                "p_pembayaranskpdkb"
            ),
            "LEFT"
        );
        $where = new Where();
        $where->literal("(e.t_verifikasispt IS NOT NULL)");
        $select->where($where);
        if ($string != null) {
            $res = $select->getSqlString();
        } else {
            $res = $select;
        }
        return $res;
    }

    public function getviewcetakssp($t_idspt)
    {
        //        $sql = "select * from view_cetak_sspd where t_idspt =" . $t_idspt . "";
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_cetak_sspd = $this->view_cetak_sspd();
        $select->from(array("z" => $view_cetak_sspd));
        $where = new Where();
        $where->literal("t_idspt =" . $t_idspt . "");
        $select->where($where);
        // echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select)->execute();
        return $state;
    }

    public function getviewcetakvalidasipembayaran($t_idspt)
    {
        $sql = "select * from view_sspd_semua_pembayaran where t_tanggalpembayaran is not null and t_statusbayarspt=true t_idspt = $t_idspt";
        $statement = $this->adapter->query($sql);
        $state = $statement->execute();
        return $state->current();
    }

    public function jumlahpembayaranperiodeini($tahun, $idnotaris, $tgl_cetak1, $tgl_cetak2)
    {
        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }

        $sql = "select sum(t_nilaipembayaranspt)
                from view_sspd_pembayaran
                where date_part('year', t_tanggalpembayaran) = " . $tahun . " AND t_tanggalpembayaran between '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' and '" . date('Y-m-d', strtotime($tgl_cetak2)) . "' AND t_tanggalpembayaran IS NOT NULL " . $where . " ";
        $statement = $this->adapter->query($sql);
        $state = $statement->execute();
        return $state->current();
    }

    public function jumlahpembayaranperiodesebelum($tahun, $idnotaris, $tgl_cetak1)
    {
        if (!empty($idnotaris)) {
            $where = " AND t_idnotarisspt = " . $idnotaris;
        } else {
            $where = " ";
        }

        $sql = "select sum(t_nilaipembayaranspt)
                from view_sspd_pembayaran
                where date_part('year', t_tanggalpembayaran) = " . $tahun . " AND t_tanggalpembayaran < '" . date('Y-m-d', strtotime($tgl_cetak1)) . "' AND t_tanggalpembayaran IS NOT NULL " . $where . " ";
        $statement = $this->adapter->query($sql);
        $state = $statement->execute();
        return $state->current();
    }

    public function ambildatapengurangan($data_get)
    {
        $sql = "SELECT * FROM view_pengurangan where t_idspt=$data_get->t_idspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function ambildatapembebasan($data_get)
    {
        $sql = "SELECT * FROM view_pembebasan where t_idspt=$data_get->t_idspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDetailNOP($iddetailspt)
    {
        $sql = "SELECT * FROM t_detailnopgabungan where t_iddetailsptbphtb::text='" . $iddetailspt . "'";
        $st = $this->adapter->query($sql);
        return $st->execute();
    }

    public function getDataSSPDArrayIdSpt($t_idspt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_cetak_sspd = $this->view_cetak_sspd();
        //        $select->from(array(
        //            "a" => "view_cetak_sspd"));
        $select->from(array(
            "a" => $view_cetak_sspd
        ));
        $select->join(array(
            "b" => "t_pembayaranspt"
        ), new \Zend\Db\Sql\Expression("a.t_idspt = b.t_idspt"), array(
            "t_tglverifikasispt_kabid"
        ), "left");
        $select->join(
            array(
                "c" => "s_jenistransaksi"
            ),
            new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"),
            array("s_kodejenistransaksi"),
            "LEFT"
        );
        $where = new Where();
        $where->literal("a.t_idspt =" . (int) $t_idspt . "");
        $select->where($where);
        //        echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function view_cetak_sspd()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt",
            "t_kohirspt",
            "t_kohirketetapanspt",
            "t_tglprosesspt",
            "t_periodespt",
            "t_idnotarisspt",
            "t_idtarifspt",
            "t_tglketetapanspt",
            "t_tgljatuhtempospt",
            "t_nopbphtbsppt",
            "t_idjenistransaksi",
            "t_idjenishaktanah",
            "t_idrefspt",
            "t_dasarspt",
            "t_totalspt",
            "t_nilaitransaksispt",
            "t_potonganspt",
            "t_thnsppt",
            "t_persyaratan",
            "t_idjenisdoktanah",
            "t_idsptsebelumnya",
            "t_tarif_pembagian_aphb_kali",
            "t_tarif_pembagian_aphb_bagi",
            "t_idtarifbphtb",
            "t_persenbphtb",
            "t_potongan_waris_hibahwasiat",
            "t_jenispendaftaran",
            "ntpd",
            "t_ptsl",
            "t_idpengurangan"
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new Expression("b.t_idspt = a.t_idspt"),
            array(
                "t_iddetailsptbphtb",
                "t_namawppembeli",
                "t_nikwppembeli",
                "t_alamatwppembeli",
                "t_kecamatanwppembeli",
                "t_kelurahanwppembeli",
                "t_kabkotawppembeli",
                "t_telponwppembeli",
                "t_kodeposwppembeli",
                "t_npwpwppembeli",
                "t_namawppenjual",
                "t_nikwppenjual",
                "t_alamatwppenjual",
                "t_kecamatanwppenjual",
                "t_kelurahanwppenjual",
                "t_kabkotawppenjual",
                "t_telponwppenjual",
                "t_kodeposwppenjual",
                "t_npwpwppenjual",
                "t_luastanah",
                "t_njoptanah",
                "t_luasbangunan",
                "t_njopbangunan",
                "t_totalnjoptanah",
                "t_totalnjopbangunan",
                "t_grandtotalnjop",
                "t_nosertifikathaktanah",
                "t_kelurahanop",
                "t_kecamatanop",
                "t_ketwaris",
                "t_terbukti",
                "t_rtwppembeli",
                "t_rwwppembeli",
                "t_alamatop",
                "t_rtop",
                "t_rwop",
                "t_dokpersyaratan",
                "t_namasppt",
                "t_tglajb",
                "t_luastanahbpn",
                "t_luasbangunanbpn",
                "t_tglajbbaru",
                "t_noajbbaru",
                "t_statuspelaporannotaris",
                "t_tglpelaporannotaris",
                "t_kabupatenop",
                "t_rtwppenjual",
                "t_rwwppenjual",
                "t_nosertifikatbaru",
                "t_tglsertifikatbaru",
                "t_inputbpn",
                "t_statuskonfirmasinotaris",
                "t_tglkonfirmasinotaris",
                "t_grandtotalnjop_aphb"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "t_pembayaranspt"),
            new Expression("c.t_idspt = a.t_idspt"),
            array(
                "t_idpembayaranspt",
                "t_kohirpembayaran",
                "t_periodepembayaran",
                "t_tanggalpembayaran",
                "t_idnotaris",
                "t_nilaipembayaranspt",
                "t_idkorekspt",
                "t_kodebayarspt",
                "t_verifikasispt",
                "t_tglverifikasispt",
                "t_pejabatverifikasispt",
                "t_statusbayarspt",
                "t_kodebayarbanksppt",
                "t_dendabulan",
                "t_pejabatpembayaranspt",
                "t_idpenerimasetoran"
            ),
            "LEFT"
        );
        $select->join(
            array("d" => "t_pemeriksaan"),
            new Expression("d.p_idpembayaranspt = c.t_idpembayaranspt"),
            array(
                "p_idpemeriksaan",
                "p_idpembayaranspt",
                "p_luastanah",
                "p_luasbangunan",
                "p_njoptanah",
                "p_njopbangunan",
                "p_totalnjoptanah",
                "p_totalnjopbangunan",
                "p_grandtotalnjop",
                "p_nilaitransaksispt",
                "p_potonganspt",
                "p_ketwaris",
                "p_terbukti",
                "p_idjenistransaksi",
                "p_idjenishaktanah",
                "p_totalspt",
                "p_nilaipembayaranspt",
                "p_nilaikurangbayar",
                "p_kohirskpdkb",
                "p_pembayaranskpdkb",
                "p_grandtotalnjop_aphb"
            ),
            "LEFT"
        );

        $select->join(
            array("k" => "t_pengurangan"),
            new Expression("k.t_idsptpengurangan = a.t_idspt"),
            array(
                "t_pengurangan",
                "t_totalpajak",
                "t_tglpengurangan",
                "t_nosk",
            ),
            "LEFT"
        );
        $select->join(
            array("e" => "s_users"),
            new Expression("e.s_iduser = a.t_idnotarisspt"),
            array(
                "s_iduser",
                "s_username",
                "s_password",
                "s_jabatan",
                "s_akses",
                "s_idpejabat_idnotaris",
                "s_tipe_pejabat",
            ),
            "LEFT"
        );
        $select->join(
            array("f" => "s_notaris"),
            new Expression("(f.s_idnotaris)::text = (e.s_idpejabat_idnotaris)::text"),
            array(
                "s_idnotaris",
                "s_namanotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("g" => "s_pejabat"),
            new Expression("g.s_idpejabat = c.t_pejabatverifikasispt"),
            array(
                "s_idpejabat",
                "s_namapejabat",
                "s_jabatanpejabat",
                "s_nippejabat",
                "s_golonganpejabat"
            ),
            "LEFT"
        );
        $select->join(
            array("h" => "s_users"),
            new Expression("h.s_iduser = c.t_idpenerimasetoran"),
            array(),
            "LEFT"
        );
        $select->join(
            array("i" => "s_pejabat"),
            new Expression("(i.s_idpejabat)::text = (h.s_idpejabat_idnotaris)::text"),
            array(
                "namapenerimasetoran" => "s_namapejabat"
            ),
            "LEFT"
        );
        $select->join(
            array("j" => "s_jenistransaksi"),
            new Expression("j.s_idjenistransaksi = a.t_idjenistransaksi"),
            array(
                "s_kodejenistransaksi"
            ),
            "LEFT"
        );
        $select->join(
            array("l" => "s_pengurangan_tarif"),
            new Expression("l.s_idpengurangan = a.t_idpengurangan"),
            array(
                "s_namapengurangan",
                "t_persenpengurangan" => "s_nilaipengurangan"
            ),
            "LEFT"
        );

        return $select;
    }

    public function fr_pembayaran_v5($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            //            "" => new Expression(""),
            "t_idspt" => new Expression("a.t_idspt"),
            "jml_syarat_input" => new Expression("(length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) "),
            "jml_syarat_validasi" => new Expression("(length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_sebenarnya" => new Expression("n.jmlsyarat"),
            "status_pendaftaran" => new Expression("(CASE
            WHEN ((length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            ELSE 2
        END)"),
            "status_validasi" => new Expression("(CASE
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL) THEN 3
            ELSE 2
        END)"),
            "t_kohirspt" => new Expression("a.t_kohirspt"),
            "s_idjenistransaksi" => new Expression("c.s_idjenistransaksi"),
            "t_periodespt" => new Expression("a.t_periodespt"),
            "t_tglprosesspt" => new Expression("a.t_tglprosesspt"),
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_statusbayarspt" => new Expression("(CASE
            WHEN ((e.t_statusbayarspt)::text = 'true'::text) THEN 'TRUE'::text
            ELSE 'FALSE'::text
        END )"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "t_persyaratan" => new Expression("a.t_persyaratan"),
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_idjenistransaksi" => new Expression("a.t_idjenistransaksi"),
            "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "t_ketetapanspt" => new Expression("e.t_ketetapanspt"),
            "t_ketetapands" => new Expression("i.t_ketetapands"),
            "t_kohirds" => new Expression("i.t_kohirds"),
            "notarisds" => new Expression("k.s_namanotaris"),
            "t_periodepembayaran" => new Expression("e.t_periodepembayaran"),
            "t_tanggalpembayaran" => new Expression("e.t_tanggalpembayaran"),
            "t_nilaipembayaranspt" => new Expression("e.t_nilaipembayaranspt"),
            "t_idnotarisspt" => new Expression("a.t_idnotarisspt"),
            "t_totalspt" => new Expression("a.t_totalspt"),
            "p_totalspt" => new Expression("f.p_totalspt"),
            "jml_pajak_v1" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_totalspt
            ELSE a.t_totalspt
        END)"),
            "jml_pajak_v2" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN
            CASE
                WHEN (a.t_totalspt < f.p_totalspt) THEN f.p_totalspt
                ELSE a.t_totalspt
            END
            ELSE a.t_totalspt
        END)"),
            "s_idpejabat_idnotaris" => new Expression("g.s_idpejabat_idnotaris"),
            "s_namanotaris" => new Expression("h.s_namanotaris"),
            "t_noajbbaru" => new Expression("b.t_noajbbaru"),
            "t_tglajbbaru" => new Expression("b.t_tglajbbaru"),
            "t_iddetailsptbphtb" => new Expression("b.t_iddetailsptbphtb"),
            "t_luastanahbpn" => new Expression("b.t_luastanahbpn"),
            "t_luasbangunanbpn" => new Expression("b.t_luasbangunanbpn"),
            "p_luastanah" => new Expression("f.p_luastanah"),
            "p_luasbangunan" => new Expression("f.p_luasbangunan"),
            "t_luastanah" => new Expression("b.t_luastanah"),
            "t_luasbangunan" => new Expression("b.t_luasbangunan"),
            "luas_tanah" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luastanah
            ELSE b.t_luastanah
        END)"),
            "luas_bangunan" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luasbangunan
            ELSE b.t_luasbangunan
        END)"),
            "t_tglsertifikatbaru" => new Expression("b.t_tglsertifikatbaru"),
            "t_nosertifikatbaru" => new Expression("b.t_nosertifikatbaru"),
            "t_kohirketetapanspt" => new Expression("a.t_kohirketetapanspt"),
            "t_nopbphtbsppt" => new Expression("a.t_nopbphtbsppt"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "t_nilaitransaksispt" => new Expression("a.t_nilaitransaksispt"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),
            "fr_luas_tanah_bpn" => new Expression("b.fr_luas_tanah_bpn"),
            "fr_luas_bangunan_bpn" => new Expression("b.fr_luas_bangunan_bpn"),
            "fr_tervalidasidua" => new Expression("a.fr_tervalidasidua"),
            "fr_validasidua" => new Expression("b.fr_validasidua"),
            "p_grandtotalnjop" => new Expression("f.p_grandtotalnjop"),
            "p_grandtotalnjop_aphb" => new Expression("f.p_grandtotalnjop_aphb"),
            "p_nilaitransaksispt" => new Expression("f.p_nilaitransaksispt"),
            "t_alamatwppembeli" => new Expression("b.t_alamatwppembeli"),
            "t_nikwppembeli" => new Expression("b.t_nikwppembeli"),
            "t_tarif_pembagian_aphb_kali" => new Expression("a.t_tarif_pembagian_aphb_kali"),
            "t_tarif_pembagian_aphb_bagi" => new Expression("a.t_tarif_pembagian_aphb_bagi"),
            "t_grandtotalnjop_aphb" => new Expression("b.t_grandtotalnjop_aphb"),
            "t_idsptsebelumnya" => new Expression("a.t_idsptsebelumnya"),
            "t_alamatwppenjual" => new Expression("b.t_alamatwppenjual"),
            "t_keteranganvalidasi" => new Expression("e.t_keteranganvalidasi"),
            "t_pejabatverifikasispt" => new Expression("e.t_pejabatverifikasispt")
        ));
        $select->from(array("e" => "t_pembayaranspt"));
        $select->join(array("a" => "t_spt"), new Expression("e.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("b" => "t_detailsptbphtb"), new Expression("b.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("c" => "s_jenistransaksi"), new Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"), array(), "LEFT");
        $select->join(array("f" => "t_pemeriksaan"), new Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"), array(), "LEFT");
        $select->join(array("i" => "t_dendasanksinotaris"), new Expression("i.t_idds = e.t_idds"), array(), "LEFT");
        $select->join(array("g" => "s_users"), new Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("j" => "s_users"), new Expression("j.s_iduser = i.t_idnotaris"), array(), "LEFT");
        $select->join(array("k" => "s_notaris"), new Expression("(k.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("n" => "fr_count_s_persyaratan"), new Expression("a.t_idjenistransaksi = n.s_idjenistransaksi"), array(), "LEFT");

        if ($string != null && $string == 1) {
            return $select->getSqlString();
        } else {
            return $select;
        }
    }
}
