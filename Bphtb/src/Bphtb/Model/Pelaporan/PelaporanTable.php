<?php

namespace Bphtb\Model\Pelaporan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;

class PelaporanTable extends AbstractTableGateway {

    protected $table = 't_pembayaranspt';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PelaporanBase());
        $this->initialize();
    }
    
    //================= datagrid semua data pelaporan notaris
    public function getjumlahdata($sTable, $count, $sWhere) {
        $sql = "SELECT ".$count." FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }
    
    public function semuadatapelaporannotaris($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl) {
        
        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }

      
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                            . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }
    
        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY ".$order_default."";
        }

        $iColumnCount = count($aColumns);
        
        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }else{
                            $tanggalcari = "" .$tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    }else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }

       
        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));
                
                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }else{
                        $tanggalcari = "" .$tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                }else {
                    if($aColumns[$i] == 's_idjenistransaksi'){
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }elseif($aColumns[$i] == 't_idnotarisspt'){
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }elseif($aColumns[$i] == 't_statusbayarspt'){
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }elseif($aColumns[$i] == 'status_validasi'){
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }else{
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        $s_iduser = $session['s_iduser']; 
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        if ($s_tipe_pejabat == 2) {
            $wherelogin_atas = ' AND t_idnotarisspt = '.$s_iduser.'';
            $wherelogin_default = ' WHERE t_tanggalpembayaran IS NOT NULL AND t_idnotarisspt = '.$s_iduser.'';
        }else{
            $wherelogin_atas = ' ';
            $wherelogin_default = ' WHERE t_tanggalpembayaran IS NOT NULL ';
        }
        
        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules)." AND t_tanggalpembayaran IS NOT NULL ".$wherelogin_atas."";
        } else {
            $sWhere = " ".$wherelogin_default." ";
        }

        

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
         
        
        if($sTable == "fr_pembayaran_v2"){
            $fr_pembayaran_v2 = $this->fr_pembayaran_v2(1);
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM (".$fr_pembayaran_v2.") as fr_pembayaran_v2 " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS,
            $sTable = " (".$fr_pembayaran_v2.") as fr_pembayaran_v2 ";
        }else{
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS,
        }
        
        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();
        
        
        
        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];
        
        
        
        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal, 
            "aaData" => array(),
        );
        
        

        foreach ($rResult as $aRow) {
            $row = array();
            
            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }
            
            
            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            }else {
                $status_bayar = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }
            
            
            
            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="'.$cekurl.'/public/img/tervalidasi.png">';
            }else {
                $status_verifikasi = '<img title="Belum di Validasi" width="20" height="20" src="'.$cekurl.'/public/img/belumdivalidasi.png">';
            }
            
            
            
            if ($aRow['t_ketetapanspt'] == 1) {
                $validasipembayaran = '<a href="pembayaran_sptbphtb/cetakbuktivalidasipembayaran?&action=cetakbuktivalidasipembayaran&t_idspt='.$aRow['t_idspt'].'" target="_blank"><i class="fa fa-fw fa-print"></i> Validasi Pembayaran</a>';
                $buktipembayaran = '<a href="pembayaran_sptbphtb/cetakbuktipembayaran?&action=cetakbuktipembayaran&t_idspt='.$aRow['t_idspt'].'" target="_blank"><i class="fa fa-fw fa-print"></i> Bukti Pembayaran</a>';
                $lihat = '<a href="pembayaran_sptbphtb/viewdata?t_idspt='.$aRow['t_idspt'].'"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';
                $hapus = '<a style="background-color:red;color:#fff;" href="#" onclick="hapus('.$aRow['t_idpembayaranspt'].'); return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
            } else {
                $validasipembayaran = ' ';
                $buktipembayaran = ' ';
                $lihat = ' ';
                $hapus = '<a style="background-color:red;color:#fff;" href="#" onclick="hapus('.$aRow['t_idpembayaranspt'].');updatepemeriksaan('.$aRow['p_idpemeriksaan'].');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
            }
            
            /*$btn = '<div class="btn-group">
                            <button aria-expanded="false" type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown">
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" style="left:-120px; border-color: blue;">
                                 '.$validasipembayaran.'
                                 '.$buktipembayaran.'    
                                 '.$lihat.'
                                 '.$hapus.'   
                            </ul>
                          </div>';*/
            
            $btn = '<div class="dropdown">
                    <button onclick="myFunction('.$aRow['t_idspt'].')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown'.$aRow['t_idspt'].'" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                                    '.$validasipembayaran.'
                                 '.$buktipembayaran.'    
                                 '.$lihat.'
                                 '.$hapus.'     
                    </div>
                  </div>'; 

            
            
            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;">'. $aRow['t_kohirspt'].'</span>';
            } else {
                $t_kohirspt = $aRow['t_kohirspt'];
            }
            
            if(!empty($aRow['t_kohirketetapanspt'])){
                $novalidasi = $aRow['t_kohirketetapanspt'];
            }else{
                $novalidasi = '';
            }
            
            if (empty($aRow['t_noajbbaru'])) {
                if ($session['s_akses'] == 1 || $session['s_akses'] == 2 || $session['s_akses'] == 3) {
                    $statuslapor = '<a href="#" class="btn btn-danger btn-sm btn-flat" onclick="openLaporkan('. $aRow['t_iddetailsptbphtb'] .', \''. str_replace('\'', '', $aRow['t_namawppembeli']) .'\', \''. str_pad($aRow['t_kohirspt'], 4, '0', STR_PAD_LEFT) .'\');return false;"  style="width:70px">Laporkan</a>';
                } else {
                    $statuslapor = '-';
                }
            } else {
                $statuslapor = 'Dilaporkan';
            }
            
            //$btn = $statuslapor;
            
            if(!empty($aRow['t_tglajbbaru'])){
                $tgl_ajb = date('d-m-Y', strtotime($aRow['t_tglajbbaru']));
            }else{
                $tgl_ajb = '';
            }
            
            if(!empty($aRow['t_tglprosesspt'])){
                $tgldaftar = "<span style='float:right;'>".date('d-m-Y', strtotime($aRow['t_tglprosesspt']))."</span>";
            }else{
                $tgldaftar = " ";
            }
            
            $mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "Mei", 6 => "Jun", 7 => "Jul", 8 => "Agus", 9 => "Sept", 10 => "Okt", 11 => "Nov", 12 => "Des");
            $tglbayar = explode('-', $aRow['t_tanggalpembayaran']);
            $month = intval($tglbayar[1]);
            $month_name = $mons[$month];
            
            
            $row = array("<center>".$no."</center>", 
                                "<center>".$t_kohirspt."</center>", 
                                "<center>".$aRow['s_namajenistransaksi']."</center>", 
                                $tgldaftar,
                                $aRow['t_namawppembeli'], 
                                "<span style='float:right;'>".number_format($aRow['t_nilaipembayaranspt'], 0, ',', '.')."</span>", 
                                "<center>".$month_name.", ".$tglbayar[0]."</center>", 
                                "<center>".$aRow['s_namanotaris']."</center>", 
                                "<center>".$aRow['t_noajbbaru']."</center>",
                                "<center>".$tgl_ajb."</center>", 
                                "<center>".$statuslapor."</center>"
                        );
            $output['aaData'][] = $row;
            $no++;
        }
        
        return $output;
    }    
    //====================== end semua data pelaporan notaris
    

    public function getGridCount(PelaporanBase $base, $id_notaris, $userRole = null) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        //$select->from('view_sspd_semua_pembayaran');
        $view_sspd_semua_pembayaran = $this->view_sspd_semua_pembayaran();
        $select->from(array("za" => $view_sspd_semua_pembayaran));
        $where = new Where();
//        $where->literal("t_statusbayarspt=true and t_noajbbaru is not null and t_tglajbbaru is not null");
        $where->literal("t_statusbayarspt=true");
        $where->isNotNull('t_verifikasispt');
        if ($userRole == "Notaris") {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->t_bulanselesaibayar != 'undefined')
            $where->literal("t_bulanselesaibayar::text LIKE '%$base->t_bulanselesaibayar%'");
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_noajbbaru != 'undefined')
            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        if ($base->t_tglajbbaru != 'undefined')
            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        if ($base->t_idnotarisspt != 'undefined')
            $where->equalTo("t_idnotarisspt", $base->t_idnotarisspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }
    
    public function getGridCountPelaporanNotaris(PelaporanBase $base, $id_notaris, $s_tipe_pejabat = null) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        //$select->from('view_sspd_semua_pembayaran');
        $view_sspd_semua_pembayaran = $this->view_sspd_semua_pembayaran();
        $select->from(array("za" => $view_sspd_semua_pembayaran));
        $where = new Where();
//        $where->literal("t_statusbayarspt=true and t_noajbbaru is not null and t_tglajbbaru is not null");
        $where->literal("t_statusbayarspt=true");
        $where->isNotNull('t_verifikasispt');
        if ($s_tipe_pejabat == 2) {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->t_bulanselesaibayar != 'undefined')
            $where->literal("t_bulanselesaibayar::text LIKE '%$base->t_bulanselesaibayar%'");
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_noajbbaru != 'undefined')
            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        if ($base->t_tglajbbaru != 'undefined')
            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        if ($base->t_idnotarisspt != 'undefined')
            $where->equalTo("t_idnotarisspt", $base->t_idnotarisspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }
    
    
    public function getGridDataPelaporanNotaris(PelaporanBase $base, $offset, $id_notaris, $s_tipe_pejabat = null) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        //$select->from('view_sspd_semua_pembayaran');
        $view_sspd_semua_pembayaran = $this->view_sspd_semua_pembayaran();
        $select->from(array("za" => $view_sspd_semua_pembayaran));
        $where = new Where();
        $where->equalTo('t_statusbayarspt', true);
//        $where->isNotNull('t_tglajbbaru');
//        $where->isNotNull('t_noajbbaru');
        $where->isNotNull('t_verifikasispt');
        if ($s_tipe_pejabat == 2) {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->t_bulanselesaibayar != 'undefined')
            $where->literal("t_bulanselesaibayar::text LIKE '%$base->t_bulanselesaibayar%'");
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_noajbbaru != 'undefined')
            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        if ($base->t_tglajbbaru != 'undefined')
            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        if ($base->t_idnotarisspt != 'undefined')
            $where->equalTo("t_idnotarisspt", $base->t_idnotarisspt);
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->order("t_tglajbbaru DESC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridData(PelaporanBase $base, $offset, $id_notaris, $userRole = null) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        //$select->from('view_sspd_semua_pembayaran');
        $view_sspd_semua_pembayaran = $this->view_sspd_semua_pembayaran();
        $select->from(array("za" => $view_sspd_semua_pembayaran));
        $where = new Where();
        $where->equalTo('t_statusbayarspt', true);
//        $where->isNotNull('t_tglajbbaru');
//        $where->isNotNull('t_noajbbaru');
        $where->isNotNull('t_verifikasispt');
        if ($userRole == "Notaris") {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int) $base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->t_bulanselesaibayar != 'undefined')
            $where->literal("t_bulanselesaibayar::text LIKE '%$base->t_bulanselesaibayar%'");
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_noajbbaru != 'undefined')
            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        if ($base->t_tglajbbaru != 'undefined')
            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        if ($base->t_idnotarisspt != 'undefined')
            $where->equalTo("t_idnotarisspt", $base->t_idnotarisspt);
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->order("t_tglajbbaru DESC");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountDendaNotaris($query, $qtype) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_pendataandendasanksinotaris');
        $where = new Where();
        $where->equalTo('t_ketetapands', 3);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataDendaNotaris($sortname, $sortorder, $query, $qtype, $start, $rp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_pendataandendasanksinotaris');
        $select->order($sortname, $sortorder);
        $where = new Where();
        $where->equalTo('t_ketetapands', 3);
        $select->where($where);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountSanksiNotaris($query, $qtype) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_pendataandendasanksinotaris');
        $where = new Where();
        $where->equalTo('t_ketetapands', 4);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataSanksiNotaris($sortname, $sortorder, $query, $qtype, $start, $rp) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_pendataandendasanksinotaris');
        $select->order($sortname, $sortorder);
        $where = new Where();
        $where->equalTo('t_ketetapands', 4);
        $select->where($where);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function UbahStatusLaporanNotaris(PelaporanBase $pb) {
        $data = array(
            't_statuspelaporannotaris' => true,
            't_tglpelaporannotaris' => date('Y-m-d'),
            't_noajbbaru' => $pb->t_noajbbaru,
            't_tglajbbaru' => date('Y-m-d', strtotime($pb->t_tglajbbaru))
        );
        $t_detailsptbphtb = new TableGateway('t_detailsptbphtb', $this->adapter);
        $t_detailsptbphtb->update($data, array(
            't_iddetailsptbphtb' => (int) $pb->t_iddetailsptbphtb
        ));
    }

    public function UbahStatusKonfirmasiNotaris(PelaporanBase $pb) {
        $data = array(
            't_statuskonfirmasinotaris' => true,
            't_tglkonfirmasinotaris' => date('Y-m-d')
        );
        $t_detailsptbphtb = new TableGateway('t_detailsptbphtb', $this->adapter);
        $t_detailsptbphtb->update($data, array(
            't_iddetailsptbphtb' => $pb->t_iddetailsptbphtb
        ));
    }

    public function saveDendaNotaris(PelaporanBase $pb) {
        $dataa = $this->getKohirDs($pb);
        $data = array(
            't_tglprosesds' => date('Y-m-d', strtotime($pb->t_tglprosesds)),
            't_periodeds' => date('Y', strtotime($pb->t_periodeds)),
            't_dendabulan' => $pb->t_dendabulan,
            't_idnotaris' => $pb->t_idnotaris,
            't_jumlahds' => str_ireplace(".", "", $pb->t_jumlahds),
            't_ketetapands' => 3,
            't_kohirds' => $dataa['t_kohirds'] + 1,
            't_kodebayards' => $pb->t_kodebayards
        );
        $tabel_denda = new TableGateway('t_dendasanksinotaris', $this->adapter);
        $tabel_denda->insert($data);
    }

    public function getKohirDs(PelaporanBase $pb) {
        $sql = "select max(t_kohirds) as t_kohirds 
                from t_dendasanksinotaris
                where t_periodeds='" . $pb->t_periodeds . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function saveSanksiNotaris(PelaporanBase $pb) {
        $dataa = $this->getKohirDs($pb);
        $data = array(
            't_tglprosesds' => date('Y-m-d', strtotime($pb->t_tglprosesds)),
            't_periodeds' => date('Y', strtotime($pb->t_periodeds)),
            't_dendabulan' => '',
            't_idnotaris' => $pb->t_idnotaris1,
            't_jumlahds' => str_ireplace(".", "", $pb->t_jumlahds),
            't_ketetapands' => 4,
            't_idspt' => $pb->t_idspt,
            't_kohirds' => $dataa['t_kohirds'] + 1,
            't_kodebayards' => $pb->t_kodebayards
        );
        $tabel_denda = new TableGateway('t_dendasanksinotaris', $this->adapter);
        $tabel_denda->insert($data);
    }

    public function hapusData(PelaporanBase $pb) {
        $tabel_denda = new TableGateway('t_dendasanksinotaris', $this->adapter);
        $tabel_denda->delete(array(
            't_idds' => $pb->t_idds
        ));
    }

    public function CariDataPembayaranDenda($bulanajb) {
        $sql = "select * from t_pembayaranspt
                where t_dendabulan='" . $bulanajb . "'";
        $statement = $this->adapter->query($sql);
        $st = $statement->execute();
        return $st->current();
    }

    public function CariDataPendataanSanksi($idspt) {
        $sql = "select * from view_pendataandendasanksinotaris
                where t_idspt='" . $idspt . "' and t_ketetapands=4";
        $statement = $this->adapter->query($sql);
        $st = $statement->execute();
        return $st->current();
    }

    public function cariDataSanksi($idspt) {
//        $sql = "select t_idnotarisspt , t_tglajbbaru, t_noajbbaru, s_namanotaris from view_sspd_semua_pembayaran
//                where t_idspt='" . $idspt . "'";
//        $statement = $this->adapter->query($sql);
//        $st = $statement->execute();
//        return $st->current();
        
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_sspd_semua_pembayaran = $this->view_sspd_semua_pembayaran();
        $select->from(array("za" => $view_sspd_semua_pembayaran));
        $select->columns(array(
            "t_idnotarisspt", "t_tglajbbaru", "t_noajbbaru", "s_namanotaris"
        ));
        $where = new Where();
        $where->literal("t_idspt='" . $idspt . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $state;
    }

    public function cariPendatanSanksi($t_idspt) {
        $sql = "select * from view_pendataandendasanksinotaris where t_idspt =" . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataDenda($t_kohirds) {
        $sql = "select * from view_pendataandendasanksinotaris where t_kohirds =" . $t_kohirds . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataPeraturan($t_ketetapan) {
        $sql = "select * from s_peraturan where s_kodeperaturan ='" . $t_ketetapan . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }
    
    public function view_sspd_semua_pembayaran(){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_kodebayarbanksppt" => new Zend\Db\Sql\Expression("e.t_kodebayarbanksppt"), 
            "t_namawppembeli" => new Zend\Db\Sql\Expression("b.t_namawppembeli"),
            "t_nikwppembeli" => new Zend\Db\Sql\Expression("b.t_nikwppembeli"),
            "t_alamatwppembeli" => new Zend\Db\Sql\Expression("b.t_alamatwppembeli"),
            "t_kecamatanwppembeli" => new Zend\Db\Sql\Expression("b.t_kecamatanwppembeli"),
            "t_kelurahanwppembeli" => new Zend\Db\Sql\Expression("b.t_kelurahanwppembeli"),
            "t_kabkotawppembeli" => new Zend\Db\Sql\Expression("b.t_kabkotawppembeli"),
            "t_telponwppembeli" => new Zend\Db\Sql\Expression("b.t_telponwppembeli"),
            "t_kodeposwppembeli" => new Zend\Db\Sql\Expression("b.t_kodeposwppembeli"),
            "t_namawppenjual" => new Zend\Db\Sql\Expression("b.t_namawppenjual"),
            "t_nikwppenjual" => new Zend\Db\Sql\Expression("b.t_nikwppenjual"),
            "t_alamatwppenjual" => new Zend\Db\Sql\Expression("b.t_alamatwppenjual"),
            "t_kecamatanwppenjual" => new Zend\Db\Sql\Expression("b.t_kecamatanwppenjual"),
            "t_kelurahanwppenjual" => new Zend\Db\Sql\Expression("b.t_kelurahanwppenjual"),
            "t_kabkotawppenjual" => new Zend\Db\Sql\Expression("b.t_kabkotawppenjual"),
            "t_telponwppenjual" => new Zend\Db\Sql\Expression("b.t_telponwppenjual"),
            "t_kodeposwppenjual" => new Zend\Db\Sql\Expression("b.t_kodeposwppenjual"),
            "t_npwpwppenjual" => new Zend\Db\Sql\Expression("b.t_npwpwppenjual"),
            "t_luastanah" => new Zend\Db\Sql\Expression("b.t_luastanah"),
            "t_njoptanah" => new Zend\Db\Sql\Expression("b.t_njoptanah"),
            "t_luasbangunan" => new Zend\Db\Sql\Expression("b.t_luasbangunan"),
            "t_njopbangunan" => new Zend\Db\Sql\Expression("b.t_njopbangunan"),
            "t_totalnjoptanah" => new Zend\Db\Sql\Expression("b.t_totalnjoptanah"),
            "t_totalnjopbangunan" => new Zend\Db\Sql\Expression("b.t_totalnjopbangunan"),
            "t_grandtotalnjop" => new Zend\Db\Sql\Expression("b.t_grandtotalnjop"),
            "t_nosertifikathaktanah" => new Zend\Db\Sql\Expression("b.t_nosertifikathaktanah"),
            "t_iddetailsptbphtb" => new Zend\Db\Sql\Expression("b.t_iddetailsptbphtb"),
            "s_namajenistransaksi" => new Zend\Db\Sql\Expression("c.s_namajenistransaksi"),
            "t_idpembayaranspt", "t_kohirpembayaran", "t_periodepembayaran", "t_tanggalpembayaran",
            "t_idnotaris", "t_nilaipembayaranspt", "t_kodebayarspt", "t_verifikasispt", 
            "t_tglverifikasispt", "t_pejabatverifikasispt", "t_statusbayarspt",
            "t_ketetapanspt",
            "t_bulanselesaibayar" => new \Zend\Db\Sql\Expression("to_char((e.t_tanggalpembayaran)::timestamp with time zone, 'MM-YYYY'::text)")
        ));
        $select->from(array("e" => "t_pembayaranspt"));
        $select->join(array("z" => "t_spt"), 
                new \Zend\Db\Sql\Expression("e.t_idspt = z.t_idspt"), array(
                    "t_idspt", "t_tglprosesspt", "t_periodespt", "t_nopbphtbsppt", "t_totalspt", 
                    "t_nilaitransaksispt", "t_kohirspt", "t_potonganspt", "t_idjenistransaksi", 
                    "t_idjenishaktanah", "t_persyaratan", "t_kohirketetapanspt", 
                    "t_tarif_pembagian_aphb_kali", "t_tarif_pembagian_aphb_bagi",
                    "t_idtarifbphtb", "t_persenbphtb"
                ), "LEFT");
        $select->join(array("b" => "t_detailsptbphtb"), 
                new \Zend\Db\Sql\Expression("b.t_idspt = z.t_idspt"), array(
                    "t_kelurahanop", "t_kecamatanop", "t_luastanahbpn", "t_luasbangunanbpn",
                    "t_tglajbbaru", "t_noajbbaru", "t_statuspelaporannotaris",
                    "t_tglpelaporannotaris", "t_kabupatenop", "t_nosertifikatbaru",
                    "t_tglsertifikatbaru", "t_inputbpn", "t_statuskonfirmasinotaris",
                    "t_grandtotalnjop_aphb"
                ), "LEFT");
        $select->join(array("c" => "s_jenistransaksi"), 
                new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = z.t_idjenistransaksi"), array(), "LEFT");
        $select->join(array("d" => "s_jenishaktanah"), 
                new \Zend\Db\Sql\Expression("d.s_idhaktanah = z.t_idjenishaktanah"), array(
                    "s_namahaktanah"
                ), "LEFT");
        $select->join(array("f" => "t_pemeriksaan"), 
                new \Zend\Db\Sql\Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"), array(
                    "p_kohirskpdkb", "p_luastanah", "p_luasbangunan", "p_njoptanah", "p_njopbangunan",
                    "p_totalnjoptanah", "p_totalnjopbangunan", "p_grandtotalnjop", 
                    "p_nilaitransaksispt", "p_potonganspt", "p_ketwaris", "p_terbukti", 
                    "p_idjenistransaksi", "p_idjenishaktanah", "p_totalspt", 
                    "p_nilaipembayaranspt", "p_nilaikurangbayar", "p_idpemeriksaan",
                    "p_grandtotalnjop_aphb"
                ), "LEFT");
        $select->join(array("g" => "s_users"), 
                new \Zend\Db\Sql\Expression("g.s_iduser = z.t_idnotarisspt"), array(
                    "t_idnotarisspt" => new \Zend\Db\Sql\Expression("s_iduser")
                ), "LEFT");
        $select->join(array("h" => "s_notaris"), 
                new \Zend\Db\Sql\Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(
                    "s_namanotaris"
                ), "LEFT");
        $select->join(array("i" => "t_dendasanksinotaris"), 
                new \Zend\Db\Sql\Expression("i.t_idds = e.t_idds"), array(
                    "t_kodebayards", "t_kohirds", "t_ketetapands"
                ), "LEFT");
        $select->join(array("j" => "s_users"), 
                new \Zend\Db\Sql\Expression("j.s_iduser = i.t_idnotaris"), array(), "LEFT");
        $select->join(array("k" => "s_notaris"), 
                new \Zend\Db\Sql\Expression("(k.s_idnotaris)::text = (j.s_idpejabat_idnotaris)::text"), array(
                    "notarisds" => new \Zend\Db\Sql\Expression("s_namanotaris")
                ), "LEFT");
        
        return $select;
    }
    
    public function fr_pembayaran_v2($string = null){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
//            "" => new Expression(""),
            "t_idspt" => new Expression("a.t_idspt"),
            "jml_syarat_input" => new Expression("(length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) "),
            "jml_syarat_validasi" => new Expression("(length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_sebenarnya" => new Expression("( SELECT count(s_persyaratan.s_idjenistransaksi) AS count
           FROM s_persyaratan
          WHERE (s_persyaratan.s_idjenistransaksi = a.t_idjenistransaksi))"),
            "status_pendaftaran" => new Expression("(CASE
            WHEN ((length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) = ( SELECT count(s_persyaratan.s_idjenistransaksi) AS count
               FROM s_persyaratan
              WHERE (s_persyaratan.s_idjenistransaksi = a.t_idjenistransaksi))) THEN 1
            ELSE 2
        END)"),
            "status_validasi" => new Expression("CASE
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) = ( SELECT count(s_persyaratan.s_idjenistransaksi) AS count
               FROM s_persyaratan
              WHERE (s_persyaratan.s_idjenistransaksi = a.t_idjenistransaksi))) THEN 1
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL) THEN 3
            ELSE 2
        END"),
            "t_kohirspt" => new Expression("a.t_kohirspt"),
            "s_idjenistransaksi" => new Expression("c.s_idjenistransaksi"),
            "t_periodespt" => new Expression("a.t_periodespt"),
            "t_tglprosesspt" => new Expression("a.t_tglprosesspt"),
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_statusbayarspt" => new Expression("(CASE
            WHEN ((e.t_statusbayarspt)::text = 'true'::text) THEN 'TRUE'::text
            ELSE 'FALSE'::text
        END)"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "t_persyaratan" => new Expression("a.t_persyaratan"),
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_idjenistransaksi" => new Expression("a.t_idjenistransaksi"),
            "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "t_ketetapanspt" => new Expression("e.t_ketetapanspt"),
            "t_ketetapands" => new Expression("i.t_ketetapands"),
            "t_kohirds" => new Expression("i.t_kohirds"),
            "notarisds" => new Expression("k.s_namanotaris"),
            "t_periodepembayaran" => new Expression("e.t_periodepembayaran"),
            "t_tanggalpembayaran" => new Expression("e.t_tanggalpembayaran"),
            "t_nilaipembayaranspt" => new Expression("e.t_nilaipembayaranspt"),
            "t_idnotarisspt" => new Expression("a.t_idnotarisspt"),
            "t_totalspt" => new Expression("a.t_totalspt"),
            "p_totalspt" => new Expression("f.p_totalspt"),
            "jml_pajak_v1" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_totalspt
            ELSE a.t_totalspt
        END)"),
            "jml_pajak_v2" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN
            CASE
                WHEN (a.t_totalspt < f.p_totalspt) THEN f.p_totalspt
                ELSE a.t_totalspt
            END
            ELSE a.t_totalspt
        END)"),
            "s_idpejabat_idnotaris" => new Expression("g.s_idpejabat_idnotaris"),
            "s_namanotaris" => new Expression("h.s_namanotaris"),
            "t_noajbbaru" => new Expression("b.t_noajbbaru"),
            "t_tglajbbaru" => new Expression("b.t_tglajbbaru"),
            "t_iddetailsptbphtb" => new Expression("b.t_iddetailsptbphtb"),
            "t_luastanahbpn" => new Expression("b.t_luastanahbpn"),
            "t_luasbangunanbpn" => new Expression("b.t_luasbangunanbpn"),
            "p_luastanah" => new Expression("f.p_luastanah"),
            "p_luasbangunan" => new Expression("f.p_luasbangunan"),
            "t_luastanah" => new Expression("b.t_luastanah"),
            "t_luasbangunan" => new Expression("b.t_luasbangunan"),
            "luas_tanah" => new Expression("CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luastanah
            ELSE b.t_luastanah
        END "),
            "luas_bangunan" => new Expression("CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luasbangunan
            ELSE b.t_luasbangunan
        END "),
            "t_tglsertifikatbaru" => new Expression("b.t_tglsertifikatbaru"),
            "t_nosertifikatbaru" => new Expression("b.t_nosertifikatbaru"),
            "t_kohirketetapanspt" => new Expression("a.t_kohirketetapanspt"),
            "t_nopbphtbsppt" => new Expression("a.t_nopbphtbsppt"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "t_nilaitransaksispt" => new Expression("a.t_nilaitransaksispt"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),            
        ));
        $select->from(array("e" => "t_pembayaranspt"));
        $select->join(array("a" => "t_spt"), new Expression("e.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("b" => "t_detailsptbphtb"), new Expression("b.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("c" => "s_jenistransaksi"), new Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"), array(), "LEFT");
        $select->join(array("f" => "t_pemeriksaan"), new Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"), array(), "LEFT");
        $select->join(array("i" => "t_dendasanksinotaris"), new Expression("i.t_idds = e.t_idds"), array(), "LEFT");
        $select->join(array("g" => "s_users"), new Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("j" => "s_users"), new Expression("j.s_iduser = i.t_idnotaris"), array(), "LEFT");
        $select->join(array("k" => "s_notaris"), new Expression("(k.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        
        if($string != null){
            return $select->getSqlString();
        }else{
            return $select;
        }
    }

}
