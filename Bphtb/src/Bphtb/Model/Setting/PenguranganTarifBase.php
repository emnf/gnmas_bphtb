<?php

namespace Bphtb\Model\Setting;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PenguranganTarifBase implements InputFilterAwareInterface {

    public $s_idpengurangan, $s_namapengurangan, $s_nilaipengurangan;
    public $page, $direction;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->s_idpengurangan = (isset($data["s_idpengurangan"])) ? $data["s_idpengurangan"] : null;
        $this->s_namapengurangan = (isset($data["s_namapengurangan"])) ? $data["s_namapengurangan"] : null;
        $this->s_nilaipengurangan = (isset($data["s_nilaipengurangan"])) ? $data["s_nilaipengurangan"] : null;    
        

        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->direction = (isset($data['direction'])) ? $data['direction'] : null;
        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();            

            $inputFilter->add($factory->createInput(array(
                        'name' => 's_idpengurangan',
                        'required' => true
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 's_namapenguranagan',
                        'required' => true
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
