<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PejabatBphtbTable extends AbstractTableGateway {

    protected $table = 's_pejabat';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PejabatBphtbBase());
        $this->initialize();
    }

    public function getdata() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }
    
    public function getdataNotaris() {
        $sql = "select a.*,b.* from s_users a 
                left join s_notaris b ON b.s_idnotaris::text = a.s_idpejabat_idnotaris::text
                where s_akses=3";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }
    
    public function getdataKecamatan() {
        $sql = "select * from s_kecamatan order by s_idkecamatan";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getdataid($pj) {
        $rowset = $this->select(array('s_idpejabat' => (int)$pj));
        $row = $rowset->current();
        return $row;
    }

    public function getdataidnotaris($pj) {
        $sql = "select a.*,b.* from s_users a 
                left join s_notaris b ON b.s_idnotaris::text = a.s_idpejabat_idnotaris::text
                where a.s_akses=3 and a.s_iduser=".$pj." ";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
        
    }

    public function checkExist(PejabatBphtbBase $pj) {
        $rowset = $this->select(array('s_nippejabat' => $pj->s_nippejabat));
        $row = $rowset->current();
        return $row;
    }

    public function checkId($id) {
        $rowset = $this->select(array('s_idpejabat' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function savedata(PejabatBphtbBase $pj) {
        if (empty($pj->s_golonganpejabat)) {
            $pj->s_golonganpejabat = NULL;
        }
        $data = array(
            's_namapejabat' => $pj->s_namapejabat,
            's_nippejabat' => $pj->s_nippejabat,
            's_jabatanpejabat' => $pj->s_jabatanpejabat,
            's_golonganpejabat' => $pj->s_golonganpejabat
        );
        $id = (int) $pj->s_idpejabat;
        if ($id == 0) {
            $data['s_status'] = 1;
            $this->insert($data);
        } else {
            if ($this->checkId($id)) {
                $data['s_status'] = $pj->s_status;
                $this->update($data, array('s_idpejabat' => $pj->s_idpejabat));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function comboBoxGolongan() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('ss_golonganpejabat');
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCount(PejabatBphtbBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);  
        $where = new \Zend\Db\Sql\Where();
        if($base->s_namapejabat != 'undefined')
            $where->literal("$this->table.s_namapejabat::text LIKE '%$base->s_namapejabat%'");     
        if($base->s_jabatanpejabat != 'undefined')
            $where->literal("$this->table.s_jabatanpejabat::text LIKE '%$base->s_jabatanpejabat%'");     
        if($base->s_nippejabat != 'undefined')
            $where->literal("$this->table.s_nippejabat::text LIKE '%$base->s_nippejabat%'"); 
        if($base->s_status != 'undefined')
            $where->literal("$this->table.s_status = '$base->s_status'");  
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(PejabatBphtbBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new \Zend\Db\Sql\Where();
        if($base->s_namapejabat != 'undefined')
            $where->literal("$this->table.s_namapejabat::text LIKE '%$base->s_namapejabat%'");     
        if($base->s_jabatanpejabat != 'undefined')
            $where->literal("$this->table.s_jabatanpejabat::text LIKE '%$base->s_jabatanpejabat%'");     
        if($base->s_nippejabat != 'undefined')
            $where->literal("$this->table.s_nippejabat::text LIKE '%$base->s_nippejabat%'"); 
        if($base->s_status != 'undefined')
            $where->literal("$this->table.s_status = '$base->s_status'"); 
        $select->where($where);
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function hapusData($pb) {
        $this->delete(array('s_idpejabat' => $pb));
    }

    public function getallid($id) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table)
                ->join('ss_golonganpejabat', 'ss_golonganpejabat.ss_idgolongan = s_pejabat.s_golonganpejabat');
        $where = new Where();
        $where->equalTo("$this->table.s_idpejabat", $id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

}
