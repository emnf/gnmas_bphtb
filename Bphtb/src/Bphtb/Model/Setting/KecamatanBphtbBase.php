<?php

namespace Bphtb\Model\Setting;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class KecamatanBphtbBase implements InputFilterAwareInterface {

    public $s_idkecamatan;
    public $s_kodekecamatan;
    public $s_namakecamatan;
    public $page;
    public $rows;
    public $sidx;
    public $sord;
    protected $inputFilter;

    public function exchangeArray($data) {
        $this->s_idkecamatan = (isset($data['s_idkecamatan'])) ? $data['s_idkecamatan'] : null;
        $this->s_kodekecamatan = (isset($data['s_kodekecamatan'])) ? $data['s_kodekecamatan'] : null;
        $this->s_namakecamatan = (isset($data['s_namakecamatan'])) ? $data['s_namakecamatan'] : null;

        $this->page = (isset($data['page'])) ? $data['page'] : null;
        $this->rows = (isset($data['rows'])) ? $data['rows'] : null;
        $this->sidx = (isset($data['sidx'])) ? $data['sidx'] : null;
        $this->sord = (isset($data['sord'])) ? $data['sord'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();            

            $inputFilter->add(array(
                'name' => 's_kodekecamatan',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    new \Zend\Validator\StringLength(
                            array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 3
                            )
                    ),
                ),
            ));
            
            $inputFilter->add(array(
                'name' => 's_namakecamatan',
                'required' => true
            ));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
