<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class HargaAcuanTable extends AbstractTableGateway {

    protected $table = 's_acuan';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new HargaAcuanBase());
        $this->initialize();
    }

    public function getGridCountAcuan(HargaAcuanBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($base->s_kd_propinsi != 'undefined')
            $where->literal("$this->table.s_kd_propinsi::text LIKE '%$base->s_kd_propinsi%'");
        if ($base->s_kd_dati2 != 'undefined')
            $where->literal("$this->table.s_kd_dati2::text LIKE '%$base->s_kd_dati2%'");
        if ($base->s_kd_kecamatan != 'undefined')
            $where->literal("$this->table.s_kd_kecamatan::text LIKE '%$base->s_kd_kecamatan%'");
        if ($base->s_kd_kelurahan != 'undefined')
            $where->literal("$this->table.s_kd_kelurahan::text LIKE '%$base->s_kd_kelurahan%'");
        if ($base->s_kd_blok != 'undefined')
            $where->literal("$this->table.s_kd_blok::text LIKE '%$base->s_kd_blok%'");
        if ($base->s_permetertanah != 'undefined')
            $where->literal("$this->table.s_permetertanah::text LIKE '%$base->s_permetertanah%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataAcuan(HargaAcuanBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($base->s_kd_propinsi != 'undefined')
            $where->literal("$this->table.s_kd_propinsi::text LIKE '%$base->s_kd_propinsi%'");
        if ($base->s_kd_dati2 != 'undefined')
            $where->literal("$this->table.s_kd_dati2::text LIKE '%$base->s_kd_dati2%'");
        if ($base->s_kd_kecamatan != 'undefined')
            $where->literal("$this->table.s_kd_kecamatan::text LIKE '%$base->s_kd_kecamatan%'");
        if ($base->s_kd_kelurahan != 'undefined')
            $where->literal("$this->table.s_kd_kelurahan::text LIKE '%$base->s_kd_kelurahan%'");
        if ($base->s_kd_blok != 'undefined')
            $where->literal("$this->table.s_kd_blok::text LIKE '%$base->s_kd_blok%'");
        if ($base->s_permetertanah != 'undefined')
            $where->literal("$this->table.s_permetertanah::text LIKE '%$base->s_permetertanah%'");
        $select->where($where);
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function savedata(HargaAcuanBase $kc) {
        $array_acuan = explode('.', $kc->t_nopacuan);
        $s_kd_propinsi = $array_acuan[0];
        $s_kd_dati2 = $array_acuan[1];
        $s_kd_kecamatan = $array_acuan[2];
        $s_kd_kelurahan = $array_acuan[3];
        $s_kd_blok = $array_acuan[4];
        $data = array(
        's_kd_propinsi' => $s_kd_propinsi,
        's_kd_dati2' => $s_kd_dati2,
        's_kd_kecamatan' => $s_kd_kecamatan,
        's_kd_kelurahan' => $s_kd_kelurahan,
        's_kd_blok' => $s_kd_blok,
        's_permetertanah' => str_ireplace('.', '', $kc->s_permetertanah)
        );
        $id = (int) $kc->s_idacuan;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_idacuan' => $kc->s_idacuan));
        }
    }

    public function getDataId($id) {
        $rowset = $this->select(array('s_idacuan' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($id) {
        $this->delete(array('s_idacuan' => $id));
    }

}
