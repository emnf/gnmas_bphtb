<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class JenisKetetapanTable extends AbstractTableGateway {

    protected $table = 's_jenisketetapan';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new JenisKetetapanBase());
        $this->initialize();
    }

    public function savedata(JenisKetetapanBase $kc) {
        $data = array(            
            's_namajenisketetapan' => $kc->s_namajenisketetapan,
            's_namasingkatjenisketetapan' => $kc->s_namasingkatjenisketetapan,
        );
        $id = (int) $kc->s_idjenisketetapan;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_idjenisketetapan' => $kc->s_idjenisketetapan));
        }
    }

    public function getGridCount(JenisKetetapanBase $kb) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        //$select->order($kb->sidx . " " . $kb->sord);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(JenisKetetapanBase $kb, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);        
        $select->order($kb->sidx . " " . $kb->sord);
        $select->limit($kb->rows = (int) $kb->rows);
        $select->offset($offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id) {
        $rowset = $this->select(array('s_idjenisketetapan' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($id) {
        $this->delete(array('s_idjenisketetapan' => $id));
    }

    public function comboBox() {
        $resultSet = $this->select();
        return $resultSet;
    }

}
