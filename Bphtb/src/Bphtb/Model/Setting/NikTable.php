<?php
namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

class NikTable extends AbstractTableGateway
{

    protected $table = "S_NIK_CAPIL";

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new \Bphtb\Model\Setting\NikBase());
        $this->initialize();
    }

    public function getdata()
    {
        return $this->select();
    }

    public function getjumlahdata($select = null)
    {
        $sql = new Sql($this->adapter);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->count();
        return $res;
    }

    public function semuadatanik($allParams = null, $input = null, $aColumns = null, $session = null, $cekurl = null)
    {
        // var_dump($input);
        // exit();

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = intval($input->getPost('iDisplayLength'));
            $sOffset = intval($input->getPost('iDisplayStart'));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = intval($input->getPost('iDisplayLength'));
                $sOffset = intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = 10;
                $sOffset = 0;
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }



        if (!empty($aOrderingRules)) {

            $sOrder = implode(", ", $aOrderingRules);
        } else {
            $sOrder = '"NIK"';
        }

        // var_dump($sOrder);
        // exit();

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        // var_dump($aFilteringRules);
        // exit();

        if (!empty($aFilteringRules)) {
            $sWhere = implode(" AND ", $aFilteringRules);
        } else {
            $sWhere = "";
        }

        // var_dump($sWhere);
        // exit();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if (!empty($sWhere) && $sWhere != "") {
            $where->literal($sWhere);
        }
        $select->where($where);

        

        $totaldata = $this->getjumlahdata($select);
        // var_dump($totaldata);
        // exit();

        $select->order(new Expression($sOrder));
        $select->limit($sLimit);
        $select->offset($sOffset);

        // echo $select->getSqlString();
        // exit();

        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();

        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $totaldata,
            "iTotalDisplayRecords" => $totaldata,
            "aaData" => array(),
        );

        //var_dump($rResult); exit();

        foreach ($res as $aRow) {
            $row = array();

            // $edit = ' <a href="setting_nik/edit?id=' . $aRow['s_id'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
            $hapus = '<a href="#" onclick="hapus(' . $aRow['IDNIK'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
            $btn = '
                <div class="dropdown">
                    <button onclick="myFunction(' . $aRow['IDNIK'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $aRow['IDNIK'] . '" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                      
                                                  ' . $hapus . ' 
                                                      
                    </div>
                  </div>';

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow["NIK"] . "</center>",
                "<center>" . $aRow["NAMA_LGKP"] . "</center>",
                "".$aRow["ALAMAT"]."",
                "<center>" . $btn . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function insertnik($dataset = null)
    {
        $i = 0; //ABAIKAN SHEET JUDUL
        foreach ($dataset as $dataset => $value) {
            if ($i != 0) {
                //CEK DATA DULU
                $sql = new Sql($this->adapter);
                $select = $sql->select();
                $select->from($this->table);
                $where = new Where();
                $where->equalTo("s_nik", $value[0]);
                $select->where($where);
                $res = $sql->prepareStatementForSqlObject($select)->execute()->current();

                if (empty($res)) {
                    $data = array(
                        's_nik' => $value[0],
                        's_nama' => $value[1]
                    );
                    $this->insert($data);
                } else {
                    $data = array(
                        's_nama' => $value[1]
                    );
                    $this->update($data, array('s_nik' => $value[0]));
                }
            }
            $i++;
        }
    }

    public function hapus($id = null)
    {
        # code...
        $this->delete(array('IDNIK' => (int)$id));
    }

    public function getdataNIK($NIK = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("a" => "S_NIK_CAPIL"));
        $select->join(array("b" => "S_KEL_CAPIL"), new Expression('b."NO_KEL" = a."NO_KEL" AND b."NO_KEC"=  a."NO_KEC"::INT'), array("NAMA_KEL"), "INNER");
        $select->join(array("c" => "S_KEC_CAPIL"), new Expression('c."NO_KEC" =  b."NO_KEC"'), array("NAMA_KEC"), "INNER");
        $where = new Where();
        $where->literal('a."NIK" = \''.$NIK.'\'');
        $select->where($where);
        $select->limit(1);
        // echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select)->execute()->current();
        return $state;
    }
}
