<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class DokTanahTable extends AbstractTableGateway {

    protected $table = 's_jenisdoktanah';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new DokTanahBase());
        $this->initialize();
    }

    public function savedata(DokTanahBase $kc) {
        $data = array(
            's_namadoktanah' => $kc->s_namadoktanah,
        );
//        var_dump($data);
//        exit();
        $id = (int) $kc->s_iddoktanah;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('s_iddoktanah' => $kc->s_iddoktanah));
        }
    }

    public function getGridCount(DokTanahBase $base) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($base->s_namadoktanah != 'undefined')
            $where->literal("$this->table.s_namadoktanah::text LIKE '%$base->s_namadoktanah%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridData(DokTanahBase $base, $offset) {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from($this->table);
        $where = new Where();
        if ($base->s_namadoktanah != 'undefined')
            $where->literal("$this->table.s_namadoktanah::text LIKE '%$base->s_namadoktanah%'");
        $select->where($where);
        $select->order("s_iddoktanah asc");
        $select->limit($base->rows = (int) $base->rows);
        $select->offset($offset = (int) $offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id) {
        $rowset = $this->select(array('s_iddoktanah' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function hapusData($id) {
        $this->delete(array('s_iddoktanah' => $id));
    }

    public function comboBox() {
        $resultSet = $this->select();
        return $resultSet;
    }

}
