<?php

namespace Bphtb\Model\Setting;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class NotifemailBase implements InputFilterAwareInterface {    
    
    public $s_idstatus; 
    public $s_status;
    
    protected $inputFilter;    
    
    public function exchangeArray($data){  
        $this->s_idstatus = isset($data['s_idstatus']) ? $data['s_idstatus'] : null;
        $this->s_status = isset($data['s_status']) ? $data['s_status'] : null;
    }
    
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}