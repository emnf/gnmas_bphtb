<?php

namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;

class PenguranganTarifTable extends AbstractTableGateway {

    protected $table = 's_pengurangan_tarif';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PenguranganTarifBase());
        $this->initialize();
    }

    public function savedata(PenguranganTarifBase $pb) {
        $data = array(
            's_nilaipengurangan' => $pb->s_nilaipengurangan,
            's_namapenguranagan' => $pb->s_namapenguranagan,
        );
        $id = (int) $pb->s_idpengurangan;
        if ($id == 0) {
            $table = new \Zend\Db\TableGateway\TableGateway('s_pengurangan_tarif', $this->adapter);
            $table->insert($data);
        } else {
            $table = new \Zend\Db\TableGateway\TableGateway('s_pengurangan_tarif', $this->adapter);
            $table->update($data, array('s_idpengurangan' => $id));
        }
    }

    // public function getGridCount(PersyaratanBase $base) {
    //     $sql = new Sql($this->adapter);
    //     $select = $sql->select();
    //     $select->from($this->table);
    //     $where = new Where();
    //     $select->join($this->table_transaksi, "$this->table_transaksi.s_idjenistransaksi = $this->table.s_idjenistransaksi", array(), "left");
    //     if ($base->s_namapersyaratan != 'undefined')
    //         $where->literal("$this->table.s_namapersyaratan::text LIKE '%$base->s_namapersyaratan%'");
    //     if ($base->s_namajenistransaksi != 'undefined')
    //         $where->literal("$this->table_transaksi.s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
    //     $select->where($where);
    //     $state = $sql->prepareStatementForSqlObject($select);
    //     $res = $state->execute();
    //     return $res->count();
    // }

    // public function getGridData(PersyaratanBase $base, $offset) {
    //     $sql = new Sql($this->adapter);
    //     $select = $sql->select();
    //     $select->from($this->table);
    //     $where = new Where();
    //     $select->join($this->table_transaksi, "$this->table_transaksi.s_idjenistransaksi = $this->table.s_idjenistransaksi", array('s_namajenistransaksi'), 'left');
    //     if ($base->s_namapersyaratan != 'undefined')
    //         $where->literal("$this->table.s_namapersyaratan::text LIKE '%$base->s_namapersyaratan%'");
    //     if ($base->s_namajenistransaksi != 'undefined')
    //         $where->literal("$this->table_transaksi.s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
    //     $select->where($where);
    //     $select->order("s_idjenistransaksi asc");
    //     $select->order("s_idpersyaratan asc");
    //     $select->limit($base->rows = (int) $base->rows);
    //     $select->offset($offset = (int) $offset);
    //     $state = $sql->prepareStatementForSqlObject($select);
    //     $res = $state->execute();
    //     return $res;
    // }

    public function getDataId($id) {
        $rowset = $this->select(array('s_idpengurangan' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function getPersenPengurangan($id)
    {
        $sql = "SELECT s_nilaipengurangan FROM s_pengurangan_tarif WHERE s_idpengurangan=". $id ."";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();        
    }
    
    // public function getpersyaratan($id) {
        
    //     $sql = "select * from s_persyaratan where s_idjenistransaksi=".$id."";
    //     $statement = $this->adapter->query($sql);
    //     $res = $statement->execute();
    //     return $res;
    // }

    // public function getDataIdTransaksis($id) {
    //     $rowset = $this->select(array('s_idjenistransaksi' => $id));
    //     return $rowset;
    // }

    // public function getDataSyaratSPT($idsyarat, $idspt) {
    //     $sql = "select t_persyaratan from t_spt where t_idspt=$idspt and t_persyaratan  like '%$idsyarat%'";
    //     $statement = $this->adapter->query($sql);
    //     $res = $statement->execute();
    //     return $res->current();
    // }

    // public function hapusData($id) {
    //     $table_persyaratan = new \Zend\Db\TableGateway\TableGateway('s_persyaratan', $this->adapter);
    //     $table_persyaratan->delete(array('s_idpersyaratan' => $id));
    // }

    public function comboBox() {
        $resultSet = $this->select();
        return $resultSet;
    }

}
