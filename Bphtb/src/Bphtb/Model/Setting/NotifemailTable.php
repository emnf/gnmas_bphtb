<?php
namespace Bphtb\Model\Setting;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class NotifemailTable extends AbstractTableGateway {
    
    protected $table = "s_notifemail";
    
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new \Bphtb\Model\Setting\NotifemailBase());
        $this->initialize();
    }

    public function getdata() {
        $rowset = $this->select();
        $row = $rowset->current();
        return $row;
    }
    
    public function savedata($status=null){        
        $data = array(
            's_status' => $status
                );
        $this->update($data,array("s_idstatus"=> 1));
    }
    
    
    
}