<?php

namespace Bphtb\Model\Verifikasi;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

//PHP MAILER ================
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Bphtb\Model\Setting\PemdaTable;

//PHP MAILER ================

class VerifikasiSPTTable extends AbstractTableGateway
{

    protected $table = 't_pembayaranspt';
    protected $PemdaTable;

    public function __construct(Adapter $adapter)
    {
        $this->PemdaTable = new PemdaTable($adapter);
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new VerifikasiSPTBase());
        $this->initialize();
    }

    //================= datagrid verifikasi
    public function getjumlahdata($sTable, $count, $sWhere)
    {        
        $sql = "SELECT " . $count . " FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }

    public function semuadatavalidasi($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        $pencarianstatusvalidasi = 2;
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        $pencarianstatusvalidasi = 2;
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 'status_pendaftaran') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        //$aFilteringRules[] = " exists (select * from fr_cekstatus_validasi b where " . $aColumns[$i] . "::text ='" . $input->getPost('sSearch_' . $i) . "') ";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 1;
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                        $pencarianstatusvalidasi = 2;
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        if (!empty($aFilteringRules)) {
            if ($pencarianstatusvalidasi == 1) {
                $statusvalidasi = " ";
            } else {
                $statusvalidasi = " AND status_validasi::text = ANY (VALUES ('1'),('4'),('2')) AND t_tglverifikasispt_kasubid IS NOT NULL AND t_pejabatverifikasispt_kasubid IS NOT NULL AND t_statusspt_kasubid IS NOT NULL";
                //exists (select * from fr_cekstatus_validasi b where status_validasi::text = ANY (VALUES ('1'),('2')))"; //status_validasi::text IN (VALUES ('1'),('2'))"; //in ('1','2')";
            }
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " " . $statusvalidasi . " ";
        } else {
            $sWhere = " WHERE status_validasi::text = ANY (VALUES ('1'),('4'),('2')) AND t_tglverifikasispt_kasubid IS NOT NULL AND t_pejabatverifikasispt_kasubid IS NOT NULL AND t_statusspt_kasubid IS NOT NULL";
            //exists (select * from fr_cekstatus_validasi b where status_validasi::text = ANY (VALUES ('1'),('2')))"; //status_validasi::text IN (VALUES ('1'),('2'))"; // in ('1','2')";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        if($sTable == "fr_pendaftaran_v5"){
            $fr_pendaftaran_v5 = $this->fr_pendaftaran_v5(1);
            $sTable = "(".$fr_pendaftaran_v5.") as fr_pendaftaran_v5";
        }

        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
        
        // die($fr_pendaftaran_v5);

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();



        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }


            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            } else {
                $status_bayar = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }

            if (($session['s_namauserrole'] == "Administrator")) {
                $admin_hapus = '<a style="background-color:red;color: #fff;" href="#" onclick="hapusAdmin(' . $aRow['t_idpembayaranspt'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus Verifikasi</a>';
            } else {
                $admin_hapus = '';
            }

            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }

            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $batal = '';
                $cetaksurat = '';
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/tervalidasi.png">';
                $cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
                if ($aRow['t_statusbayarspt'] == 'TRUE') {
                    $edit = '';
                    if (($aRow['fr_tervalidasidua'] == 1) || ($aRow['fr_validasidua'] == 1)) {
                        $validasikedua = '';
                    } else {
                        $validasikedua = '<a style="background-color:#008d4c;color: #fff;" href="verifikasi_spt/inputvalidasikedua?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-check-square-o"></i> Validasi Ke Dua</a>    ';
                    }
                } else {
                    $validasikedua = '';
                    $edit = '<a href="verifikasi_spt/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                }

                //btn validasi kabid
                $btn_validasikabid = '';
            } else {
                if ($aRow['status_validasi'] == 4) { //ASUBID
                    $status_verifikasi = '<img title="Telah Diverifikasi Kasubid" width="20" height="20" src="' . $cekurl . '/public/img/icon/verifikasi1.png">';
                } else if ($aRow['status_validasi'] == 2) { //TIDAK LENGKAP
                    $status_verifikasi = '<img title="Syarat Validasi Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/validasi_tidaklengkap.png">';
                }


                $cetaksurat = '<a href="#" onclick="openCetakBukti(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Pemberitahuan</a>';
                $validasikedua = '';

                if ($aRow['t_inputbpn'] == true) {
                    $edit = '<a href="verifikasi_spt/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                    $batal = '';
                } else {
                    $edit = '<a href="verifikasi_spt/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                    //BERKAS & VALIDASI DIPISAH ============
                    //                    $batal = '<a href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');return false;"><i class="fa fa-fw fa-undo"></i> Batal</a>';
                    //BERKAS & VALIDASI DIPISAH ============

                    //BERKAS & VALIDASI DIGABUNG ============
                    $batal = '<a href="#" onclick="hapusAdmin(' . $aRow['t_idpembayaranspt'] . ');return false;"><i class="fa fa-fw fa-undo"></i> Batal</a>';
                    //BERKAS & VALIDASI DIGABUNG ============
                }

                //btn validasi kabid
                if ($aRow['t_statusspt_kabid'] == 2) {
                    $btn_validasikabid = '<button class="btn btn-sm_fr btn-danger" href="#"><i class="fa fa-fw fa-eye"></i> DITOLAK</button>';
                } else {
                    $btn_validasikabid = '<a class="btn" href="verifikasi_spt/validasikabid?t_idspt=' . $aRow['t_idspt'] . '" style="background-color:#0caf78;color:white;"><i class="fa fa-check-square-o"></i> Validasi Kabid</a>';
                }
            }

            if (!empty($aRow['p_idpemeriksaan'])) {
                $jmlpajak = "<span style='float:right;color:blue;'>" . number_format($aRow['p_totalspt'], 0, ',', '.') . "</span>";
                $cetaksuratpenelitian = '<a href="#" onclick="openCetakPenelitian(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Penelitian</a>';
            } else {
                $jmlpajak = "<span style='float:right;'>" . number_format($aRow['t_totalspt'], 0, ',', '.') . "</span>";
                $cetaksuratpenelitian = '';
            }

            if ($aRow['fr_validasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-check-square-o"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-minus-circle"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 2) {
                $warnatr = '<span style="background-color:blue;color: #fff;"> &nbsp; <i class="fa fa-fw fa-plus-circle"></i> </span> &nbsp;';
            } else {
                $warnatr = '';
            }

            if (!empty($aRow['t_idsptsebelumnya'])) {
                $cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ', ' . $aRow['t_idsptsebelumnya'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
            } else {
                //$cetaksspd = '<a href="#" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';

            }

            $btn = '<div class="dropdown">
                    <button onclick="myFunction(' . $aRow['t_idspt'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $aRow['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                                ' . $cetaksspd . '
                                <!-- <a href="#" onclick="openCetak(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Bukti</a> -->
                              ' . //$cetaksuratpenelitian 
                '
                                <a href="verifikasi_spt/viewdata?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>
                                ' . //$cetaksurat.'  
                '
                                ' . $edit . '    
                                ' . $batal . '
                                ' . $validasikedua . '
                                ' . $admin_hapus . '   
                                    
                    </div>
                  </div>';


            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }

            //terkait tgl jatuh tempo kodebayar
            if (!empty($aRow['t_kodebayarbanksppt']) && $aRow['t_kodebayarbanksppt'] != "" && $aRow['t_statusbayarspt'] != "TRUE") {
                $hariawal = date('Y-m-d');
                $hariakhir = date('Y-m-d', strtotime($aRow['t_tgljatuhtempokodebayarspt']));
                $beda = $this->IntervalDays($hariawal, $hariakhir) + 1;

                if ($beda <= 0) { //apabila tgl jatuh tempo kodebayar habis
                    $status_kodebayar = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-clock-o"></i> Habis</a>';
                } else {
                    $status_kodebayar = '<a class="btn btn-warning btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-clock-o"></i> ' . $beda . ' Hari</a>';
                }
            } else {
                $status_kodebayar = '';
            }

            if (!empty($aRow['t_tglverifikasispt_kabid']) && $aRow['t_tglverifikasispt_kabid'] != "") {
                $tgl_validasi = date('d-m-Y', strtotime($aRow['t_tglverifikasispt_kabid']));
            } else {
                $tgl_validasi = "";
            }

            //HITUNG PERBEDAAN WAKTU
            $waktu_tanggap = "";
            if($aRow["t_waktuawaldaftar"] != NULL && $aRow["t_waktuawalproses"] != NULL){
                $waktu_awal = date_create($aRow["t_waktuawaldaftar"]);
                $waktu_akhir = date_create($aRow["t_waktuawalproses"]);
                $diff  = date_diff($waktu_awal, $waktu_akhir);
                // var_dump($diff);exit();
                
                if($diff->d > 0){
                    $waktu_tanggap .="".$diff->d." Hari ";
                }
                if($diff->h > 0){
                    $waktu_tanggap .="".$diff->h." Jam ";
                }
                if($diff->i > 0){
                    $waktu_tanggap .="".$diff->i." Menit ";
                }
                if($diff->s >0){
                    $waktu_tanggap .="".$diff->s." Detik";
                }
            }
            //HITUNG PERBEDAAN WAKTU

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $warnatr . " " . $t_kohirspt . "</center>",
                "<center>" . $novalidasi . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",

                "<center>" . $tgl_validasi . "</center>",
                $aRow['t_nopbphtbsppt'],
                $aRow['t_namawppembeli'],
                $jmlpajak,
                "<center>" . $aRow['t_kodebayarbanksppt'] . "</center>",
                "<center>" . $status_bayar . "</center>",
                "<center>" . $status_pendaftaran . "</center>",
                "<center>" . $status_verifikasi . "</center>",
                $aRow['s_namapejabat'],
                "<b style='text-align: center; '>" .$waktu_tanggap . "</b>",
                "<center>" . $btn . "</center>",
                //                "<center>" . $btn_validasikabid . "</center>",
                //                "<center>" . $status_kodebayar . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function semuadatapendaftaran($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 't_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    if ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " "; //AND t_tglverifikasispt IS NULL AND status_pendaftaran = 1
        } else {
            $sWhere = " "; //WHERE t_tglverifikasispt IS NULL AND status_pendaftaran = 1
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 


        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();

        //var_dump($sql);
        //exit();

        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            $btn = '<a href="#" class="btn btn-block btn-info btn-sm" onclick="pilihPendataanSspdBphtb(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-hand-pointer-o"></i> PILIH</a>';



            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow['t_kohirspt'] . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</center>",
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                $aRow['t_namawppembeli'],
                $aRow['t_namawppenjual'],
                "<center>" . $btn . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                $aRow['s_namanotaris']
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    //================= end datagrid verifikasi


    public function temukanDataPembayaran(VerifikasiSPTBase $spt)
    {
        $sql = "select * from view_sspd a left join t_pembayaranspt b on b.t_idspt = a.t_idspt where  b.t_kohirpembayaran=" . $spt->t_kohirpembayaran . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDatas($spt)
    {
        $sql = "select * from view_sspd_pembayaran where t_idpembayaranspt = " . $spt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function savedata(VerifikasiSPTBase $verspt, $t_pejabatverifikasi)
    {
        $id_pembayaran = (int)$verspt->t_idpembayaranspt;
        $data = array(
            't_verifikasispt' => \Zend\Json\Json::encode($verspt->t_persyaratanverifikasi),
            't_tglverifikasispt' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_pejabatverifikasispt' => $t_pejabatverifikasi,
            't_kodebayarbanksppt' => $verspt->t_kodebayarbanksppt,
            't_keteranganvalidasi' => $verspt->t_keteranganvalidasi,
        );
        if ($id_pembayaran == 0) {
            $data['t_idspt'] = $verspt->t_idspt;
            $data['t_idnotaris'] = $verspt->t_idnotarisspt;
            $this->insert($data);
        } else {
            $this->update($data, array('t_idpembayaranspt' => $verspt->t_idpembayaranspt));
        }
    }

    public function savedataVerifikasiKasubid(VerifikasiSPTBase $verspt, $t_pejabatverifikasi, $ar_pemda = null, $cektabelpersyaratan = null, $jml_syarat = null, $jml_syarat_verifikasi = null, $urlapp = null, $dataspt = null)
    {
        $datanotaris = $this->getDataNotarisIdUser($verspt->t_idnotarisspt);
        $ar_pemda = $this->PemdaTable->getdata();
        $id_pembayaran = (int)$verspt->t_idpembayaranspt;
        $data = array(
            //BERKAS
            't_verifikasispt' => \Zend\Json\Json::encode($verspt->t_persyaratanverifikasi),
            't_tglverifikasispt' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_pejabatverifikasispt' => $t_pejabatverifikasi,
            //            't_keteranganvalidasi' => $verspt->t_keteranganvalidasi,
            //KASUBID
            't_tglverifikasispt_kasubid' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_pejabatverifikasispt_kasubid' => $t_pejabatverifikasi,
            't_statusspt_kasubid' => 1,
            //KABID
            //            't_tglverifikasispt_kabid' => null,
            //            't_pejabatverifikasispt_kabid' => null,
            //            't_statusspt_kabid' => null,
            //            't_kodebayarbanksppt' => null,
            //            't_tgljatuhtempokodebayarspt' => null,
        );
        
        // OPSI POTONGAN WARIS_HIBAH_WASIAT
        if($dataspt["t_idjenistransaksi"] == 4 || $dataspt["t_idjenistransaksi"] == 5){
            $sql = new Sql($this->adapter);
            $update = $sql->update();
            $update->table('t_spt');
            $update->set(array(
                't_potongan_waris_hibahwasiat' => $dataspt["t_potongan_waris_hibahwasiat"],
                't_totalspt' => $dataspt["t_totalspt"]
            ));
            $update->where('t_idspt = ' . (int)$verspt->t_idspt . '');
            $statement = $sql->prepareStatementForSqlObject($update);
            $statement->execute();
        }
        // OPSI POTONGAN WARIS_HIBAH_WASIAT

        //VALIDASI KABID DIGABUNG
        $data['t_tglverifikasispt_kabid'] = $data['t_tglverifikasispt'];
        $data['t_pejabatverifikasispt_kabid'] = $t_pejabatverifikasi;
        $data['t_statusspt_kabid'] = 1;

        
        

        if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {
            $datamax = $this->getmaxkohirSPT($verspt->t_idspt);

            $data['t_kodebayarbanksppt'] = str_pad($ar_pemda->s_kodeprovinsi, 2, '0', STR_PAD_LEFT) . str_pad($ar_pemda->s_kodekabkot, 2, '0', STR_PAD_LEFT) . str_pad(11, 2, '0', STR_PAD_LEFT) . str_pad(1, 2, '0', STR_PAD_LEFT) . str_pad(date('y', strtotime($verspt->t_tglverifikasispt)), 2, '0', STR_PAD_LEFT) . str_pad($datamax['t_kohirketetapanspt'], 5, "0", STR_PAD_LEFT);
            //$data['t_tgljatuhtempokodebayarspt'] = date('Y-m-d', strtotime("+7 days"));
            $ntpd = $ar_pemda->s_kodeprovinsi.$ar_pemda->s_kodekabkot.'112'.date('Y', strtotime($verspt->t_tglverifikasispt)).str_pad($datamax['t_kohirketetapanspt'], 4, "0", STR_PAD_LEFT);

            $data2 = array(
                't_kohirketetapanspt' => $datamax['t_kohirketetapanspt'],
                'ntpd' => $ntpd
            );

            $sql = new Sql($this->adapter);
            $update = $sql->update();
            $update->table('t_spt');
            $update->set($data2);
            $update->where('t_idspt = ' . (int)$verspt->t_idspt . '');
            $statement = $sql->prepareStatementForSqlObject($update);
            $statement->execute();
        } else {
            $data['t_kodebayarbanksppt'] = null;
        }
        
        //VALIDASI KABID DIGABUNG

        if ($id_pembayaran == 0) {
            $data['t_idspt'] = $verspt->t_idspt;
            $data['t_idnotaris'] = $verspt->t_idnotarisspt;
            //BERKAS ===
            $data['t_keteranganvalidasi'] = $verspt->t_keteranganvalidasi;
            //BERKAS ===

            // WAKTU AWAL PROSES
            $data["t_waktuawalproses"] = date("Y-m-d H:i:s");
            // WAKTU AWAL PROSES

            $this->insert($data);

            //SET. NOTIF EMAIL
            $s_email = $this->getsetnotifemail();
            if ((!empty($verspt->t_keteranganvalidasi) && $verspt->t_keteranganvalidasi != "") && $s_email['s_status'] == 1) {
                //PHP MAILER ===========================  
                require 'public/PHPMailer/src/PHPMailer.php';
                require 'public/PHPMailer/src/Exception.php';
                require 'public/PHPMailer/src/SMTP.php';

                $mail = new PHPMailer();
                $mail->IsSMTP();

                //GMAIL config
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                //$mail->SMTPSecure = "ssl";                 // sets the prefix to the server
                //$mail->Host = "smtp.gmail.com";
                //$mail->Port = 465 ;
                $mail->Host = 'tls://smtp.gmail.com:587';

                $mail->Username   = "bpprd.tanahbumbukab@gmail.com";  // GMAIL username
                $mail->Password   = "b1p2p3r4d51";
                $mail->SMTPDebug = 0;
                //=== untuk keterangan debug server dan client = 2
                //=== untuk client = 1
                //=== unutk tidak muncul = 0

                //End Gmail
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );

                $mail->From       = "bpprd.tanahbumbukab@gmail.com";
                $mail->FromName   = "e-Bphtb " . str_ireplace("Kabupaten ", "", $ar_pemda->s_namakabkot) . "";
                $mail->Subject    = "Notifikasi Validasi";

                $mail->AddEmbeddedImage('./' . $ar_pemda->s_logo, 'logo_1');
                $html = '
                    <table style="font-family:Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif; max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
                        <tbody>
                            <tr style="margin: 0px; padding: 0px;">
                                <td style="margin: 0px; padding: 0px;"></td>
                                <td bgcolor="#FFFFFF" style="margin: 0px auto; padding: 0px; display: block; max-width: 600px; clear: both;">

                                    <div style="max-width: 600px; display: block; border-collapse: collapse; margin: 0px auto; padding: 30px 15px; border: 1px solid rgb(231, 231, 231);">
                                        <table style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif; max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
                                            <tbody>
                                            <tr>
                                                    <td style="padding: 15px 20px 10px;" bgcolor="#2069b3">
                                            <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; color: rgb(255, 255, 255);" border="0">
                                                <tbody><tr>
                                                    <td width="280">
                                                        <img src="cid:logo_1" style="border: 0px; min-height: auto; width: 41px; outline: 0px;">
                                                    </td>
                                                    <td width="280" align="right" style="font-size: 20px;">
                                                        Notifikasi Keterangan Validasi
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                                </tr>
                                            <tr style="margin: 0px; padding: 0px;">
                                                <td style="margin: 0px; padding: 0px;">
                                                    <h4 style="font-size: 20px; padding: 30px 0px 10px; font-weight: bold; color: rgb(78, 78, 78);">
                                                    Pendaftaran No. ' . $dataspt['t_kohirspt'] . ' Periode ' . $dataspt['t_periodespt'] . '
                                                    </h4>
                                                    <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">Hi, <b>' . $datanotaris['s_namanotaris'] . '</b>.</p>
                                                    <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">
                                                        Kami informasikan untuk berkas dengan informasi dibawah ini :
                                                    </p>
                                                    <div style="text-align: left; margin: 20px; padding: 0px;" align="center">
                                                        <table width="100%" border="0" style=" border-collapse: collapse;">
                                                            <tr>
                                                                <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                                No. Daftar</td>
                                                                <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                                <td style="padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['t_kohirspt'] . '</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                                Periode</td>
                                                                <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                                <td style=" padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['t_periodespt'] . '</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                                Nama WP</td>
                                                                <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                                <td style=" padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['t_namawppembeli'] . '</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                                Jenis Transaksi</td>
                                                                <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                                <td style="padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['s_namajenistransaksi'] . '</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                                NOP</td>
                                                                <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                                <td style="padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['t_nopbphtbsppt'] . '</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 150px; padding: 5px; font-weight: bold; color: red; border-bottom: 1px solid rgb(217, 217, 217);">
                                                                Keterangan Validasi</td>
                                                                <td style="width: 5px; padding: 5px; color: red; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                                <td style="padding: 5px; color: red; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                                ' . $verspt->t_keteranganvalidasi . '</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">
                                                        Mohon untuk mengecek berkas tersebut. Terima Kasih.
                                                    </p>
                                                    <div style="text-align: center; margin: 20px; padding: 0px;" align="center">
                                                        <a href="' . $urlapp . '/pendataan_sspd/detailnotif?t_idspt=' . $dataspt['t_idspt'] . '" style="color: rgb(255, 255, 255); text-decoration: none; display: inline-block; vertical-align: middle; line-height: 20px; font-size: 13px; font-weight: 600; text-align: center; white-space: nowrap; border-radius: 3px; background-color: #2069b3; margin: 0px; padding: 9px 14px; border: 1px solid #2e5cb8" target="_blank" data-saferedirecturl="' . $urlapp . '/pendataan_sspd/detailnotif?t_idspt=' . $dataspt['t_idspt'] . '">
                                                            LINK E-BPHTB
                                                        </a>
                                                    </div>
                                                    <p style="font-weight: normal; font-size: 14px; line-height: 1.6; border-top: 3px solid rgb(208, 208, 208); margin: 40px 0px 0px; padding: 10px 0px 0px;">
                                                        <small style="color: rgb(153, 153, 153); margin: 0px; padding: 0px;">
                                                            Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                                                        </small>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </td>
                                <td style="margin: 0px; padding: 0px;"></td>
                            </tr>
                        </tbody></table>
                    <table style="max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px 0px 60px; padding: 0px; clear: both;" bgcolor="transparent">
                        <tbody>
                            <tr style="margin: 0px; padding: 0px;">
                                <tr style="margin: 0px; padding: 0px;">
                                    <td style="margin: 0px; padding: 0px;"></td>
                                    <td style="margin: 0px auto; padding: 0px; display: block; max-width: 600px; clear: both;">
                                        <div style="max-width: 600px; display: block; border-collapse: collapse; background-color: rgb(247, 247, 247); margin: 0px auto; padding: 20px 15px; border-color: rgb(231, 231, 231); border-style: solid; border-width: 0px 1px 1px;">
                                        <table width="100%" style="max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
                                            <tbody style="margin: 0px; padding: 0px;">
                                                <tr style="margin: 0px; padding: 0px;">
                                                    <td valign="middle" style="margin: 0px; padding: 0px; width: 7%;">
                                                        <img src="cid:logo_1" style="border: 0px; min-height: auto; width: 41px; outline: 0px;">
                                                    </td>
                                                    <td valign="middle" style="margin: 0px; padding: 0px; width: 53%;">
                                                        <p style="color: rgb(145, 144, 142); font-size: 10px; line-height: 150%; font-weight: normal; margin: 0px; padding: 0px;">
                                                            Jika butuh bantuan, gunakan halaman <a href="http://bpprd.tanahbumbukab.go.id/?page_id=344" style="color: #2069b3; text-decoration: none; margin: 0px; padding: 0px;" target="_blank" data-saferedirecturl="http://bpprd.tanahbumbukab.go.id/?page_id=344">Kontak Kami</a>.
                                                            <br style="margin: 0px; padding: 0px;">
                                                            © 2019, ' . $ar_pemda->s_namasingkatinstansi . ' ' . $ar_pemda->s_namakabkot . '
                                                        </p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            <td style="margin: 0px; padding: 0px;"></td>
                        </tr>
                    </tbody>
                    </table>';
                $mail->MsgHTML($html);

                $mail->AddReplyTo("bpprd.tanahbumbukab@gmail.com", "BPPRD MINAHASA SELATAN"); //optional
                $mail->AddAddress($datanotaris['s_email'], $datanotaris['s_namanotaris']);
                $mail->IsHTML(true); // send as HTML

                if (!$mail->Send()) { //to see if we return a message or a value bolean
                    echo "Mailer Error: " . $mail->ErrorInfo;
                };
                //PHP MAILER ===========================
            }
        } else {
            $this->update($data, array('t_idpembayaranspt' => $verspt->t_idpembayaranspt));
        }
    }

    public function updatenosspd_validasikedua($idspt, $max)
    {
        $ar_pemda = $this->PemdaTable->getdata();
        
        $data = array(
            't_kohirketetapanspt' => $max,
            'ntpd' => $ar_pemda->s_kodeprovinsi.$ar_pemda->s_kodekabkot.'112'.date('Y').str_pad($max, 4, "0", STR_PAD_LEFT)
        ); //+ 1
        $t_spt = new TableGateway('t_spt', $this->adapter);
        $t_spt->update($data, array('t_idspt' => $idspt));
    }


    public function updatenosspd(VerifikasiSPTBase $verspt, $max)
    {
        $data = array(
            't_kohirketetapanspt' => $max
        ); //+ 1
        $t_spt = new TableGateway('t_spt', $this->adapter);
        $t_spt->update($data, array('t_idspt' => $verspt->t_idspt));
    }

    public function savedataverifikasipembayaran(VerifikasiSPTBase $verspt, $t_pejabatverifikasi)
    {
        $id = (int)$verspt->t_idpembayaranspt;
        $data = array(
            //Berhubungan dengan Verifikasi
            't_idspt' => $verspt->t_idspt,
            't_idnotaris' => $verspt->t_idnotarisspt,
            't_verifikasispt' => \Zend\Json\Json::encode($verspt->t_persyaratanverifikasi),
            't_tglverifikasispt' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_pejabatverifikasispt' => $t_pejabatverifikasi,
            //Berhubungan dengan Pembayaran
            't_periodepembayaran' => date('Y', strtotime($verspt->t_tglverifikasispt)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_ketetapanspt' => 1,
            't_nilaipembayaranspt' => 0,
            't_statusbayarspt' => true,
            't_pejabatpembayaranspt' => $t_pejabatverifikasi,
            't_idpenerimasetoran' => $t_pejabatverifikasi,
        );
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array('t_idpembayaranspt' => (int)$verspt->t_idpembayaranspt));
        }
    }

    public function savepembayarannihil(VerifikasiSPTBase $verspt, $t_pejabatverifikasi)
    {
        $ar_pemda = $this->PemdaTable->getdata();
        $id = (int)$verspt->t_idpembayaranspt;
        $data = array(
            //Berhubungan dengan Pembayaran
            't_periodepembayaran' => date('Y', strtotime($verspt->t_tglverifikasispt)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_ketetapanspt' => 1,
            't_nilaipembayaranspt' => 0,
            't_statusbayarspt' => true,
            't_pejabatpembayaranspt' => $t_pejabatverifikasi,
            //'t_kodebayarbanksppt' => null,
            't_idpenerimasetoran' => $t_pejabatverifikasi,
            't_tglpembayaran_system' => date('Y-m-d H:i:s'),
            't_reffpembayaran' => "7106".date("ymdHis").$this->generateRandomString(16),
            't_kodebankbayar' => '01',
            't_bankbayar' => 'BENDAHARA PENERIMA',
            't_viapembayaran' => 1,
            't_kohirpembayaran' => $this->getnopembayaran(date('Y', strtotime($verspt->t_tglverifikasispt)))
        );

        $this->update($data, array('t_idpembayaranspt' => (int)$verspt->t_idpembayaranspt));

        // CREATE NTPD
        $sql = new Sql($this->adapter);
        $ntpd = $ar_pemda->s_kodeprovinsi.$ar_pemda->s_kodekabkot.'112'.$data["t_periodepembayaran"].str_pad($data["t_kohirpembayaran"], 4, '0', STR_PAD_LEFT);
        $update = $sql->update("t_spt")->set(array("ntpd" => $ntpd))->where(array("t_idspt" => (int) $verspt->t_idspt));
        $sql->prepareStatementForSqlObject($update)->execute();
    }
    
    public function generateRandomString($length) {
        $characters = '0123456789'; //abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getnopembayaran($periode = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_kohirpembayaran" => new Expression("(COALESCE(MAX(t_kohirpembayaran),0) + 1)")
        ));
        $select->from('t_pembayaranspt');
        $where = new Where();
        if ($periode != null) {
            $where->literal("t_periodepembayaran='" . $periode . "'");
        } else {
            $where->literal("t_periodepembayaran='" . date("Y") . "'");
        }
        $select->where($where);
        // echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res['t_kohirpembayaran'];
    }

    public function savedataverbayarbpn(VerifikasiSPTBase $verspt, $idspt, $kodebayarbank)
    {
        $data = array(
            //Berhubungan dengan Verifikasi
            't_idspt' => $idspt,
            't_idnotaris' => $verspt->t_idnotaris,
            't_verifikasispt' => $verspt->t_verifikasispt,
            't_tglverifikasispt' => $verspt->t_tglverifikasispt,
            't_pejabatverifikasispt' => $verspt->t_pejabatverifikasispt,
            //Berhubungan dengan Pembayaran
            't_periodepembayaran' => date('Y'),
            't_tanggalpembayaran' => date('Y-m-d'),
            't_ketetapanspt' => 1,
            't_nilaipembayaranspt' => 0,
            't_statusbayarspt' => true
        );
        $this->insert($data);
    }

    public function savedataverifikasi_validasikedua(VerifikasiSPTBase $verspt, $idspt, $kohir, $tglproses, $ar_pemda)
    {
        $datanotaris = $this->getDataNotarisIdUser($verspt->t_idnotaris);
        // $kd_bayar = str_pad($ar_pemda->s_kodeprovinsi, 2, '0', STR_PAD_LEFT) . str_pad($ar_pemda->s_kodekabkot, 2, '0', STR_PAD_LEFT) . str_pad(5, 2, '0', STR_PAD_LEFT) . str_pad($datanotaris['s_kodenotaris'], 2, '0', STR_PAD_LEFT) . str_pad(date('y', strtotime($tglproses)), 2, '0', STR_PAD_LEFT) . str_pad($kohir, 5, "0", STR_PAD_LEFT);
        $kd_bayar = str_pad($ar_pemda->s_kodeprovinsi, 2, '0', STR_PAD_LEFT) . str_pad($ar_pemda->s_kodekabkot, 2, '0', STR_PAD_LEFT) . str_pad(11, 2, '0', STR_PAD_LEFT) . str_pad(2, 2, '0', STR_PAD_LEFT) . str_pad(date('y', strtotime($verspt->t_tglverifikasispt)), 2, '0', STR_PAD_LEFT) . str_pad($kohir, 5, "0", STR_PAD_LEFT);
        $data = array(
            't_idspt' => $idspt,
            't_idnotaris' => $verspt->t_idnotaris,
            't_verifikasispt' => $verspt->t_verifikasispt,
            't_tglverifikasispt' => date('Y-m-d'), //$verspt->t_tglverifikasispt,
            't_pejabatverifikasispt' => $verspt->t_pejabatverifikasispt,
            //'t_kodebayarbanksppt' => '1002'.date('y', strtotime($tglproses)).str_pad($kohir,7,"0",STR_PAD_LEFT),
            't_tglverifikasispt_kasubid' => date('Y-m-d'),
            't_pejabatverifikasispt_kasubid' => $verspt->t_pejabatverifikasispt,
            't_statusspt_kasubid' => 1,
            't_kodebayarbanksppt' => $kd_bayar,
            't_tglverifikasispt_kabid' => date('Y-m-d'),
            't_pejabatverifikasispt_kabid' => $verspt->t_pejabatverifikasispt,
            't_statusspt_kabid' => 1,
            //'t_tgljatuhtempokodebayarspt' => date('Y-m-d', strtotime("+7 days")),
        );

        $this->insert($data);
    }

    public function savedataverifikasibpn(VerifikasiSPTBase $verspt, $idspt, $kodebayarbank)
    {
        $data = array(
            't_idspt' => $idspt,
            't_idnotaris' => $verspt->t_idnotaris,
            't_verifikasispt' => $verspt->t_verifikasispt,
            't_tglverifikasispt' => $verspt->t_tglverifikasispt,
            't_pejabatverifikasispt' => $verspt->t_pejabatverifikasispt,
            't_kodebayarbanksppt' => $kodebayarbank
        );
        $this->insert($data);
    }

    public function savedatapemeriksaan($dt, $p_totalnjoptanah, $p_totalnjopbangunan, $p_grandtotalnjop, $p_grandtotalnjop_aphb, $p_potonganspt, $p_totalspt)
    {
        $id_bayar = $this->getIdPembayaran();

        if ($dt->p_idpemeriksaan == 0) {

            $data = array(
                //"p_idpembayaranspt" => $dt->t_idpembayaranspt, //$id_bayar['max'],
                "p_luastanah" => str_ireplace(".", "", $dt->p_luastanah),
                "p_luasbangunan" => str_ireplace(".", "", $dt->p_luasbangunan),
                "p_njoptanah" => str_ireplace(".", "", $dt->p_njoptanah),
                "p_njopbangunan" => str_ireplace(".", "", $dt->p_njopbangunan),
                "p_totalnjoptanah" => $p_totalnjoptanah, //str_ireplace(".", "", $dt->p_totalnjoptanah),
                "p_totalnjopbangunan" => $p_totalnjopbangunan, //str_ireplace(".", "", $dt->p_totalnjopbangunan),
                "p_grandtotalnjop" => $p_grandtotalnjop, //str_ireplace(".", "", $dt->p_grandtotalnjop),
                "p_nilaitransaksispt" => str_ireplace(".", "", $dt->p_nilaitransaksispt),
                "p_ketwaris" => $dt->p_ketwaris,
                "p_terbukti" => $dt->p_terbukti,
                "p_idjenistransaksi" => $dt->p_idjenistransaksi,
                "p_idjenishaktanah" => $dt->p_idjenishaktanah,
                "p_potonganspt" => $p_potonganspt, //str_ireplace(".", "", $dt->p_potonganspt),
                "p_totalspt" => $p_totalspt, //str_ireplace(".", "", $dt->p_totalspt)
                "p_grandtotalnjop_aphb" => $p_grandtotalnjop_aphb
            );

            if (!empty($dt->t_idpembayaranspt)) {
                $data['p_idpembayaranspt'] = $dt->t_idpembayaranspt;
            } else {
                $data['p_idpembayaranspt'] = $id_bayar['max'];
            }


            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->insert($data);
        } else {
            $data = array(
                //"p_idpembayaranspt" => $dt->t_idpembayaranspt, //$id_bayar['max'],
                "p_luastanah" => str_ireplace(".", "", $dt->p_luastanah),
                "p_luasbangunan" => str_ireplace(".", "", $dt->p_luasbangunan),
                "p_njoptanah" => str_ireplace(".", "", $dt->p_njoptanah),
                "p_njopbangunan" => str_ireplace(".", "", $dt->p_njopbangunan),
                "p_totalnjoptanah" => $p_totalnjoptanah, //str_ireplace(".", "", $dt->p_totalnjoptanah),
                "p_totalnjopbangunan" => $p_totalnjopbangunan, //str_ireplace(".", "", $dt->p_totalnjopbangunan),
                "p_grandtotalnjop" => $p_grandtotalnjop, //str_ireplace(".", "", $dt->p_grandtotalnjop),
                "p_nilaitransaksispt" => str_ireplace(".", "", $dt->p_nilaitransaksispt),
                "p_ketwaris" => $dt->p_ketwaris,
                "p_terbukti" => $dt->p_terbukti,
                "p_idjenistransaksi" => $dt->p_idjenistransaksi,
                "p_idjenishaktanah" => $dt->p_idjenishaktanah,
                "p_potonganspt" => $p_potonganspt, //str_ireplace(".", "", $dt->p_potonganspt),
                "p_totalspt" => $p_totalspt, //str_ireplace(".", "", $dt->p_totalspt)
                "p_grandtotalnjop_aphb" => $p_grandtotalnjop_aphb
            );


            $data['p_idpemeriksaan'] = $dt->p_idpemeriksaan;
            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->update($data, array('p_idpemeriksaan' => $dt->p_idpemeriksaan));
        }
    }

    public function savedatapemeriksaan2($dt)
    {
        $id_bayar = $this->getIdPembayaran();

        if (!empty($dt->p_t_grandtotalnjop_aphb)) {
            $aphbtot = str_ireplace(".", "", $dt->p_t_grandtotalnjop_aphb);
        } else {
            $aphbtot = 0;
        }

        $data = array(
            "p_idpembayaranspt" => $id_bayar['max'],
            "p_luastanah" => str_ireplace(".", "", $dt->p_luastanah),
            "p_luasbangunan" => str_ireplace(".", "", $dt->p_luasbangunan),
            "p_njoptanah" => str_ireplace(".", "", $dt->p_njoptanah),
            "p_njopbangunan" => str_ireplace(".", "", $dt->p_njopbangunan),
            "p_totalnjoptanah" => str_ireplace(".", "", $dt->p_totalnjoptanah),
            "p_totalnjopbangunan" => str_ireplace(".", "", $dt->p_totalnjopbangunan),
            "p_grandtotalnjop" => str_ireplace(".", "", $dt->p_grandtotalnjop),
            "p_nilaitransaksispt" => str_ireplace(".", "", $dt->p_nilaitransaksispt),
            "p_ketwaris" => $dt->p_ketwaris,
            "p_terbukti" => $dt->p_terbukti,
            "p_idjenistransaksi" => $dt->p_idjenistransaksi,
            "p_idjenishaktanah" => $dt->p_idjenishaktanah,
            "p_potonganspt" => str_ireplace(".", "", $dt->p_potonganspt),
            "p_totalspt" => str_ireplace(".", "", $dt->p_totalspt),
            "p_grandtotalnjop_aphb" => $aphbtot
        );
        if ($dt->p_idpemeriksaan == 0) {
            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->insert($data);
        } else {
            $data['p_idpemeriksaan'] = $dt->p_idpemeriksaan;
            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->update($data, array('p_idpemeriksaan' => $dt->p_idpemeriksaan));
        }
    }

    public function savedatapemeriksaan_hapus($dt)
    {
        $id_bayar = $this->getIdPembayaran();



        if (!empty($dt->t_idpembayaranspt)) {


            $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
            $t_pemeriksaan->delete(array('p_idpembayaranspt' => $dt->t_idpembayaranspt));
        } else { }
    }

    public function batalVerifikasi($verspt)
    {
        //        $dp = $this->getDatas($verspt);
        //        $data = array(
        //            't_verifikasispt' => NULL,
        //            't_tglverifikasispt' => NULL,
        ////            't_pejabatverifikasispt' => $pembspt->t_alamatwppenjual,
        //        );
        //        if ($dp['t_statusbayarspt']) {
        //            $this->update($data, array('t_idpembayaranspt' => (int) $verspt));
        //        } else {
        $sql = "select t_idspt from t_pembayaranspt where t_idpembayaranspt = " . $verspt . " ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->current();

        $data = array(
            't_kohirketetapanspt' => null,
            'ntpd' => null
        ); //+ 1
        $t_spt = new TableGateway('t_spt', $this->adapter);
        $t_spt->update($data, array('t_idspt' => $res['t_idspt']));

        $data2 = array(
            't_statuspelaporannotaris' => null,
            't_tglpelaporannotaris' => null,
            't_noajbbaru' => null,
            't_tglajbbaru' => null
        );

        $t_detailsptbphtb = new TableGateway('t_detailsptbphtb', $this->adapter);
        $t_detailsptbphtb->update($data2, array(
            't_idspt' => $res['t_idspt']
        ));


        $this->delete(array('t_idpembayaranspt' => (int)$verspt));

        //HAPUS CHAT
        $sql = new Sql($this->adapter);
        $delete = $sql->delete();
        $delete->from('fr_psn_validasi');
        $delete->where('t_idspt = ' . (int)$res['t_idspt'] . '');
        $sql->prepareStatementForSqlObject($delete)->execute();
        //        }
    }

    public function batalVerifikasi2($verspt)
    {
        //        $dp = $this->getDatas($verspt);
        //        $data = array(
        //            't_verifikasispt' => NULL,
        //            't_tglverifikasispt' => NULL,
        ////            't_pejabatverifikasispt' => $pembspt->t_alamatwppenjual,
        //        );
        //        if ($dp['t_statusbayarspt']) {
        //            $this->update($data, array('t_idpembayaranspt' => (int) $verspt));
        //        } else {
        $this->delete(array('t_idpembayaranspt' => (int)$verspt));
        //        }
    }

    //batal verifikasi kasubid
    public function batalverifikasikasubid($verspt)
    {
        $data = array(
            't_tglverifikasispt_kasubid' => null,
            't_pejabatverifikasispt_kasubid' => null,
            't_keterangan_kasubid' => null,
            't_tglverifikasispt_kabid' => null,
            't_pejabatverifikasispt_kabid' => null,
            't_keterangan_kabid' => null,
            't_statusspt_kasubid' => null,
            't_statusspt_kabid' => null,
            't_tgljatuhtempokodebayarspt' => null,
            't_kodebayarbanksppt' => null,
        );

        $this->update($data, array('t_idpembayaranspt' => (int)$verspt));
    }



    public function hapusDataPemeriksaan($id)
    {
        $t_pemeriksaan = new TableGateway('t_pemeriksaan', $this->adapter);
        $t_pemeriksaan->delete(array('p_idpembayaranspt' => $id));
    }

    public function getIdPembayaran()
    {
        $sql = "select max(t_idpembayaranspt) from t_pembayaranspt";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getSptid($t_idpembayaranspt)
    {
        $rowset = $this->select(array('t_idpembayaranspt' => $t_idpembayaranspt));
        $row = $rowset->current();
        return $row;
    }

    public function getViewPendaftaran($t_idspt)
    {
        $sql = "select * from view_pendaftaran where t_idspt = " . $t_idspt . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getHargaAcuan($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok)
    {
        $sql = "select * from s_acuan where s_kd_propinsi = '" . $s_kd_propinsi . "' 
                and s_kd_dati2 = '" . $s_kd_dati2 . "' 
                and s_kd_kecamatan = '" . $s_kd_kecamatan . "' 
                and s_kd_kelurahan = '" . $s_kd_kelurahan . "'
                and s_kd_blok = '" . $s_kd_blok . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getHargaHistoryNJOPTanah($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $t_idpembayaranspt)
    {
        // $sql = "select max(njoptanahtransaksi) as njoptanahtransaksi , max(njoptanah) as njoptanah from view_harganjoptanah
        //         where t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
        //         and t_idpembayaranspt not in ($t_idpembayaranspt)";
        //         // echo $sql;exit();
        // $statement = $this->adapter->query($sql);
        // return $statement->execute()->current();

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_harganjoptanah = $this->view_harganjoptanah();
        $select->from(array("view_harganjoptanah" => $view_harganjoptanah));
        $select->columns(array(
            "njoptanahtransaksi" => new Expression("max(njoptanahtransaksi)"),
            "njoptanah" => new Expression("max(njoptanah)")
        ));
        $where = new \Zend\Db\Sql\Where();
        $where->literal("t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
                and t_idpembayaranspt not in ($t_idpembayaranspt)");
        $where->literal("status_pendaftaran = '1' AND status_validasi = '1'");
        $select->where($where);
        // echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getHargaHistoryNJOPTanahpilih($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $t_idspt)
    {
        //        $sql = "select max(njoptanahtransaksi) as njoptanahtransaksi , max(njoptanah) as njoptanah from view_harganjoptanah
        //                where t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
        //                and t_idspt not in ($t_idspt)";
        //        echo $sql;exit();
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute()->current();

        // $sql = new Sql($this->adapter);
        // $select = $sql->select();
        // $select->columns(array(
        //     "t_nopbphtbsppt",
        //     //            "njoptanahtransaksi" => new \Zend\Db\Sql\Expression("((((b.t_grandtotalnjop_aphb -
        //     //        CASE
        //     //            WHEN (b.t_totalnjopbangunan IS NULL) THEN (0)::bigint
        //     //            ELSE b.t_totalnjopbangunan
        //     //        END))::numeric / b.t_luastanah) * (1)::numeric)"),
        //     "njoptanahtransaksi" => new \Zend\Db\Sql\Expression("((((b.t_grandtotalnjop_aphb - b.t_totalnjopbangunan))::numeric / b.t_luastanah) * (1)::numeric)"),
        //     //            "njoptanah" => new \Zend\Db\Sql\Expression("(((a.t_nilaitransaksispt -
        //     //        CASE
        //     //            WHEN (b.t_totalnjopbangunan IS NULL) THEN (0)::bigint
        //     //            ELSE b.t_totalnjopbangunan
        //     //        END))::numeric / b.t_luastanah)"),
        //     "njoptanah" => new \Zend\Db\Sql\Expression("(((a.t_nilaitransaksispt - b.t_totalnjopbangunan ))::numeric / b.t_luastanah)"),
        //     "t_grandtotalnjop_aphb" => new \Zend\Db\Sql\Expression("b.t_grandtotalnjop_aphb"),
        //     "t_nilaitransaksispt",
        //     "t_idspt",
        //     "t_idpembayaranspt" => new \Zend\Db\Sql\Expression("c.t_idpembayaranspt"),
        //     "t_alamatop" => new \Zend\Db\Sql\Expression("b.t_alamatop"),
        //     "t_kelurahanop" => new \Zend\Db\Sql\Expression("b.t_kelurahanop"),
        //     "t_kecamatanop" => new \Zend\Db\Sql\Expression("b.t_kecamatanop"),
        //     "t_rtop" => new \Zend\Db\Sql\Expression("b.t_rtop"),
        //     "t_rwop" => new \Zend\Db\Sql\Expression("b.t_rwop"),
        //     "t_kabupatenop" => new \Zend\Db\Sql\Expression("b.t_kabupatenop"),
        //     "t_tanggalpembayaran" => new \Zend\Db\Sql\Expression("c.t_tanggalpembayaran"),
        //     "t_luastanah" => new \Zend\Db\Sql\Expression("b.t_luastanah"),
        //     "t_njoptanah" => new \Zend\Db\Sql\Expression("b.t_njoptanah"),
        //     "t_totalnjoptanah" => new \Zend\Db\Sql\Expression("b.t_totalnjoptanah"),
        //     "t_luasbangunan" => new \Zend\Db\Sql\Expression("b.t_luasbangunan"),
        //     "t_njopbangunan" => new \Zend\Db\Sql\Expression("b.t_njopbangunan"),
        //     "t_totalnjopbangunan" => new \Zend\Db\Sql\Expression("b.t_totalnjopbangunan"),
        // ));
        // $select->from(array("a" => "t_spt"));
        // $select->join(
        //     array("b" => "t_detailsptbphtb"),
        //     new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
        //     array(),
        //     "LEFT"
        // );
        // $select->join(
        //     array("c" => "t_pembayaranspt"),
        //     new \Zend\Db\Sql\Expression("c.t_idspt = a.t_idspt"),
        //     array(),
        //     "LEFT"
        // );
        // $select->join(
        //     array("d" => "fr_pendaftaran_v5"),
        //     new \Zend\Db\Sql\Expression("a.t_idspt = d.t_idspt"),
        //     array(
        //         "status_pendaftaran",
        //         "status_validasi"
        //     ),
        //     "LEFT"
        // );
        // $where = new \Zend\Db\Sql\Where();
        // $where->literal("a.t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
        //         and a.t_idspt != $t_idspt");
        // $where->literal("d.status_pendaftaran = '1' AND d.status_validasi = '1'");
        // $select->where($where);
        // $select->order(new \Zend\Db\Sql\Expression("t_nilaitransaksispt DESC"));
        // // echo $select->getSqlString();exit();
        // $state = $sql->prepareStatementForSqlObject($select);
        // $res = $state->execute()->current();
        // return $res;

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_harganjoptanah = $this->view_harganjoptanah();
        $select->from(array("view_harganjoptanah" => $view_harganjoptanah));
        $select->columns(array(
            "njoptanahtransaksi" => new Expression("max(njoptanahtransaksi)"),
            "njoptanah" => new Expression("max(njoptanah)")
        ));
        $where = new \Zend\Db\Sql\Where();
        $where->literal("t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
                and t_idspt != $t_idspt");
        $where->literal("status_pendaftaran = '1' AND status_validasi = '1'");
        $select->where($where);
        // echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getPresentase($nilaipresentase)
    {
        $sql = "select * from s_presentase where s_presentase >= " . $nilaipresentase . " and s_presentasemin <= " . $nilaipresentase . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getmaxkohir($periode)
    {
        //$sql = "select max(t_kohirketetapanspt) as t_kohirketetapanspt from t_spt";
        $periodeSPT = ($periode!=null)?$periode: date('Y');
        $sql = "SELECT COALESCE(max(t_kohirketetapanspt)+1,1) as t_kohirketetapanspt FROM t_spt WHERE t_periodespt::text='" . $periodeSPT . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getperingatanvalidasi($idnotaris)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        //$select->from("fr_pembayaran_v5");
        $fr_pembayaran_v5 = $this->fr_pembayaran_v5();
        $select->from(array("z" => $fr_pembayaran_v5));
        $where = new \Zend\Db\Sql\Where();
        $where->literal("t_idnotarisspt = " . $idnotaris . " and status_validasi = 2"); // ADA KETERANGAN DAN STATUS VALIDASI BELUM LENGKAP //AND T_KETERANGANVALIDASI IS NOT NULL 
        $select->where($where);
        //echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getpesanmasuk($idnotaris = null, $session = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("fr_psn_validasi");
        $select->columns(["id_psnvalidasi", "t_idspt", "t_kohirspt", "t_dari", "t_isipesan", "t_psndilihat", "t_untuk", "t_waktukirim"]);
        $select->join("s_users", "s_users.s_iduser = fr_psn_validasi.t_dari", ["s_nama_dari" => "s_username", "s_jabatan_dari" => "s_jabatan"], "left");
        $select->join(["a" => "s_users"], "a.s_iduser = fr_psn_validasi.t_untuk", ["s_nama_untuk" => "s_username", "s_jabatan_untuk" => "s_jabatan"], "left");
        $select->join("fr_pembayaran_v5", "fr_pembayaran_v5.t_idspt = fr_psn_validasi.t_idspt", [], "left");
        $select->join("fr_pendaftaran_v5", "fr_pendaftaran_v5.t_idspt = fr_psn_validasi.t_idspt", array(), "LEFT");
        $where = new \Zend\Db\Sql\Where();

        if (($session['s_akses'] == 1) || ($session['s_akses'] == 2) || ($session['s_akses'] == 8) || ($session['s_akses'] == 9)) {
            $where->literal("fr_psn_validasi.t_dari != " . $session['s_iduser'] . " ");
        } else {

            if (!empty($idnotaris)) {
                $where->literal("fr_pembayaran_v5.t_idnotarisspt = " . $idnotaris . " and fr_psn_validasi.t_dari != " . $idnotaris . " ");
            }
        }
        $select->where($where);
        $select->order("t_waktukirim DESC");
        //echo $select->getSqlString(); exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getpesanmasuklimit($idnotaris = null, $session = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("fr_psn_validasi");
        $select->columns(
            array("id_psnvalidasi", "t_idspt", "t_kohirspt", "t_dari", "t_isipesan", "t_psndilihat", "t_untuk", "t_waktukirim")
        );
        $select->join(
            "s_users",
            new Expression("s_users.s_iduser = fr_psn_validasi.t_dari"),
            array("s_nama_dari" => "s_username", "s_jabatan_dari" => "s_jabatan", "s_akses_dari" => "s_akses"),
            "left"
        );
        $select->join(
            array("a" => "s_users"),
            new Expression("a.s_iduser = fr_psn_validasi.t_untuk"),
            array("s_nama_untuk" => "s_username", "s_jabatan_untuk" => "s_jabatan", "s_akses_untuk" => "s_akses"),
            "left"
        );
//        $select->join(
//            "fr_pembayaran_v5",
//            "fr_pembayaran_v5.t_idspt = fr_psn_validasi.t_idspt",
//            [],
//            "left"
//        );
        $fr_pembayaran_v5 = $this->fr_pembayaran_v5();
        $select->join(array(
            "fr_pembayaran_v5" => $fr_pembayaran_v5
        ),
            "fr_pembayaran_v5.t_idspt = fr_psn_validasi.t_idspt",
            array(),
            "left"
        );
        $select->join(
            "fr_pendaftaran_v5",
            "fr_pendaftaran_v5.t_idspt = fr_psn_validasi.t_idspt",
            array(),
            "LEFT"
        );
        $select->join(
            array("b" => "s_notaris"),
            new \Zend\Db\Sql\Expression("b.s_idnotaris = s_users.s_idpejabat_idnotaris::INT AND s_users.s_tipe_pejabat = '2'"),
            array(
                "s_namanotaris_dari" => "s_namanotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_pejabat"),
            new \Zend\Db\Sql\Expression("c.s_idpejabat = s_users.s_idpejabat_idnotaris::INT AND s_users.s_tipe_pejabat = '1'"),
            array(
                "s_namapejabat_dari" => "s_namapejabat",
            ),
            "LEFT"
        );
        $select->join(
            array("bb" => "s_notaris"),
            new \Zend\Db\Sql\Expression("bb.s_idnotaris = A.s_idpejabat_idnotaris::INT AND a.s_tipe_pejabat = '2'"),
            array(
                "s_namanotaris_untuk" => "s_namanotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("cc" => "s_pejabat"),
            new \Zend\Db\Sql\Expression("cc.s_idpejabat = A.s_idpejabat_idnotaris::INT AND a.s_tipe_pejabat = '1'"),
            array(
                "s_namapejabat_untuk" => "s_namapejabat",
            ),
            "LEFT"
        );
        $where = new \Zend\Db\Sql\Where();

        if (($session['s_akses'] == 1) || ($session['s_akses'] == 2) || ($session['s_akses'] == 8 || ($session['s_akses'] == 9))) {
            $where->literal("fr_psn_validasi.t_dari != " . $session['s_iduser'] . " ");
            $where->literal("s_users.s_akses = '3'"); //HANYA PESAN DARI NOTARIS SAJA
            $where->literal("fr_pendaftaran_v5.status_validasi = 2"); //HANYA BERKAS YG TIDAK LENGKAP
        } else {

            if (!empty($idnotaris)) {
                $where->literal("fr_pembayaran_v5.t_idnotarisspt = " . $idnotaris . " AND fr_psn_validasi.t_dari != " . $idnotaris . " ");
                $where->literal("fr_pendaftaran_v5.status_validasi = 2"); //HANYA BERKAS YG TIDAK LENGKAP
            }
        }
        $select->where($where);
        $select->order("t_waktukirim DESC");
        $select->limit(10);
        $select->offset(0);
        // echo $select->getSqlString();
        // exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function savedataVerifikasiBerkas(VerifikasiSPTBase $verspt, $t_pejabatverifikasi)
    {
        $id_pembayaran = (int)$verspt->t_idpembayaranspt;
        $data = array(
            't_verifikasispt' => \Zend\Json\Json::encode($verspt->t_persyaratanverifikasi),
            't_tglverifikasispt' => date('Y-m-d', strtotime($verspt->t_tglverifikasispt)),
            't_pejabatverifikasispt' => $t_pejabatverifikasi,
            't_keteranganvalidasi' => $verspt->t_keteranganvalidasi,
        );
        if ($id_pembayaran == 0) {
            $data['t_idspt'] = $verspt->t_idspt;
            $data['t_idnotaris'] = $verspt->t_idnotarisspt;
            $this->insert($data);
        } else {
            $this->update($data, array('t_idpembayaranspt' => $verspt->t_idpembayaranspt));
        }
    }

    public function semuadataverifikasiberkas($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        $pencarianstatusvalidasi = 2;
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        $pencarianstatusvalidasi = 2;
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 'status_pendaftaran') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 2;
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        //$aFilteringRules[] = " exists (select * from fr_cekstatus_validasi b where " . $aColumns[$i] . "::text ='" . $input->getPost('sSearch_' . $i) . "') ";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                        $pencarianstatusvalidasi = 1;
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                        $pencarianstatusvalidasi = 2;
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }




        if (!empty($aFilteringRules)) {
            if ($pencarianstatusvalidasi == 1) {
                $statusvalidasi = " ";
            } else {
                $statusvalidasi = " AND status_validasi::text = ANY (VALUES ('5'),('2'),('4'),('1')) ";
                //exists (select * from fr_cekstatus_validasi b where status_validasi::text = ANY (VALUES ('1'),('2')))"; //status_validasi::text IN (VALUES ('1'),('2'))"; //in ('1','2')";
            }
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " " . $statusvalidasi . " ";
        } else {
            $sWhere = " WHERE status_validasi::text = ANY (VALUES ('5'),('2'),('4'),('1')) ";
            //exists (select * from fr_cekstatus_validasi b where status_validasi::text = ANY (VALUES ('1'),('2')))"; //status_validasi::text IN (VALUES ('1'),('2'))"; // in ('1','2')";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();



        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }


            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            } else {
                $status_bayar = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }

            if (($session['s_namauserrole'] == "Administrator")) {
                $admin_hapus = '<a style="background-color:red;color: #fff;" href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus Verifikasi</a>';
            } else {
                $admin_hapus = '';
            }

            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }

            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 5) || ($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 4) || ($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {

                if ($aRow['status_validasi'] == 5) {
                    $batal = '<a href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');return false;"><i class="fa fa-fw fa-undo"></i> Batal</a>';
                } else {
                    $batal = '';
                }
                $cetaksurat = '';
                $status_verifikasi = '<img title="Berkas Sudah Diverifikasi" width="20" height="20" src="' . $cekurl . '/public/img/ok.png">';
                if ($aRow['t_statusbayarspt'] == 'TRUE') {
                    $edit = '';
                } else if (($aRow['status_validasi'] == 4) || ($aRow['status_validasi'] == 1)) {
                    $edit = '';
                } else {
                    $edit = '<a href="verifikasi_spt_berkas/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                }
            } else {
                $status_verifikasi = '<img title="Syarat Validasi Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/validasi_tidaklengkap.png">';

                $cetaksurat = '<a href="#" onclick="openCetakBukti(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Pemberitahuan</a>';

                if ($aRow['t_inputbpn'] == true) {
                    $edit = '<a href="verifikasi_spt_berkas/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                    $batal = '';
                } else {
                    $edit = '<a href="verifikasi_spt_berkas/edit?t_idpembayaranspt=' . $aRow['t_idpembayaranspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                    $batal = '<a href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');return false;"><i class="fa fa-fw fa-undo"></i> Batal</a>';
                }
            }

            if (!empty($aRow['p_idpemeriksaan'])) {
                $jmlpajak = "<span style='float:right;color:blue;'>" . number_format($aRow['p_totalspt'], 0, ',', '.') . "</span>";
                $cetaksuratpenelitian = '<a href="#" onclick="openCetakPenelitian(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Penelitian</a>';
            } else {
                $jmlpajak = "<span style='float:right;'>" . number_format($aRow['t_totalspt'], 0, ',', '.') . "</span>";
                $cetaksuratpenelitian = '';
            }



            if ($aRow['fr_validasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-check-square-o"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-minus-circle"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 2) {
                $warnatr = '<span style="background-color:blue;color: #fff;"> &nbsp; <i class="fa fa-fw fa-plus-circle"></i> </span> &nbsp;';
            } else {
                $warnatr = '';
            }

            $btn = '<div class="dropdown">
                    <button onclick="myFunction(' . $aRow['t_idspt'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $aRow['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                                <a href="#" onclick="openCetak(' . $aRow['t_kohirspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> Surat Bukti</a>
                              ' . $cetaksuratpenelitian . '   
                                <a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>
                                ' . $cetaksurat . '  
                                 
                                ' . $edit . '    
                                ' . $batal . '
                                ' . $admin_hapus . '   
                                    
                    </div>
                  </div>';


            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="verifikasi_spt_berkas/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }


            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $warnatr . " " . $t_kohirspt . "</center>",
                "<center>" . $novalidasi . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tglverifikasispt'])) . "</center>",
                $aRow['t_namawppembeli'],
                $jmlpajak,
                "<center>" . $aRow['t_kodebayarbanksppt'] . "</center>",
                "<center>" . $status_bayar . "</center>",
                "<center>" . $status_pendaftaran . "</center>",
                "<center>" . $status_verifikasi . "</center>",
                "<center>" . $btn . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function semuadataselesaiverifikasiberkas($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 't_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    if ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            //BERKSA & VALIDASI DIPISAH =====================
            //$sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND status_validasi::text = ANY (VALUES ('5')) AND t_tglverifikasispt IS NOT NULL AND t_pejabatverifikasispt IS NOT NULL ";
            //BERKSA & VALIDASI DIPISAH =====================

            //BERKAS & VALIDASI DIGABUNG ===================
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND status_validasi::text = ANY (VALUES ('3')) ";
            //BERKAS & VALIDASI DIGABUNG ===================
        } else {
            //BERKSA & VALIDASI DIPISAH =====================
            //$sWhere = " WHERE status_validasi::text = ANY (VALUES ('5')) AND t_tglverifikasispt IS NOT NULL AND t_pejabatverifikasispt IS NOT NULL ";
            //BERKSA & VALIDASI DIPISAH =====================

            //BERKAS & VALIDASI DIGABUNG ===================
            $sWhere = " WHERE status_validasi::text = ANY (VALUES ('3')) ";
            //BERKAS & VALIDASI DIGABUNG ===================
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 

        //die($sql);
        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();

        //var_dump($sql);
        //exit();

        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            $btn = '<a href="#" class="btn btn-block btn-info btn-sm" onclick="pilihPendataanSspdBphtb(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-hand-pointer-o"></i> PILIH</a>';



            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow['t_kohirspt'] . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</center>",
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                $aRow['t_namawppembeli'],
                $aRow['t_namawppenjual'],
                "<center>" . $btn . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                $aRow['s_namanotaris']
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function validasikabid($post, $session, $dataspt, $penolakan, $ar_pemda)
    {

        if ($penolakan != 1) { //disetujui

            //kodebayar
            // 10       = kode dati2
            // 10       = kode jenis pajak bphtb
            // 01       = jenis surat ketetapan SPTPD
            // 18       = tgl verifikasi kabid
            // 000001   = no ketetapanspt

            $datamax = $this->getmaxkohirSPT($post['t_idspt']); //$datamax = $this->getmaxkohir();

            $data = array(
                't_kodebayarbanksppt' => $ar_pemda->s_kodekabkot . '1001' . date('y') . str_pad($datamax['t_kohirketetapanspt'], 6, "0", STR_PAD_LEFT),
                't_tglverifikasispt_kabid' => date('Y-m-d'),
                't_pejabatverifikasispt_kabid' => $session['s_iduser'],
                't_statusspt_kabid' => 1,
                't_tgljatuhtempokodebayarspt' => date('Y-m-d', strtotime("+7 days")),
            );

            //apabila ada pemeriksaan maka totalspt yg dipake pemeriksaan
            if (!empty($dataspt['p_idpemeriksaan']) && $dataspt['p_idpemeriksaan'] != "") {
                $totalspt = $dataspt['p_totalspt'];
            } else {
                $totalspt = $dataspt['t_totalspt'];
            }

            if ((int)$totalspt <= 0) {
                $data['t_kodebayarbanksppt'] = null;
                $data['t_periodepembayaran'] = date('Y');
                $data['t_tanggalpembayaran'] = date('Y-m-d');
                $data['t_ketetapanspt'] = 1;
                $data['t_nilaipembayaranspt'] = 0;
                $data['t_idpenerimasetoran'] = $session['s_iduser'];
                $data['t_statusbayarspt'] = true;
                $data['t_pejabatpembayaranspt'] = $session['s_iduser'];
            }

            $this->update($data, array("t_idspt" => $post['t_idspt']));

            $data2 = array(
                't_kohirketetapanspt' => $datamax['t_kohirketetapanspt'],
            );

            $sql = new Sql($this->adapter);
            $update = $sql->update();
            $update->table('t_spt');
            $update->set($data2);
            $update->where('t_idspt = ' . $post['t_idspt'] . '');
            $statement = $sql->prepareStatementForSqlObject($update);
            $res = $statement->execute();
            return $res;
        } else { //ditolak
            $data = array(
                't_tglverifikasispt_kabid' => date('Y-m-d'),
                't_pejabatverifikasispt_kabid' => $session['s_iduser'],
                't_statusspt_kabid' => 2,
            );

            $this->update($data, array("t_idspt" => $post['t_idspt']));
            return $res;
        }
    }

    public function IntervalDays($CheckIn, $CheckOut)
    {
        $CheckInX = explode("-", $CheckIn);
        $CheckOutX = explode("-", $CheckOut);
        $date1 = mktime(0, 0, 0, $CheckInX[1], $CheckInX[2], $CheckInX[0]);
        $date2 = mktime(0, 0, 0, $CheckOutX[1], $CheckOutX[2], $CheckOutX[0]);
        //$date1 =  mktime(0, 0, 0, $CheckInX[0],$CheckInX[2],$CheckInX[1]);  // tahun - bulan - tgl
        //$date2 =  mktime(0, 0, 0, $CheckOutX[0],$CheckOutX[2],$CheckOutX[1]); // tahun - bulan - tgl
        $interval = ($date2 - $date1) / (3600 * 24);
        // returns numberofdays
        return $interval;
    }

    public function getmaxkohirSPT($idspt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_kohirketetapanspt"
        ));
        $select->from("t_spt");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idspt", (int)$idspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();

        if ($res['t_kohirketetapanspt']==null) {
            $select = $sql->select();
            $select->columns(array(
                "t_kohirketetapanspt" => new \Zend\Db\Sql\Expression("COALESCE(max(t_kohirketetapanspt)+1,1)"),
            ));
            $select->from("t_spt");
            $where = new \Zend\Db\Sql\Where();
            $where->literal("t_periodespt::text='" . date('Y') . "'");
            $select->where($where);
            $state = $sql->prepareStatementForSqlObject($select);
            $res = $state->execute()->current();
        }

        return $res;
    }

    public function getHargaHistoryNJOPTanahpilih2($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $t_idspt)
    {
        //        $sql = "select * from view_history_tanah
        //                where t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
        //                and t_idspt not in ($t_idspt) and t_tipeverifikasi = 1 ORDER BY t_nilaitransaksispt DESC LIMIT 10";
        //        $statement = $this->adapter->query($sql);
        //        return $statement->execute();

        // $sql = new Sql($this->adapter);
        // $select = $sql->select();
        // $select->columns(array(
        //     "t_nopbphtbsppt",
        //     //            "njoptanah_pbb" => new \Zend\Db\Sql\Expression("((((b.t_grandtotalnjop_aphb -
        //     //        CASE
        //     //            WHEN (b.t_totalnjopbangunan IS NULL) THEN (0)::bigint
        //     //            ELSE b.t_totalnjopbangunan
        //     //        END))::numeric / b.t_luastanah) * (1)::numeric)"),
        //     "njoptanah_pbb" => new \Zend\Db\Sql\Expression("((((b.t_grandtotalnjop_aphb - b.t_totalnjopbangunan))::numeric / b.t_luastanah) * (1)::numeric)"),
        //     //            "njoptanah" => new \Zend\Db\Sql\Expression("(((a.t_nilaitransaksispt -
        //     //        CASE
        //     //            WHEN (b.t_totalnjopbangunan IS NULL) THEN (0)::bigint
        //     //            ELSE b.t_totalnjopbangunan
        //     //        END))::numeric / b.t_luastanah)"),
        //     "njoptanah" => new \Zend\Db\Sql\Expression("(((a.t_nilaitransaksispt - b.t_totalnjopbangunan ))::numeric / b.t_luastanah)"),
        //     "t_grandtotalnjop_aphb" => new \Zend\Db\Sql\Expression("b.t_grandtotalnjop_aphb"),
        //     "t_nilaitransaksispt",
        //     "t_idspt",
        //     "t_idpembayaranspt" => new \Zend\Db\Sql\Expression("c.t_idpembayaranspt"),
        //     "t_alamatop" => new \Zend\Db\Sql\Expression("b.t_alamatop"),
        //     "t_kelurahanop" => new \Zend\Db\Sql\Expression("b.t_kelurahanop"),
        //     "t_kecamatanop" => new \Zend\Db\Sql\Expression("b.t_kecamatanop"),
        //     "t_rtop" => new \Zend\Db\Sql\Expression("b.t_rtop"),
        //     "t_rwop" => new \Zend\Db\Sql\Expression("b.t_rwop"),
        //     "t_kabupatenop" => new \Zend\Db\Sql\Expression("b.t_kabupatenop"),
        //     "t_tanggalpembayaran" => new \Zend\Db\Sql\Expression("c.t_tanggalpembayaran"),
        //     "t_luastanah" => new \Zend\Db\Sql\Expression("b.t_luastanah"),
        //     "t_njoptanah" => new \Zend\Db\Sql\Expression("b.t_njoptanah"),
        //     "t_totalnjoptanah" => new \Zend\Db\Sql\Expression("b.t_totalnjoptanah"),
        //     "t_luasbangunan" => new \Zend\Db\Sql\Expression("b.t_luasbangunan"),
        //     "t_njopbangunan" => new \Zend\Db\Sql\Expression("b.t_njopbangunan"),
        //     "t_totalnjopbangunan" => new \Zend\Db\Sql\Expression("b.t_totalnjopbangunan"),
        // ));
        // $select->from(array("a" => "t_spt"));
        // $select->join(
        //     array("b" => "t_detailsptbphtb"),
        //     new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
        //     array(),
        //     "LEFT"
        // );
        // $select->join(
        //     array("c" => "t_pembayaranspt"),
        //     new \Zend\Db\Sql\Expression("c.t_idspt = a.t_idspt"),
        //     array(),
        //     "LEFT"
        // );
        // $select->join(
        //     array("d" => "fr_pendaftaran_v5"),
        //     new \Zend\Db\Sql\Expression("a.t_idspt = d.t_idspt"),
        //     array(
        //         "status_pendaftaran",
        //         "status_validasi"
        //     ),
        //     "LEFT"
        // );
        // $where = new \Zend\Db\Sql\Where();
        // $where->literal("a.t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
        //         and a.t_idspt != $t_idspt");
        // $where->literal("d.status_pendaftaran = '1' AND d.status_validasi = '1'");
        // $select->where($where);
        // $select->order(new \Zend\Db\Sql\Expression("t_nilaitransaksispt DESC"));
        // $select->limit(10);
        // //echo $select->getSqlString();exit();
        // $state = $sql->prepareStatementForSqlObject($select);
        // $res = $state->execute();
        // return $res;

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_harganjoptanah = $this->view_harganjoptanah();
        $select->from(array("view_harganjoptanah" => $view_harganjoptanah));
        $where = new \Zend\Db\Sql\Where();
        $where->literal("t_nopbphtbsppt like '%" . $s_kd_propinsi . "." . $s_kd_dati2 . "." . $s_kd_kecamatan . "." . $s_kd_kelurahan . "." . $s_kd_blok . "%'
                and t_idspt != $t_idspt");
        $where->literal("status_pendaftaran = '1' AND status_validasi = '1'");
        $select->where($where);
        $select->order(new \Zend\Db\Sql\Expression("t_nilaitransaksispt DESC"));
        $select->limit(10);
        // echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataNotarisIdUser($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "s_idpejabat_idnotaris",
            "s_iduser",
            "s_username",
            "s_password",
            "s_jabatan",
            "s_akses",
            "s_tipe_pejabat",
            "s_email"
        ));
        $select->from("s_users");
        $select->join(
            "s_notaris",
            new \Zend\Db\Sql\Expression("s_users.s_idpejabat_idnotaris::INTEGER = s_notaris.s_idnotaris"),
            array(
                "s_idnotaris",
                "s_namanotaris",
                "s_alamatnotaris",
                "s_kodenotaris",
                "s_sknotaris",
                "s_tgl1notaris",
                "s_tgl2notaris",
                "s_statusnotaris"
            ),
            "LEFT"
        );
        $where = new \Zend\Db\Sql\Where();
        $where->literal("s_users.s_iduser = '" . $id . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getsetnotifemail()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_notifemail");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }
    
    public function fr_pembayaran_v5($string = null){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
//            "" => new Expression(""),
            "t_idspt" => new Expression("a.t_idspt"),
            "jml_syarat_input" => new Expression("(length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) "),
            "jml_syarat_validasi" => new Expression("(length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_sebenarnya" => new Expression("n.jmlsyarat"),
            "status_pendaftaran" => new Expression("(CASE
            WHEN ((length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            ELSE 2
        END)"),
            "status_validasi" => new Expression("(CASE
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL) THEN 3
            ELSE 2
        END)"),
            "t_kohirspt" => new Expression("a.t_kohirspt"),
            "s_idjenistransaksi" => new Expression("c.s_idjenistransaksi"),
            "t_periodespt" => new Expression("a.t_periodespt"),
            "t_tglprosesspt" => new Expression("a.t_tglprosesspt"),
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_statusbayarspt" => new Expression("(CASE
            WHEN ((e.t_statusbayarspt)::text = 'true'::text) THEN 'TRUE'::text
            ELSE 'FALSE'::text
        END )"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "t_persyaratan" => new Expression("a.t_persyaratan"),
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_idjenistransaksi" => new Expression("a.t_idjenistransaksi"),
            "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "t_ketetapanspt" => new Expression("e.t_ketetapanspt"),
            "t_ketetapands" => new Expression("i.t_ketetapands"),
            "t_kohirds" => new Expression("i.t_kohirds"),
            "notarisds" => new Expression("k.s_namanotaris"),
            "t_periodepembayaran" => new Expression("e.t_periodepembayaran"),
            "t_tanggalpembayaran" => new Expression("e.t_tanggalpembayaran"),
            "t_nilaipembayaranspt" => new Expression("e.t_nilaipembayaranspt"),
            "t_idnotarisspt" => new Expression("a.t_idnotarisspt"),
            "t_totalspt" => new Expression("a.t_totalspt"),
            "p_totalspt" => new Expression("f.p_totalspt"),
            "jml_pajak_v1" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_totalspt
            ELSE a.t_totalspt
        END)"),
            "jml_pajak_v2" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN
            CASE
                WHEN (a.t_totalspt < f.p_totalspt) THEN f.p_totalspt
                ELSE a.t_totalspt
            END
            ELSE a.t_totalspt
        END)"),
            "s_idpejabat_idnotaris" => new Expression("g.s_idpejabat_idnotaris"),
            "s_namanotaris" => new Expression("h.s_namanotaris"),
            "t_noajbbaru" => new Expression("b.t_noajbbaru"),
            "t_tglajbbaru" => new Expression("b.t_tglajbbaru"),
            "t_iddetailsptbphtb" => new Expression("b.t_iddetailsptbphtb"),
            "t_luastanahbpn" => new Expression("b.t_luastanahbpn"),
            "t_luasbangunanbpn" => new Expression("b.t_luasbangunanbpn"),
            "p_luastanah" => new Expression("f.p_luastanah"),
            "p_luasbangunan" => new Expression("f.p_luasbangunan"),
            "t_luastanah" => new Expression("b.t_luastanah"),
            "t_luasbangunan" => new Expression("b.t_luasbangunan"),
            "luas_tanah" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luastanah
            ELSE b.t_luastanah
        END)"),
            "luas_bangunan" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luasbangunan
            ELSE b.t_luasbangunan
        END)"),
            "t_tglsertifikatbaru" => new Expression("b.t_tglsertifikatbaru"), 
            "t_nosertifikatbaru" => new Expression("b.t_nosertifikatbaru"),
            "t_kohirketetapanspt" => new Expression("a.t_kohirketetapanspt"),
            "t_nopbphtbsppt" => new Expression("a.t_nopbphtbsppt"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "t_nilaitransaksispt" => new Expression("a.t_nilaitransaksispt"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),
            "fr_luas_tanah_bpn" => new Expression("b.fr_luas_tanah_bpn"),
            "fr_luas_bangunan_bpn" => new Expression("b.fr_luas_bangunan_bpn"),
            "fr_tervalidasidua" => new Expression("a.fr_tervalidasidua"),
            "fr_validasidua" => new Expression("b.fr_validasidua"),
            "p_grandtotalnjop" => new Expression("f.p_grandtotalnjop"),
            "p_grandtotalnjop_aphb" => new Expression("f.p_grandtotalnjop_aphb"),
            "p_nilaitransaksispt" => new Expression("f.p_nilaitransaksispt"),
            "t_alamatwppembeli" => new Expression("b.t_alamatwppembeli"),
            "t_nikwppembeli" => new Expression("b.t_nikwppembeli"),
            "t_tarif_pembagian_aphb_kali" => new Expression("a.t_tarif_pembagian_aphb_kali"),
            "t_tarif_pembagian_aphb_bagi" => new Expression("a.t_tarif_pembagian_aphb_bagi"),
            "t_grandtotalnjop_aphb" => new Expression("b.t_grandtotalnjop_aphb"),
            "t_idsptsebelumnya" => new Expression("a.t_idsptsebelumnya"),
            "t_alamatwppenjual" => new Expression("b.t_alamatwppenjual"),
            "t_keteranganvalidasi" => new Expression("e.t_keteranganvalidasi"),
            "t_pejabatverifikasispt" => new Expression("e.t_pejabatverifikasispt"),
            "t_idsptsebelumnya" => new Expression("a.t_idsptsebelumnya")
        ));
        $select->from(array("e" => "t_pembayaranspt"));
        $select->join(array("a" => "t_spt"), new Expression("e.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("b" => "t_detailsptbphtb"), new Expression("b.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("c" => "s_jenistransaksi"), new Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"), array(), "LEFT");
        $select->join(array("f" => "t_pemeriksaan"), new Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"), array(), "LEFT");
        $select->join(array("i" => "t_dendasanksinotaris"), new Expression("i.t_idds = e.t_idds"), array(), "LEFT");
        $select->join(array("g" => "s_users"), new Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("j" => "s_users"), new Expression("j.s_iduser = i.t_idnotaris"), array(), "LEFT");
        $select->join(array("k" => "s_notaris"), new Expression("(k.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("n" => "fr_count_s_persyaratan"), new Expression("a.t_idjenistransaksi = n.s_idjenistransaksi"), array(), "LEFT");
        
        if($string != null && $string == 1){
            return $select->getSqlString();
        }else{
            return $select;
        }

        // echo $select;die;
    }

    public function view_harganjoptanah($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_nopbphtbsppt",
            //            "njoptanahtransaksi" => new \Zend\Db\Sql\Expression("((((b.t_grandtotalnjop_aphb -
            //        CASE
            //            WHEN (b.t_totalnjopbangunan IS NULL) THEN (0)::bigint
            //            ELSE b.t_totalnjopbangunan
            //        END))::numeric / b.t_luastanah) * (1)::numeric)"),
            "njoptanahtransaksi" => new \Zend\Db\Sql\Expression("((((b.t_grandtotalnjop_aphb - b.t_totalnjopbangunan))::numeric / b.t_luastanah) * (1)::numeric)"),
            //            "njoptanah" => new \Zend\Db\Sql\Expression("(((a.t_nilaitransaksispt -
            //        CASE
            //            WHEN (b.t_totalnjopbangunan IS NULL) THEN (0)::bigint
            //            ELSE b.t_totalnjopbangunan
            //        END))::numeric / b.t_luastanah)"),
            "njoptanah" => new \Zend\Db\Sql\Expression("(((a.t_nilaitransaksispt - b.t_totalnjopbangunan ))::numeric / b.t_luastanah)"),
            "t_grandtotalnjop_aphb" => new \Zend\Db\Sql\Expression("b.t_grandtotalnjop_aphb"),
            "t_nilaitransaksispt",
            "t_idspt",
            "t_idpembayaranspt" => new \Zend\Db\Sql\Expression("c.t_idpembayaranspt"),
            "t_alamatop" => new \Zend\Db\Sql\Expression("b.t_alamatop"),
            "t_kelurahanop" => new \Zend\Db\Sql\Expression("b.t_kelurahanop"),
            "t_kecamatanop" => new \Zend\Db\Sql\Expression("b.t_kecamatanop"),
            "t_rtop" => new \Zend\Db\Sql\Expression("b.t_rtop"),
            "t_rwop" => new \Zend\Db\Sql\Expression("b.t_rwop"),
            "t_kabupatenop" => new \Zend\Db\Sql\Expression("b.t_kabupatenop"),
            "t_tanggalpembayaran" => new \Zend\Db\Sql\Expression("c.t_tanggalpembayaran"),
            "t_luastanah" => new \Zend\Db\Sql\Expression("b.t_luastanah"),
            "t_njoptanah" => new \Zend\Db\Sql\Expression("b.t_njoptanah"),
            "t_totalnjoptanah" => new \Zend\Db\Sql\Expression("b.t_totalnjoptanah"),
            "t_luasbangunan" => new \Zend\Db\Sql\Expression("b.t_luasbangunan"),
            "t_njopbangunan" => new \Zend\Db\Sql\Expression("b.t_njopbangunan"),
            "t_totalnjopbangunan" => new \Zend\Db\Sql\Expression("b.t_totalnjopbangunan"),
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
            array(),
            "LEFT"
        );
        $select->join(
            array("c" => "t_pembayaranspt"),
            new \Zend\Db\Sql\Expression("c.t_idspt = a.t_idspt"),
            array(),
            "LEFT"
        );
        $select->join(
            array("d" => "fr_pendaftaran_v5"),
            new \Zend\Db\Sql\Expression("a.t_idspt = d.t_idspt"),
            array(
                "status_pendaftaran",
                "status_validasi"
            ),
            "LEFT"
        );

        if($string != null){
            return $select->getSqlString();
        }else{
            return $select;
        }
    }

    public function fr_pendaftaran_v5($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt",
            "jml_syarat_input" => new Expression("( LENGTH ( TRANSLATE (( a.t_persyaratan ) :: TEXT, '[\"]1234567890' :: TEXT, '' :: TEXT )) + 1 )"),
            // "jml_syarat_validasi" => new Expression("( LENGTH ( TRANSLATE (( e.t_verifikasispt ) :: TEXT, '[\"]1234567890' :: TEXT, '' :: TEXT )) + 1 )"),
            "jml_syarat_validasi" => new Expression("CASE WHEN e.t_verifikasispt = 'null' THEN 0 
                ELSE LENGTH ( TRANSLATE (( e.t_verifikasispt ) :: TEXT, '[\"]1234567890' :: TEXT, '' :: TEXT )) + 1 END"),
            "jml_syarat_sebenarnya" => new Expression("j.jmlsyarat"),
            "status_pendaftaran" => new Expression("CASE 
                WHEN ((LENGTH (TRANSLATE (( A.t_persyaratan ) :: TEXT, '[\"]1234567890' :: TEXT, '' :: TEXT )) + 1 ) = j.jmlsyarat ) 
                THEN
                    1 ELSE 2 
                END"),
            "status_validasi" => new Expression("CASE
		
            WHEN (((
                        LENGTH (
                        TRANSLATE (( e.t_verifikasispt ) :: TEXT, '[\"]1234567890' :: TEXT, '' :: TEXT )) + 1 
                        ) = j.jmlsyarat 
                    ) 
                AND ( e.t_statusspt_kabid = 1 )) AND e.t_verifikasispt != 'null' THEN
                1 
                WHEN (((
                            LENGTH (
                            TRANSLATE (( e.t_verifikasispt ) :: TEXT, '[\"]1234567890' :: TEXT, '' :: TEXT )) + 1 
                            ) = j.jmlsyarat 
                        ) 
                    AND ( e.t_statusspt_kasubid = 1 )) AND e.t_verifikasispt != 'null' THEN
                    4 
                    WHEN ((
                            LENGTH (
                            TRANSLATE (( e.t_verifikasispt ) :: TEXT, '[\"]1234567890' :: TEXT, '' :: TEXT )) + 1 
                            ) = j.jmlsyarat AND e.t_verifikasispt != 'null'
                        ) THEN
                        5 
                        WHEN (( LENGTH ( TRANSLATE (( e.t_verifikasispt ) :: TEXT, '[\"]1234567890' :: TEXT, '' :: TEXT )) + 1 ) IS NULL ) THEN
                        3 ELSE 2 
                    END"), //1 KADBID //4 KASUBID //5 VERIFIKASI BERKAS //2 BELUM LENGKAP //3 BELUM DIPERIKSA
            "t_kohirspt",
            "s_idjenistransaksi" => new Expression("c.s_idjenistransaksi"),
            "t_periodespt",
            "t_tglprosesspt",
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_statusbayarspt" => new Expression("CASE	
                    WHEN (( e.t_statusbayarspt ) :: TEXT = 'true' :: TEXT ) THEN
                    'TRUE' :: TEXT ELSE'FALSE' :: TEXT 
                END"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "t_persyaratan",
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_idjenistransaksi",
            "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "t_idnotarisspt",
            "p_totalspt" => new Expression("f.p_totalspt"),
            "t_totalspt",
            "jml_pajak_v1" => new Expression("CASE	
                    WHEN ( f.p_idpemeriksaan IS NOT NULL ) THEN
                    f.p_totalspt ELSE A.t_totalspt 
                END"),
            "jml_pajak_v2" => new Expression("CASE
                WHEN ( f.p_idpemeriksaan IS NOT NULL ) THEN
                CASE
                        WHEN ( A.t_totalspt < f.p_totalspt ) THEN
                        f.p_totalspt ELSE A.t_totalspt 
                    END ELSE A.t_totalspt 
                END"),
            "t_kohirketetapanspt",
            "s_namanotaris" => new Expression("i.s_namanotaris"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),
            "t_nopbphtbsppt",
            "fr_tervalidasidua",
            "fr_validasidua" => new Expression("b.fr_validasidua"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "p_grandtotalnjop" => new Expression("f.p_grandtotalnjop"),
            "p_grandtotalnjop_aphb" => new Expression("f.p_grandtotalnjop_aphb"),
            "t_nilaitransaksispt",
            "p_nilaitransaksispt" => new Expression("f.p_nilaitransaksispt"),
            "t_alamatwppembeli" => new Expression("b.t_alamatwppembeli"),
            "t_idsptsebelumnya",
            "t_tglverifikasispt_kasubid" => new Expression("e.t_tglverifikasispt_kasubid"),
            "t_pejabatverifikasispt_kasubid" => new Expression("e.t_pejabatverifikasispt_kasubid"),
            "t_statusspt_kasubid" => new Expression("e.t_statusspt_kasubid"),
            "t_keterangan_kasubid" => new Expression("e.t_keterangan_kasubid"),
            "t_tglverifikasispt_kabid" => new Expression("e.t_tglverifikasispt_kabid"),
            "t_pejabatverifikasispt_kabid" => new Expression("e.t_pejabatverifikasispt_kabid"),
            "t_statusspt_kabid" => new Expression("e.t_statusspt_kabid"),
            "t_keterangan_kabid" => new Expression("e.t_keterangan_kabid"),
            "t_tgljatuhtempokodebayarspt" => new Expression("e.t_tgljatuhtempokodebayarspt"),
            "t_pejabatverifikasispt" => new Expression("e.t_pejabatverifikasispt"),
            "t_waktuawaldaftar",
            "t_waktuawalproses" => new Expression("e.t_waktuawalproses"),
            // "waktu_respon" => new Expression("(t_waktuawalproses::TIMESTAMP - t_waktuawaldaftar::TIMESTAMP)"),
            "t_status_esignature" => new Expression("e.t_status_esignature"),
            "s_namapejabat" => new Expression("l.s_namapejabat")
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(array("b" => "t_detailsptbphtb"), new Expression("b.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("c" => "s_jenistransaksi"), new Expression("c.s_idjenistransaksi = a.t_idjenistransaksi "), array(), "LEFT");
        $select->join(array("e" => "t_pembayaranspt"), new Expression("e.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("f" => "t_pemeriksaan"), new Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"), array(), "LEFT");
        $select->join(array("h" => "s_users"), new Expression("h.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("i" => "s_notaris"), new Expression("(i.s_idnotaris) :: TEXT = ( h.s_idpejabat_idnotaris ) :: TEXT"), array(), "LEFT");
        $select->join(array("k" => "s_users"), new Expression("k.s_iduser = e.t_pejabatverifikasispt"), array(), "LEFT");
        $select->join(array("l" => "s_pejabat"), new Expression("(l.s_idpejabat) :: TEXT = ( k.s_idpejabat_idnotaris ) :: TEXT"), array(), "LEFT");
        $select->join(array("j" => "fr_count_s_persyaratan"), new Expression("a.t_idjenistransaksi = j.s_idjenistransaksi"), array(), "LEFT");
        // echo $select->getSqlString();exit();
        if($string != null){
            return $select->getSqlString();
        }else{
            return $select;
        }
    }

    public function jml_data_blm_validasiberkas() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("t_spt");
        $select->columns(
            array("t_idspt")
        );
        $select->join(
            "t_pembayaranspt",
            new Expression("t_pembayaranspt.t_idspt = t_spt.t_idspt"),
            array("t_tglverifikasispt"),
            "left"
        );
        $where = new \Zend\Db\Sql\Where();
        $where->literal("t_pembayaranspt.t_tglverifikasispt IS NULL");
        $select->where($where);
        $select->getSqlString();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

}
