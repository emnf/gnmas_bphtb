<?php

namespace Bphtb\Model\Verifikasi;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Bphtb\Model\Setting\PemdaTable;

class VerifikasiESignatureTable extends AbstractTableGateway
{

    protected $table = 't_pembayaranspt';
    protected $PemdaTable;

    public function __construct(Adapter $adapter)
    {
        $this->PemdaTable = new PemdaTable($adapter);
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new VerifikasiESignatureBase());
        $this->initialize();
    }

    //================= datagrid verifikasi
    public function getjumlahdata($sTable, $count, $sWhere)
    {
        $sql = "SELECT " . $count . " FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }

    
    public function fr_pembayaran_v5($string = null){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt" => new Expression("a.t_idspt"),
            "jml_syarat_input" => new Expression("(length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) "),
            "jml_syarat_validasi" => new Expression("(length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_sebenarnya" => new Expression("n.jmlsyarat"),
            "status_pendaftaran" => new Expression("(CASE
            WHEN ((length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            ELSE 2
        END)"),
            "status_validasi" => new Expression("(CASE
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL) THEN 3
            ELSE 2
        END)"),
            "t_kohirspt" => new Expression("a.t_kohirspt"),
            "s_idjenistransaksi" => new Expression("c.s_idjenistransaksi"),
            "t_periodespt" => new Expression("a.t_periodespt"),
            "t_tglprosesspt" => new Expression("a.t_tglprosesspt"),
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_statusbayarspt" => new Expression("(CASE
            WHEN ((e.t_statusbayarspt)::text = 'true'::text) THEN 'TRUE'::text
            ELSE 'FALSE'::text
        END )"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "t_persyaratan" => new Expression("a.t_persyaratan"),
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_idjenistransaksi" => new Expression("a.t_idjenistransaksi"),
            "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "t_ketetapanspt" => new Expression("e.t_ketetapanspt"),
            "t_ketetapands" => new Expression("i.t_ketetapands"),
            "t_kohirds" => new Expression("i.t_kohirds"),
            "notarisds" => new Expression("k.s_namanotaris"),
            "t_periodepembayaran" => new Expression("e.t_periodepembayaran"),
            "t_tanggalpembayaran" => new Expression("e.t_tanggalpembayaran"),
            "t_nilaipembayaranspt" => new Expression("e.t_nilaipembayaranspt"),
            "t_idnotarisspt" => new Expression("a.t_idnotarisspt"),
            "t_totalspt" => new Expression("a.t_totalspt"),
            "p_totalspt" => new Expression("f.p_totalspt"),
            "jml_pajak_v1" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_totalspt
            ELSE a.t_totalspt
        END)"),
            "jml_pajak_v2" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN
            CASE
                WHEN (a.t_totalspt < f.p_totalspt) THEN f.p_totalspt
                ELSE a.t_totalspt
            END
            ELSE a.t_totalspt
        END)"),
            "s_idpejabat_idnotaris" => new Expression("g.s_idpejabat_idnotaris"),
            "s_namanotaris" => new Expression("h.s_namanotaris"),
            "t_noajbbaru" => new Expression("b.t_noajbbaru"),
            "t_tglajbbaru" => new Expression("b.t_tglajbbaru"),
            "t_iddetailsptbphtb" => new Expression("b.t_iddetailsptbphtb"),
            "t_luastanahbpn" => new Expression("b.t_luastanahbpn"),
            "t_luasbangunanbpn" => new Expression("b.t_luasbangunanbpn"),
            "p_luastanah" => new Expression("f.p_luastanah"),
            "p_luasbangunan" => new Expression("f.p_luasbangunan"),
            "t_luastanah" => new Expression("b.t_luastanah"),
            "t_luasbangunan" => new Expression("b.t_luasbangunan"),
            "luas_tanah" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luastanah
            ELSE b.t_luastanah
        END)"),
            "luas_bangunan" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luasbangunan
            ELSE b.t_luasbangunan
        END)"),
            "t_tglsertifikatbaru" => new Expression("b.t_tglsertifikatbaru"), 
            "t_nosertifikatbaru" => new Expression("b.t_nosertifikatbaru"),
            "t_kohirketetapanspt" => new Expression("a.t_kohirketetapanspt"),
            "t_nopbphtbsppt" => new Expression("a.t_nopbphtbsppt"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "t_nilaitransaksispt" => new Expression("a.t_nilaitransaksispt"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),
            "fr_luas_tanah_bpn" => new Expression("b.fr_luas_tanah_bpn"),
            "fr_luas_bangunan_bpn" => new Expression("b.fr_luas_bangunan_bpn"),
            "fr_tervalidasidua" => new Expression("a.fr_tervalidasidua"),
            "fr_validasidua" => new Expression("b.fr_validasidua"),
            "p_grandtotalnjop" => new Expression("f.p_grandtotalnjop"),
            "p_grandtotalnjop_aphb" => new Expression("f.p_grandtotalnjop_aphb"),
            "p_nilaitransaksispt" => new Expression("f.p_nilaitransaksispt"),
            "t_alamatwppembeli" => new Expression("b.t_alamatwppembeli"),
            "t_nikwppembeli" => new Expression("b.t_nikwppembeli"),
            "t_tarif_pembagian_aphb_kali" => new Expression("a.t_tarif_pembagian_aphb_kali"),
            "t_tarif_pembagian_aphb_bagi" => new Expression("a.t_tarif_pembagian_aphb_bagi"),
            "t_grandtotalnjop_aphb" => new Expression("b.t_grandtotalnjop_aphb"),
            "t_idsptsebelumnya" => new Expression("a.t_idsptsebelumnya"),
            "t_alamatwppenjual" => new Expression("b.t_alamatwppenjual"),
            "t_keteranganvalidasi" => new Expression("e.t_keteranganvalidasi"),
            "t_pejabatverifikasispt" => new Expression("e.t_pejabatverifikasispt"),
            "t_idsptsebelumnya" => new Expression("a.t_idsptsebelumnya"),
            "t_status_esignature"
        ));
        $select->from(array("e" => "t_pembayaranspt"));
        $select->join(array("a" => "t_spt"), new Expression("e.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("b" => "t_detailsptbphtb"), new Expression("b.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("c" => "s_jenistransaksi"), new Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"), array(), "LEFT");
        $select->join(array("f" => "t_pemeriksaan"), new Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"), array(), "LEFT");
        $select->join(array("i" => "t_dendasanksinotaris"), new Expression("i.t_idds = e.t_idds"), array(), "LEFT");
        $select->join(array("g" => "s_users"), new Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("j" => "s_users"), new Expression("j.s_iduser = i.t_idnotaris"), array(), "LEFT");
        $select->join(array("k" => "s_notaris"), new Expression("(k.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("n" => "fr_count_s_persyaratan"), new Expression("a.t_idjenistransaksi = n.s_idjenistransaksi"), array(), "LEFT");
        
        if($string != null && $string == 1){
            return $select->getSqlString();
        }else{
            return $select;
        }

        // echo $select;die;
    }

    public function semuadatapembayaran($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    }else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }

        $status_esignature = "AND t_status_esignature IS NULL";
        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_namajenistransaksi') {
                        $aFilteringRules[] = " s_idjenistransaksi::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    }elseif ($aColumns[$i] == 't_status_esignature') {
                        if($input->getPost('sSearch_' . $i) == '1'){
                            $aFilteringRules[] = "t_status_esignature IS NULL";
                        }elseif($input->getPost('sSearch_' . $i) == '2'){
                            $aFilteringRules[] = "t_status_esignature = '1'";
                        }elseif($input->getPost('sSearch_' . $i) == '3'){
                            $status_esignature = "";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }

                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND t_tanggalpembayaran IS NOT NULL";
        } else {
            $sWhere = " WHERE t_tanggalpembayaran IS NOT NULL ".$status_esignature;
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }
        
        if($sTable == "fr_pembayaran_v5"){
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM (".$this->fr_pembayaran_v5(1).") as fr_pembayaran_v5 ". $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
            $sTable = "(".$this->fr_pembayaran_v5(1).") as fr_pembayaran_v5 ";
        }else{
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
        }
       
        // die($sql);

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();



        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            } else {
                $status_bayar = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }

            $validasiesignature = '<a href="javascript:void(0)" class="btn btn-warning btn-sm" onclick="openVerifikasiESignature(' . $aRow['t_idspt'] . ')"><i class="fa fa-fw fa-pencil"></i> E-signature</a>';
            if($aRow['t_status_esignature'] != 1){
                $status_esignature = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
                $cetaksspd = '';
            }else{
                $status_esignature = '<a class="btn btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
                $cetaksspd = '<a class="btn btn-info" href="javascript:void(0)" onclick="cetaksspd_esignature('.$aRow['t_kodebayarbanksppt'].',1)"><i class="fa fa-fw fa-print"></i> SSPD LQ</a>'.
                '<a class="btn btn-info" href="javascript:void(0)" onclick="cetaksspd_esignature('.$aRow['t_kodebayarbanksppt'].',2)"><i class="fa fa-fw fa-print"></i> SSPD</a>';
            }

            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $t_kohirspt . "</center>",
                "<center>" . $novalidasi . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglverifikasispt'])) . "</span>",
                $aRow['t_namawppembeli'],
                "<span style='float:right;'>" . number_format($aRow['t_nilaipembayaranspt'], 0, ',', '.') . "</span>",
                "<center>" . $aRow['t_kodebayarbanksppt'] . "</center>",
                "<center>" . $status_bayar . "</center>",
                "<center>" . $status_esignature . "</center>",
                "<center>" . $validasiesignature." ".$cetaksspd . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function updateESignature($idspt=null, $iduser=null, $code=null, $messaage=null)
    {
        $sql = new Sql($this->adapter);
        
        // UPDATE TABLE PEMBAYARAN
        if($code == 1){
            $data = array(
                't_status_esignature' => '1',
                't_waktuproses_esignature' => date('Y-m-d H:i:s'),
                't_pejabat_esignature' => $iduser
            );

            $update = $sql->update($this->table);
            $update->set($data);
            $update->where(array('t_idspt' => (int) $idspt));
            $sql->prepareStatementForSqlObject($update)->execute();
        }

        // INSERT LOG
        $data = array(
            'timestamp' => date("Y-m-d H:i:s"),
            'message' => $messaage,
            's_iduser' => $iduser,
            't_idspt' => $idspt
        );
        $insert = $sql->insert('passphrase_log');
        $insert->values($data);
        $res = $sql->prepareStatementForSqlObject($insert)->execute();

        return $res;
    }

}
