<?php

namespace Bphtb\Model\Pendataan;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Where;

//PHP MAILER ================
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Zend\Db\Sql\Expression;
use Bphtb\Model\Verifikasi\VerifikasiSPTTable;
use Zend\Debug\Debug;

//PHP MAILER ================

class SSPDBphtbTable extends AbstractTableGateway
{

    protected $table = 't_detailsptbphtb';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new SSPDBphtbBase());
        $this->initialize();
    }

    //==================== datagrid daftar pendaftaran
    public function getjumlahdata($sTable, $count, $sWhere)
    {
        $sql = "SELECT " . $count . " FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }

    public function semuadatapendaftaran($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {
        $VerifikasiSPTTable = new VerifikasiSPTTable($this->adapter);

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_pendaftaran') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        if ($s_tipe_pejabat == 2) {
            $wherelogin_atas = ' AND t_idnotarisspt = ' . $s_iduser . '';
            $wherelogin_default = ' WHERE t_idnotarisspt = ' . $s_iduser . '';
        } else {
            $wherelogin_atas = ' ';
            $wherelogin_default = ' ';
        }

        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " " . $wherelogin_atas . "";
        } else {
            $sWhere = " " . $wherelogin_default . " ";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }

        if ($sTable = 'fr_pendaftaran_v5') {
            $fr_pendaftaran_v5 = $VerifikasiSPTTable->fr_pendaftaran_v5(1);
            $sTable = "(" . $fr_pendaftaran_v5 . ") AS fr_pendaftaran_v5 ";
        }

        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 

        // echo $sql;exit();

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();

        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );

        //var_dump($rResult); exit();

        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            if (!empty($aRow['p_idpemeriksaan'])) {
                $jmlpajak = "<span style='float:right;'>" . number_format($aRow['p_totalspt'], 0, ',', '.') . "</span>";
            } else {
                $jmlpajak = "<span style='float:right;'>" . number_format($aRow['t_totalspt'], 0, ',', '.') . "</span>";
            }



            $lihat = '<a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';

            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syaratlengkap.png">';
            } else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/syarattidaklengkap.png">';
            }

            //============ bisa cetak terus walaupun belum divalidasi

            if ($aRow['fr_validasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-check-square-o"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 1) {
                $warnatr = '<span style="background-color:red;color: #fff;"> &nbsp; <i class="fa fa-fw fa-minus-circle"></i> </span> &nbsp;';
            } elseif ($aRow['fr_tervalidasidua'] == 2) {
                $warnatr = '<span style="background-color:blue;color: #fff;"> &nbsp; <i class="fa fa-fw fa-plus-circle"></i> </span> &nbsp;';
            } else {
                $warnatr = '';
            }



            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/tervalidasi.png">';
                $cetaksspd = '<a href="#" class="bg-green" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
                $edit = '';
                $hapus = '';
                //if ($aRow['t_statusbayarspt'] == true) {
                //   $lihat = '<a href="pendataan_sspd/viewdata?t_idspt='.$aRow['t_idspt'].'">Lihat</a>'; 
                //} else {
                //    $lihat = '<a href="pendataan_sspd/viewdata?t_idspt='.$aRow['t_idspt'].'">Lihat</a> </li>'; //<a href='pendataan_sspd/edit?t_idspt= $aRow[t_idspt]' class='btn btn-warning btn-sm btn-flat' style='width:50px'>Edit</a>
                //}

                if (!empty($aRow['t_idsptsebelumnya'])) {
                    $cetaksspd = '<a href="#" class="bg-green" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ', ' . $aRow['t_idsptsebelumnya'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
                } else {
                    $cetaksspd = '<a href="#" class="bg-green" onclick="openCetakSSPD(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-print"></i> SSPD</a>';
                    // $cetaksspd = "";
                }
            } else {
                $cetaksspd = "";
                if (empty($aRow['t_verifikasispt'])) {
                    $status_verifikasi = '<img title="Belum di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/belumdivalidasi.png">';
                    if (($aRow['fr_validasidua'] == 1) || ($aRow['fr_tervalidasidua'] == 1) || ($aRow['fr_tervalidasidua'] == 2)) {

                        if (($session['s_tipe_pejabat'] == 1) || ($session['s_tipe_pejabat'] == 0)) {
                            $edit = ' <a href="pendataan_sspd/edit?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                        } else {
                            $edit = '';
                        }
                    } else {
                        $edit = ' <a href="pendataan_sspd/edit?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                    }

                    $hapus = '<a href="#" onclick="hapus(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
                } else {

                    //status verifikasi
                    if ($aRow['status_validasi'] == 5) { //verifikasi berkas lengkap
                        $status_verifikasi = '<img title="Berkas Sudah Diverifikasi" width="20" height="20" src="' . $cekurl . '/public/img/ok.png">';
                    } else if ($aRow['status_validasi'] == 4) { //verifikasi kasubid
                        $status_verifikasi = '<img title="Telah DIverifikasi Kasubid" width="20" height="20" src="' . $cekurl . '/public/img/icon/verifikasi1.png">';
                    } else {
                        $status_verifikasi = '<img title="Syarat Validasi Tidak Lengkap" width="20" height="20" src="' . $cekurl . '/public/img/validasi_tidaklengkap.png">';
                    }

                    if ($aRow['t_inputbpn'] == true) {
                        //$lihat = '<a href="pendataan_sspd/viewdata?t_idspt='.$aRow['t_idspt'].'">Lihat</a>';
                        $cetaksspd = '';
                        $edit = '';
                        $hapus = '';
                    } else {
                        //$lihat = '<a href="pendataan_sspd/viewdata?t_idspt='.$aRow['t_idspt'].'">Lihat</a>';
                        $cetaksspd = '';
                        if (($aRow['fr_validasidua'] == 1) || ($aRow['fr_tervalidasidua'] == 1) || ($aRow['fr_tervalidasidua'] == 2)) {

                            if (($session['s_tipe_pejabat'] == 1) || ($session['s_tipe_pejabat'] == 0)) {
                                $edit = ' <a href="pendataan_sspd/edit?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                            } else {
                                $edit = '';
                            }
                        } else {
                            $edit = ' <a href="pendataan_sspd/edit?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-edit"></i> Edit</a>';
                        }
                    }
                }
            }


            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn_fr btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            } else {
                $status_bayar = '<a class="btn_fr btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }

            if (($session['s_namauserrole'] == "Administrator")) {
                $admin_hapus = '<a style="background-color:red;color:#fff;" href="#" onclick="hapusall(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus Semua</a>';
            } else {
                $admin_hapus = '';
            }

            $uploadfile = '<a href="pendataan_sspd/uploadfilesyarat?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-upload"></i> Upload File</a>';

            if ($aRow['t_status_esignature'] == 1) {
                $cetaksspd_signature = '<a href="javascript:void(0)" class="bg-green" onclick="openCetakSSPDESignature(' . $aRow['t_kodebayarbanksppt'] . ');"><i class="fa fa-fw fa-print"></i> SSPD E-Signature</a>';
            } else {
                $cetaksspd_signature = '';
            }
            // Debug::dump($aRow);
            // exit;
            // if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {

            if (($aRow['t_statusbayarspt']) && ($aRow['status_validasi'] == 1)) {
                $ntpd = '<a href="pendataan_sspd/cetakntpd?&action=cetakntpd&t_idspt=' . $aRow['t_idspt'] . '" target="_blank" class="bg-blue"><i class="fa fa-fw fa-print"></i> NTPD</a>';
            } else {
                $ntpd = '';
            }

            $btn = '
                <div class="dropdown">
                    <button onclick="myFunction(' . $aRow['t_idspt'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $aRow['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                      
                                                  ' . $cetaksspd . '  
                                                  ' . $ntpd . '  
                                                  ' . $cetaksspd_signature . '  
                                                  ' . $lihat . '
                                                  ' . $edit . '    
                                                  ' . $hapus . ' 
                                                  ' . $admin_hapus . ' 
                                                  ' . $uploadfile . '
                                                      
                    </div>
                  </div>';

            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pendataan_sspd/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }

            $s_tipe_pejabat = $session['s_tipe_pejabat'];
            if ($s_tipe_pejabat == 2) {
                $row = array(
                    "<center>" . $no . "</center>",
                    "<center>" . $warnatr . " " . $t_kohirspt . "</center>",
                    "<center>" . $novalidasi . "</center>",
                    "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                    "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</span>",
                    $aRow['t_nopbphtbsppt'],
                    $aRow['t_namawppembeli'],
                    "<span style='float:right;'>" . number_format($aRow['jml_pajak_v1'], 0, ',', '.') . "</span>",
                    "<center>" . $status_bayar . "</center>",
                    "<center>" . $status_pendaftaran . "</center>",
                    "<center>" . $status_verifikasi . "</center>",
                    "<center>" . $btn . "</center>"
                );
            } else {
                $row = array(
                    "<center>" . $no . "</center>",
                    "<center>" . $warnatr . " " . $t_kohirspt . "</center>",
                    "<center>" . $novalidasi . "</center>",
                    "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                    $aRow['s_namanotaris'],
                    "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</span>",
                    $aRow['t_nopbphtbsppt'],
                    $aRow['t_namawppembeli'],
                    "<span style='float:right;'>" . number_format($aRow['jml_pajak_v1'], 0, ',', '.') . "</span>",
                    "<center>" . $status_bayar . "</center>",
                    "<center>" . $status_pendaftaran . "</center>",
                    "<center>" . $status_verifikasi . "</center>",
                    "<center>" . $btn . "</center>"
                );
            }
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    //==================== end datagrid daftar pendaftaran


    public function getDataId($id)
    {
        $rowset = $this->select(function (Select $select) use ($id) {
            $select->where(array(
                't_spt.t_idspt' => $id
            ));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
            $select->join('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi = t_spt.t_idjenistransaksi');
            $select->join('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah = t_spt.t_idjenishaktanah');
            // Build the SQL string for debugging
            // $sql = $this->getSql();
            // $sqlString = $sql->buildSqlString($select);
            // echo $sqlString;
            // exit;
        });


        $row = $rowset->current();

        return $row;
    }

    public function getDataId_all($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("a" => "view_sspd_pembayaran"));
        $select->join(
            array("b" => "t_spt"),
            new \Zend\Db\Sql\Expression("a.t_idspt = b.t_idspt"),
            array(
                "t_jenispendaftaran",
                "t_approve_pengurangpembebas",
                "t_keterangan"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "t_pembebasan"),
            new \Zend\Db\Sql\Expression("a.t_idspt = c.t_idsptpembebasan"),
            array(
                "t_totalsptsebelumnya_pembebasan" => new \Zend\Db\Sql\Expression("c.t_totalsptsebelumnya"),
                "t_persenpembebasan",
                "t_pembebasan",
                "t_totalpajak_pembebasan" => new \Zend\Db\Sql\Expression("c.t_totalpajak")
            ),
            "LEFT"
        );
        $select->join(
            array("d" => "t_pengurangan"),
            new \Zend\Db\Sql\Expression("a.t_idspt = d.t_idsptpengurangan"),
            array(
                "t_totalsptsebelumnya_pengurangan" => new \Zend\Db\Sql\Expression("d.t_totalsptsebelumnya"),
                "t_persenpengurangan",
                "t_pengurangan",
                "t_totalpajak_pengurangan" => new \Zend\Db\Sql\Expression("d.t_totalpajak")
            ),
            "LEFT"
        );
        $select->join(
            array("e" => "t_pembayaranspt"),
            new \Zend\Db\Sql\Expression("a.t_idspt = e.t_idspt"),
            array(
                "t_keteranganvalidasi"
            ),
            "LEFT"
        );
        $where = new Where();
        $where->equalTo('a.t_idspt', (int)$id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    /*
      public function getDataId($id)
      {
      $rowset = $this->select(function (Select $select) use($id) {
      $select->where(array(
      't_spt.t_idspt' => $id
      ));
      $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
      });
      $row = $rowset->current();
      return $row;
      } */

    public function getGridCount(SSPDBphtbBase $base, $id_notaris, $userRole = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($userRole == "Notaris") {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");

        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        //        if ($base->t_noajbbaru != 'undefined')
        //            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        //        if ($base->t_tglajbbaru != 'undefined')
        //            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridCountPendataanSSPD(SSPDBphtbBase $base, $id_notaris, $s_tipe_pejabat = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($s_tipe_pejabat == 2) {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");

        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        //        if ($base->t_noajbbaru != 'undefined')
        //            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        //        if ($base->t_tglajbbaru != 'undefined')
        //            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function databelumverifikasiBphtb($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        if ($query) {
            $where = " WHERE $qtype::text LIKE '%" . $query . "%' and t_ketetapanspt is null";
        } else {
            $where = " WHERE t_ketetapanspt is null";
        }
        $sort = "ORDER BY $sortname $sortorder";

        //$limit = "offset $start limit $rp ";
        $limit = "limit $rp offset $start";



        $sql = "select * from view_sspd_blm_validasi " . @$where . " " . @$sort . " " . @$limit . "  "; // date_part('year', s_tanggaltarifbphtb) = ".date('Y')."";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getGridDataPendataanSSPD(SSPDBphtbBase $base, $offset, $id_notaris, $s_tipe_pejabat = null)
    {
        // Tambahkan field s_iduser pada view_sspd_semua_pembayaran dari join table user
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($s_tipe_pejabat == 2) {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int)$base->t_kohirspt;
        $tanggal = date('Y-m-d', strtotime($base->t_tglprosesspt));
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", $tanggal);
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");

        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        //        if ($base->t_noajbbaru != 'undefined')
        //            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        //        if ($base->t_tglajbbaru != 'undefined')
        //            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->limit($base->rows = (int)$base->rows);
        $select->offset($offset = (int)$offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridData(SSPDBphtbBase $base, $offset, $id_notaris, $userRole = null)
    {
        // Tambahkan field s_iduser pada view_sspd_semua_pembayaran dari join table user
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($userRole == "Notaris") {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $kohir = (int)$base->t_kohirspt;
        $tanggal = date('Y-m-d', strtotime($base->t_tglprosesspt));
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", $tanggal);
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");

        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        //        if ($base->t_noajbbaru != 'undefined')
        //            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        //        if ($base->t_tglajbbaru != 'undefined')
        //            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->limit($base->rows = (int)$base->rows);
        $select->offset($offset = (int)$offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountBlmVerifikasi($query, $qtype, $bulan = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_blm_validasi');
        $where = new Where();
        if ($qtype && $bulan == null) {
            $where->literal("$qtype::text LIKE '%$query%'");
        }
        if ($bulan != null) {
            $where->literal("EXTRACT(MONTH from t_tglprosesspt) = '" . $bulan . "'");
        }
        $where->literal('t_ketetapanspt is null');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataBlmVerifikasi($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_blm_validasi');
        $where = new Where();
        $select->order($sortname, $sortorder);
        if ($query) {
            $select->where("$qtype::text LIKE '%$query%'");
        }
        $where->literal('t_ketetapanspt is null');
        $select->where($where);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountPembayaran($query, $qtype)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_sudah_tervalidasi');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%' and t_tanggalpembayaran IS NULL");
        } else {
            $where->literal("t_tanggalpembayaran IS NULL");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataPembayaran($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_sudah_tervalidasi');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%' and t_tanggalpembayaran IS NULL");
        } else {
            $where->isNull("t_tanggalpembayaran");
        }
        $select->where($where);
        $select->order($sortname, $sortorder);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountDS($query, $qtype)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_pembayarandenda');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%'");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataDS($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_pembayarandenda');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%'");
        }
        $select->where($where);
        $select->order($sortname, $sortorder);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getGridCountVerifikasi($query, $qtype)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%' and t_tglverifikasispt IS NULL");
        } else {
            $where->literal("t_tglverifikasispt IS NULL");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataVerifikasi($sortname, $sortorder, $query, $qtype, $start, $rp)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();

        if ($query) {
            $where->literal("$qtype::text LIKE '%$query%' and t_tglverifikasispt IS NULL and t_ketetapanspt=1");
        } else {
            $where->literal("t_tglverifikasispt IS NULL and t_ketetapanspt=1");
        }
        $select->where($where);
        $select->order($sortname, $sortorder);
        $select->limit($rp);
        $select->offset($start);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function savedatadetail(SSPDBphtbBase $sspddetail, $idspt = null, $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb)
    {
        $id = (int)$sspddetail->t_iddetailsptbphtb;
        if (!empty($sspddetail->t_tglajbbaru)) {
            $tglajbbaru = date("Y-m-d", strtotime($sspddetail->t_tglajbbaru));
            $noajbaru = $sspddetail->t_noajbbaru;
        } else {
            $tglajbbaru = null;
            $noajbaru = null;
        }


        //if(!empty($dt->t_grandtotalnjop_aphb)){
        //    $t_grandtotalnjop_aphb = $dt->t_grandtotalnjop_aphb;
        //}else{
        //     $t_grandtotalnjop_aphb = 0;
        //}

        if (!empty($sspddetail->t_njoptanah_sismiop)) {
            $t_njoptanah_sismiop = $sspddetail->t_njoptanah_sismiop;
        } else {
            $t_njoptanah_sismiop = 0;
        }

        if (!empty($sspddetail->t_njopbangunan_sismiop)) {
            $t_njopbangunan_sismiop = $sspddetail->t_njopbangunan_sismiop;
        } else {
            $t_njopbangunan_sismiop = 0;
        }

        if (!empty($sspddetail->t_luastanah_sismiop)) {
            $t_luastanah_sismiop = $sspddetail->t_luastanah_sismiop;
        } else {
            $t_luastanah_sismiop = 0;
        }

        if (!empty($sspddetail->t_luasbangunan_sismiop)) {
            $t_luasbangunan_sismiop = $sspddetail->t_luasbangunan_sismiop;
        } else {
            $t_luasbangunan_sismiop = 0;
        }

        if ($t_njopbangunan == "NaN") {
            $t_njopbangunan = 0;
        }

        if ($t_njopbangunan_sismiop == "NaN") {
            $t_njopbangunan_sismiop = 0;
        }

        $data = array(
            't_idspt' => $idspt,
            // Data Wajib Pajak
            't_namawppembeli' => $sspddetail->t_namawppembeli,
            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
            // Data Objek Pajak
            't_namasppt' => $sspddetail->t_namasppt,
            't_alamatop' => $sspddetail->t_alamatop,
            't_rtop' => $sspddetail->t_rtop,
            't_rwop' => $sspddetail->t_rwop,
            't_kelurahanop' => $sspddetail->t_kelurahanop,
            't_kecamatanop' => $sspddetail->t_kecamatanop,
            't_kabupatenop' => $sspddetail->t_kabupatenop,
            't_luastanah' => (int)$t_luastanah, //str_ireplace(".", "", $sspddetail->t_luastanah),
            't_njoptanah' => (int)$t_njoptanah, //str_ireplace(".", "", $sspddetail->t_njoptanah),
            't_totalnjoptanah' => (int)$t_totalnjoptanah, //str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
            't_luasbangunan' => (int)$t_luasbangunan, //str_ireplace(".", "", $sspddetail->t_luasbangunan),
            't_njopbangunan' => (int)$t_njopbangunan, //str_ireplace(".", "", $sspddetail->t_njopbangunan),
            't_totalnjopbangunan' => (int)$t_totalnjopbangunan, //str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
            't_grandtotalnjop' => $t_grandtotalnjop, //str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,
            't_ketwaris' => $sspddetail->t_ketwaris,
            't_terbukti' => $sspddetail->t_terbukti,
            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),
            't_namasppt' => $sspddetail->t_namasppt,
            //'t_tglajbbaru' => $tglajbbaru,
            //'t_noajbbaru' => $noajbaru,
            // Data Penjual
            't_namawppenjual' => $sspddetail->t_namawppenjual,
            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
            't_rwwppenjual' => $sspddetail->t_rwwppenjual,
            't_luastanah_sismiop' => $t_luastanah_sismiop, //$sspddetail->t_luastanah_sismiop,
            't_luasbangunan_sismiop' => $t_luasbangunan_sismiop, //$sspddetail->t_luasbangunan_sismiop,
            't_njoptanah_sismiop' => $t_njoptanah_sismiop, //$sspddetail->t_njoptanah_sismiop,
            't_njopbangunan_sismiop' => (int)$t_njopbangunan_sismiop, //$sspddetail->t_njopbangunan_sismiop,
            't_grandtotalnjop_aphb' => $t_grandtotalnjop_aphb, //str_ireplace(".", "", $t_grandtotalnjop_aphb)
        );
        //$datahistlog = array(
        //    'hislog_opr_id' => $session['s_iduser'],
        //    'hislog_opr_user' => $session['s_username'] . "-" . $session['s_jabatan'],
        //    'hislog_opr_nama' => $session['s_username'],
        //    'hislog_time' => date("Y-m-d h:i:s")
        //);
        // Debug::dump($data);
        // exit;
        if ($id == 0) {
            // var_dump($data); die;
            $this->insert($data);
            $rowset = $this->select(array('t_idspt' => $data['t_idspt']));
            $row = $rowset->current();
            //$datahistlog['hislog_idspt'] = $idspt;
            //$datahistlog['hislog_action'] = "SIMPAN DATA PENDAFTARAN - " . $sspddetail->t_namawppembeli . "-" . $sspddetail->t_nikwppembeli;
            //$tabel_histlog = new TableGateway('history_log', $this->adapter);
            //$tabel_histlog->insert($datahistlog);
        } else {
            $data['t_idspt'] = $sspddetail->t_idspt;
            $this->update($data, array(
                't_iddetailsptbphtb' => $sspddetail->t_iddetailsptbphtb
            ));
            $rowset = $this->select(array('t_idspt' => $sspddetail->t_idspt));
            $row = $rowset->current();
            //$datahistlog['hislog_idspt'] = $sspddetail->t_idspt;
            //$datahistlog['hislog_action'] = "UPDATE DATA PENDAFTARAN - " . $sspddetail->t_namawppembeli . "-" . $sspddetail->t_nikwppembeli;
            //$tabel_histlog = new TableGateway('history_log', $this->adapter);
            //$tabel_histlog->insert($datahistlog);
        }

        return $row;
    }

    public function savedatadetail_validasikedua(SSPDBphtbBase $sspddetail, $idspt = null, $input, $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb)
    {
        $id = (int)$sspddetail->t_iddetailsptbphtb;
        if (!empty($sspddetail->t_tglajbbaru)) {
            $tglajbbaru = date("Y-m-d", strtotime($sspddetail->t_tglajbbaru));
            $noajbaru = $sspddetail->t_noajbbaru;
        } else {
            $tglajbbaru = null;
            $noajbaru = null;
        }


        //if(!empty($dt->t_grandtotalnjop_aphb)){
        //    $t_grandtotalnjop_aphb = $dt->t_grandtotalnjop_aphb;
        //}else{
        //     $t_grandtotalnjop_aphb = 0;
        //}

        $data = array(
            't_idspt' => $idspt,
            // Data Wajib Pajak
            't_namawppembeli' => $sspddetail->t_namawppembeli,
            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
            // Data Objek Pajak
            't_namasppt' => $sspddetail->t_namasppt,
            't_alamatop' => $sspddetail->t_alamatop,
            't_rtop' => $sspddetail->t_rtop,
            't_rwop' => $sspddetail->t_rwop,
            't_kelurahanop' => $sspddetail->t_kelurahanop,
            't_kecamatanop' => $sspddetail->t_kecamatanop,
            't_kabupatenop' => $sspddetail->t_kabupatenop,
            't_luastanah' => $t_luastanah, //str_ireplace(".", "", $sspddetail->t_luastanah),
            't_njoptanah' => $t_njoptanah, //str_ireplace(".", "", $sspddetail->t_njoptanah),
            't_totalnjoptanah' => $t_totalnjoptanah, //str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
            't_luasbangunan' => $t_luasbangunan, //str_ireplace(".", "", $sspddetail->t_luasbangunan),
            't_njopbangunan' => $t_njopbangunan, //str_ireplace(".", "", $sspddetail->t_njopbangunan),
            't_totalnjopbangunan' => $t_totalnjopbangunan, //str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
            't_grandtotalnjop' => $t_grandtotalnjop, //str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,
            't_ketwaris' => $sspddetail->t_ketwaris,
            't_terbukti' => $sspddetail->t_terbukti,
            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),
            't_tglajbbaru' => $tglajbbaru,
            't_noajbbaru' => $noajbaru,
            // Data Penjual
            't_namawppenjual' => $sspddetail->t_namawppenjual,
            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
            't_rwwppenjual' => $sspddetail->t_rwwppenjual,
            't_luastanah_sismiop' => $sspddetail->t_luastanah_sismiop,
            't_luasbangunan_sismiop' => $sspddetail->t_luasbangunan_sismiop,
            't_njoptanah_sismiop' => $sspddetail->t_njoptanah_sismiop,
            't_njopbangunan_sismiop' => $sspddetail->t_njopbangunan_sismiop,
            't_grandtotalnjop_aphb' => $t_grandtotalnjop_aphb, //str_ireplace(".", "", $t_grandtotalnjop_aphb)
            'fr_validasidua' => 1
        );

        $this->insert($data);
    }

    //    public function savedatadetail(SSPDBphtbBase $sspddetail, $idspt = null) {
    //        $id = (int) $sspddetail->t_iddetailsptbphtb;
    //        $data = array(
    //            't_idspt' => $idspt,
    //            // Data Wajib Pajak
    //            't_namawppembeli' => $sspddetail->t_namawppembeli,
    //            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
    //            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
    //            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
    //            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
    //            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
    //            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
    //            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
    //            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
    //            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
    //            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
    //            // Data Objek Pajak
    //            't_namasppt' => $sspddetail->t_namasppt,
    //            't_alamatop' => $sspddetail->t_alamatop,
    //            't_rtop' => $sspddetail->t_rtop,
    //            't_rwop' => $sspddetail->t_rwop,
    //            't_kelurahanop' => $sspddetail->t_kelurahanop,
    //            't_kecamatanop' => $sspddetail->t_kecamatanop,
    //            't_kabupatenop' => $sspddetail->t_kabupatenop,
    //            't_luastanah' => str_ireplace(".", "", $sspddetail->t_luastanah),
    //            't_njoptanah' => str_ireplace(".", "", $sspddetail->t_njoptanah),
    //            't_totalnjoptanah' => str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
    //            't_luasbangunan' => str_ireplace(".", "", $sspddetail->t_luasbangunan),
    //            't_njopbangunan' => str_ireplace(".", "", $sspddetail->t_njopbangunan),
    //            't_totalnjopbangunan' => str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
    //            't_grandtotalnjop' => str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
    //            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,
    //            't_ketwaris' => $sspddetail->t_ketwaris,
    //            't_terbukti' => $sspddetail->t_terbukti,
    //            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),
    //            't_namasppt' => $sspddetail->t_namasppt,
    //            // Data Penjual
    //            't_namawppenjual' => $sspddetail->t_namawppenjual,
    //            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
    //            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
    //            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
    //            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
    //            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
    //            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
    //            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
    //            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
    //            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
    //            't_rwwppenjual' => $sspddetail->t_rwwppenjual
    //        );
    //        if ($id == 0) {
    //            $this->insert($data);
    //        } else {
    //            $data['t_idspt'] = $sspddetail->t_idspt;
    //            $this->update($data, array(
    //                't_iddetailsptbphtb' => $sspddetail->t_iddetailsptbphtb
    //            ));
    //        }
    //    }

    public function savedatadetailbpn(SSPDBphtbBase $sspddetail, $idspt, $t_luastanahbpn, $t_luasbangunanbpn)
    {
        $data = array(
            't_idspt' => $idspt,
            // Data Wajib Pajak
            't_namawppembeli' => $sspddetail->t_namawppembeli,
            't_nikwppembeli' => $sspddetail->t_nikwppembeli,
            't_npwpwppembeli' => $sspddetail->t_npwpwppembeli,
            't_alamatwppembeli' => $sspddetail->t_alamatwppembeli,
            't_rtwppembeli' => $sspddetail->t_rtwppembeli,
            't_rwwppembeli' => $sspddetail->t_rwwppembeli,
            't_kecamatanwppembeli' => $sspddetail->t_kecamatanwppembeli,
            't_kelurahanwppembeli' => $sspddetail->t_kelurahanwppembeli,
            't_kabkotawppembeli' => $sspddetail->t_kabkotawppembeli,
            't_telponwppembeli' => $sspddetail->t_telponwppembeli,
            't_kodeposwppembeli' => $sspddetail->t_kodeposwppembeli,
            // Data Objek Pajak
            't_namasppt' => $sspddetail->t_namasppt,
            't_alamatop' => $sspddetail->t_alamatop,
            't_rtop' => $sspddetail->t_rtop,
            't_rwop' => $sspddetail->t_rwop,
            't_kelurahanop' => $sspddetail->t_kelurahanop,
            't_kecamatanop' => $sspddetail->t_kecamatanop,
            't_kabupatenop' => $sspddetail->t_kabupatenop,
            't_luastanah' => $t_luastanahbpn,
            't_njoptanah' => str_ireplace(".", "", $sspddetail->t_njoptanah),
            't_totalnjoptanah' => str_ireplace(".", "", $sspddetail->t_totalnjoptanah),
            't_luasbangunan' => $t_luasbangunanbpn,
            't_njopbangunan' => str_ireplace(".", "", $sspddetail->t_njopbangunan),
            't_totalnjopbangunan' => str_ireplace(".", "", $sspddetail->t_totalnjopbangunan),
            't_grandtotalnjop' => str_ireplace(".", "", $sspddetail->t_grandtotalnjop),
            't_nosertifikathaktanah' => $sspddetail->t_nosertifikathaktanah,
            't_ketwaris' => $sspddetail->t_ketwaris,
            't_terbukti' => $sspddetail->t_terbukti,
            't_tglajb' => date('Y-m-d', strtotime($sspddetail->t_tglajb)),
            't_namasppt' => $sspddetail->t_namasppt,
            // Data Penjual
            't_namawppenjual' => $sspddetail->t_namawppenjual,
            't_nikwppenjual' => $sspddetail->t_nikwppenjual,
            't_npwpwppenjual' => $sspddetail->t_npwpwppenjual,
            't_alamatwppenjual' => $sspddetail->t_alamatwppenjual,
            't_kelurahanwppenjual' => $sspddetail->t_kelurahanwppenjual,
            't_kecamatanwppenjual' => $sspddetail->t_kecamatanwppenjual,
            't_kabkotawppenjual' => $sspddetail->t_kabkotawppenjual,
            't_telponwppenjual' => $sspddetail->t_telponwppenjual,
            't_kodeposwppenjual' => $sspddetail->t_kodeposwppenjual,
            't_rtwppenjual' => $sspddetail->t_rtwppenjual,
            't_rwwppenjual' => $sspddetail->t_rwwppenjual,
            't_inputbpn' => true
        );
        $this->insert($data);
    }

    public function savedatatglajbbaru(SSPDBphtbBase $sspddetail)
    {
        $data = array(
            't_tglajbbaru' => date('Y-m-d', strtotime($sspddetail->t_tglajbbaru)),
            't_noajbbaru' => $sspddetail->t_noajbbaru
        );
        $this->update($data, array(
            't_iddetailsptbphtb' => $sspddetail->t_iddetailsptbphtb
        ));
    }

    public function getPendataanSspdBphtb(SSPDBphtbBase $ss)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_sspd_pembayaran");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idspt", $ss->t_idspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function getPengurangan($idspt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_pengurangan");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idspt", $idspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function getPendataanDS($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_pembayarandenda");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idds", (int)$id);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function getSkpdkbBphtb(SSPDBphtbBase $ss)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("view_sspd_validasi");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idspt", $ss->t_idspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function hapusData(SSPDBphtbBase $kb)
    {
        $this->delete(array(
            't_idspt' => $kb->t_idspt
        ));
    }

    public function hapusall($idspt, $session = null)
    {


        $dataspt = "select t_idsptsebelumnya from t_spt where t_idspt = " . $idspt . "";
        $id_spt_sebelumnya = $this->adapter->query($dataspt)->execute()->current();

        if (!empty($id_spt_sebelumnya['t_idsptsebelumnya'])) {

            $update = "update t_spt set fr_tervalidasidua =3 where t_idspt= " . $id_spt_sebelumnya['t_idsptsebelumnya'] . "";
            $this->adapter->query($update)->execute();
        } else {
        }

        $sql = new Sql($this->adapter);

        //ambil data t_spt
        $select = $sql->select();
        $select->from("t_spt");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idspt", (int)$idspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $data = $state->execute()->current();
        $data['t_iduserhapus'] = $session['s_iduser'];
        if ($data['t_approve_pengurangpembebas'] == false) {
            $t_approve_pengurangpembebas = 0;
        } else if ($data['t_approve_pengurangpembebas'] == true) {
            $t_approve_pengurangpembebas = 1;
        }
        $data['t_approve_pengurangpembebas'] = $t_approve_pengurangpembebas;
        if ($data['t_keterangan'] == "") {
            $data['t_keterangan'] = null;
        }

        //simpan data t_spt_hapus
        $insert = $sql->insert('t_spt_hapus');
        $insert->values($data);
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);

        // hapus file t_spt
        $input = "DELETE FROM t_spt where t_idspt= " . $idspt . "";
        $this->adapter->query($input)->execute();

        //ambil data t_detailsptbphtb
        $select = $sql->select();
        $select->from("t_detailsptbphtb");
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("t_idspt", (int)$idspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $data = $state->execute()->current();
        if (!empty($data)) {
            $data['t_iduserhapus'] = $session['s_iduser'];

            //simpan data t_detailsptbphtb_hapus
            $insert = $sql->insert('t_detailsptbphtb_hapus');
            $insert->values($data);
            $selectString = $sql->getSqlStringForSqlObject($insert);
            $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);

            // hapus data t_detailsptbphtb   
            $this->delete(array(
                't_idspt' => $idspt
            ));
        }

        // cari data id_pembayaran
        $caridata_idpembayaran = "select t_idpembayaranspt from t_pembayaranspt where t_idspt = " . $idspt . "";
        $id_pembayaran = $this->adapter->query($caridata_idpembayaran)->execute()->current();

        if (!empty($id_pembayaran)) {
            $hapusdatapemeriksaan = "DELETE FROM t_pemeriksaan where p_idpembayaranspt = " . $id_pembayaran['t_idpembayaranspt'] . "";
            $this->adapter->query($hapusdatapemeriksaan)->execute();
        }

        // hapus data t_pembayaranspt
        $input2 = "DELETE FROM t_pembayaranspt where t_idspt= " . $idspt . "";
        $this->adapter->query($input2)->execute();
        //echo "Data Sukses Dihapus";
    }

    public function hapusall2($idspt)
    {

        // hapus file t_spt
        $input = "DELETE FROM t_spt where t_idspt= " . $idspt . "";
        $this->adapter->query($input)->execute();

        // hapus data t_detailsptbphtb   
        $this->delete(array(
            't_idspt' => $idspt
        ));

        // cari data id_pembayaran
        $caridata_idpembayaran = "select t_idpembayaranspt from t_pembayaranspt where t_idspt = " . $idspt . "";
        $id_pembayaran = $this->adapter->query($caridata_idpembayaran)->execute()->current();

        $hapusdatapemeriksaan = "DELETE FROM t_pemeriksaan where p_idpembayaranspt = " . $id_pembayaran['t_idpembayaranspt'] . "";
        $this->adapter->query($hapusdatapemeriksaan)->execute();


        // hapus data t_pembayaranspt
        $input2 = "DELETE FROM t_pembayaranspt where t_idspt= " . $idspt . "";
        $this->adapter->query($input2)->execute();
        //echo "Data Sukses Dihapus";
    }

    public function hapusDataWaris($id)
    {
        $t_penerimawaris = new \Zend\Db\TableGateway\TableGateway('t_penerimawaris', $this->adapter);
        $t_penerimawaris->delete(array(
            't_idspt' => $id
        ));
    }

    // belum valid
    public function temukanDataHistory(SSPDBphtbBase $kb)
    {
        $sql = "select * from view_sspd a
                left join t_pembayaranspt b on b.t_idspt = a.t_idspt
                left join t_penerimawaris c on c.t_idspt = a.t_idspt
                where a.t_nikwppembeli='" . $kb->t_nikwppembeli . "' or c.t_nikpenerima ='" . $kb->t_nikwppembeli . "'
                and a.t_periodespt='" . date('Y', strtotime($kb->t_tglprosesspt)) . "' order by a.t_idspt desc";



        $statement = $this->adapter->query($sql);

        // var_dump($sql);
        // exit();

        return $statement->execute()->current();
    }

    // belum valid
    public function temukanDataHistory2(SSPDBphtbBase $kb)
    {
        $sql = "select * from view_sspd a
                left join t_pembayaranspt b on b.t_idspt = a.t_idspt
                where a.t_nikwppembeli='" . $kb->t_nikwppembeli . "' 
                and a.t_periodespt='" . date('Y', strtotime($kb->t_tglprosesspt)) . "'";
        $statement = $this->adapter->query($sql);
        //var_dump($sql);
        //exit();

        return $statement->execute();
    }

    public function temukanDataNPOPTKP(SSPDBphtbBase $kb)
    {
        $sql = "select * from s_tarifnpoptkp
                where s_idjenistransaksinpotkp='" . $kb->t_idjenistransaksi . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataTarifBphtb()
    {
        //$sql = "select * from s_tarifbphtb where s_idtarifbphtb=2";
        $sql = "select * from s_tarifbphtb where date_part('year'::text, s_tanggaltarifbphtb) = " . date('Y') . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataTarifBphtb_tahun($tahun)
    {
        //$sql = "select * from s_tarifbphtb where s_idtarifbphtb=2";
        $sql = "select * from s_tarifbphtb where date_part('year'::text, s_tanggaltarifbphtb) = " . $tahun . "";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanPersyaratan($kb)
    {
        $sql = "select * from s_persyaratan where s_idjenistransaksi =$kb->t_idjenistransaksi order by s_idpersyaratan";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    // Belum valid
    public function gethistorybphtb($nikpembeli, $idspt, $periode)
    {
        // $nikpembeli = $nikpembeli == null ? 0 : $nikpembeli;
        // $idspt = $idspt == null ? 0 : $idspt;
        // $periode = $periode == null ? 0 : $periode;

        $sql = "select * from view_sspd a
                    left join t_pembayaranspt b on b.t_idspt = a.t_idspt 
                    where a.t_nikwppembeli='" . $nikpembeli . "' and a.t_idspt not in (" . $idspt . ")
                    and a.t_periodespt='" . $periode . "' order by a.t_idspt";
        $statement = $this->adapter->query($sql);
        $data = $statement->execute();


        return $data;
    }

    public function savedata_koreksiluas($id, $t_luastanahbpn, $t_luasbangunanbpn)
    {
        $data = array(
            't_luastanahbpn' => $t_luastanahbpn,
            't_luasbangunanbpn' => $t_luasbangunanbpn
        );
        $update = $this->update($data, array(
            't_iddetailsptbphtb' => $id
        ));
        return $update;
    }

    public function savedata_sertifikatbaru($id, $t_nosertifikatbaru, $t_tglsertifikatbaru)
    {
        $data = array(
            't_nosertifikatbaru' => $t_nosertifikatbaru,
            't_tglsertifikatbaru' => date('Y-m-d', strtotime($t_tglsertifikatbaru))
        );
        $update = $this->update($data, array(
            't_iddetailsptbphtb' => $id
        ));
        return $update;
    }

    public function getSptid($t_iddetailsptbphtb)
    {
        $rowset = $this->select(array(
            't_iddetailsptbphtb' => $t_iddetailsptbphtb
        ));
        $row = $rowset->current();
        return $row;
    }

    public function getDataPemeriksaan($id)
    {
        $sql = "select * from view_sspd_semua_pembayaran where t_idspt=$id";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataPemeriksaan1($id)
    {
        $sql = "select * from view_sspd_semua_pembayaran where t_iddetailsptbphtb=$id";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataIdBpn($id)
    {

        $rowset = $this->select(function (Select $select) use ($id) {
            $select->where(array(
                't_spt.t_idspt' => $id
            ));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
            $select->join('s_jenistransaksi', 't_spt.t_idjenistransaksi = s_jenistransaksi.s_idjenistransaksi');
            $select->join('s_jenishaktanah', 't_spt.t_idjenishaktanah = s_jenishaktanah.s_idhaktanah');
            $select->join('t_pembayaranspt', 't_pembayaranspt.t_idspt = t_spt.t_idspt');
            $datapemeriksaan = $this->getDataPemeriksaan($id);
            if ($datapemeriksaan['p_idpemeriksaan'] != null) {
                $select->join('t_pemeriksaan', 't_pemeriksaan.p_idpembayaranspt = t_pembayaranspt.t_idpembayaranspt');
            }
        });
        $row = $rowset->current();
        return $row;
    }

    public function getSemuaData($t_iddetailsptbphtb)
    {
        $rowset = $this->select(function (Select $select) use ($t_iddetailsptbphtb) {
            $select->where(array(
                't_detailsptbphtb.t_iddetailsptbphtb' => $t_iddetailsptbphtb
            ));
            $select->join('t_spt', 't_detailsptbphtb.t_idspt = t_spt.t_idspt');
            $select->join('s_jenistransaksi', 't_spt.t_idjenistransaksi = s_jenistransaksi.s_idjenistransaksi');
            $select->join('s_jenishaktanah', 't_spt.t_idjenishaktanah = s_jenishaktanah.s_idhaktanah');
            $select->join('t_pembayaranspt', 't_pembayaranspt.t_idspt = t_spt.t_idspt');
            $datapemeriksaan = $this->getDataPemeriksaan1($t_iddetailsptbphtb);
            if ($datapemeriksaan['p_idpemeriksaan'] != null) {
                $select->join('t_pemeriksaan', 't_pemeriksaan.p_idpembayaranspt = t_pembayaranspt.t_idpembayaranspt');
            }
        });

        //var_dump($rowset); exit();

        $row = $rowset->current();

        return $row;
    }

    public function jumlahsyarat($id_syarat)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('s_persyaratan');
        $where = new Where();
        $where->literal("s_idjenistransaksi = '$id_syarat'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridCountSismiop(SSPDBphtbBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_terbit_ajb = $this->view_terbit_ajb();
        //$select->from('view_terbit_ajb');
        $select->from(array("z" => $view_terbit_ajb));
        $where = new Where();
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        if ($base->t_noajbbaru != 'undefined')
            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        if ($base->t_tglajbbaru != 'undefined')
            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataSismiop(SSPDBphtbBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $view_terbit_ajb = $this->view_terbit_ajb();
        //$select->from('view_terbit_ajb');
        $select->from(array("z" => $view_terbit_ajb));
        $where = new Where();
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_totalspt != 'undefined')
            $where->literal("t_totalspt::text LIKE '%$base->t_totalspt%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        if ($base->t_noajbbaru != 'undefined')
            $where->literal("t_noajbbaru::text LIKE '%$base->t_noajbbaru%'");
        if ($base->t_tglajbbaru != 'undefined')
            $where->equalTo("t_tglajbbaru", date('Y-m-d', strtotime($base->t_tglajbbaru)));
        $select->where($where);
        $select->limit($base->rows = (int)$base->rows);
        $select->offset($offset = (int)$offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getJumlahPendataan(SSPDBphtbBase $base, $id_notaris, $userRole = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        if ($userRole == "Notaris") {
            $where->equalTo('t_idnotarisspt', $id_notaris);
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function saveDetailNOP($values, $iddetail = null)
    {
        $tg = new \Zend\Db\TableGateway\TableGateway('t_detailnopgabungan', $this->adapter);
        if ($iddetail == null) :
            return $tg->insert($values);
        else :
            return $tg->update($values, ['t_iddetailnopgabungan' => $iddetail]);
        endif;
    }

    public function checkDetailNOP($iddetailspt, $nop, $tahun)
    {
        $tg = new \Zend\Db\TableGateway\TableGateway('t_detailnopgabungan', $this->adapter);
        $select = $tg->select(['t_iddetailsptbphtb' => $iddetailspt, 't_nopbphtbsppt' => $nop, 't_thnsppt' => $tahun]);
        return $select->current();
    }

    //============================== datagrid notifikasi validasi

    public function semuadatanotifvalidasi($input, $aColumns, $session, $cekurl, $allParams)
    {

        $sql = new \Zend\Db\Sql\Sql($this->adapter);
        $select = $sql->select();
        //$select->from("FR_PSN_VALIDASI");
        $fr_pembayaran_v5 = $this->fr_pembayaran_v5();
        //$select->from("fr_pembayaran_v5");
        $select->from(array("z" => $fr_pembayaran_v5));
        $select->join(
            "s_users",
            "s_users.s_userid = fr_pembayaran_v5.t_pejabatverifikasispt",
            array("s_jabatan"),
            "left"
        );
        $select->join(
            "s_pejabat",
            "s_pejabat.s_idpejabat = s_users.s_idpejabat_idnotaris",
            array("s_namapejabat"),
            "left"
        );

        $where = new \Zend\Db\Sql\Where();

        $where->literal("fr_pembayaran_v5.status_validasi = 2");

        $s_iduser = $session['s_userid'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        if ($s_tipe_pejabat == 2) {
            $where->literal("fr_pembayaran_v5.t_idnotarisspt = " . $s_iduser);
        } else {
        }


        if (($input->getPost('sSearch_1')) || ($input->getPost('sSearch_1') == '0')) {
            $where->literal("lower(fr_pembayaran_v5.t_kohirspt) like lower('%" . $input->getPost('sSearch_1') . "%')");
        }
        if (($input->getPost('sSearch_2')) || ($input->getPost('sSearch_2') == '0')) {
            $where->literal("lower(s_pejabat.s_namapejabat) like lower('%" . $input->getPost('sSearch_2') . "%')");
        }
        if (($input->getPost('sSearch_3')) || ($input->getPost('sSearch_3') == '0')) {
            $tanggal = explode('-', $input->getPost('sSearch_3'));
            if (count($tanggal) > 1) {
                if (count($tanggal) > 2) {
                    $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                    $where->literal("fr_pembayaran_v5.t_tglverifikasispt like '%" . $tanggalcari . "%'");
                } else {
                    $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                    $where->literal("fr_pembayaran_v5.t_tglverifikasispt like '%" . $tanggalcari . "%'");
                }
            }
        }
        if (($input->getPost('sSearch_4')) || ($input->getPost('sSearch_4') == '0')) {
            $where->literal("lower(fr_pembayaran_v5.t_keteranganvalidasi) like lower('%" . $input->getPost('sSearch_4') . "%')");
        }




        $select->where($where);

        //echo $select->getSqlString(); exit();

        //================ menghitung jumlah datane coy
        $totaldata = $this->getjumlahdata($select, "", "");
        $iTotal = $totaldata;
        //================ end menghitung jumlah datane coy

        //================ ordernya coy
        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = $aColumns[intval($input->getPost('iSortCol_' . $i))] . " " . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }


        if (!empty($aOrderingRules)) {
            $select->order(implode(", ", $aOrderingRules));
        } else {
            $select->order("fr_pembayaran_v5.t_tglverifikasispt desc");
        }
        //================ end ordernya coy

        //================ pagination e coy
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $select->limit(intval($input->getPost('iDisplayLength')));
            $select->offset(intval($input->getPost('iDisplayStart')));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $select->limit(intval($input->getPost('iDisplayLength')));
                $select->offset(intval($input->getPost('iDisplayStart')));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $select->limit(10);
                $select->offset(0);
                $no = 1;
            }
        }
        //================ end pagination e coy

        //echo $select->getSqlString(); exit();

        $statement = $sql->prepareStatementForSqlObject($select);
        $rResult = $statement->execute();


        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );

        //var_dump($rResult); exit();

        foreach ($rResult as $aRow) {
            $row = array();

            if (!empty($aRow['s_namapejabat'])) {
                $namapejabat = $aRow['s_namapejabat'];
            } else {
                $namapejabat = $aRow['s_jabatan'];
            }

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow['t_kohirspt'] . "</center>",
                $namapejabat,
                "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglverifikasispt'])) . "</span>",
                $aRow['t_keteranganvalidasi'],
                "<center><a href='" . $cekurl . "/pendataan_sspd/detailnotif?t_idspt=" . $aRow['t_idspt'] . "' class='btn btn-sm btn-warning'>LIHAT NOTIFIKASI</a>",

            );

            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    //============================== end datagrid notifikasi validasi

    public function get_fr_pembayaran_v5($id)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        //        $select->from('fr_pembayaran_v5');
        $fr_pembayaran_v5 = $this->fr_pembayaran_v5();
        $select->from(array("z" => $fr_pembayaran_v5));
        $where = new Where();
        $where->equalTo('t_idspt', $id);
        $select->where($where);
        //        echo $select->getSqlString();exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();


        return $res;
    }

    public function datasemuapesanvaidasi($idspt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("fr_psn_validasi");
        $select->columns(array("id_psnvalidasi", "t_idspt", "t_kohirspt", "t_dari", "t_isipesan", "t_psndilihat", "t_untuk", "t_waktukirim" => new Expression("TO_CHAR(t_waktukirim, 'YYYY-MM-DD HH24:MI:SS')")));
        $select->join(
            "s_users",
            new Expression("s_users.s_iduser = fr_psn_validasi.t_dari"),
            array("s_nama_dari" => "s_username", "s_jabatan_dari" => "s_jabatan", "s_akses_dari" => "s_akses"),
            "left"
        );
        $select->join(
            array("a" => "s_users"),
            new Expression("a.s_iduser = fr_psn_validasi.t_untuk"),
            array("s_nama_untuk" => "s_username", "s_jabatan_untuk" => "s_jabatan", "s_akses_untuk" => "s_akses"),
            "left"
        );
        $select->join(
            array("b" => "s_notaris"),
            new \Zend\Db\Sql\Expression("b.s_idnotaris = s_users.s_idpejabat_idnotaris::INT AND s_users.s_tipe_pejabat = '2'"),
            array(
                "s_namanotaris_dari" => "s_namanotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_pejabat"),
            new \Zend\Db\Sql\Expression("c.s_idpejabat = s_users.s_idpejabat_idnotaris::INT AND s_users.s_tipe_pejabat = '1'"),
            array(
                "s_namapejabat_dari" => "s_namapejabat",
            ),
            "LEFT"
        );
        $select->join(
            array("bb" => "s_notaris"),
            new \Zend\Db\Sql\Expression("bb.s_idnotaris = A.s_idpejabat_idnotaris::INT AND a.s_tipe_pejabat = '2'"),
            array(
                "s_namanotaris_untuk" => "s_namanotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("cc" => "s_pejabat"),
            new \Zend\Db\Sql\Expression("cc.s_idpejabat = A.s_idpejabat_idnotaris::INT AND a.s_tipe_pejabat = '1'"),
            array(
                "s_namapejabat_untuk" => "s_namapejabat",
            ),
            "LEFT"
        );
        $where = new Where();
        $where->literal("t_idspt =" . $idspt . " ");
        $select->where($where);
        $select->order("t_waktukirim");
        // echo $select->getSqlString();
        // exit();
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function kirimpesankepetugasvalidasi($data, $isi_pesan, $session, $ar_pemda = null, $urlapp = null)
    {
        $T_IDSPT   = $data['t_idspt'];
        $T_KOHIR = $data['t_kohirspt'];

        if ($session['s_akses'] == 3) {
            $T_DARI = $data['t_idnotarisspt'];
            $T_UNTUK = $data['t_pejabatverifikasispt'];
        } else {
            $T_DARI = $session['s_iduser'];
            $T_UNTUK = $data['t_idnotarisspt'];
        }
        $WAKTU_KIRIM = date('Y-m-d h:i:s');

        $sqlinsert = "INSERT INTO fr_psn_validasi (t_idspt, t_kohirspt, t_dari, t_isipesan, t_psndilihat, t_untuk)
                                VALUES(" . $T_IDSPT . ", " . $T_KOHIR . ", " . $T_DARI . ", '" . $isi_pesan . "', 0, " . $T_UNTUK . "  ) ";

        $res = $this->adapter->query($sqlinsert)->execute();

        require 'public/PHPMailer/src/PHPMailer.php';
        require 'public/PHPMailer/src/Exception.php';
        require 'public/PHPMailer/src/SMTP.php';

        $notifemail = $this->getsetnotifemail();
        if ($notifemail['s_status'] == 1 && $session['s_akses'] != 3) { //JIKA NOTIF AKTIF


            $sql = new Sql($this->adapter);
            //            $select = $sql->select();
            //            $select->from("fr_psn_validasi");
            //            $where = new Where();
            //            $where->literal("t_idspt ='".$T_IDSPT."'");
            //            $where->literal("t_kohirspt ='".$T_KOHIR."'");
            //            $where->literal("t_isipesan ilike '%".$isi_pesan."%'");
            //            $where->literal("t_psndilihat ='0'");
            //            $where->literal("t_untuk ='".$T_UNTUK."'");
            //            $select->where($where);
            //            $state = $sql->prepareStatementForSqlObject($select);
            //            //echo $state->getSql();exit();
            //            $res = $state->execute()->current();

            //DATA PENGIRIM
            $datanotaris = $this->getinformasiuser($data['t_idnotarisspt']);

            $pengirim = $this->getinformasiuser($T_DARI);

            //PHP MAILER ===========================  
            $mail = new PHPMailer();
            $mail->IsSMTP();

            //GMAIL config
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            //$mail->SMTPSecure = "ssl";                 // sets the prefix to the server
            //$mail->Host = "smtp.gmail.com";
            //$mail->Port = 465 ;
            $mail->Host = 'tls://smtp.gmail.com:587';

            $mail->Username   = "bpprd.tanahbumbukab@gmail.com";  // GMAIL username
            $mail->Password   = "b1p2p3r4d51";            // mygmail password
            $mail->SMTPDebug = 0;
            //=== untuk keterangan debug server dan client = 2
            //=== untuk client = 1
            //=== unutk tidak muncul = 0

            //End Gmail
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->From       = "bpprd@tanahbumbukab.go.id";
            $mail->FromName   = "e-Bphtb " . str_ireplace("Kabupaten ", "", $ar_pemda->s_namakabkot) . "";
            $mail->Subject    = "Notifikasi Pesan";

            $mail->AddEmbeddedImage('./' . $ar_pemda->s_logo, 'logo_1');
            $html = '
                <table style="font-family:Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif; max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
        <tbody>
        <tr style="margin: 0px; padding: 0px;">
            <td style="margin: 0px; padding: 0px;"></td>
            <td bgcolor="#FFFFFF" style="margin: 0px auto; padding: 0px; display: block; max-width: 600px; clear: both;">

                <div style="max-width: 600px; display: block; border-collapse: collapse; margin: 0px auto; padding: 30px 15px; border: 1px solid rgb(231, 231, 231);">
                    <table style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif; max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent" border="0">
                        <tbody>
                            <tr>
                                <td style="padding: 15px 20px 10px;" bgcolor="#2069b3">
                        <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; color: rgb(255, 255, 255);" border="0">
                            <tbody><tr>
                                <td width="280">
                                    <img src="cid:logo_1" style="border: 0px; min-height: auto; width: 41px; outline: 0px;">
                                </td>
                                <td width="280" align="right" style="font-size: 20px;">
                                    Notifikasi Pesan
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                            </tr>
                        <tr style="margin: 0px; padding: 0px;">
                            <td style="margin: 0px; padding: 0px;">
                                <h4 style="font-size: 20px; padding: 30px 0px 10px; font-weight: bold; color: rgb(78, 78, 78);">
                                Pendaftaran No. ' . $data['t_kohirspt'] . ' Periode ' . $data['t_periodespt'] . '
</h4>
                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">Hi, <b>' . $datanotaris['s_namanotaris'] . '</b>.</p>
                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">
                                    Anda mendapatkan notifikasi pesan :
                                </p>
                                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;font-size:15px">
                            <tbody><tr>
                                <td width="220" style="padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">Pengirim :</td>
                                <td width="340" style="font-weight:bold;padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">
                                    ' . $pengirim['s_namapejabat'] . '
                                </td>
                            </tr>
                            <tr>
                                <td width="220" style="padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">Pesan :</td>
                                <td width="340" style="font-weight:bold;padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">' . $isi_pesan . '</td>
                            </tr>
                        </tbody>
                                </table>
                                <div style="text-align: center; margin: 20px; padding: 0px;" align="center">
                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">
                                    Silahkan cek pesan notifikasi, dengan link dibawah.
                                </p>
                                </div>
                                <div style="text-align: center; margin: 20px; padding: 0px;" align="center">
                                    <a href="' . $urlapp . '/pendataan_sspd/detailnotif?t_idspt=' . $data['t_idspt'] . '" style="color: rgb(255, 255, 255); text-decoration: none; display: inline-block; vertical-align: middle; line-height: 20px; font-size: 13px; font-weight: 600; text-align: center; white-space: nowrap; border-radius: 3px; background-color: #2069b3; margin: 0px; padding: 9px 14px; border: 1px solid #2e5cb8;" target="_blank" data-saferedirecturl="' . $urlapp . '/pendataan_sspd/detailnotif?t_idspt=' . $data['t_idspt'] . '">
                                        LINK E-BPHTB
                                    </a>
                                </div>
                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; border-top: 3px solid rgb(208, 208, 208); margin: 40px 0px 0px; padding: 10px 0px 0px;">
                                    <small style="color: rgb(153, 153, 153); margin: 0px; padding: 0px;">
                                        Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                                    </small>
                                </p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
            </td>
            <td style="margin: 0px; padding: 0px;"></td>
        </tr>
    </tbody></table>
    <table style="max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px 0px 60px; padding: 0px; clear: both;" bgcolor="transparent">
<tbody>
    <tr style="margin: 0px; padding: 0px;">
        <tr style="margin: 0px; padding: 0px;">
            <td style="margin: 0px; padding: 0px;"></td>
            <td style="margin: 0px auto; padding: 0px; display: block; max-width: 600px; clear: both;">
                <div style="max-width: 600px; display: block; border-collapse: collapse; background-color: rgb(247, 247, 247); margin: 0px auto; padding: 20px 15px; border-color: rgb(231, 231, 231); border-style: solid; border-width: 0px 1px 1px;">
                <table width="100%" style="max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
                    <tbody style="margin: 0px; padding: 0px;">
                        <tr style="margin: 0px; padding: 0px;">
                            <td valign="middle" style="margin: 0px; padding: 0px; width: 7%;">
                                <img src="cid:logo_1" style="border: 0px; min-height: auto; width: 41px; outline: 0px;">
                            </td>
                            <td valign="middle" style="margin: 0px; padding: 0px; width: 53%;">
                                <p style="color: rgb(145, 144, 142); font-size: 10px; line-height: 150%; font-weight: normal; margin: 0px; padding: 0px;">
                                    Jika butuh bantuan, gunakan halaman <a href="http://bpprd.tanahbumbukab.go.id/?page_id=344" style="color: #2069b3; text-decoration: none; margin: 0px; padding: 0px;" target="_blank" data-saferedirecturl="http://bpprd.tanahbumbukab.go.id/?page_id=344">Kontak Kami</a>.
                                    <br style="margin: 0px; padding: 0px;">
                                    © 2019, ' . $ar_pemda->s_namasingkatinstansi . ' ' . $ar_pemda->s_namakabkot . '
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </td>
    <td style="margin: 0px; padding: 0px;"></td>
</tr>
</tbody>
</table>';
            $mail->MsgHTML($html);

            $mail->AddReplyTo("bpprd@tanahbumbukab.go.id", "BPPRD MINAHASA SELATAN"); //optional
            $mail->AddAddress($datanotaris['s_email'], $datanotaris['s_namanotaris']);
            $mail->IsHTML(true); // send as HTML

            if (!$mail->Send()) { //to see if we return a message or a value bolean
                echo "Mailer Error: " . $mail->ErrorInfo;
            };
            //PHP MAILER ===========================
        } else if ($notifemail['s_status'] == 1 && $session['s_akses'] == 3) {

            //DATA PENGIRIM
            $datanotaris = $this->getinformasiuser($data['t_idnotarisspt']);

            // $penerima = $this->getinformasiuser($T_UNTUK);
            $datapenerima = $this->getdatoperator();
            // var_dump($datapenerima);
            // exit();

            //PHP MAILER ===========================  
            $mail = new PHPMailer();
            $mail->IsSMTP();

            //GMAIL config
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            //$mail->SMTPSecure = "ssl";                 // sets the prefix to the server
            //$mail->Host = "smtp.gmail.com";
            //$mail->Port = 465 ;
            $mail->Host = 'tls://smtp.gmail.com:587';

            $mail->Username   = "bpprd.tanahbumbukab@gmail.com";  // GMAIL username
            $mail->Password   = "b1p2p3r4d51";            // mygmail password

            $mail->SMTPDebug = 0;
            //=== untuk keterangan debug server dan client = 2
            //=== untuk client = 1
            //=== unutk tidak muncul = 0

            //End Gmail
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            //$mail->setFrom("bpprd.tanahbumbukab@gmail.com", "BPPRD MINAHASA SELATAN");
            $mail->From       = "bpprd.tanahbumbukab@gmail.com";
            $mail->FromName   = "e-Bphtb " . str_ireplace("Kabupaten ", "", $ar_pemda->s_namakabkot) . "";
            $mail->Subject    = "Notifikasi Pesan";

            $mail->AddEmbeddedImage('./' . $ar_pemda->s_logo, 'logo_1');
            $html = '
                <table style="font-family:Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif; max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
        <tbody>
        <tr style="margin: 0px; padding: 0px;">
            <td style="margin: 0px; padding: 0px;"></td>
            <td bgcolor="#FFFFFF" style="margin: 0px auto; padding: 0px; display: block; max-width: 600px; clear: both;">

                <div style="max-width: 600px; display: block; border-collapse: collapse; margin: 0px auto; padding: 30px 15px; border: 1px solid rgb(231, 231, 231);">
                    <table style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif; max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent" border="0">
                        <tbody>
                            <tr>
                                <td style="padding: 15px 20px 10px;" bgcolor="#2069b3">
                        <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; color: rgb(255, 255, 255);" border="0">
                            <tbody><tr>
                                <td width="280">
                                    <img src="cid:logo_1" style="border: 0px; min-height: auto; width: 41px; outline: 0px;">
                                </td>
                                <td width="280" align="right" style="font-size: 20px;">
                                    Notifikasi Pesan
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                            </tr>
                        <tr style="margin: 0px; padding: 0px;">
                            <td style="margin: 0px; padding: 0px;">
                                <h4 style="font-size: 20px; padding: 30px 0px 10px; font-weight: bold; color: rgb(78, 78, 78);">
                                Pendaftaran No. ' . $data['t_kohirspt'] . ' Periode ' . $data['t_periodespt'] . '
</h4>
                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">Hi, </p>
                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">
                                    Anda mendapatkan notifikasi pesan :
                                </p>
                                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;font-size:15px">
                            <tbody><tr>
                                <td width="220" style="padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">Pengirim :</td>
                                <td width="340" style="font-weight:bold;padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">
                                    ' . $datanotaris['s_namanotaris'] . '
                                </td>
                            </tr>
                            <tr>
                                <td width="220" style="padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">Pesan :</td>
                                <td width="340" style="font-weight:bold;padding:10px 0;border-bottom:1px solid #d9d9d9;vertical-align:top;line-height:1.6em">' . $isi_pesan . '</td>
                            </tr>
                        </tbody>
                                </table>
                                <div style="text-align: center; margin: 20px; padding: 0px;" align="center">
                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">
                                    Silahkan cek pesan notifikasi, dengan link dibawah.
                                </p>
                                </div>
                                <div style="text-align: center; margin: 20px; padding: 0px;" align="center">
                                    <a href="' . $urlapp . '/pendataan_sspd/detailnotif?t_idspt=' . $data['t_idspt'] . '" style="color: rgb(255, 255, 255); text-decoration: none; display: inline-block; vertical-align: middle; line-height: 20px; font-size: 13px; font-weight: 600; text-align: center; white-space: nowrap; border-radius: 3px; background-color: #2069b3; margin: 0px; padding: 9px 14px; border: 1px solid #2e5cb8;" target="_blank" data-saferedirecturl="' . $urlapp . '/pendataan_sspd/detailnotif?t_idspt=' . $data['t_idspt'] . '">
                                        LINK E-BPHTB
                                    </a>
                                </div>
                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; border-top: 3px solid rgb(208, 208, 208); margin: 40px 0px 0px; padding: 10px 0px 0px;">
                                    <small style="color: rgb(153, 153, 153); margin: 0px; padding: 0px;">
                                        Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                                    </small>
                                </p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
            </td>
            <td style="margin: 0px; padding: 0px;"></td>
        </tr>
    </tbody></table>
    <table style="max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px 0px 60px; padding: 0px; clear: both;" bgcolor="transparent">
<tbody>
    <tr style="margin: 0px; padding: 0px;">
        <tr style="margin: 0px; padding: 0px;">
            <td style="margin: 0px; padding: 0px;"></td>
            <td style="margin: 0px auto; padding: 0px; display: block; max-width: 600px; clear: both;">
                <div style="max-width: 600px; display: block; border-collapse: collapse; background-color: rgb(247, 247, 247); margin: 0px auto; padding: 20px 15px; border-color: rgb(231, 231, 231); border-style: solid; border-width: 0px 1px 1px;">
                <table width="100%" style="max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
                    <tbody style="margin: 0px; padding: 0px;">
                        <tr style="margin: 0px; padding: 0px;">
                            <td valign="middle" style="margin: 0px; padding: 0px; width: 7%;">
                                <img src="cid:logo_1" style="border: 0px; min-height: auto; width: 41px; outline: 0px;">
                            </td>
                            <td valign="middle" style="margin: 0px; padding: 0px; width: 53%;">
                                <p style="color: rgb(145, 144, 142); font-size: 10px; line-height: 150%; font-weight: normal; margin: 0px; padding: 0px;">
                                    Jika butuh bantuan, gunakan halaman <a href="http://bpprd.tanahbumbukab.go.id/?page_id=344" style="color: #2069b3; text-decoration: none; margin: 0px; padding: 0px;" target="_blank" data-saferedirecturl="http://bpprd.tanahbumbukab.go.id/?page_id=344">Kontak Kami</a>.
                                    <br style="margin: 0px; padding: 0px;">
                                    © 2019, ' . $ar_pemda->s_namasingkatinstansi . ' ' . $ar_pemda->s_namakabkot . '
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </td>
    <td style="margin: 0px; padding: 0px;"></td>
</tr>
</tbody>
</table>';
            $mail->MsgHTML($html);

            $mail->AddReplyTo("bpprd@tanahbumbukab.go.id", "BPPRD MINAHASA SELATAN"); //optional
            // $mail->AddAddress('bpprd.tanahbumbukab@gmail.com', 'BPPRD MINAHASA SELATAN');
            foreach ($datapenerima as $datapenerima => $value) {
                # code...
                $mail->AddAddress($value['s_email'], $value['s_namapejabat']);
            }
            $mail->IsHTML(true); // send as HTML

            if (!$mail->Send()) { //to see if we return a message or a value bolean
                echo "Mailer Error: " . $mail->ErrorInfo;
            };
            //PHP MAILER ===========================
        }

        return $res;
    }

    public function getsetnotifemail()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from("s_notifemail");
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    //    public function getDataNotarisIdUser($id){
    //        $sql = new Sql($this->adapter);
    //        $select = $sql->select();
    //        $select->columns(array(
    //            "s_idpejabat_idnotaris",
    //            "s_iduser",
    //            "s_username",
    //            "s_password",
    //            "s_jabatan",
    //            "s_akses",
    //            "s_tipe_pejabat",
    //            "s_email"
    //        ));
    //        $select->from("s_users");
    //        $select->join("s_notaris", 
    //                new \Zend\Db\Sql\Expression("s_users.s_idpejabat_idnotaris::INTEGER = s_notaris.s_idnotaris"), 
    //                array(
    //                    "s_idnotaris",
    //                    "s_namanotaris",
    //                    "s_alamatnotaris",
    //                    "s_kodenotaris",
    //                    "s_sknotaris",
    //                    "s_tgl1notaris",
    //                    "s_tgl2notaris",
    //                    "s_statusnotaris"), "LEFT");
    //        $where = new \Zend\Db\Sql\Where();
    //        $where->literal("s_users.s_iduser = '".$id."'");
    //        $select->where($where);
    //        $state = $sql->prepareStatementForSqlObject($select);
    //        $res = $state->execute()->current();
    //        return $res;
    //    }

    public function getinformasiuser($id = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "s_iduser", "s_username", "s_password", "s_jabatan", "s_akses", "s_idpejabat_idnotaris",
            "s_tipe_pejabat", "s_email", "s_status", "s_resetakun"
        ));
        $select->from(array("a" => "s_users"));
        $select->join(
            array("b" => "s_notaris"),
            new \Zend\Db\Sql\Expression("b.s_idnotaris = a.s_idpejabat_idnotaris::INT AND a.s_tipe_pejabat = '2'"),
            array(
                "s_idnotaris", "s_namanotaris", "s_alamatnotaris", "s_kodenotaris", "s_sknotaris",
                "s_tgl1notaris", "s_tgl2notaris", "s_statusnotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_pejabat"),
            new \Zend\Db\Sql\Expression("c.s_idpejabat = a.s_idpejabat_idnotaris::INT AND a.s_tipe_pejabat = '1'"),
            array(
                "s_idpejabat", "s_namapejabat", "s_jabatanpejabat", "s_nippejabat",
                "s_golonganpejabat", "s_statuspejabat" => new \Zend\Db\Sql\Expression("c.s_status")
            ),
            "LEFT"
        );
        $select->join(
            array("d" => "ss_golonganpejabat"),
            new \Zend\Db\Sql\Expression("d.ss_idgolongan = c.s_golonganpejabat"),
            array(
                "ss_idgolongan", "ss_namagolongan", "ss_pangkatgolongan"
            ),
            "LEFT"
        );
        $where = new \Zend\Db\Sql\Where();
        $where->literal("a.s_iduser = '" . (int)$id . "'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res;
    }

    public function getdatoperator()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("a" => "s_users"));
        $select->join(
            array("c" => "s_pejabat"),
            new \Zend\Db\Sql\Expression("c.s_idpejabat = a.s_idpejabat_idnotaris::INT AND a.s_tipe_pejabat = '1'"),
            array(
                "s_idpejabat", "s_namapejabat", "s_jabatanpejabat", "s_nippejabat",
                "s_golonganpejabat", "s_statuspejabat" => new \Zend\Db\Sql\Expression("c.s_status")
            ),
            "LEFT"
        );
        $select->join(
            array("d" => "ss_golonganpejabat"),
            new \Zend\Db\Sql\Expression("d.ss_idgolongan = c.s_golonganpejabat"),
            array(
                "ss_idgolongan", "ss_namagolongan", "ss_pangkatgolongan"
            ),
            "LEFT"
        );
        $where = new \Zend\Db\Sql\Where();
        $where->literal("a.s_akses in (1,2,8,9)"); //AMDIN OPERATOR KASUBID KABID
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }


    public function view_terbit_ajb()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_idspt", "t_tglprosesspt", "t_periodespt", "t_nopbphtbsppt", "t_totalspt",
            "t_nilaitransaksispt", "t_kohirspt", "t_potonganspt", "t_idjenistransaksi",
            "t_idjenishaktanah", "t_thnsppt", "t_persyaratan", "t_input_sismiop", "t_idnotarisspt"
        ));
        $select->from(array("a" => "t_spt"));
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new \Zend\Db\Sql\Expression("b.t_idspt = a.t_idspt"),
            array(
                "t_namawppembeli", "t_nikwppembeli", "t_alamatwppembeli", "t_kecamatanwppembeli",
                "t_kelurahanwppembeli", "t_kabkotawppembeli", "t_telponwppembeli",
                "t_kodeposwppembeli", "t_npwpwppembeli", "t_namawppenjual",
                "t_nikwppenjual", "t_alamatwppenjual", "t_kecamatanwppenjual",
                "t_kelurahanwppenjual", "t_kabkotawppenjual", "t_telponwppenjual",
                "t_kodeposwppenjual", "t_npwpwppenjual", "t_luastanah", "t_njoptanah",
                "t_luasbangunan", "t_njopbangunan", "t_totalnjoptanah", "t_totalnjopbangunan",
                "t_grandtotalnjop", "t_nosertifikathaktanah", "t_iddetailsptbphtb",
                "t_kelurahanop", "t_kecamatanop", "t_kabupatenop", "t_rtwppembeli",
                "t_rwwppembeli",
                "t_alamatop", "t_rtop", "t_rwop", "t_tglajbbaru", "t_noajbbaru",
                "t_inputbpn", "t_statuskonfirmasinotaris", "t_tglkonfirmasinotaris",
                "t_rtwppenjual", "t_rwwppenjual", "t_nosertifikatbaru", "t_tglsertifikatbaru",
                "t_inputbpn", "t_statuskonfirmasinotaris", "t_luastanah_sismiop",
                "t_luasbangunan_sismiop", "t_njoptanah_sismiop", "t_njopbangunan_sismiop"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_jenistransaksi"),
            new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"),
            array(
                "s_namajenistransaksi"
            ),
            "LEFT"
        );
        $select->join(array("d" => "s_jenishaktanah"), new \Zend\Db\Sql\Expression("d.s_idhaktanah = a.t_idjenishaktanah"), array(
            "s_namahaktanah"
        ), "LEFT");
        $select->join(
            array("e" => "t_pembayaranspt"),
            new \Zend\Db\Sql\Expression("e.t_idspt = a.t_idspt"),
            array(
                "t_idpembayaranspt", "t_kohirpembayaran", "t_periodepembayaran",
                "t_tanggalpembayaran", "t_idnotaris", "t_nilaipembayaranspt",
                "t_kodebayarspt", "t_verifikasispt", "t_tglverifikasispt",
                "t_pejabatverifikasispt", "t_statusbayarspt", "t_ketetapanspt",
                "t_kodebayarbanksppt"
            ),
            "LEFT"
        );
        $select->join(
            array("f" => "t_pemeriksaan"),
            new Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"),
            array(
                "p_kohirskpdkb", "p_luastanah", "p_luasbangunan", "p_njoptanah", "p_njopbangunan",
                "p_totalnjoptanah", "p_totalnjopbangunan", "p_grandtotalnjop", "p_nilaitransaksispt",
                "p_potonganspt", "p_ketwaris", "p_terbukti", "p_idjenistransaksi", "p_idjenishaktanah",
                "p_totalspt", "p_nilaipembayaranspt", "p_nilaikurangbayar", "p_idpemeriksaan"
            ),
            "LEFT"
        );
        $select->join(
            array("g" => "s_users"),
            new \Zend\Db\Sql\Expression("g.s_iduser = a.t_idnotarisspt"),
            array(
                "t_idnotarisspt" => new Expression("g.s_iduser")
            ),
            "LEFT"
        );
        $select->join(array("h" => "s_notaris"), new \Zend\Db\Sql\Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(
            "s_namanotaris", "s_alamatnotaris"
        ), "LEFT");
        $where = new Where();
        $where->literal("((b.t_tglajbbaru IS NOT NULL) AND (b.t_noajbbaru IS NOT NULL))");
        $select->where($where);

        return $select;
    }

    public function fr_pembayaran_v5($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            //            "" => new Expression(""),
            "t_idspt" => new Expression("a.t_idspt"),
            "jml_syarat_input" => new Expression("(length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) "),
            "jml_syarat_validasi" => new Expression("(length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_sebenarnya" => new Expression("n.jmlsyarat"),
            "status_pendaftaran" => new Expression("(CASE
            WHEN ((length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            ELSE 2
        END)"),
            "status_validasi" => new Expression("(CASE
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL) THEN 3
            ELSE 2
        END)"),
            "t_kohirspt" => new Expression("a.t_kohirspt"),
            "s_idjenistransaksi" => new Expression("c.s_idjenistransaksi"),
            "t_periodespt" => new Expression("a.t_periodespt"),
            "t_tglprosesspt" => new Expression("a.t_tglprosesspt"),
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_statusbayarspt" => new Expression("(CASE
            WHEN ((e.t_statusbayarspt)::text = 'true'::text) THEN 'TRUE'::text
            ELSE 'FALSE'::text
        END )"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "t_persyaratan" => new Expression("a.t_persyaratan"),
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_idjenistransaksi" => new Expression("a.t_idjenistransaksi"),
            "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "t_ketetapanspt" => new Expression("e.t_ketetapanspt"),
            "t_ketetapands" => new Expression("i.t_ketetapands"),
            "t_kohirds" => new Expression("i.t_kohirds"),
            "notarisds" => new Expression("k.s_namanotaris"),
            "t_periodepembayaran" => new Expression("e.t_periodepembayaran"),
            "t_tanggalpembayaran" => new Expression("e.t_tanggalpembayaran"),
            "t_nilaipembayaranspt" => new Expression("e.t_nilaipembayaranspt"),
            "t_idnotarisspt" => new Expression("a.t_idnotarisspt"),
            "t_totalspt" => new Expression("a.t_totalspt"),
            "p_totalspt" => new Expression("f.p_totalspt"),
            "jml_pajak_v1" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_totalspt
            ELSE a.t_totalspt
        END)"),
            "jml_pajak_v2" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN
            CASE
                WHEN (a.t_totalspt < f.p_totalspt) THEN f.p_totalspt
                ELSE a.t_totalspt
            END
            ELSE a.t_totalspt
        END)"),
            "s_idpejabat_idnotaris" => new Expression("g.s_idpejabat_idnotaris"),
            "s_namanotaris" => new Expression("h.s_namanotaris"),
            "t_noajbbaru" => new Expression("b.t_noajbbaru"),
            "t_tglajbbaru" => new Expression("b.t_tglajbbaru"),
            "t_iddetailsptbphtb" => new Expression("b.t_iddetailsptbphtb"),
            "t_luastanahbpn" => new Expression("b.t_luastanahbpn"),
            "t_luasbangunanbpn" => new Expression("b.t_luasbangunanbpn"),
            "p_luastanah" => new Expression("f.p_luastanah"),
            "p_luasbangunan" => new Expression("f.p_luasbangunan"),
            "t_luastanah" => new Expression("b.t_luastanah"),
            "t_luasbangunan" => new Expression("b.t_luasbangunan"),
            "luas_tanah" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luastanah
            ELSE b.t_luastanah
        END)"),
            "luas_bangunan" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luasbangunan
            ELSE b.t_luasbangunan
        END)"),
            "t_tglsertifikatbaru" => new Expression("b.t_tglsertifikatbaru"),
            "t_nosertifikatbaru" => new Expression("b.t_nosertifikatbaru"),
            "t_kohirketetapanspt" => new Expression("a.t_kohirketetapanspt"),
            "t_nopbphtbsppt" => new Expression("a.t_nopbphtbsppt"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "t_nilaitransaksispt" => new Expression("a.t_nilaitransaksispt"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),
            "fr_luas_tanah_bpn" => new Expression("b.fr_luas_tanah_bpn"),
            "fr_luas_bangunan_bpn" => new Expression("b.fr_luas_bangunan_bpn"),
            "fr_tervalidasidua" => new Expression("a.fr_tervalidasidua"),
            "fr_validasidua" => new Expression("b.fr_validasidua"),
            "p_grandtotalnjop" => new Expression("f.p_grandtotalnjop"),
            "p_grandtotalnjop_aphb" => new Expression("f.p_grandtotalnjop_aphb"),
            "p_nilaitransaksispt" => new Expression("f.p_nilaitransaksispt"),
            "t_alamatwppembeli" => new Expression("b.t_alamatwppembeli"),
            "t_nikwppembeli" => new Expression("b.t_nikwppembeli"),
            "t_tarif_pembagian_aphb_kali" => new Expression("a.t_tarif_pembagian_aphb_kali"),
            "t_tarif_pembagian_aphb_bagi" => new Expression("a.t_tarif_pembagian_aphb_bagi"),
            "t_grandtotalnjop_aphb" => new Expression("b.t_grandtotalnjop_aphb"),
            "t_idsptsebelumnya" => new Expression("a.t_idsptsebelumnya"),
            "t_alamatwppenjual" => new Expression("b.t_alamatwppenjual"),
            "t_keteranganvalidasi" => new Expression("e.t_keteranganvalidasi"),
            "t_pejabatverifikasispt" => new Expression("e.t_pejabatverifikasispt")
        ));
        $select->from(array("e" => "t_pembayaranspt"));
        $select->join(array("a" => "t_spt"), new Expression("e.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("b" => "t_detailsptbphtb"), new Expression("b.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("c" => "s_jenistransaksi"), new Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"), array(), "LEFT");
        $select->join(array("f" => "t_pemeriksaan"), new Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"), array(), "LEFT");
        $select->join(array("i" => "t_dendasanksinotaris"), new Expression("i.t_idds = e.t_idds"), array(), "LEFT");
        $select->join(array("g" => "s_users"), new Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("j" => "s_users"), new Expression("j.s_iduser = i.t_idnotaris"), array(), "LEFT");
        $select->join(array("k" => "s_notaris"), new Expression("(k.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("n" => "fr_count_s_persyaratan"), new Expression("a.t_idjenistransaksi = n.s_idjenistransaksi"), array(), "LEFT");

        if ($string != null && $string == 1) {
            return $select->getSqlString();
        } else {
            return $select;
        }
    }
}
