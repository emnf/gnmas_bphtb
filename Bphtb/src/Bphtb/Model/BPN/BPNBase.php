<?php
namespace Bphtb\Model\BPN;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BPNBase implements InputFilterAwareInterface {

    public $bpn_id, $nomor_akta, $tgl_akta, $nama_ppat, $nop, $ntpd, $nomor_induk_bidang,
            $koordinat_x, $koordinat_y, $nik, $npwp, $nama_wp, $kelurahan_op, $kecamatan_op,
            $kab_kota_op, $luastanah_op, $jenis_hak, $tgl_transaksi;

    public function exchangeArray($data) {
        $this->bpn_id = $data['bpn_id'];
        $this->nomor_akta = $data['nomor_akta'];
        $this->tgl_akta = $data['tanggal_akta'];
        $this->nama_ppat = $data['nama_ppat'];
        $this->nop = $data['nop'];
        $this->ntpd = $data['ntpd'];
        $this->nomor_induk_bidang = $data['nomor_induk_bidang'];
        $this->koordinat_x = $data['koordinat_x'];
        $this->koordinat_y = $data['koordinat_y'];
        $this->nik = $data['nik'];
        $this->npwp = $data['npwp'];
        $this->nama_wp = $data['nama_wp'];
        $this->kelurahan_op = $data['kelurahan_op'];
        $this->kecamatan_op = $data['kecamatan_op'];
        $this->kab_kota_op = $data['kota_op'];
        $this->luastanah_op = $data['luastanah_op'];
        $this->jenis_hak = $data['jenis_hak'];
        $this->tgl_transaksi = $data['tgl_transaksi'];
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not Used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
