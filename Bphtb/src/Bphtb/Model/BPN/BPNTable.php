<?php
namespace Bphtb\Model\BPN;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;


class BPNTable extends AbstractTableGateway {

    protected $table = 'bpn';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new BPNBase());
        $this->initialize();
    }

    public function save(BPNBase $base) {
        $data = [
            'nomor_akta' => $base->nomor_akta,
            'tgl_akta' => date('Y-m-d', strtotime($base->tgl_akta)),
            'nama_ppat' => $base->nama_ppat,
            'nop' => $base->nop,
            'ntpd' => $base->ntpd,
            'nomor_induk_bidang' => $base->nomor_induk_bidang,
            'koordinat_x' => $base->koordinat_x,
            'koordinat_y' => $base->koordinat_y,
            'nik' => $base->nik,
            'npwp' => $base->npwp,
            'nama_wp' => $base->nama_wp,
            'kelurahan_op' => $base->kelurahan_op,
            'kecamatan_op' => $base->kecamatan_op,
            'kab_kota_op' => $base->kab_kota_op,
            'luastanah_op' => $base->luastanah_op,
            'jenis_hak' => $base->jenis_hak,
            'tgl_transaksi' => $base->tgl_transaksi
        ];

        return $this->insert($data);
    }

    public function save2(BPNBase $base){
        $data = [
            'nomor_akta' => $base->nomor_akta,
            'tgl_akta' => $base->tgl_akta,
            'nama_ppat' => $base->nama_ppat,
            'nop' => $base->nop,
            'ntpd' => $base->ntpd,
            'nomor_induk_bidang' => $base->nomor_induk_bidang,
            'koordinat_x' => $base->koordinat_x,
            'koordinat_y' => $base->koordinat_y,
            'nik' => $base->nik,
            'npwp' => $base->npwp,
            'nama_wp' => $base->nama_wp,
            'kelurahan_op' => $base->kelurahan_op,
            'kecamatan_op' => $base->kecamatan_op,
            'kab_kota_op' => $base->kab_kota_op,
            'luastanah_op' => $base->luastanah_op,
            'jenis_hak' => $base->jenis_hak,
            'tgl_transaksi' => $base->tgl_transaksi
        ];

        return $this->insert($data);
    }

    public function clearByDate($date){
        $date = date('Y-m-d', strtotime($date));
        return $this->delete(['tgl_transaksi' => $date]);
    }

    public function getDataById($bpn_id)
    {
        $rowset = $this->select(array('bpn_id' => $bpn_id));
        return $rowset->current();
    }

    public function getDataByDate($tgl_awal, $tgl_akhir)
    {
        $sql = "SELECT * FROM bpn WHERE tgl_transaksi BETWEEN '$tgl_awal' and '$tgl_akhir' ORDER BY tgl_transaksi";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getDataByTglProses($tgl)
    {
        $sql = "SELECT * FROM bpn WHERE tgl_transaksi = '$tgl' ORDER BY tgl_transaksi";
        $statement = $this->adapter->query($sql);
        return $statement->execute();
    }

    public function getAkunBpn() {
        $sql = "select * from s_akunbpn_pemda";
        $statement = $this->adapter->query($sql);
        $row = $statement->execute()->current();
        return $row;
    }
}