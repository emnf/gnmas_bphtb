<?php

namespace Bphtb\Model\BPN;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;


class BPNAkunTable extends AbstractTableGateway
{

    protected $table = 's_akunbpn_pemda';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new BPNAkunBase());
        $this->initialize();
    }

    public function getdata()
    {
        $rowset = $this->select();
        return $rowset->current();
    }

    public function savedata(BPNAkunBase $base)
    {
        $data = array(
            "username" => $base->username,
            "password" => $base->password

        );

        $id = (int) $base->akunbpn_id;
        if ($id == 0) {
            $this->insert($data);
        } else {
            $this->update($data, array("akunbpn_id" => $base->akunbpn_id));
        }
    }

    public function getAkunBpn()
    {
        $sql = "select * from s_akunbpn_pemda";
        $statement = $this->adapter->query($sql);
        $row = $statement->execute()->current();
        return $row;
    }
}
