<?php

//modul pembayaran

namespace Bphtb\Model\Pembayaran;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

class PembayaranSptTable extends AbstractTableGateway
{

    protected $table = 't_pembayaranspt', $tableview = 'view_sspd_semua_pembayaran';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new PembayaranSptBase());
        $this->initialize();
    }

    //================= datagrid pembayaran
    public function getjumlahdata($sTable, $count, $sWhere)
    {
        $sql = "SELECT " . $count . " FROM " . $sTable . "" . $sWhere;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute()->count();
        return $res;
    }

    public function semuadatapembayaran($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {
        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }

        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }

                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND t_tanggalpembayaran IS NOT NULL";
        } else {
            $sWhere = " WHERE t_tanggalpembayaran IS NOT NULL";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }

        if ($sTable == "fr_pembayaran_v5") {
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM (" . $this->fr_pembayaran_v5(1) . ") as fr_pembayaran_v5 " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
            $sTable = "(" . $this->fr_pembayaran_v5(1) . ") as fr_pembayaran_v5 ";
        } else {
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
        }

        //        echo $sql;exit();

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();



        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }


            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            } else {
                $status_bayar = '<a class="btn btn-danger btn-sm_fr" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }



            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/tervalidasi.png">';
            } else {
                $status_verifikasi = '<img title="Belum di Validasi" width="20" height="20" src="' . $cekurl . '/public/img/belumdivalidasi.png">';
            }



            if ($aRow['t_ketetapanspt'] == 1) {
                $validasipembayaran = '<a href="pembayaran_sptbphtb/cetakbuktivalidasipembayaran?&action=cetakbuktivalidasipembayaran&t_idspt=' . $aRow['t_idspt'] . '" target="_blank"><i class="fa fa-fw fa-print"></i> Validasi Pembayaran</a>';
                $buktipembayaran = '<a href="pembayaran_sptbphtb/cetakbuktipembayaran?&action=cetakbuktipembayaran&t_idspt=' . $aRow['t_idspt'] . '" target="_blank"><i class="fa fa-fw fa-print"></i> Bukti Pembayaran</a>';
                $lihat = '<a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';
                $hapus = '<a style="background-color:red;color:#fff;" href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . '); return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
            } else {
                $validasipembayaran = ' ';
                $lihat = ' ';
                $buktipembayaran = '<a href="pembayaran_sptbphtb/cetakbuktipembayaran?&action=cetakbuktipembayaran&t_idspt=' . $aRow['t_idspt'] . '" target="_blank"><i class="fa fa-fw fa-print"></i> Bukti Pembayaran</a>';
                $hapus = '<a style="background-color:red;color:#fff;" href="#" onclick="hapus(' . $aRow['t_idpembayaranspt'] . ');updatepemeriksaan(' . $aRow['p_idpemeriksaan'] . ');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus</a>';
            }

            /*$btn = '<div class="btn-group">
                            <button aria-expanded="false" type="button" class="btn btn-info dropdown-toggle btn-sm" data-toggle="dropdown">
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" style="left:-120px; border-color: blue;">
                                 '.$validasipembayaran.'
                                 '.$buktipembayaran.'    
                                 '.$lihat.'
                                 '.$hapus.'   
                            </ul>
                          </div>';*/

            $btn = '<div class="dropdown">
                    <button onclick="myFunction(' . $aRow['t_idspt'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $aRow['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:-120px; border-color: blue;">
                                    ' . $validasipembayaran . '
                                 ' . $buktipembayaran . '    
                                 ' . $lihat . '
                                 ' . $hapus . '     
                    </div>
                  </div>';



            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            if (!empty($aRow['t_kohirketetapanspt'])) {
                $novalidasi = $aRow['t_kohirketetapanspt'];
            } else {
                $novalidasi = '';
            }

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $t_kohirspt . "</center>",
                "<center>" . $novalidasi . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",
                "<span style='float:right;'>" . date('d-m-Y', strtotime($aRow['t_tglverifikasispt'])) . "</span>",
                $aRow['t_namawppembeli'],
                "<span style='float:right;'>" . number_format($aRow['t_nilaipembayaranspt'], 0, ',', '.') . "</span>",
                "<center>" . $aRow['t_kodebayarbanksppt'] . "</center>",
                "<center>" . $status_bayar . "</center>",
                "<center>" . $status_verifikasi . "</center>",
                "<center>" . $btn . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }

    public function semuadatatervalidasi($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    if ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }
                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND t_tanggalpembayaran IS NULL AND t_kodebayarbanksppt IS NOT NULL AND status_validasi = 1";
        } else {
            $sWhere = " WHERE t_tanggalpembayaran IS NULL AND t_kodebayarbanksppt IS NOT NULL AND status_validasi = 1";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }

        if ($sTable == "fr_pembayaran_v5") {
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM (" . $this->fr_pembayaran_v5(1) . ") as fr_pembayaran_v5 " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
            $sTable = "(" . $this->fr_pembayaran_v5(1) . ") as fr_pembayaran_v5 ";
        } else {
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
        }


        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();

        //var_dump($sql);
        //exit();

        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            /*
            if ($aRow['status_pendaftaran'] == 1) {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Lengkap" width="20" height="20" src="'.$cekurl.'/public/img/syaratlengkap.png">';
            }else {
                $status_pendaftaran = '<img title="Syarat Pendaftaran Tidak Lengkap" width="20" height="20" src="'.$cekurl.'/public/img/syarattidaklengkap.png">';
            }
            
            
            if ($aRow['t_statusbayarspt'] == 'TRUE') {
                $status_bayar = '<a class="btn btn-success btn-sm" style="cursor:default;"><i class="fa fa-fw fa-money"></i> SUDAH</a>';
            }else {
                $status_bayar = '<a class="btn btn-danger btn-sm" style="cursor:default;"><i class="fa fa-fw fa-money"></i> BELUM</a>';
            }
            
            if(($session['s_namauserrole'] == "Administrator")){
                $admin_hapus = '<li style="background-color:red;"><a style="color:#fff;" href="#" onclick="hapusall('. $aRow['t_idspt'].');return false;"><i class="fa fa-fw fa-bitbucket"></i> Hapus Semua</a></li>';
            }else{
                $admin_hapus = '';
            }
            
            
            
            if (($aRow['status_pendaftaran'] == 1) && ($aRow['status_validasi'] == 1)) {
                $batal = '';
                $cetaksurat = '';
                $status_verifikasi = '<img title="Data sudah di Validasi" width="20" height="20" src="'.$cekurl.'/public/img/tervalidasi.png">';
                if ($aRow['t_statusbayarspt'] == 'TRUE') {
                    $edit = '';
                } else {
                    $edit = '<li><a href="verifikasi_spt/edit?t_idpembayaranspt='. $aRow['t_idpembayaranspt'] .'"><i class="fa fa-fw fa-edit"></i> Edit</a></li>';
                }
                
            }else {
                $status_verifikasi = '<img title="Belum di Validasi" width="20" height="20" src="'.$cekurl.'/public/img/belumdivalidasi.png">';
                    
                $cetaksurat = '<a href="#" onclick="openCetakBukti('. $aRow['t_kohirspt'].');return false;">Surat Pemberitahuan</a>';
                
                
                if ($aRow['t_inputbpn'] == true) {
                    $edit = '<li><a href="verifikasi_spt/edit?t_idpembayaranspt='. $aRow['t_idpembayaranspt'].'"><i class="fa fa-fw fa-edit"></i> Edit</a></li>';
                    $batal = '';
                } else {
                    $edit = '<li><a href="verifikasi_spt/edit?t_idpembayaranspt='. $aRow['t_idpembayaranspt'].'"><i class="fa fa-fw fa-edit"></i> Edit</a></li>';
                    $batal = '<li><a href="#" onclick="hapus('.$aRow['t_idpembayaranspt'].');return false;">Batal</a></li>';
                }
            }
            
            if(!empty($aRow['p_idpemeriksaan'])) {
                $jmlpajak = "<span style='float:right;color:blue;'>".number_format($aRow['p_totalspt'], 0, ',', '.')."</span>";
                $cetaksuratpenelitian = '<li><a href="#" onclick="openCetakPenelitian('. $aRow['t_kohirspt'].');return false;"><i class="fa fa-fw fa-print"></i> Surat Penelitian</a></li>';
            
            }else {
                $jmlpajak = "<span style='float:right;'>".number_format($aRow['t_totalspt'], 0, ',', '.')."</span>";
                $cetaksuratpenelitian = '';
            }  
            
            */

            $btn = '<a href="#" class="btn btn-block btn-info btn-sm" onclick="pilihPendataanSspdBphtb(' . $aRow['t_idspt'] . ');return false;"><i class="fa fa-fw fa-hand-pointer-o"></i> PILIH</a>';

            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;">' . $aRow['t_kohirspt'] . '</span>';
            } else {
                $t_kohirspt = $aRow['t_kohirspt'];
            }




            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $t_kohirspt . "</center>",
                "<center>" . $aRow['s_namajenistransaksi'] . "</center>",

                "<center>" . date('d-m-Y', strtotime($aRow['t_tglprosesspt'])) . "</center>",
                "<center>" . $aRow['t_kodebayarbanksppt'] . "</center>",
                $aRow['t_namawppembeli'],

                $aRow['t_namawppenjual'],
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",

                "<span style='float:right;'>" . number_format($aRow['jml_pajak_v1'], 0, ',', '.') . "</span>",
                "<center>" . $btn . "</center>"
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }


    public function semuadatabpn($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }

        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }

        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules);
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }


        $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit;
        // var_dump($sql); die;

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();

        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];

        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );

        foreach ($rResult as $aRow) {
            $row = array();

            $btn = '<div class="dropdown">
                        <button onclick="myFunction(' . $aRow['bpn_id'] . ')" class="dropbtn btn-info dropdown-toggle btn-sm_fr">&nbsp;&nbsp;&nbsp;<span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                        <div id="myDropdown' . $aRow['bpn_id'] . '" class="dropdown-content dropdown-menu" style="left:10px; border-color: blue;">
                        <a href="javascript:void(0)" onclick="openDetile(' . $aRow['bpn_id'] . ');"><i class="fa fa-fw fa-info"></i> Detile</a>
                        </div>
                    </div>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            $tgl_transaksi = ($aRow['tgl_transaksi'] == null) ? '-' : date('d-m-Y', strtotime($aRow['tgl_transaksi']));

            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $btn . "</center>",
                "<center>" . date('d-m-Y', strtotime($aRow['tgl_transaksi'])) . "</center>",
                "<center>" . $aRow['nomor_akta'] . "</center>",
                $aRow['nama_ppat'],
                "<center>" . $aRow['nop'] . "</center>",
                "<center>" . $aRow['jenis_hak'] . "</center>",
                $aRow['nama_wp'],
                "<center>" . $aRow['kelurahan_op'] . "</center>",
                "<center>" . $aRow['kecamatan_op'] . "</center>",
                "<center>" . $aRow['kab_kota_op'] . "</center>"                
            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }


    public function semuadatakpppratama($sTable, $count, $input, $order_default, $aColumns, $session, $cekurl)
    {

        $aOrderingRules = array();
        $sLimit = "";
        if ($input->getPost('iDisplayStart') && $input->getPost('iDisplayLength') != '-1') {
            $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
            //var_dump($sLimit);
            //exit();
            $no = 1 + intval($input->getPost('iDisplayStart'));
        } else {
            if (intval($input->getPost('iDisplayLength')) >= 1) {
                $sLimit = " LIMIT " . intval($input->getPost('iDisplayLength')) . " OFFSET " . intval($input->getPost('iDisplayStart'));
                $no = 1 + intval($input->getPost('iDisplayStart'));
            } else {
                $sLimit = " LIMIT 10 OFFSET 0";
                $no = 1;
            }
        }


        $aOrderingRules = array();
        if ($input->getPost('iSortCol_0')) {
            $iSortingCols = intval($input->getPost('iSortingCols'));
            for ($i = 0; $i < $iSortingCols; $i++) {
                if ($input->getPost('bSortable_' . intval($input->getPost('iSortCol_' . $i))) == 'true') {
                    $aOrderingRules[] = " " . $aColumns[intval($input->getPost('iSortCol_' . $i))] . "  "
                        . ($input->getPost('sSortDir_' . $i) === 'asc' ? 'asc' : 'desc');
                }
            }
        }

        if (!empty($aOrderingRules)) {
            $sOrder = " ORDER BY " . implode(", ", $aOrderingRules);
        } else {
            $sOrder = " ORDER BY " . $order_default . "";
        }

        $iColumnCount = count($aColumns);

        if ($input->getPost('sSearch') && $input->getPost('sSearch') != "") {
            $aFilteringRules = array();
            for ($i = 0; $i < $iColumnCount; $i++) {
                if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true') {
                    $tanggal = explode('-', $input->getPost('sSearch'));
                    if (count($tanggal) > 1) {
                        if (count($tanggal) > 2) {
                            $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        } else {
                            $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                            $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                        }
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch') . "%'";
                    }
                }
            }
            if (!empty($aFilteringRules)) {
                $aFilteringRules = array('(' . implode(" OR ", $aFilteringRules) . ')');
            }
        }


        for ($i = 0; $i < $iColumnCount; $i++) {
            if ($input->getPost('bSearchable_' . $i) && $input->getPost('bSearchable_' . $i) == 'true' && $input->getPost('sSearch_' . $i) != '') {
                $tanggal = explode('-', $input->getPost('sSearch_' . $i));

                if (count($tanggal) > 1) {
                    if (count($tanggal) > 2) {
                        $tanggalcari = "" . $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    } else {
                        $tanggalcari = "" . $tanggal[1] . "-" . $tanggal[0] . "";
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $tanggalcari . "%'";
                    }
                } else {
                    if ($aColumns[$i] == 's_idjenistransaksi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_idnotarisspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 't_statusbayarspt') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } elseif ($aColumns[$i] == 'status_validasi') {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  = '" . $input->getPost('sSearch_' . $i) . "'";
                    } else {
                        $aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                    }

                    //$aFilteringRules[] = " " . $aColumns[$i] . "::text  ILIKE '%" . $input->getPost('sSearch_' . $i) . "%'";
                }

                $datacariall = $input->getPost('sSearch_' . $i);
            }
        }



        if (!empty($aFilteringRules)) {
            $sWhere = " WHERE " . implode(" AND ", $aFilteringRules) . " AND t_tanggalpembayaran IS NOT NULL";
        } else {
            $sWhere = " WHERE t_tanggalpembayaran IS NOT NULL";
        }

        $aQueryColumns = array();
        foreach ($aColumns as $col) {
            if ($col != ' ') {
                $aQueryColumns[] = $col;
            }
        }

        if ($sTable == "fr_pembayaran_v5") {
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM (" . $this->fr_pembayaran_v5(1) . ") as fr_pembayaran_v5 " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
            $sTable = "(" . $this->fr_pembayaran_v5(1) . ") as fr_pembayaran_v5 ";
        } else {
            $sql = "SELECT " . implode(", ", $aQueryColumns) . "
                        FROM " . $sTable . " " . $sWhere . $sOrder . $sLimit; //count(*) OVER() AS SQL_CALC_FOUND_ROWS, 
        }

        //var_dump($sql);
        //exit();

        $statement = $this->adapter->query($sql);
        $rResult = $statement->execute();



        $totaldata = $this->getjumlahdata($sTable, $count, $sWhere);
        $iTotal = $totaldata; //$totaldata['COUNT('.$count.')'];



        $output = array(
            "sEcho" => intval($input->getPost('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array(),
        );



        foreach ($rResult as $aRow) {
            $row = array();

            //$btn = '<a class="btn btn-success btn-xs" href="#" onClick="showModals(\'' . $aRow['t_idspt'] . '\')" title="Edit"><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" href="#" onClick="deleteUser(\'' . $aRow['t_idspt'] . '\')" title="Hapus"><i class="fa fa-bitbucket"></i></a>';
            for ($i = 0; $i < $iColumnCount; $i++) {
                $row[] = $aRow[$aColumns[$i]];
            }

            if ($aRow['t_inputbpn'] == true) {
                $t_kohirspt = '<span class="badge" style="background-color:#CC0000;"><a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a></span>';
            } else {
                $t_kohirspt = '<a href="pembayaran_sptbphtb/viewdata?t_idspt=' . $aRow['t_idspt'] . '">' . $aRow['t_kohirspt'] . '</a>';
            }

            $aphb_kali = $aRow['t_tarif_pembagian_aphb_kali'];
            $aphb_bagi = $aRow['t_tarif_pembagian_aphb_bagi'];

            if (!empty($aRow['p_idpemeriksaan'])) {

                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    if ($aRow['p_grandtotalnjop'] > $aRow['p_nilaitransaksispt']) {
                        $npop = $aRow['p_grandtotalnjop'];
                    } else {
                        $npop = $aRow['p_nilaitransaksispt'];
                    }
                } else {
                    if ($aRow['p_grandtotalnjop_aphb'] > $aRow['p_nilaitransaksispt']) {
                        $npop = $aRow['p_grandtotalnjop_aphb'];
                    } else {
                        $npop = $aRow['p_nilaitransaksispt'];
                    }
                }
            } else {
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    if ($aRow['t_grandtotalnjop'] > $aRow['t_nilaitransaksispt']) {
                        $npop = $aRow['t_grandtotalnjop'];
                    } else {
                        $npop = $aRow['t_nilaitransaksispt'];
                    }
                } else {
                    if ($aRow['t_grandtotalnjop_aphb'] > $aRow['t_nilaitransaksispt']) {
                        $npop = $aRow['t_grandtotalnjop_aphb'];
                    } else {
                        $npop = $aRow['t_nilaitransaksispt'];
                    }
                }
            }


            $row = array(
                "<center>" . $no . "</center>",
                "<center>" . $aRow['t_kohirspt'] . "</center>",
                "" . $aRow['s_namanotaris'] . "",
                "<center>" . date('d-m-Y', strtotime($aRow['t_tanggalpembayaran'])) . "</center>",
                "<center>" . $aRow['t_nopbphtbsppt'] . "</center>",
                $aRow['t_namawppembeli'],
                "" . $aRow['t_alamatwppembeli'] . "",
                "<center>" . $aRow['t_nikwppembeli'] . "</center>",
                "<span style='float:right;'>" . number_format($npop, 0, ',', '.') . "</span>",


            );
            $output['aaData'][] = $row;
            $no++;
        }

        return $output;
    }


    //================= end datagrid pembayaran

    public function temukanDataSspd(PembayaranSptBase $spt)
    {
        $sql = "select * from view_sspd_pembayaran where t_kohirspt::text='" . $spt->t_kohirpembayaran . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function temukanDataSkpdkb(PembayaranSptBase $spt)
    {
        $sql = "select * from view_sspd_validasi where p_kohirskpdkb::text='" . $spt->p_kohirskpdkb . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function cekJumlahhari()
    {
        $sql = "select * from s_tempo";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function cekValiditasSSPD(PembayaranSptBase $spt, $jumlahhari)
    {
        $sql = "select * from view_sspd where t_kohirspt=" . $spt->t_kohirpembayaran . " and t_tglprosesspt <= '" . date('Y-m-d', strtotime($spt->t_tanggalpembayaran)) . "' and (t_tglprosesspt + interval '$jumlahhari days') >= '" . date('Y-m-d', strtotime($spt->t_tanggalpembayaran)) . "'";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    //Simpan Pembayaran SSPD / SKPDKB
    //Lokasi : Tambah Pembayaran
    public function savedataPembayaran(PembayaranSptBase $pembspt, $iduser)
    {
        $data = array(
            't_idspt' => $pembspt->t_idspt,
            't_periodepembayaran' => date('Y', strtotime($pembspt->t_tanggalpembayaran)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($pembspt->t_tanggalpembayaran)),
            't_nilaipembayaranspt' => str_ireplace(".", "", $pembspt->t_nilaipembayaranspt),
            't_statusbayarspt' => true,
            't_idpenerimasetoran' => $iduser
        );
        $this->insert($data);
    }

    public function updatedatapembayaranTrue(PembayaranSptBase $pembspt, $iduser)
    {
        // var_dump( $this->getnopembayaran(date('Y', strtotime($pembspt->t_tanggalpembayaran))));exit();
        $data = array(
            't_statusbayarspt' => true,
            't_periodepembayaran' => date('Y', strtotime($pembspt->t_tanggalpembayaran)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($pembspt->t_tanggalpembayaran)),
            't_ketetapanspt' => 1,
            't_nilaipembayaranspt' => str_ireplace(".", "", $pembspt->t_nilaipembayaranspt),
            't_idpenerimasetoran' => $iduser,
            't_pejabatpembayaranspt' => $iduser,
            't_tglpembayaran_system' => date('Y-m-d H:i:s'),
            't_reffpembayaran' => "7106" . date("ymdHis") . $this->generateRandomString(16),
            't_kodebankbayar' => '01',
            't_bankbayar' => 'BENDAHARA PENERIMA',
            't_viapembayaran' => 1,
            't_kohirpembayaran' => $this->getnopembayaran(date('Y', strtotime($pembspt->t_tanggalpembayaran)))
        );
        // var_dump($data);exit();
        $t_pembayaranspt = new \Zend\Db\TableGateway\TableGateway('t_pembayaranspt', $this->adapter);
        $t_pembayaranspt->update($data, array('t_idpembayaranspt' => $pembspt->t_idpembayaranspt));

        // echo \Zend\Debug\Debug::dump($t_pembayaranspt, $label = null, $echo = false); //$t_pembayaranspt->getSqlString();
    }

    public function generateRandomString($length)
    {
        $characters = '0123456789'; //abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getnopembayaran($periode = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_kohirpembayaran" => new Expression("(COALESCE(MAX(t_kohirpembayaran),0) + 1)")
        ));
        $select->from('t_pembayaranspt');
        $where = new Where();
        if ($periode != null) {
            $where->literal("t_periodepembayaran='" . $periode . "'");
        } else {
            $where->literal("t_periodepembayaran='" . date("Y") . "'");
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute()->current();
        return $res['t_kohirpembayaran'];
    }

    public function updatedatapemeriksaanTrue(PembayaranSptBase $pembspt)
    {
        $data = array(
            'p_pembayaranskpdkb' => true
        );
        $t_pemeriksaan = new \Zend\Db\TableGateway\TableGateway('t_pemeriksaan', $this->adapter);
        $t_pemeriksaan->update($data, array('p_idpemeriksaan' => $pembspt->p_idpemeriksaan));
    }

    public function updatedatapemeriksaanFalse(PembayaranSptBase $pembspt)
    {
        $data = array(
            'p_pembayaranskpdkb' => false
        );
        $t_pemeriksaan = new \Zend\Db\TableGateway\TableGateway('t_pemeriksaan', $this->adapter);
        $t_pemeriksaan->update($data, array('p_idpemeriksaan' => $pembspt->p_idpemeriksaan));
    }

    //Simpan Pembayaran SSPD / SKPDKB
    //Lokasi : Tambah Pembayaran
    public function savedataPembayaranDenda(PembayaranSptBase $pembspt)
    {
        $data = array(
            't_idds' => $pembspt->t_idds,
            't_periodepembayaran' => date('Y', strtotime($pembspt->t_tanggalpembayaran)),
            't_tanggalpembayaran' => date('Y-m-d', strtotime($pembspt->t_tanggalpembayaran)),
            't_nilaipembayaranspt' => str_ireplace(".", "", $pembspt->t_jumlahds),
            't_kodebayarbanksppt' => $pembspt->t_kodebayards,
            't_statusbayarspt' => true,
        );
        $this->insert($data);
    }

    //jumlah SSPD dan SKPDKB yang dibayarkan
    //Lokasi : Index Pembayaran
    public function getGridCount(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        //$select->from('view_sspd_semua_pembayaran');
        $view_sspd_semua_pembayaran = $this->view_sspd_semua_pembayaran();
        $select->from(array("za" => $view_sspd_semua_pembayaran));
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodepembayaran != 'undefined')
            $where->equalTo("t_periodepembayaran", $base->t_periodepembayaran);
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_kodebayarbanksppt != 'undefined')
            $where->literal("t_kodebayarbanksppt::text LIKE '%$base->t_kodebayarbanksppt%'");
        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        if ($base->t_nilaipembayaranspt != 'undefined')
            $where->literal("t_nilaipembayaranspt::text LIKE '%$base->t_nilaipembayaranspt%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    //Data SSPD dan SKPDKB yang dibayarkan
    //Lokasi : Index Pembayaran
    public function getGridData(PembayaranSptBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        //$select->from('view_sspd_semua_pembayaran');
        $view_sspd_semua_pembayaran = $this->view_sspd_semua_pembayaran();
        $select->from(array("za" => $view_sspd_semua_pembayaran));
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodepembayaran != 'undefined')
            $where->equalTo("t_periodepembayaran", $base->t_periodepembayaran);
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_kodebayarbanksppt != 'undefined')
            $where->literal("t_kodebayarbanksppt::text LIKE '%$base->t_kodebayarbanksppt%'");
        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        if ($base->t_nilaipembayaranspt != 'undefined')
            $where->literal("t_nilaipembayaranspt::text LIKE '%$base->t_nilaipembayaranspt%'");
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->order("t_tanggalpembayaran DESC");
        $select->limit($base->rows = (int)$base->rows);
        $select->offset($offset = (int)$offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getRealisasiAnggaran()
    {
        $sql = "select sum(t_nilaipembayaranspt) from t_pembayaranspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    //Jumlah Data Verifikasi
    //Lokasi : Index Verifikasi
    public function getGridCountVerifikasi(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->isNotNull('t_tglverifikasispt');
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglverifikasispt != 'undefined')
            $where->equalTo("t_tglverifikasispt", date('Y-m-d', strtotime($base->t_tglverifikasispt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        if ($base->t_kodebayarbanksppt != 'undefined')
            $where->literal("t_kodebayarbanksppt::text LIKE '%$base->t_kodebayarbanksppt%'");
        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    //Data Verifikasi
    //Lokasi : Index Verifikasi
    public function getGridDataVerifikasi(PembayaranSptBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->isNotNull('t_tglverifikasispt');
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodespt != 'undefined')
            $where->equalTo("t_periodespt", $base->t_periodespt);
        if ($base->t_tglverifikasispt != 'undefined')
            $where->equalTo("t_tglverifikasispt", date('Y-m-d', strtotime($base->t_tglverifikasispt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->s_namajenistransaksi != 'undefined')
            $where->literal("s_namajenistransaksi::text LIKE '%$base->s_namajenistransaksi%'");
        if ($base->t_kodebayarbanksppt != 'undefined')
            $where->literal("t_kodebayarbanksppt::text LIKE '%$base->t_kodebayarbanksppt%'");
        if ($base->t_statusbayarspt == 'FALSE') {
            $where->isNull("t_statusbayarspt");
        } else {
            if ($base->t_statusbayarspt != 'undefined')
                $where->equalTo("t_statusbayarspt", $base->t_statusbayarspt);
        }
        $select->where($where);
        $select->order("t_kohirspt DESC");
        $select->limit($base->rows = (int)$base->rows);
        $select->offset($offset = (int)$offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    public function getDataId($id)
    {
        $rowset = $this->select(function (Select $select) use ($id) {
            $select->where(array('t_pembayaranspt.t_idpembayaranspt' => $id));
            $select->join('view_sspd', 't_pembayaranspt.t_idspt = view_sspd.t_idspt');
            $select->join('view_sspd_semua_pembayaran', 'view_sspd.t_idspt = view_sspd_semua_pembayaran.t_idspt');
        });
        $row = $rowset->current();
        return $row;
    }

    public function getDataIdEdit($t_idpembayaran)
    {


        /*$sql = new \Zend\Db\Sql\Sql ( $this->adapter );
		$select = $sql->select ();
		$select->from ( array (
				"a" => $this->tableview
		) );
                $where = new \Zend\Db\Sql\Where ();
		$where->equalTo ( "t_idpembayaranspt", $t_idpembayaran );
                $select->where ( $where );
		$state = $sql->prepareStatementForSqlObject ( $select );
		$res = $state->execute ();
		return $res->current ();*/
        $sql = "select * from view_sspd_semua_pembayaran where t_idpembayaranspt=$t_idpembayaran";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getDataPemeriksaanId($t_idpembayaran)
    {
        //$sql = "select * from t_pemeriksaan where p_idpembayaranspt=$t_idpembayaran";
        $sql = "select a.*,c.t_tarif_pembagian_aphb_kali, c.t_tarif_pembagian_aphb_bagi from t_pemeriksaan a  
                    LEFT JOIN t_pembayaranspt b ON a.p_idpembayaranspt = b.t_idpembayaranspt
                    left join t_spt c on b.t_idspt = c.t_idspt
                where a.p_idpembayaranspt=$t_idpembayaran";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    //Hapus Data Pembayaran
    //Lokasi : Index Pembayaran
    public function hapusData(PembayaranSptBase $kb)
    {
        $data = array(
            't_statusbayarspt' => NULL,
            't_periodepembayaran' => NULL,
            't_tanggalpembayaran' => NULL,
            't_ketetapanspt' => NULL,
            't_nilaipembayaranspt' => NULL,
            't_idpenerimasetoran' => NULL,
            't_pejabatpembayaranspt' => NULL,
            't_tglpembayaran_system' => NULL,
            't_reffpembayaran' => NULL,
            't_kodebankbayar' => NULL,
            't_bankbayar' => NULL,
            't_viapembayaran' => NULL,
            't_kohirpembayaran' => NULL
        );

        $bayar = $this->update($data, array('t_idpembayaranspt' => $kb->t_idpembayaranspt));
        if ($bayar) {
            return true;
        }
        return false;
    }

    public function getDataPembayaran($t_idspt)
    {
        $sql = "select * from t_pembayaranspt where t_idspt=$t_idspt";
        $statement = $this->adapter->query($sql);
        return $statement->execute()->current();
    }

    public function getGridCountBpn(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $select->where("t_tanggalpembayaran is not null");
        $where->equalTo('t_ketetapanspt', '1');
        $where->isNull('t_inputbpn');
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodepembayaran != 'undefined')
            $where->equalTo("t_periodepembayaran", $base->t_periodepembayaran);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_tglverifikasispt != 'undefined')
            $where->equalTo("t_tglverifikasispt", date('Y-m-d', strtotime($base->t_tglverifikasispt)));
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_luastanah != 'undefined')
            $where->literal("t_luastanah::text LIKE '%$base->t_luastanah%'");
        if ($base->t_luasbangunan != 'undefined')
            $where->literal("t_luasbangunan::text LIKE '%$base->t_luasbangunan%'");
        if ($base->t_luastanahbpn != 'undefined')
            $where->literal("t_luastanahbpn::text LIKE '%$base->t_luastanahbpn%'");
        if ($base->t_luasbangunanbpn != 'undefined')
            $where->literal("t_luasbangunanbpn::text LIKE '%$base->t_luasbangunanbpn%'");
        if ($base->t_nosertifikatbaru != 'undefined')
            $where->literal("t_nosertifikatbaru::text LIKE '%$base->t_nosertifikatbaru%'");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getGridDataBpn(PembayaranSptBase $base, $offset)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $select->where("t_tanggalpembayaran is not null");
        $where->equalTo('t_ketetapanspt', '1');
        $where->isNull('t_inputbpn');
        $kohir = (int)$base->t_kohirspt;
        if ($base->t_kohirspt != 'undefined')
            $where->literal("t_kohirspt::text LIKE '%$kohir%'");
        if ($base->t_periodepembayaran != 'undefined')
            $where->equalTo("t_periodepembayaran", $base->t_periodepembayaran);
        if ($base->t_tglprosesspt != 'undefined')
            $where->equalTo("t_tglprosesspt", date('Y-m-d', strtotime($base->t_tglprosesspt)));
        if ($base->t_namawppembeli != 'undefined')
            $where->literal("t_namawppembeli::text LIKE '%$base->t_namawppembeli%'");
        if ($base->t_tglverifikasispt != 'undefined')
            $where->equalTo("t_tglverifikasispt", date('Y-m-d', strtotime($base->t_tglverifikasispt)));
        if ($base->t_tanggalpembayaran != 'undefined')
            $where->equalTo("t_tanggalpembayaran", date('Y-m-d', strtotime($base->t_tanggalpembayaran)));
        if ($base->t_luastanah != 'undefined')
            $where->literal("t_luastanah::text LIKE '%$base->t_luastanah%'");
        if ($base->t_luasbangunan != 'undefined')
            $where->literal("t_luasbangunan::text LIKE '%$base->t_luasbangunan%'");
        if ($base->t_luastanahbpn != 'undefined')
            $where->literal("t_luastanahbpn::text LIKE '%$base->t_luastanahbpn%'");
        if ($base->t_luasbangunanbpn != 'undefined')
            $where->literal("t_luasbangunanbpn::text LIKE '%$base->t_luasbangunanbpn%'");
        if ($base->t_nosertifikatbaru != 'undefined')
            $where->literal("t_nosertifikatbaru::text LIKE '%$base->t_nosertifikatbaru%'");
        $select->where($where);
        $select->order("t_tanggalpembayaran DESC");
        $select->limit($base->rows = (int)$base->rows);
        $select->offset($offset = (int)$offset);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res;
    }

    //jumlah SSPD dan SKPDKB yang dibayarkan
    //Lokasi : Index Pembayaran
    public function getJumlahPembayaran(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahVerifikasi(PembayaranSptBase $base)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->isNotNull('t_tglverifikasispt');
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahVerifikasilogin($idlogin)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_pembayaran');
        $where = new Where();
        $where->isNotNull('t_tglverifikasispt');
        $where->literal("t_idnotarisspt = " . $idlogin . "");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahPembayaranlogin($idlogin)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $where->literal("t_idnotarisspt = " . $idlogin . "");
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getJumlahPembayaranlapor($role, $idlogin)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('view_sspd_semua_pembayaran');
        $where = new Where();
        $where->isNotNull("t_tanggalpembayaran");
        $where->isNotNull("t_noajbbaru");
        if ($role == 3) {
            $where->literal("t_idnotarisspt = " . $idlogin . "");
        } else {
        }
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->count();
    }

    public function getDataIdArray($t_idpembayaranspt)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from('t_pembayaranspt');
        $where = new Where();
        $where->equalTo("t_idpembayaranspt", (int)$t_idpembayaranspt);
        $select->where($where);
        $state = $sql->prepareStatementForSqlObject($select);
        $res = $state->execute();
        return $res->current();
    }

    public function view_sspd_semua_pembayaran()
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            "t_kodebayarbanksppt" => new Zend\Db\Sql\Expression("e.t_kodebayarbanksppt"),
            "t_namawppembeli" => new Zend\Db\Sql\Expression("b.t_namawppembeli"),
            "t_nikwppembeli" => new Zend\Db\Sql\Expression("b.t_nikwppembeli"),
            "t_alamatwppembeli" => new Zend\Db\Sql\Expression("b.t_alamatwppembeli"),
            "t_kecamatanwppembeli" => new Zend\Db\Sql\Expression("b.t_kecamatanwppembeli"),
            "t_kelurahanwppembeli" => new Zend\Db\Sql\Expression("b.t_kelurahanwppembeli"),
            "t_kabkotawppembeli" => new Zend\Db\Sql\Expression("b.t_kabkotawppembeli"),
            "t_telponwppembeli" => new Zend\Db\Sql\Expression("b.t_telponwppembeli"),
            "t_kodeposwppembeli" => new Zend\Db\Sql\Expression("b.t_kodeposwppembeli"),
            "t_namawppenjual" => new Zend\Db\Sql\Expression("b.t_namawppenjual"),
            "t_nikwppenjual" => new Zend\Db\Sql\Expression("b.t_nikwppenjual"),
            "t_alamatwppenjual" => new Zend\Db\Sql\Expression("b.t_alamatwppenjual"),
            "t_kecamatanwppenjual" => new Zend\Db\Sql\Expression("b.t_kecamatanwppenjual"),
            "t_kelurahanwppenjual" => new Zend\Db\Sql\Expression("b.t_kelurahanwppenjual"),
            "t_kabkotawppenjual" => new Zend\Db\Sql\Expression("b.t_kabkotawppenjual"),
            "t_telponwppenjual" => new Zend\Db\Sql\Expression("b.t_telponwppenjual"),
            "t_kodeposwppenjual" => new Zend\Db\Sql\Expression("b.t_kodeposwppenjual"),
            "t_npwpwppenjual" => new Zend\Db\Sql\Expression("b.t_npwpwppenjual"),
            "t_luastanah" => new Zend\Db\Sql\Expression("b.t_luastanah"),
            "t_njoptanah" => new Zend\Db\Sql\Expression("b.t_njoptanah"),
            "t_luasbangunan" => new Zend\Db\Sql\Expression("b.t_luasbangunan"),
            "t_njopbangunan" => new Zend\Db\Sql\Expression("b.t_njopbangunan"),
            "t_totalnjoptanah" => new Zend\Db\Sql\Expression("b.t_totalnjoptanah"),
            "t_totalnjopbangunan" => new Zend\Db\Sql\Expression("b.t_totalnjopbangunan"),
            "t_grandtotalnjop" => new Zend\Db\Sql\Expression("b.t_grandtotalnjop"),
            "t_nosertifikathaktanah" => new Zend\Db\Sql\Expression("b.t_nosertifikathaktanah"),
            "t_iddetailsptbphtb" => new Zend\Db\Sql\Expression("b.t_iddetailsptbphtb"),
            "s_namajenistransaksi" => new Zend\Db\Sql\Expression("c.s_namajenistransaksi"),
            "t_idpembayaranspt", "t_kohirpembayaran", "t_periodepembayaran", "t_tanggalpembayaran",
            "t_idnotaris", "t_nilaipembayaranspt", "t_kodebayarspt", "t_verifikasispt",
            "t_tglverifikasispt", "t_pejabatverifikasispt", "t_statusbayarspt",
            "t_ketetapanspt",
            "t_bulanselesaibayar" => new \Zend\Db\Sql\Expression("to_char((e.t_tanggalpembayaran)::timestamp with time zone, 'MM-YYYY'::text)")
        ));
        $select->from(array("e" => "t_pembayaranspt"));
        $select->join(
            array("z" => "t_spt"),
            new \Zend\Db\Sql\Expression("e.t_idspt = z.t_idspt"),
            array(
                "t_idspt", "t_tglprosesspt", "t_periodespt", "t_nopbphtbsppt", "t_totalspt",
                "t_nilaitransaksispt", "t_kohirspt", "t_potonganspt", "t_idjenistransaksi",
                "t_idjenishaktanah", "t_persyaratan", "t_kohirketetapanspt",
                "t_tarif_pembagian_aphb_kali", "t_tarif_pembagian_aphb_bagi",
                "t_idtarifbphtb", "t_persenbphtb"
            ),
            "LEFT"
        );
        $select->join(
            array("b" => "t_detailsptbphtb"),
            new \Zend\Db\Sql\Expression("b.t_idspt = z.t_idspt"),
            array(
                "t_kelurahanop", "t_kecamatanop", "t_luastanahbpn", "t_luasbangunanbpn",
                "t_tglajbbaru", "t_noajbbaru", "t_statuspelaporannotaris",
                "t_tglpelaporannotaris", "t_kabupatenop", "t_nosertifikatbaru",
                "t_tglsertifikatbaru", "t_inputbpn", "t_statuskonfirmasinotaris",
                "t_grandtotalnjop_aphb"
            ),
            "LEFT"
        );
        $select->join(
            array("c" => "s_jenistransaksi"),
            new \Zend\Db\Sql\Expression("c.s_idjenistransaksi = z.t_idjenistransaksi"),
            array(),
            "LEFT"
        );
        $select->join(
            array("d" => "s_jenishaktanah"),
            new \Zend\Db\Sql\Expression("d.s_idhaktanah = z.t_idjenishaktanah"),
            array(
                "s_namahaktanah"
            ),
            "LEFT"
        );
        $select->join(
            array("f" => "t_pemeriksaan"),
            new \Zend\Db\Sql\Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"),
            array(
                "p_kohirskpdkb", "p_luastanah", "p_luasbangunan", "p_njoptanah", "p_njopbangunan",
                "p_totalnjoptanah", "p_totalnjopbangunan", "p_grandtotalnjop",
                "p_nilaitransaksispt", "p_potonganspt", "p_ketwaris", "p_terbukti",
                "p_idjenistransaksi", "p_idjenishaktanah", "p_totalspt",
                "p_nilaipembayaranspt", "p_nilaikurangbayar", "p_idpemeriksaan",
                "p_grandtotalnjop_aphb"
            ),
            "LEFT"
        );
        $select->join(
            array("g" => "s_users"),
            new \Zend\Db\Sql\Expression("g.s_iduser = z.t_idnotarisspt"),
            array(
                "t_idnotarisspt" => new \Zend\Db\Sql\Expression("s_iduser")
            ),
            "LEFT"
        );
        $select->join(
            array("h" => "s_notaris"),
            new \Zend\Db\Sql\Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"),
            array(
                "s_namanotaris"
            ),
            "LEFT"
        );
        $select->join(
            array("i" => "t_dendasanksinotaris"),
            new \Zend\Db\Sql\Expression("i.t_idds = e.t_idds"),
            array(
                "t_kodebayards", "t_kohirds", "t_ketetapands"
            ),
            "LEFT"
        );
        $select->join(
            array("j" => "s_users"),
            new \Zend\Db\Sql\Expression("j.s_iduser = i.t_idnotaris"),
            array(),
            "LEFT"
        );
        $select->join(
            array("k" => "s_notaris"),
            new \Zend\Db\Sql\Expression("(k.s_idnotaris)::text = (j.s_idpejabat_idnotaris)::text"),
            array(
                "notarisds" => new \Zend\Db\Sql\Expression("s_namanotaris")
            ),
            "LEFT"
        );

        return $select;
    }

    public function fr_pembayaran_v5($string = null)
    {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(array(
            //            "" => new Expression(""),
            "t_idspt" => new Expression("a.t_idspt"),
            "jml_syarat_input" => new Expression("(length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) "),
            "jml_syarat_validasi" => new Expression("(length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1)"),
            "jml_syarat_sebenarnya" => new Expression("n.jmlsyarat"),
            "status_pendaftaran" => new Expression("(CASE
            WHEN ((length(translate((a.t_persyaratan)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            ELSE 2
        END)"),
            "status_validasi" => new Expression("(CASE
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) = n.jmlsyarat) THEN 1
            WHEN ((length(translate((e.t_verifikasispt)::text, '[\"]1234567890'::text, ''::text)) + 1) IS NULL) THEN 3
            ELSE 2
        END)"),
            "t_kohirspt" => new Expression("a.t_kohirspt"),
            "s_idjenistransaksi" => new Expression("c.s_idjenistransaksi"),
            "t_periodespt" => new Expression("a.t_periodespt"),
            "t_tglprosesspt" => new Expression("a.t_tglprosesspt"),
            "t_namawppembeli" => new Expression("b.t_namawppembeli"),
            "t_statusbayarspt" => new Expression("(CASE
            WHEN ((e.t_statusbayarspt)::text = 'true'::text) THEN 'TRUE'::text
            ELSE 'FALSE'::text
        END )"),
            "s_namajenistransaksi" => new Expression("c.s_namajenistransaksi"),
            "t_persyaratan" => new Expression("a.t_persyaratan"),
            "t_verifikasispt" => new Expression("e.t_verifikasispt"),
            "t_idjenistransaksi" => new Expression("a.t_idjenistransaksi"),
            "t_inputbpn" => new Expression("b.t_inputbpn"),
            "t_tglverifikasispt" => new Expression("e.t_tglverifikasispt"),
            "p_idpemeriksaan" => new Expression("f.p_idpemeriksaan"),
            "t_idpembayaranspt" => new Expression("e.t_idpembayaranspt"),
            "t_kodebayarbanksppt" => new Expression("e.t_kodebayarbanksppt"),
            "t_ketetapanspt" => new Expression("e.t_ketetapanspt"),
            "t_ketetapands" => new Expression("i.t_ketetapands"),
            "t_kohirds" => new Expression("i.t_kohirds"),
            "notarisds" => new Expression("k.s_namanotaris"),
            "t_periodepembayaran" => new Expression("e.t_periodepembayaran"),
            "t_tanggalpembayaran" => new Expression("e.t_tanggalpembayaran"),
            "t_nilaipembayaranspt" => new Expression("e.t_nilaipembayaranspt"),
            "t_idnotarisspt" => new Expression("a.t_idnotarisspt"),
            "t_totalspt" => new Expression("a.t_totalspt"),
            "p_totalspt" => new Expression("f.p_totalspt"),
            "jml_pajak_v1" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_totalspt
            ELSE a.t_totalspt
        END)"),
            "jml_pajak_v2" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN
            CASE
                WHEN (a.t_totalspt < f.p_totalspt) THEN f.p_totalspt
                ELSE a.t_totalspt
            END
            ELSE a.t_totalspt
        END)"),
            "s_idpejabat_idnotaris" => new Expression("g.s_idpejabat_idnotaris"),
            "s_namanotaris" => new Expression("h.s_namanotaris"),
            "t_noajbbaru" => new Expression("b.t_noajbbaru"),
            "t_tglajbbaru" => new Expression("b.t_tglajbbaru"),
            "t_iddetailsptbphtb" => new Expression("b.t_iddetailsptbphtb"),
            "t_luastanahbpn" => new Expression("b.t_luastanahbpn"),
            "t_luasbangunanbpn" => new Expression("b.t_luasbangunanbpn"),
            "p_luastanah" => new Expression("f.p_luastanah"),
            "p_luasbangunan" => new Expression("f.p_luasbangunan"),
            "t_luastanah" => new Expression("b.t_luastanah"),
            "t_luasbangunan" => new Expression("b.t_luasbangunan"),
            "luas_tanah" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luastanah
            ELSE b.t_luastanah
        END)"),
            "luas_bangunan" => new Expression("(CASE
            WHEN (f.p_idpemeriksaan IS NOT NULL) THEN f.p_luasbangunan
            ELSE b.t_luasbangunan
        END)"),
            "t_tglsertifikatbaru" => new Expression("b.t_tglsertifikatbaru"),
            "t_nosertifikatbaru" => new Expression("b.t_nosertifikatbaru"),
            "t_kohirketetapanspt" => new Expression("a.t_kohirketetapanspt"),
            "t_nopbphtbsppt" => new Expression("a.t_nopbphtbsppt"),
            "t_grandtotalnjop" => new Expression("b.t_grandtotalnjop"),
            "t_nilaitransaksispt" => new Expression("a.t_nilaitransaksispt"),
            "t_namawppenjual" => new Expression("b.t_namawppenjual"),
            "fr_luas_tanah_bpn" => new Expression("b.fr_luas_tanah_bpn"),
            "fr_luas_bangunan_bpn" => new Expression("b.fr_luas_bangunan_bpn"),
            "fr_tervalidasidua" => new Expression("a.fr_tervalidasidua"),
            "fr_validasidua" => new Expression("b.fr_validasidua"),
            "p_grandtotalnjop" => new Expression("f.p_grandtotalnjop"),
            "p_grandtotalnjop_aphb" => new Expression("f.p_grandtotalnjop_aphb"),
            "p_nilaitransaksispt" => new Expression("f.p_nilaitransaksispt"),
            "t_alamatwppembeli" => new Expression("b.t_alamatwppembeli"),
            "t_nikwppembeli" => new Expression("b.t_nikwppembeli"),
            "t_tarif_pembagian_aphb_kali" => new Expression("a.t_tarif_pembagian_aphb_kali"),
            "t_tarif_pembagian_aphb_bagi" => new Expression("a.t_tarif_pembagian_aphb_bagi"),
            "t_grandtotalnjop_aphb" => new Expression("b.t_grandtotalnjop_aphb"),
            "t_idsptsebelumnya" => new Expression("a.t_idsptsebelumnya"),
            "t_alamatwppenjual" => new Expression("b.t_alamatwppenjual"),
            "t_keteranganvalidasi" => new Expression("e.t_keteranganvalidasi"),
            "t_pejabatverifikasispt" => new Expression("e.t_pejabatverifikasispt")
        ));
        $select->from(array("e" => "t_pembayaranspt"));
        $select->join(array("a" => "t_spt"), new Expression("e.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("b" => "t_detailsptbphtb"), new Expression("b.t_idspt = a.t_idspt"), array(), "LEFT");
        $select->join(array("c" => "s_jenistransaksi"), new Expression("c.s_idjenistransaksi = a.t_idjenistransaksi"), array(), "LEFT");
        $select->join(array("f" => "t_pemeriksaan"), new Expression("f.p_idpembayaranspt = e.t_idpembayaranspt"), array(), "LEFT");
        $select->join(array("i" => "t_dendasanksinotaris"), new Expression("i.t_idds = e.t_idds"), array(), "LEFT");
        $select->join(array("g" => "s_users"), new Expression("g.s_iduser = a.t_idnotarisspt"), array(), "LEFT");
        $select->join(array("h" => "s_notaris"), new Expression("(h.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("j" => "s_users"), new Expression("j.s_iduser = i.t_idnotaris"), array(), "LEFT");
        $select->join(array("k" => "s_notaris"), new Expression("(k.s_idnotaris)::text = (g.s_idpejabat_idnotaris)::text"), array(), "LEFT");
        $select->join(array("n" => "fr_count_s_persyaratan"), new Expression("a.t_idjenistransaksi = n.s_idjenistransaksi"), array(), "LEFT");

        if ($string != null && $string == 1) {
            return $select->getSqlString();
        } else {
            return $select;
        }
    }
}
