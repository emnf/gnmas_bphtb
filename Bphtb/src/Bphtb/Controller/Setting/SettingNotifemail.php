<?php

namespace Bphtb\Controller\Setting;

class SettingNotifemail extends \Zend\Mvc\Controller\AbstractActionController {

    public function indexAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getServiceLocator()->get('PemdaTable')->getdata();
        $data = $this->getServiceLocator()->get("NotifemailTable")->getdata();
        
        $req = $this->getRequest();
        if($req->isPost()){
            $this->getServiceLocator()->get("NotifemailTable")->savedata($req->getPost("s_status"));
            $data = $this->getServiceLocator()->get("NotifemailTable")->getdata();
        }
        
        $view = new \Zend\View\Model\ViewModel(array(
            'data' => $data,
        ));
        
        $datane = array(
            "menu_setting" => "menu_setting",
            'side_notifemail' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
            'session' => $session
        );
        $this->layout()->setVariables($datane);
        return $view;
    }

}
