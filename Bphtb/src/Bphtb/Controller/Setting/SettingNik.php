<?php

namespace Bphtb\Controller\Setting;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class SettingNik extends \Zend\Mvc\Controller\AbstractActionController
{

    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getServiceLocator()->get('PemdaTable')->getdata();
        $view = new \Zend\View\Model\ViewModel(array(
            // 'data' => $data,
        ));

        $datane = array(
            "menu_setting" => "menu_setting",
            'side_nik' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
            'session' => $session
        );
        $this->layout()->setVariables($datane);
        return $view;
    }

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . ':' . $_SERVER['SERVER_PORT'] . '' . $uri->getPath();
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array)$this->getEvent()->getRouteMatch()->getParams();

        $input = $this->getRequest();

        $aColumns = array(
            '"IDNIK"',
            '"NIK"',
            '"NAMA_LGKP"',
            '"ALAMAT"',
        );

        // var_dump($input);
        // exit();

        $rResult = $this->getServiceLocator()->get("NikTable")->semuadatanik($allParams, $input, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function formtambahAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getServiceLocator()->get('PemdaTable')->getdata();
        $req = $this->getRequest();
        if ($req->isPost()) {
            // $post = array_merge_recursive(
            //     $this->getRequest()->getPost()->toArray(),
            //     $this->getRequest()->getFiles()->toArray()
            // );
            // var_dump($post);
            // exit();

            $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

            if (isset($_FILES['file']['name']) && in_array($_FILES['file']['type'], $file_mimes)) {

                $arr_file = explode('.', $_FILES['file']['name']);
                // var_dump($arr_file);
                // exit();
                $extension = end($arr_file);

                if ('csv' == $extension) {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $spreadsheet = $reader->load($_FILES['file']['tmp_name']);

                $sheetData = $spreadsheet->getActiveSheet()->toArray();
                // var_dump($sheetData);
                // exit();
                $this->getServiceLocator()->get("NikTable")->insertnik($sheetData);
                return $this->redirect()->toRoute('setting_nik');
            }
        }
        $view = new \Zend\View\Model\ViewModel(array());

        $datane = array(
            "menu_setting" => "menu_setting",
            'side_nik' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
            'session' => $session
        );
        $this->layout()->setVariables($datane);
        return $view;
    }

    public function hapusAction()
    {
        $allParams = (array)$this->getEvent()->getRouteMatch()->getParams();
        // var_dump($allParams);
        // exit();
        $this->getServiceLocator()->get("NikTable")->hapus($allParams['id']);
        return $this->getResponse();
    }
}
