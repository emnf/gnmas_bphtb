<?php

namespace Bphtb\Controller\Pendataan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Math\Rand;
use Bphtb\Form\Pendataan\SSPDFrm;
use Bphtb\Helper\ValidationHelper;
use mPDF;
use QRcode;
use Zend\Debug\Debug;

class PendataanSSPD extends AbstractActionController
{

    protected $tbl_sspd, $tbl_jenistransaksi, $tbl_haktanah, $tbl_nop;
    protected $tbl_spt, $tbl_pendataan, $tbl_notaris, $tbl_persyaratan, $tbl_doktanah, $tbl_sspdwaris, $tbl_pengurangantarif;
    protected $routeMatch, $tbl_pemda, $tbl_pejabat;
    protected $options;
    // PHP File Upload error message codes:
    // http://php.net/manual/en/features.file-upload.errors.php
    protected $error_messages = array(
        1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
        3 => 'The uploaded file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk',
        8 => 'A PHP extension stopped the file upload',
        'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini',
        'max_file_size' => 'Ukuran file terlalu besar, mohon disesuaikan',
        'min_file_size' => 'File is too small',
        'accept_file_types' => 'Jenis file tidak diperbolehkan',
        'max_number_of_files' => 'Maximum number of files exceeded',
        'max_width' => 'Image exceeds maximum width',
        'min_width' => 'Image requires a minimum width',
        'max_height' => 'Image exceeds maximum height',
        'min_height' => 'Image requires a minimum height',
        'abort' => 'File upload aborted',
        'image_resize' => 'Failed to resize image'
    );
    protected $image_objects = array();

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath();
    }

    public function indexAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $data_mengetahuilengkap_sspd = $this->getTblPejabat()->getdata();

        if (($session['s_akses'] == "1") || ($session['s_akses'] == "2") || ($session['s_akses'] == "8") || ($session['s_akses'] == "9")) {
            $show_hide_combo_notaris = "";
            $idnotarisdatacari = 1;
        } else if ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
            $idnotarisdatacari = 2;
        } else {
            $show_hide_combo_notaris = "";
            $idnotarisdatacari = "";
        }
        //var_dump($idnotarisdatacari);exit();

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();

        $form = new \Bphtb\Form\Pendataan\SSPDFrm();
        $frmpendaftaran = new SSPDFrm(null, null, null, null, $this->populateComboNotaris(), null, $this->populateComboNotaris(), null);
        $view = new ViewModel(
            array(
                'form' => $form,
                'data_mengetahuilengkap_sspd' => $data_mengetahuilengkap_sspd,
                'frm' => $frmpendaftaran,
                'data_mengetahuilengkap_sspd' => $data_mengetahuilengkap_sspd,
                'datajenistransaksi' => $datajenistransaksi,
                'idnotarisdatacari' => $idnotarisdatacari,
                'datanotaris' => $ar_notaris,
                'show_hide_combo_notaris' => $show_hide_combo_notaris
            )
        );
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'fr_pendaftaran_v5';
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_idspt DESC";
        if ($s_tipe_pejabat == 2) {
            $aColumns = array(
                't_idspt',
                't_kohirspt',
                't_kohirketetapanspt',
                's_idjenistransaksi',
                't_tglprosesspt',
                't_nopbphtbsppt',
                't_namawppembeli',
                'jml_pajak_v1',
                't_statusbayarspt',
                'status_pendaftaran',
                'status_validasi',
                's_namajenistransaksi',
                't_persyaratan',
                't_verifikasispt',
                't_idjenistransaksi',
                't_inputbpn',
                't_periodespt',
                't_totalspt',
                'fr_tervalidasidua',
                'fr_validasidua',
                't_idsptsebelumnya',
                't_status_esignature',
                't_kodebayarbanksppt',
            );
        } else {
            $aColumns = array(
                't_idspt',
                't_kohirspt',
                't_kohirketetapanspt',
                's_idjenistransaksi',
                't_idnotarisspt',
                't_tglprosesspt',
                't_nopbphtbsppt',
                't_namawppembeli',
                'jml_pajak_v1',
                't_statusbayarspt',
                'status_pendaftaran',
                'status_validasi',
                's_namajenistransaksi',
                't_persyaratan',
                't_verifikasispt',
                't_idjenistransaksi',
                't_inputbpn',
                't_periodespt',
                't_totalspt',
                's_namanotaris',
                'fr_tervalidasidua',
                'fr_validasidua',
                't_idsptsebelumnya',
                't_status_esignature',
                't_kodebayarbanksppt',
            );
        }

        $panggildata = $this->getServiceLocator()->get("SSPDBphtbTable");
        $rResult = $panggildata->semuadatapendaftaran($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());

        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGrid2Action()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $base = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        //============= Model\Pendataan\SSPDBphtbTable
        $count = $this->getTblSSPDBphtb()->getGridCountPendataanSSPD($base, $session['s_iduser'], $session['s_tipe_pejabat']); //getGridCount //$session['s_namauserrole']
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;

        //============ Model\Pendataan\SSPDBphtbTable
        $data = $this->getTblSSPDBphtb()->getGridDataPendataanSSPD($base, $start, $session['s_iduser'], $session['s_tipe_pejabat']); // getGridData $session['s_namauserrole']
        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            if ($row['t_inputbpn'] == true) {
                $s .= "<td> <span class='badge' style='background-color:#CC0000;'>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . " </span> </td>";
            } else {
                $s .= "<td>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . "</td>";
            }
            $s .= "<td>" . $row['t_periodespt'] . "</td>";
            $s .= "<td>" . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . "</td>";
            $s .= "<td>" . $row['t_namawppembeli'] . "</td>";
            if (!empty($row['p_idpemeriksaan'])) {
                $s .= "<td><div style='text-align:right'>" . number_format($row['p_totalspt'], 0, ',', '.') . "</div></td>";
            } else {
                $s .= "<td><div style='text-align:right'>" . number_format($row['t_totalspt'], 0, ',', '.') . "</div></td>";
            }
            $s .= "<td>" . $row['s_namajenistransaksi'] . "</td>";
            $result_array_syarat = \Zend\Json\Json::decode($row['t_persyaratan']);
            $jml_syarat = count($result_array_syarat);
            $result_array_syarat_verifikasi = \Zend\Json\Json::decode($row['t_verifikasispt']);

            $jml_syarat_verifikasi = count($result_array_syarat_verifikasi);
            $cektabelpersyaratan = $this->getTblJenTran()->jumlahsyarat($row['t_idjenistransaksi']);

            if ($cektabelpersyaratan == $jml_syarat) {
                $s .= "<td>Lengkap</td>";
            } else {
                $s .= "<td>Belum Lengkap</td>";
            }
            if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {
                $status_verifikasi = "Tervalidasi";
                if ($row['t_statusbayarspt'] == true) {
                    $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a></td>";
                } else {
                    $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a> </td>"; //<a href='pendataan_sspd/edit?t_idspt= $row[t_idspt]' class='btn btn-warning btn-sm btn-flat' style='width:50px'>Edit</a>
                }
            } else {
                if (empty($row['t_verifikasispt'])) {
                    $status_verifikasi = "";
                    $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a> <a href='pendataan_sspd/edit?t_idspt= $row[t_idspt]' class='btn btn-warning btn-sm btn-flat' style='width:50px'>Edit</a></td>";
                } else {
                    $status_verifikasi = "Belum Lengkap";
                    if ($row['t_inputbpn'] == true) {
                        $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a></td>";
                    } else {
                        $edit = "<td><a href='pendataan_sspd/viewdata?t_idspt= $row[t_idspt]' class='btn btn-primary btn-sm btn-flat' style='width:50px'>Lihat</a> <a href='pendataan_sspd/edit?t_idspt=$row[t_idspt]' class='btn btn-warning btn-sm btn-flat' style='width:50px'>Edit</a> <a href='#' onclick='hapus(" . $row['t_idspt'] . ");return false; 'class='btn btn-danger btn-sm btn-flat' style='width:50px'>Hapus</a></td>";
                    }
                }
            }



            $s .= "<td>" . $status_verifikasi . "</td>";
            if ($row['t_statusbayarspt'] == true) {
                $status_bayar = "Sudah Dibayar";
            } else {
                $status_bayar = "Belum Dibayar";
            }
            $s .= "<td>" . $status_bayar . "</td>";
            $cetaksurat_sspd = " <a href='#' onclick='openCetakSSPD(" . $row['t_idspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:80px'>SSPD</a>";
            //$s .= "<td><a href='pendataan_sspd/cetaksspdbphtb?&action=cetaksspd&t_idspt=$row[t_idspt]' target='_blank' class='btn btn-success btn-sm btn-flat'>SSPD</a></td>";
            $s .= "<td>" . $cetaksurat_sspd . "</td>";
            $s .= "" . $edit . " ";
            if (($session['s_namauserrole'] == "Administrator")) {
                $s .= "<td><a href='#' onclick='hapusall(" . $row['t_idspt'] . ");return false; 'class='btn btn-danger btn-sm btn-flat' style='width:50px'>Hapus Semua</a></td>";
            } else {
            }

            $s .= "<tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $show_hide_combo_notaris = "display:none;";
        /* if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") ||  ($session['s_tipe_pejabat'] == 1)) {
          $show_hide_combo_notaris = "display:inherit;";
          $notaris = 2;
          } elseif ($session['s_tipe_pejabat'] == 2) {
          //if ($session['s_namauserrole'] == "Notaris") {

          $show_hide_combo_notaris = "display:none;";
          $notaris = 1;
          } */
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1) || ($session['s_akses'] == 8) || ($session['s_akses'] == 2) || ($session['s_akses'] == 9)) {
            $show_hide_combo_notaris = "display:inherit;";
            if ($session['s_namauserrole'] == "Administrator") {
                $notaris = 3;
            } else {
                $notaris = 2;
            }
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {

            $show_hide_combo_notaris = "display:none;";
            $notaris = 1;
        } else {
            $show_hide_combo_notaris = "display:none;";
            $notaris = '';
        }

        $req = $this->getRequest();
        $datane = $req->getPost();
        if (!empty($datane['t_idspt'])) {
            $dataki = $this->getTblSSPDBphtb()->getDataId($datane['t_idspt']);
            $frm = new SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), $this->populatePersyaratanId($datane['t_idjenistransaksi']), null, $this->comboPenguranganTarif());
        } else {
            $frm = new SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), null, null, $this->comboPenguranganTarif());
        }
        $data_tarif = $this->getTblSSPDBphtb()->temukanDataTarifBPHTB();
        if ($req->isPost()) {
            $kb1 = new \Bphtb\Model\Pendataan\SPTBase();
            $kb = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setInputFilter($kb1->getInputFilter());
            $frm->setInputFilter($kb->getInputFilter($session['s_namauserrole']));
            $frm->setData($req->getPost());
            if ($frm->isValid()) {
                $kb1->exchangeArray($frm->getData());
                $kb->exchangeArray($frm->getData());
                $this->getTblSPT()->persyaratan($req->getPost());
                asort($kb->t_persyaratan);
                $kb->t_persyaratan = implode(",", $kb->t_persyaratan);
                $kb->t_persyaratan = explode(",", $kb->t_persyaratan);
                if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1) || ($session['s_akses'] == 8) || ($session['s_akses'] == 2) || ($session['s_akses'] == 9)) {
                    $idnotaris = $kb->t_idnotarisspt;
                    $idpendaftar = $session['s_iduser'];
                }
                //if ($session['s_namauserrole'] == "Notaris") {
                if ($session['s_tipe_pejabat'] == 2) {
                    $idnotaris = $session['s_iduser'];
                    $idpendaftar = $session['s_iduser'];
                }
                // var_dump($kb1); die;
                //======== eksekusi simpan di Model\Pendataan\SPTTable
                sleep(3);
                //$this->getTblSPT()->savedata($kb1, $kb, $idnotaris, $idpendaftar, $req->getPost());
                //=================== hitung lagi biar aman coy
                $t_luastanah = str_ireplace(".", "", $kb->t_luastanah);
                $t_njoptanah = str_ireplace(".", "", $kb->t_njoptanah);

                $t_luasbangunan = str_ireplace(".", "", $kb->t_luasbangunan);
                $t_njopbangunan = str_ireplace(".", "", $kb->t_njopbangunan);

                $aphb_kali = $req->getPost()->t_tarif_pembagian_aphb_kali;
                $aphb_bagi = $req->getPost()->t_tarif_pembagian_aphb_bagi;

                $t_nilaitransaksispt = str_ireplace(".", "", $kb->t_nilaitransaksispt);

                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
                    $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
                    $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

                    $t_grandtotalnjop_aphb = $t_grandtotalnjop;
                } else {
                    $aphb = $aphb_kali / $aphb_bagi;

                    $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
                    $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
                    $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

                    $t_grandtotalnjop_hitung = $t_grandtotalnjop * $aphb;
                    $t_grandtotalnjop_aphb = ceil($t_grandtotalnjop_hitung);
                }

                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

                    if ($t_grandtotalnjop > $t_nilaitransaksispt) {
                        $t_npopspt = $t_grandtotalnjop;
                    } else {
                        $t_npopspt = $t_nilaitransaksispt;
                    }
                } else {
                    if ($t_grandtotalnjop_aphb > $t_nilaitransaksispt) {
                        $t_npopspt = $t_grandtotalnjop_aphb;
                    } else {
                        $t_npopspt = $t_nilaitransaksispt;
                    }
                }

                $tahunsekarang = date('Y');
                $tahunproses = date('Y', strtotime($kb->t_tglprosesspt));
                $ceknik = $this->getTblSPT()->ceknik($kb->t_nikwppembeli, $tahunproses);

                $ceknpoptkp = $this->getTblSPT()->cekpotongannpoptkp($kb->t_idjenistransaksi);
                $id = (int) $kb1->t_idspt;
                //$id = (int) $kb->t_iddetailsptbphtb;
                if ($id == 0) {
                    $cektarifbphtb = $this->getTblSPT()->gettarifbphtbsekarang();
                    $tarifbphtbsekarang = $cektarifbphtb['s_tarifbphtb'];
                    if (!empty($ceknik['t_idspt'])) {
                        //==================== dapat 60jt satu kali & 300jt satu kali dalam 1th
                        //jika hibah wasiat (4) / waris (5) data transaksinya trus cek lagi 
                        if (($ceknik['t_idjenistransaksi'] == 4) || ($ceknik['t_idjenistransaksi'] == 5)) {
                            //jika transaksi bukan 4/5 maka cek lagi hibah wasiat
                            if (($kb->t_idjenistransaksi == 4) || ($kb->t_idjenistransaksi == 5)) {
                                $t_potonganspt = 0; //TIDAK DAPAt
                                //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                            } else {
                                //cek lagi bener gak ra ana hibahwasiat / waris
                                $ceknik_selainwarishibahwasiat = $this->getTblSPT()->ceknikselainwarishibahwasiat($kb->t_nikwppembeli, $tahunproses);
                                if (!empty($ceknik_selainwarishibahwasiat['t_idspt'])) {
                                    $t_potonganspt = 0; //TIDAK DAPAT
                                    //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                } else {
                                    $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                    //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                }
                            }
                        } else {
                            if (($kb->t_idjenistransaksi == 4) || ($kb->t_idjenistransaksi == 5)) {
                                $ceknik_warishibahwasiat = $this->getTblSPT()->ceknikwarishibahwasiat($kb->t_nikwppembeli, $tahunproses);
                                if (!empty($ceknik_warishibahwasiat['t_idspt'])) {
                                    $t_potonganspt = 0; //TIDAK DAPAT
                                    //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                } else {
                                    $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                    //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                }
                            } else {
                                $t_potonganspt = 0; //TIDAK DAPAT
                                //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                            }
                        }
                        //==================== end dapat 60jt satu kali & 300jt satu kali dalam 1th
                        //$t_potonganspt = 0;
                    } else {
                        $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                    }
                } else {
                    $cektarifbphtb = $this->getTblSPT()->gettarifbphtbspt($id);
                    $tarifbphtbsekarang = $cektarifbphtb['t_persenbphtb'];

                    if (!empty($ceknik['t_idspt'])) {
                        if ($ceknik['t_idspt'] == $id) {
                            //$t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                            //=========== cek lagi
                            if (($ceknik['t_idjenistransaksi'] == 4) || ($ceknik['t_idjenistransaksi'] == 5)) {
                                $ceknik_warishibahwasiat = $this->getTblSPT()->ceknikwarishibahwasiat($kb->t_nikwppembeli, $tahunproses);
                                if ($ceknik_warishibahwasiat['t_idspt'] == $id) {
                                    //$t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                    //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                    if (($kb->t_idjenistransaksi == 4) || ($kb->t_idjenistransaksi == 5)) {
                                        $ceknik_selainwarishibahwasiat = $this->getTblSPT()->ceknikselainwarishibahwasiat($kb->t_nikwppembeli, $tahunproses);
                                        if (($ceknik_selainwarishibahwasiat['t_idspt'] == $id) || empty($ceknik_selainwarishibahwasiat['t_idspt'])) {
                                            $t_potonganspt = $ceknpoptkp['s_tarifnpotkp']; //DAPAT
                                            //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                        } else {
                                            $t_potonganspt = 0; //TIDAK DAPAT
                                            //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                        }
                                    } else {
                                        $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                        //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                    }
                                } else {
                                    $t_potonganspt = 0; //TIDAK DAPAT
                                    //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                }
                            } else {
                                $ceknik_selainwarishibahwasiat = $this->getTblSPT()->ceknikselainwarishibahwasiat($kb->t_nikwppembeli, $tahunproses);
                                if ($ceknik_selainwarishibahwasiat['t_idspt'] == $id) {
                                    //$t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                    //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                    if (($kb->t_idjenistransaksi == 4) || ($kb->t_idjenistransaksi == 5)) {
                                        $ceknik_warishibahwasiat = $this->getTblSPT()->ceknikwarishibahwasiat($kb->t_nikwppembeli, $tahunproses);
                                        if (($ceknik_warishibahwasiat['t_idspt'] == $id) || empty($ceknik_warishibahwasiat['t_idspt'])) {
                                            $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                            //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                        } else {
                                            $t_potonganspt = 0; //TIDAK DAPAT
                                            //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                        }
                                    } else {
                                        $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                        //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                    }
                                } else {
                                    $t_potonganspt = 0; //TIDAK DAPAT
                                    //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                }
                            }
                            //=========== end cek lagi
                        } else {
                            //==================== dapat 60jt satu kali & 300jt satu kali dalam 1th
                            //jika hibah wasiat (4) / waris (5)
                            if (($ceknik['t_idjenistransaksi'] == 4) || ($ceknik['t_idjenistransaksi'] == 5)) {
                                if (($kb->t_idjenistransaksi == 4) || ($kb->t_idjenistransaksi == 5)) {
                                    $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                    $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                } else {
                                    $ceknik_selainwarishibahwasiat = $this->getTblSPT()->ceknikselainwarishibahwasiat($kb->t_nikwppembeli, $tahunproses);
                                    if (($ceknik_selainwarishibahwasiat['t_idspt'] == $id) || empty($ceknik_selainwarishibahwasiat['t_idspt'])) {
                                        $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                        $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                    } else {
                                        $t_potonganspt = 0; //TIDAK DAPAT
                                        $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                    }
                                }
                            } else {
                                if (($kb->t_idjenistransaksi == 4) || ($kb->t_idjenistransaksi == 5)) {
                                    $ceknik_warishibahwasiat = $this->getTblSPT()->ceknikwarishibahwasiat($kb->t_nikwppembeli, $tahunproses);
                                    if (($ceknik_warishibahwasiat['t_idspt'] == $id) || empty($ceknik_warishibahwasiat['t_idspt'])) {
                                        $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                        $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                    } else {
                                        $t_potonganspt = 0; //TIDAK DAPAT
                                        $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                    }
                                } else {
                                    $t_potonganspt = 0; //TIDAK DAPAT
                                    $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                }
                            }
                            //==================== end dapat 60jt satu kali & 300jt satu kali dalam 1th
                            //$t_potonganspt = 0;
                        }
                    } else {
                        $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                    }
                }


                if (($kb->t_idjenistransaksi == 4) || ($kb->t_idjenistransaksi == 5)) {
                    // DEFAULT DAPAT POTONGAN 50%
                    // $t_potongan_waris_hibahwasiat = 50;
                    // $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;

                    $t_potongan_waris_hibahwasiat = 0;
                    $hitung_potonganwaris = 1;
                } else {
                    $t_potongan_waris_hibahwasiat = 0;
                    $hitung_potonganwaris = 1;
                }

                //$t_potonganspt = str_ireplace(".", "", $ex->t_potonganspt);
                //$t_npopspt = str_ireplace(".", "", $ex->t_npopspt);
                $t_persenbphtb = $tarifbphtbsekarang; //str_ireplace(".", "", $ex->t_persenbphtb);
                if (($t_potonganspt == '0') || ($t_potonganspt == null) || ($t_potonganspt == '')) {
                    $t_potonganspt = 0;
                    $t_npopkpspt = $t_npopspt;
                    $t_totalspt = ceil($t_npopkpspt * $t_persenbphtb / 100 * $hitung_potonganwaris);
                } else {

                    $npop = $t_npopspt;
                    $npopkp = $npop - $t_potonganspt;
                    if ($npopkp <= 0) {
                        $t_npopkpspt = 0;
                        $t_totalspt = 0;
                    } else {
                        $t_npopkpspt = $npopkp;
                        $t_totalspt = ceil($t_npopkpspt * $t_persenbphtb / 100 * $hitung_potonganwaris);
                    }
                }

                // potaongan / penghapusan bphtb FASUM
                if ($kb1->t_idpengurangan != null) {
                    $penguranagan = $t_totalspt * $kb1->t_persenpengurangan / 100;
                    $t_totalspt = $t_totalspt - $penguranagan;
                }

                //=================== hitung lagi biar aman coy

                //$data = $this->getTblSPT()->savedata($kb1, $kb->t_nopbphtbsppt, $kb->t_kodebayarbanksppt, $kb->t_idjenistransaksi, $kb->t_idjenishaktanah, $kb->t_totalspt, $kb->t_nilaitransaksispt, $kb->t_potonganspt, $idnotaris, $idpendaftar, $req->getPost());
                $data = $this->getTblSPT()->savedata($kb1, $kb->t_nopbphtbsppt, $kb->t_kodebayarbanksppt, $kb->t_idjenistransaksi, $kb->t_idjenishaktanah, $t_totalspt, $kb->t_nilaitransaksispt, $t_potonganspt, $idnotaris, $idpendaftar, $req->getPost(), $t_potongan_waris_hibahwasiat);
                // var_dump($kb1, $kb->t_nopbphtbsppt, $kb->t_kodebayarbanksppt, $kb->t_idjenistransaksi, $kb->t_idjenishaktanah, $t_totalspt, $kb->t_nilaitransaksispt, $t_potonganspt, $idnotaris, $idpendaftar, $req->getPost(), $t_potongan_waris_hibahwasiat);
                // exit;
                if (gettype($data) == 'object') {
                    //$this->Tools()->getService('SSPDBphtbTable')->savedatadetail($kb, $data->t_idspt, $session);
                    $datadetail = (array) $this->getTblSSPDBphtb()->savedatadetail($kb, $data->t_idspt, $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb);
                    $idsptnya = $data->t_idspt;
                } else {
                    //$this->Tools()->getService('SSPDBphtbTable')->savedatadetail($kb, $data['t_idspt'], $session);
                    $datadetail = (array) $this->getTblSSPDBphtb()->savedatadetail($kb, $data['t_idspt'], $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb);
                    $idsptnya = $data['t_idspt'];
                }
                if ($kb1->t_jenispendaftaran == '4') {
                    $nopGabungan = $_SESSION['eBPHTBSESSIONKABTANAHBUMBU']['NOP_GABUNGAN'];
                    foreach ($nopGabungan as $col => $row) :
                        $row['t_iddetailsptbphtb'] = $datadetail['t_iddetailsptbphtb'];
                        $detailnop = (array) $this->getTblSSPDBphtb()->checkDetailNOP($datadetail['t_iddetailsptbphtb'], $row['t_nopbphtbsppt'], $row['t_thnsppt']);
                        $this->getTblSSPDBphtb()->saveDetailNOP($row, $detailnop['t_iddetailnopgabungan']);
                    endforeach;
                    unset($_SESSION['eBPHTBSESSIONKABTANAHBUMBU']['NOP_GABUNGAN']);
                }

                // $data = $this->getTblSPT()->savedata($kb1, $kb, $idnotaris, $req->getPost());
                // if (gettype($data) == 'object') {
                //     $this->getTblSSPDBphtb()->savedatadetail($kb, $data->t_idspt);
                // } else {
                //     $this->getTblSSPDBphtb()->savedatadetail($kb, $data['t_idspt']);
                // }
                // for ($ii = 0; $ii < count($req->getPost('t_namapenerimawaris')); $ii ++) {
                //     $nama = $req->getPost('t_namapenerimawaris');
                //     $alamat = $req->getPost('t_alamatpenerimawaris');
                //     $nik = $req->getPost('t_nikpenerimawaris');
                //     $this->getTblWaris()->savedatawaris($nama[$ii], $alamat[$ii], $nik[$ii]);
                // }
                return $this->redirect()->toRoute('pendataan_sspd');
            } else {
                if (!empty($datane['t_idspt'])) {
                    $frm->bind($dataki);
                    $frm->get("t_persyaratan")->setValue(\Zend\Json\Json::decode($dataki->t_persyaratan));
                }
            }
        }

        $cektarifbphtb2 = $this->getTblSPT()->gettarifbphtbsekarang();
        $tarifbphtbsekarang2 = $cektarifbphtb2['s_tarifbphtb'];

        $view = new ViewModel(array(
            'frm' => $frm,
            'tarifbphtb' => $data_tarif['s_tarifbphtb'],
            'show_hide_combo_notaris' => $show_hide_combo_notaris,
            'idtarifbphtb' => $data_tarif['s_idtarifbphtb'],
            'notaris' => $notaris,
            's_namauserrole' => $session['s_tipe_pejabat'],
            'tarifbphtb' => $tarifbphtbsekarang2
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // Edit Permohonan BPHTB
    // Lokasi : Permohonan BPHTB
    public function editAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1) || ($session['s_akses'] == 8) || ($session['s_akses'] == 2) || ($session['s_akses'] == 9)) {
            $show_hide_combo_notaris = "display:inherit;";
            $notaris = 2;
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
            $notaris = 1;
        }
        $string = Rand::getString(6, 'abcdefghijklmnopqrstuvwxyz-1234567890_ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), null, null, $this->comboPenguranganTarif());
        $req = $this->getRequest();
        $jenispendaftaran = "none";
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId($id);
            // Debug::dump($data);
            // exit;
            $datahistory = $this->getTblSSPDBphtb()->gethistorybphtb($data->t_nikwppembeli, $data->t_idspt, $data->t_periodespt);
            // var_dump($data); die;
            if ($data->t_idjenistransaksi == 5) {
                $show_hide_penerima_waris = "display:inherit;";
            } else {
                $show_hide_penerima_waris = "display:none;";
            }
            $data_penerimawaris = $this->getTblWaris()->CariPenerimaWaris($data->t_idspt);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $t_luastanah = str_ireplace('.', '', $data->t_luastanah);
            $t_luasbangunan = str_ireplace('.', '', $data->t_luasbangunan);
            $data->t_luastanah = number_format(($t_luastanah / 100), 0, ',', '.');
            $data->t_luasbangunan = number_format(($t_luasbangunan / 100), 0, ',', '.');
            $frm2 = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), $this->populatePersyaratanId($data->t_idjenistransaksi), null, $this->comboPenguranganTarif());
            $frm2->bind($data);
            $frm2->get("t_terbukti")->setValue(\Zend\Json\Json::decode($data->t_terbukti));
            $frm2->get("t_persyaratan")->setValue(\Zend\Json\Json::decode($data->t_persyaratan));

            $data_tarif = $this->getTblSSPDBphtb()->temukanDataTarifBphtb_tahun($data->t_periodespt);

            if (!empty($data->t_keterangan) && $data->t_keterangan != "") {
                $jenispendaftaran = "block";
            }
        }

        //========== panggil Model\Pendataan\SSPDBphtbTable
        $fr_tarif = $data->t_persenbphtb;
        $tahunproses = date('Y', strtotime($data->t_tglprosesspt));
        $ceknik = $this->getTblSPT()->ceknik($data->t_nikwppembeli, $tahunproses);
        if (!empty($ceknik['t_idspt'])) {
            $idceknikidspt = $ceknik['t_idspt'];
        } else {
            $idceknikidspt = '';
        }

        $view = new ViewModel(array(
            'frm' => $frm2,
            'data' => $data->t_terbukti,
            'show_hide_combo_notaris' => $show_hide_combo_notaris,
            'datahistory' => $datahistory,
            'show_hide_penerima_waris' => $show_hide_penerima_waris,
            'data_penerimawaris' => $data_penerimawaris,
            'idtarifbphtb' => $data_tarif['s_idtarifbphtb'],
            'notaris' => $notaris,
            't_tarif_pembagian_aphb_kali' => $data->t_tarif_pembagian_aphb_kali,
            't_tarif_pembagian_aphb_bagi' => $data->t_tarif_pembagian_aphb_bagi,
            't_grandtotalnjop_aphb' => $data->t_grandtotalnjop_aphb,
            'tarifbphtbsekarang' => $fr_tarif,
            'idceknikidspt' => $idceknikidspt,
            't_potongan_waris_hibahwasiat' => $data->t_potongan_waris_hibahwasiat,
            'jenispendaftaran' => $jenispendaftaran,
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function viewdataAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            //            var_dump($data);exit();
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function viewdata2Action()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId($id);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $t_luastanah = str_ireplace('.', '', $data->t_luastanah);
            $t_luasbangunan = str_ireplace('.', '', $data->t_luasbangunan);
            $data->t_luastanah = number_format(($t_luastanah / 100), 0, ',', '.');
            $data->t_luasbangunan = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // Hapus Permohonan
    // Lokasi : index Pendataan
    public function HapusAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $kb1 = new \Bphtb\Model\Pendataan\SPTBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setInputFilter($kb1->getInputFilter());
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $idspt = $req->getPost();
                $kb->exchangeArray($frm->getData());
                $kb1->exchangeArray($frm->getData());
                //=============== Model\Pendataan\SSPDBphtbTable
                $this->getTblSSPDBphtb()->hapusData($kb);
                $this->getTblSpt()->hapusDataSpt($kb1);
                $this->getTblSSPDBphtb()->hapusDataWaris($idspt->t_idspt);
            }
        }
        return $res;
    }

    public function hapusallAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $this->getTblSSPDBphtb()->hapusall($this->params("page"), $session);
        return $this->getResponse();
    }

    // Mencari History Transaksi BPHTB
    // Lokasi : Tambah dan Edit Permohonan BPHTB
    public function historybphtbAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();

        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $posnya = $req->getPost();
                // var_dump($ex);exit();
                // DATA WP PENERIMA HAK
                if (!empty($posnya->t_nikwppembeli)) {

                    $data = $this->getTblSSPDBphtb()->temukanDataHistory($ex);
                    // if ($posnya->t_nikwppembeli == $data['t_nikpenerima']) { // ki kanggo waris jo paijo
                    //     $data['t_namawppembeli'] = $data['t_namapenerima'];
                    //     $data['t_alamatpembeli'] = $data['t_alamatpenerima'];
                    //     $data['t_npwpwppembeli'] = '';
                    //     $data['t_rtwppembeli'] = '';
                    //     $data['t_rwwppembeli'] = '';
                    //     $data['t_kelurahanwppembeli'] = '';
                    //     $data['t_kecamatanwppembeli'] = '';
                    //     $data['t_kabkotawppembeli'] = '';
                    //     $data['t_kodeposwppembeli'] = '';
                    //     $data['t_telponwppembeli'] = '';
                    // }

                    //APABILA DIHISTORI KOSONG AMBIL DATA NIK
                    // if(empty($data['t_namawppembeli']) || $data['t_namawppembeli'] == ""){
                    //GET DATA NIK
                    $datanik = null;
                    // $datanik = $this->getServiceLocator()->get("NikTable")->getdataNIK($req->getPost("t_nikwppembeli"));
                    // var_dump($datanik);exit();
                    // if(!empty($datanik)){
                    //     $data['t_namawppembeli'] = $datanik['NAMA_LGKP'];
                    //     $data['t_alamatwppembeli'] = $datanik['ALAMAT'];
                    //     $data['t_npwpwppembeli'] = '';
                    //     $data['t_rtwppembeli'] = str_pad($datanik['NO_RT'], 3, '0', STR_PAD_LEFT);
                    //     $data['t_rwwppembeli'] = str_pad($datanik['NO_RW'], 2, '0', STR_PAD_LEFT);
                    //     $data['t_kelurahanwppembeli'] = $datanik['NAMA_KEL'];
                    //     $data['t_kecamatanwppembeli'] = $datanik['NAMA_KEC'];
                    //     $data['t_kabkotawppembeli'] = 'KABUPATEN MINAHASA SELATAN';
                    //     $data['t_kodeposwppembeli'] = str_pad($datanik['KODE_POS'], 5, '0', STR_PAD_LEFT);
                    //     $data['t_telponwppembeli'] = '';
                    // }else{
                    //     $data['t_namawppembeli'] = null;
                    //     $data['t_alamatwppembeli'] = null;
                    //     $data['t_npwpwppembeli'] = null;
                    //     $data['t_rtwppembeli'] = null;
                    //     $data['t_rwwppembeli'] = null;
                    //     $data['t_kelurahanwppembeli'] = null;
                    //     $data['t_kecamatanwppembeli'] = null;
                    //     $data['t_kabkotawppembeli'] = null;
                    //     $data['t_kodeposwppembeli'] = null;
                    //     $data['t_telponwppembeli'] = null;
                    // }
                    // }

                    $data_table = $this->getTblSSPDBphtb()->temukanDataHistory2($ex);
                    $html = "<table class='table table-striped'>";
                    $html .= "<tr>";
                    $html .= "<th>NOP</th>";
                    $html .= "<th>NPOP</th>";
                    $html .= "<th>NPOPTKP</th>";
                    $html .= "<th>NPOPKP</th>";
                    $html .= "<th>Tanggal Bayar</th>";
                    $html .= "<th>Jumlah Pembayaran</th>";
                    $html .= "</tr>";
                    foreach ($data_table as $row) {
                        $npopkp = $row['t_totalspt'] * 100 / 5;
                        if ($row['t_nilaitransaksispt'] > $row['t_grandtotalnjop']) {
                            $npop = $row['t_nilaitransaksispt'];
                        } else {
                            $npop = $row['t_grandtotalnjop'];
                        }
                        $html .= "<tr>";
                        $html .= "<td> " . $row['t_nopbphtbsppt'] . "-" . $row['t_thnsppt'] . " </td>";
                        $html .= "<td> " . number_format($npop, 0, ',', '.') . " </td>";
                        $html .= "<td> " . number_format($row['t_potonganspt'], 0, ',', '.') . " </td>";
                        $html .= "<td> " . number_format($npopkp, 0, ',', '.') . " </td>";
                        if (empty($row['t_tanggalpembayaran'])) {
                            $html .= "<td></td>";
                        } else {
                            $html .= "<td> " . date('d-m-Y', strtotime($row['t_tanggalpembayaran'])) . " </td>";
                        }
                        $html .= "<td> " . number_format($row['t_nilaipembayaranspt'], 0, ',', '.') . " </td>";
                        $html .= "</tr>";
                    }
                    $html .= "</table>";

                    $data['raw_return'] = $html;
                } else if (!empty($posnya->t_nikwppenjual) && $posnya->t_nikwppenjual != "") {
                    // $ex->t_nikwppembeli = $posnya->t_nikwppenjual;
                    // $data = $this->getTblSSPDBphtb()->temukanDataHistory($ex);
                    // // var_dump($data);exit();

                    // if(!empty($data) && $data["t_nikwppembeli"] != ""){
                    //     $data["t_namawppenjual"] = $data['t_namawppembeli'];
                    //     $data["t_alamatwppenjual"] = $data['t_alamatwppembeli'];
                    //     $data["t_npwpwppenjual"] = $data['t_npwpwppembeli'];
                    //     $data['t_rtwppenjual'] = str_pad($data['t_rtwppembeli'], 3, '0', STR_PAD_LEFT);
                    //     $data['t_rwwppenjual'] = str_pad($data['t_rwwppembeli'], 2, '0', STR_PAD_LEFT);
                    //     $data['t_kelurahanwppenjual'] = $data['t_kelurahanwppembeli'];
                    //     $data['t_kecamatanwppenjual'] = $data['t_kecamatanwppembeli'];
                    //     $data['t_kabkotawppenjual'] = $data['t_kabkotawppembeli'];
                    //     $data['t_kodeposwppenjual'] = str_pad($data['t_kodeposwppembeli'], 5, '0', STR_PAD_LEFT);
                    //     $data['t_telponwppenjual'] = $data['t_telponwppembeli'];
                    // }else{
                    // DATA NIK WP PELEPAS HAK
                    // $datanik = $this->getServiceLocator()->get("NikTable")->getdataNIK($req->getPost("t_nikwppenjual"));
                    // var_dump($datanik);exit();
                    if (!empty($datanik)) {
                        $data['t_namawppenjual'] = $datanik['NAMA_LGKP'];
                        $data['t_alamatwppenjual'] = $datanik['ALAMAT'];
                        $data['t_npwpwppenjual'] = '';
                        $data['t_rtwppenjual'] = str_pad($datanik['NO_RT'], 3, '0', STR_PAD_LEFT);
                        $data['t_rwwppenjual'] = str_pad($datanik['NO_RW'], 2, '0', STR_PAD_LEFT);
                        $data['t_kelurahanwppenjual'] = $datanik['NAMA_KEL'];
                        $data['t_kecamatanwppenjual'] = $datanik['NAMA_KEC'];
                        $data['t_kabkotawppenjual'] = 'KABUPATEN MINAHASA SELATAN';
                        $data['t_kodeposwppenjual'] = str_pad($datanik['KODE_POS'], 5, '0', STR_PAD_LEFT);
                        $data['t_telponwppenjual'] = '';
                    } else {
                        $data = array();
                    }
                    // }

                } else {
                    $data = array();
                }

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Mencari Nilai NOP dari database PBB
    // Lokasi : Permohonan SSPD
    public function datanopAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SPPTBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                //$formatnop = str_ireplace('.', '', $ex->t_nopbphtbsppt);
                //$ex->t_nopbphtbsppt = $formatnop;
                $data = $this->getTblSPPT()->temukanData($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function TampilPersyaratanAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data_table = $this->getTblSSPDBphtb()->temukanPersyaratan($req->getPost());
                $countpersyaratan = count($data_table);
                $i = 1;
                $html = "<div>";
                $html .= "<div class='col-sm-12'>
                              <div class='form-group'>
                                 <span  class='col-sm-2'></span>
                                    <div class='col-sm-3'>
                                        <input type='checkbox' id='CheckAll' name='CheckAll' onClick='modify_boxes($countpersyaratan)'> <span style='color:green'>Centang Semua</span>
                                    </div>
                              </div>
                          </div>";
                foreach ($data_table as $row) {
                    $html .= "<div class='col-sm-12'>
                              <div class='form-group'><label class='col-sm-2 control-label'></label>";
                    $html .= "<div class='col-sm-10'>";
                    $html .= "<input id='t_persyaratan' name='t_persyaratan[]' type='checkbox' value='" . $row['s_idpersyaratan'] . "'>  ";
                    $html .= $row['s_namapersyaratan'];
                    $html .= "</div>";
                    $html .= "</div>";
                    $html .= "</div>";
                    $i++;
                }
                $html .= "</div>";

                $data['raw_return'] = $html;

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // ============================ cetak data pendaftaran
    public function cetakdatapendaftaranAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();

        $sm = $this->getServiceLocator();

        $this->tbl_sspd = $sm->get("SSPDTable");

        $req = $this->getRequest();
        if ($req->isGet()) {
            $data_get = $req->getQuery();

            //========================= Model\Pencetakan\SSPDTable
            $ar_DataVerifikasi = $this->tbl_sspd->getDataPendaftaran($data_get->periode_spt, $data_get->tgl_verifikasi1, $data_get->tgl_verifikasi2, $session['s_iduser'], $data_get->t_idnotarisspt, $session['s_tipe_pejabat']);
            $ar_tglcetak = $data_get->tgl_cetak;
            $ar_periodespt = $data_get->periode_spt;
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
            $ar_pemda = $this->tbl_pemda->getdata();
            /* $pdf = new \DOMPDFModule\View\Model\PdfModel();
              $pdf->setOption('filename', 'DataPendaftaran.pdf');
              $pdf->setOption('paperSize', 'legal');
              $pdf->setOption('paperOrientation', 'landscape');
              $pdf->setVariables(array(
              'data_Verifikasi' => $ar_DataVerifikasi,
              'tgl_cetak' => $ar_tglcetak,
              'periode_spt' => $ar_periodespt,
              'tgl_verifikasi1' => $data_get->tgl_verifikasi1,
              'tgl_verifikasi2' => $data_get->tgl_verifikasi2,
              'nama_login' => $session['s_namauserrole'],
              'ar_pemda' => $ar_pemda
              ));
              return $pdf; */
            $view = new ViewModel(array(
                'data_Verifikasi' => $ar_DataVerifikasi,
                'tgl_cetak' => $ar_tglcetak,
                'periode_spt' => $ar_periodespt,
                'tgl_verifikasi1' => $data_get->tgl_verifikasi1,
                'tgl_verifikasi2' => $data_get->tgl_verifikasi2,
                'nama_login' => $session['s_namauserrole'],
                'ar_pemda' => $ar_pemda,
                'cetak' => $data_get->cetak,
                'session' => $session,
            ));
            $data = array(
                'nilai' => '3', //no layout
                'session' => $session,
            );
            $this->layout()->setVariables($data);
            return $view;
        }
    }

    public function formcetakdatapendaftaranAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $show_hide_combo_notaris = "display:none;";
        $frm = new SSPDFrm(null, null, null, null, $this->populateComboNotaris());
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "";
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
        }
        $view = new ViewModel(array(
            'frm' => $frm,
            'show_hide_combo_notaris' => $show_hide_combo_notaris
        ));
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    // ============================ end cetak data pendaftaran
    // ============================ cetak data status berkas
    public function formcetakdatastatusberkasAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $show_hide_combo_notaris = "display:none;";
        $frm = new SSPDFrm(null, null, null, null, $this->populateComboNotaris());
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "";
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
        }
        $view = new ViewModel(array(
            'frm' => $frm,
            'show_hide_combo_notaris' => $show_hide_combo_notaris
        ));
        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakdatastatusberkasAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $sm = $this->getServiceLocator();
        $this->tbl_sspd = $sm->get("SSPDTable");
        $this->tbl_sspdsyarat = $sm->get("SSPDBphtbTable");
        $req = $this->getRequest();
        if ($req->isGet()) {
            $data_get = $req->getQuery();

            //========================= Model\Pencetakan\SSPDTable
            $ar_DataVerifikasi = $this->tbl_sspd->getDataStatusBerkas($data_get->periode_spt, $data_get->tgl_cetakstatusberkas1, $data_get->tgl_cetakstatusberkas2, $session['s_iduser'], $data_get->t_idnotarisspt, $session['s_tipe_pejabat']);
            $ar_tglcetak = $data_get->tgl_cetak;
            $ar_periodespt = $data_get->periode_spt;
            $this->tbl_pemda = $sm->get("PemdaTable");
            $ar_pemda = $this->tbl_pemda->getdata();
            $datahasil = array();
            foreach ($ar_DataVerifikasi as $key => $v) {
                // array_push($datahasil, $v);
                $result_array_syarat = \Zend\Json\Json::decode($v['t_persyaratan']);
                $jml_syarat = count($result_array_syarat);
                $result_array_verifikasi = \Zend\Json\Json::decode($v['t_verifikasispt']);
                $jml_verifikasi = count($result_array_verifikasi);
                $cektabelpersyaratan = $this->tbl_sspdsyarat->jumlahsyarat($v['t_idjenistransaksi']);
                if ($jml_syarat == $cektabelpersyaratan) {
                    $v['ceksyarat'] = 1;
                } else {
                    $v['ceksyarat'] = 2;
                }

                if ($jml_syarat == $jml_verifikasi) {
                    $v['cekverifikasi'] = 1;
                } else {
                    if (empty($v['t_verifikasispt'])) {
                        $v['cekverifikasi'] = 3;
                    } else {
                        $v['cekverifikasi'] = 2;
                    }
                }
                array_push($datahasil, $v);
            }
            /* $pdf = new \DOMPDFModule\View\Model\PdfModel();
              $pdf->setOption('filename', 'DataPendaftaran.pdf');
              $pdf->setOption('paperSize', 'legal');
              $pdf->setOption('paperOrientation', 'landscape');
              $pdf->setVariables(array(
              'data_Verifikasi' => $datahasil,
              'tgl_cetak' => $ar_tglcetak,
              'periode_spt' => $ar_periodespt,
              'tgl_verifikasi1' => $data_get->tgl_cetakstatusberkas1,
              'tgl_verifikasi2' => $data_get->tgl_cetakstatusberkas2,
              'nama_login' => $session['s_namauserrole'],
              'ar_pemda' => $ar_pemda
              ));
              return $pdf; */
            $view = new ViewModel(array(
                'data_Verifikasi' => $datahasil,
                'tgl_cetak' => $ar_tglcetak,
                'periode_spt' => $ar_periodespt,
                'tgl_verifikasi1' => $data_get->tgl_cetakstatusberkas1,
                'tgl_verifikasi2' => $data_get->tgl_cetakstatusberkas2,
                'nama_login' => $session['s_namauserrole'],
                'ar_pemda' => $ar_pemda,
                'cetak' => $data_get->cetak,
                'session' => $session,
            ));
            $data1 = array(
                'nilai' => '3', //no layout
                'session' => $session,
            );
            $this->layout()->setVariables($data1);
            return $view;
        }
    }

    // Semua Penghitungan BPHTB dari 13 Transaksi
    public function hitungBphtbAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());

                if (!empty($ex->t_idjenistransaksi)) {
                    $data = $this->getTblSSPDBphtb()->temukanDataNPOPTKP($ex);

                    //============== hitungan luas BPHTB
                    $t_luastanah = str_ireplace(".", "", $ex->t_luastanah);
                    $t_njoptanah = str_ireplace(".", "", $ex->t_njoptanah);
                    $t_luasbangunan = str_ireplace(".", "", $ex->t_luasbangunan);
                    $t_njopbangunan = str_ireplace(".", "", $ex->t_njopbangunan);
                    $t_nilaitransaksispt = str_ireplace(".", "", $ex->t_nilaitransaksispt);

                    $input = $this->getRequest();
                    $aphb_kali = $input->getPost('t_tarif_pembagian_aphb_kali');
                    $aphb_bagi = $input->getPost('t_tarif_pembagian_aphb_bagi');

                    if ($input->getPost('t_idpengurangan') != '') {

                        $pengurangan = $this->getPengurnaganTarif()->getDataId($input->getPost('t_idpengurangan'));
                    }
                    $t_persenpengurangan = isset($pengurangan->s_nilaipengurangan) ? $pengurangan->s_nilaipengurangan : null;
                    // var_dump($t_persenpengurangan); die;
                    //$hibah_kali = $input->getPost('t_tarif_pembagian_kali');
                    //$hibah_bagi = $input->getPost('t_tarif_pembagian_bagi');

                    if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                        $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
                        $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
                        $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

                        $t_grandtotalnjop_aphb = $t_grandtotalnjop;
                    } else {
                        $aphb = $aphb_kali / $aphb_bagi;

                        $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
                        $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
                        $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

                        $t_grandtotalnjop_hitung = $t_grandtotalnjop * $aphb;
                        $t_grandtotalnjop_aphb = ceil($t_grandtotalnjop_hitung);

                        /* $t_totalnjoptanah_hitung = $t_luastanah * $t_njoptanah * $aphb;
                          $t_totalnjoptanah = round($t_totalnjoptanah_hitung,0,PHP_ROUND_HALF_EVEN);

                          $t_totalnjopbangunan_hitung = $t_luasbangunan * $t_njopbangunan * $aphb;
                          $t_totalnjopbangunan = round($t_totalnjopbangunan_hitung,0,PHP_ROUND_HALF_EVEN);
                          $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan; */
                    }

                    $data['t_totalnjoptanah'] = $t_totalnjoptanah;
                    $data['t_totalnjopbangunan'] = $t_totalnjopbangunan;
                    $data['t_grandtotalnjop'] = $t_grandtotalnjop;
                    $data['t_grandtotalnjop_aphb'] = $t_grandtotalnjop_aphb;
                    $data['t_persenpengurangan'] = $t_persenpengurangan;

                    if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

                        if ($t_grandtotalnjop > $t_nilaitransaksispt) {
                            $data['t_npopspt'] = $t_grandtotalnjop;
                            //hitungBphtb();
                        } else {
                            $data['t_npopspt'] = $t_nilaitransaksispt;
                            //hitungBphtb();
                        }
                    } else {
                        if ($t_grandtotalnjop_aphb > $t_nilaitransaksispt) {
                            $data['t_npopspt'] = $t_grandtotalnjop_aphb;
                            //hitungBphtb();
                        } else {
                            $data['t_npopspt'] = $t_nilaitransaksispt;
                            //hitungBphtb();
                        }
                    }

                    //========================= cek dapat potongan apa gak
                    $tahunsekarang = date('Y');
                    $tahunproses = date('Y', strtotime($ex->t_tglprosesspt));
                    $ceknik = $this->getTblSPT()->ceknik($ex->t_nikwppembeli, $tahunproses);
                    //var_dump($ceknik);exit();

                    $ceknpoptkp = $this->getTblSPT()->cekpotongannpoptkp($ex->t_idjenistransaksi);
                    $id = (int) $ex->t_idspt;
                    //$id = (int) $kb->t_iddetailsptbphtb;
                    if (($id == 0) || ($id == null)) {
                        $cektarifbphtb = $this->getTblSPT()->gettarifbphtbsekarang();
                        $tarifbphtbsekarang = $cektarifbphtb['s_tarifbphtb'];
                        if (!empty($ceknik['t_idspt'])) {
                            //==================== dapat 60jt satu kali & 300jt satu kali dalam 1th
                            //jika hibah wasiat (4) / waris (5) data transaksinya trus cek lagi 
                            if (($ceknik['t_idjenistransaksi'] == 4) || ($ceknik['t_idjenistransaksi'] == 5)) {
                                //jika transaksi bukan 4/5 maka cek lagi hibah wasiat
                                if (($ex->t_idjenistransaksi == 4) || ($ex->t_idjenistransaksi == 5)) {
                                    //TIDAK DAPAT SEHARUSNYA
                                    $t_potonganspt = 0;
                                    $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                } else {
                                    //cek lagi bener gak ra ana hibahwasiat / waris
                                    $ceknik_selainwarishibahwasiat = $this->getTblSPT()->ceknikselainwarishibahwasiat($ex->t_nikwppembeli, $tahunproses);
                                    if (!empty($ceknik_selainwarishibahwasiat['t_idspt'])) {
                                        //TIDAK DAPAT SEHARUSNYA
                                        $t_potonganspt = 0;
                                        $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                    } else {
                                        //DAPAT SEHARUSNYA
                                        $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                        $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                    }
                                }
                            } else {
                                if (($ex->t_idjenistransaksi == 4) || ($ex->t_idjenistransaksi == 5)) {
                                    $ceknik_warishibahwasiat = $this->getTblSPT()->ceknikwarishibahwasiat($ex->t_nikwppembeli, $tahunproses);
                                    if (!empty($ceknik_warishibahwasiat['t_idspt'])) {
                                        //TIDAK DAPAT SEHARUSNYA
                                        $t_potonganspt = 0;
                                        $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                    } else {
                                        //DAPAT SEHARUSNYA
                                        $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                        $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                    }
                                } else {
                                    //TIDAK DAPAT SEHARUSNYA
                                    $t_potonganspt = 0;
                                    $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                }
                            }
                            //==================== end dapat 60jt satu kali & 300jt satu kali dalam 1th
                            //$t_potonganspt = 0;
                            //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP '.$ceknik['t_idjenistransaksi'];
                        } else {
                            //DAPAT SEHARUSNYA
                            $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                            $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                        }
                    } else {
                        $cektarifbphtb = $this->getTblSPT()->gettarifbphtbspt($id);
                        $tarifbphtbsekarang = $cektarifbphtb['t_persenbphtb'];

                        if (!empty($ceknik['t_idspt'])) {
                            if ($ceknik['t_idspt'] == $id) {
                                //$t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                //$data['ket_npoptkp'] = 'Dapat NPOPTKP we';
                                //=========== cek lagi
                                if (($ceknik['t_idjenistransaksi'] == 4) || ($ceknik['t_idjenistransaksi'] == 5)) {
                                    $ceknik_warishibahwasiat = $this->getTblSPT()->ceknikwarishibahwasiat($ex->t_nikwppembeli, $tahunproses);
                                    if ($ceknik_warishibahwasiat['t_idspt'] == $id) {
                                        //$t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                        //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                        if (($ex->t_idjenistransaksi == 4) || ($ex->t_idjenistransaksi == 5)) {
                                            $ceknik_selainwarishibahwasiat = $this->getTblSPT()->ceknikselainwarishibahwasiat($ex->t_nikwppembeli, $tahunproses);
                                            if (($ceknik_selainwarishibahwasiat['t_idspt'] == $id) || empty($ceknik_selainwarishibahwasiat['t_idspt'])) {
                                                //DAPAT SEHARUSNYA
                                                $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                                $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                            } else {
                                                //TIDAK DAPAT SEHARUSNYA
                                                $t_potonganspt = 0;
                                                $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                            }
                                        } else {
                                            //DAPAT SEHARUSNYA
                                            $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                            $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                        }
                                    } else {
                                        //TIDAK DAPAT SEHARUSNYA
                                        $t_potonganspt = 0;
                                        $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                    }
                                } else {
                                    $ceknik_selainwarishibahwasiat = $this->getTblSPT()->ceknikselainwarishibahwasiat($ex->t_nikwppembeli, $tahunproses);
                                    if ($ceknik_selainwarishibahwasiat['t_idspt'] == $id) {
                                        //$t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                        //$data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                        if (($ex->t_idjenistransaksi == 4) || ($ex->t_idjenistransaksi == 5)) {
                                            $ceknik_warishibahwasiat = $this->getTblSPT()->ceknikwarishibahwasiat($ex->t_nikwppembeli, $tahunproses);
                                            if (($ceknik_warishibahwasiat['t_idspt'] == $id) || empty($ceknik_warishibahwasiat['t_idspt'])) {
                                                //DAPAT SEHARUSNYA
                                                $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                                $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                            } else {
                                                //TIDAK DAPAT SEHARUSNYA
                                                $t_potonganspt = 0;
                                                $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                            }
                                        } else {
                                            //DAPAT SEHARUSNYA
                                            $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                            $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                        }
                                    } else {
                                        //TIDAK DAPAT SEHARUSNYA
                                        $t_potonganspt = 0;
                                        $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                    }
                                }
                                //=========== end cek lagi
                            } else {
                                //==================== dapat 60jt satu kali & 300jt satu kali dalam 1th
                                //jika hibah wasiat (4) / waris (5)
                                if (($ceknik['t_idjenistransaksi'] == 4) || ($ceknik['t_idjenistransaksi'] == 5)) {
                                    if (($ex->t_idjenistransaksi == 4) || ($ex->t_idjenistransaksi == 5)) {
                                        //TIDAK DAPAT SEHARUSNYA
                                        $t_potonganspt = 0;
                                        $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                    } else {
                                        $ceknik_selainwarishibahwasiat = $this->getTblSPT()->ceknikselainwarishibahwasiat($ex->t_nikwppembeli, $tahunproses);
                                        if (($ceknik_selainwarishibahwasiat['t_idspt'] == $id) || empty($ceknik_selainwarishibahwasiat['t_idspt'])) {
                                            //DAPAT SEHARUSNYA
                                            $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                            $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                        } else {
                                            //TIDAK DAPAT SEHARUSNYA
                                            $t_potonganspt = 0;
                                            $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                        }
                                    }
                                } else {
                                    if (($ex->t_idjenistransaksi == 4) || ($ex->t_idjenistransaksi == 5)) {
                                        $ceknik_warishibahwasiat = $this->getTblSPT()->ceknikwarishibahwasiat($ex->t_nikwppembeli, $tahunproses);
                                        if (($ceknik_warishibahwasiat['t_idspt'] == $id) || empty($ceknik_warishibahwasiat['t_idspt'])) {
                                            //DAPAT SEHARUSNYA
                                            $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                                            $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                                        } else {
                                            //TIDAK DAPAT SEHARUSNYA
                                            $t_potonganspt = 0;
                                            $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                        }
                                    } else {
                                        //TIDAK DAPAT SEHARUSNYA
                                        $t_potonganspt = 0;
                                        $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                                    }
                                }
                                //==================== end dapat 60jt satu kali & 300jt satu kali dalam 1th
                                //$t_potonganspt = 0;
                                //$data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                            }
                        } else {
                            //DAPAT SEHARUSNYA
                            $t_potonganspt = $ceknpoptkp['s_tarifnpotkp'];
                            $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                        }
                    }
                    //========================= cek dapat potongan apa gak   

                    if (($ex->t_idjenistransaksi == 4) || ($ex->t_idjenistransaksi == 5)) {
                        // $data['t_potongan_waris_hibahwasiat'] = 50;
                        // $hitung_potonganwaris = $data['t_potongan_waris_hibahwasiat'] / 100;

                        $data['t_potongan_waris_hibahwasiat'] = 0;
                        $hitung_potonganwaris = 1;
                    } else {
                        $data['t_potongan_waris_hibahwasiat'] = 0;
                        $hitung_potonganwaris = 1;
                    }
                    //$t_potonganspt = str_ireplace(".", "", $ex->t_potonganspt);
                    $t_npopspt = str_ireplace(".", "", $ex->t_npopspt);
                    $t_persenbphtb = str_ireplace(".", "", $ex->t_persenbphtb);
                    if ($t_potonganspt == '0') {
                        $data['t_potonganspt'] = 0;
                        $data['t_npopkpspt'] = $data['t_npopspt']; //$t_npopspt;
                        $data['t_totalspt'] = ceil($data['t_npopkpspt'] * $t_persenbphtb / 100 * $hitung_potonganwaris);
                    } else {
                        $data['t_potonganspt'] = $t_potonganspt; // $data['s_tarifnpotkp'];
                        $npop = $data['t_npopspt']; // $t_npopspt;
                        $npopkp = $npop - $data['t_potonganspt'];
                        if ($npopkp <= 0) {
                            $data['t_npopkpspt'] = 0;
                            $data['t_totalspt'] = 0;
                        } else {
                            $data['t_npopkpspt'] = $npopkp;
                            $data['t_totalspt'] = ceil($data['t_npopkpspt'] * $t_persenbphtb / 100 * $hitung_potonganwaris);
                        }
                    }

                    if ($t_persenpengurangan != null) {
                        $pengurangan = $data['t_totalspt'] * $t_persenpengurangan / 100;
                        $data['t_totalspt'] = $data['t_totalspt'] - $pengurangan;
                    }
                } else {
                    $data = array();
                }

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Menghitung nilai njop pbb
    public function hitungnjopAction()
    {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $t_luastanah = str_ireplace(".", "", $ex->t_luastanah);
                $t_njoptanah = str_ireplace(".", "", $ex->t_njoptanah);
                $t_luasbangunan = str_ireplace(".", "", $ex->t_luasbangunan);
                $t_njopbangunan = str_ireplace(".", "", $ex->t_njopbangunan);

                $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
                $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
                $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;
                $data = array(
                    "t_totalnjoptanah" => $t_totalnjoptanah,
                    "t_totalnjopbangunan" => $t_totalnjopbangunan,
                    "t_grandtotalnjop" => $t_grandtotalnjop
                );
                if ($ex->t_idjenistransaksi != 1 || $ex->t_idjenistransaksi != 8) {
                    $data["t_nilaitransaksispt"] = $t_grandtotalnjop;
                }
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function cetakpermohonanpenelitianAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $ar_sspd = $this->getSSPD()->getdatapenelitian($data_get);
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'PermohonanPenelitian.pdf');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $data_get->tgl_cetak_penelitian
        ));
        return $pdf;
    }

    // public function cetaksspdbphtbDOMPDFAction() {
    //     $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
    //     $req = $this->getRequest();
    //     if ($req->isGet()) {
    //         $base = new \Bphtb\Model\Pencetakan\SSPDBase();
    //         $frm->setData($req->getQuery());
    //         $ar_sebelum = "";
    //         if ($frm->isValid()) {
    //             $base->exchangeArray($frm->getData());
    //             $app_path = $this->getServiceLocator()->get('app_path') . "/";
    //             $data_get = $req->getQuery();
    //             if ($data_get->action == 'cetaksspd') { // nyetak sspd dari tombol cetak dalam tabel grid
    //                 //=============== Model\Pencetakan\SSPDTable
    //                 $ar_sspd = $this->getSSPD()->getDataSSPDArrayIdSpt($data_get->t_idspt); //$ar_sspd = $this->getSSPD()->getviewcetakssp($data_get->t_idspt);
    //                 $data_sspd = $this->getSSPD()->getviewcetakssp($data_get->t_idspt);
    //                 $dataidsptsebelum = $this->getSSPD()->getdataidsptsebelumnya($data_get->t_idspt);
    //                 if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) { // jika ada spt sebelumnya yang dikoreksi bpn
    //                     $ar_sebelum = $this->getSSPD()->getdatassspdsebelumnya($dataidsptsebelum['t_idsptsebelumnya']);
    //                 }
    //                 $ar_tglcetak = date('d-m-Y');
    //             } else { // nyetak sspd yang dari pop up modal
    //                 $ar_sspd = $this->getSSPD()->getdatasspd($base);
    //                 $data_sspd = $this->getSSPD()->getdatasspd($base);
    //                 $ar_tglcetak = $base->tgl_cetak;
    //             }
    //             $ar_pemda = $this->getPemda()->getdata();

    //             $ar_Mengetahui = $this->getTblPejabat()->getdataid((int)$data_get->mengetahuibphtbsspd);
    //         }
    //     }

    //     $data_sspd_array = $data_sspd->current();
    //     $detail_nop_array = array();
    //     if($data_sspd_array['t_jenispendaftaran']==4){
    //         // nop gabungan
    //         $detail_nop = $this->getSSPD()->getDetailNOP($data_sspd_array['t_iddetailsptbphtb']);
    //         foreach ($detail_nop as $row):
    //             $detail_nop_array[] = $row;
    //         endforeach;
    //     }

    // //    var_dump($detail_nop_array);die;

    // //    $data_sspd = $ar_sspd2->current();
    // //    var_dump($ar_sspd);die;
    // //    var_dump($ar_sspd2);die;
    //     $pdf = new \DOMPDFModule\View\Model\PdfModel();
    // //    $pdf->setOption('filename', 'SSPD.pdf');
    //     $pdf->setOption('paperSize', 'A4');
    //     $pdf->setOption('paperOrientation', 'potrait');
    //     $pdf->setVariables(array(
    //         'app_path' => $app_path,
    //         'data_sspd' => $ar_sspd,
    //         'data_pemda' => $ar_pemda,
    //         'tgl_cetak' => $ar_tglcetak,
    //         'ar_sebelum' => $ar_sebelum,
    //         'dataidsptsebelum' => $dataidsptsebelum,
    //         'data_mengetahui' => $ar_Mengetahui,
    //         'cetakversi' => $data_get->cetakversi,
    //         'detail_nop' => $detail_nop_array,
    //     ));
    //     return $pdf;
    // }

    public function cetaksspdbphtbAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            $ar_sebelum = "";
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();
                if ($data_get->action == 'cetaksspd') { // nyetak sspd dari tombol cetak dalam tabel grid
                    //=============== Model\Pencetakan\SSPDTable
                    $ar_sspd = $this->getSSPD()->getDataSSPDArrayIdSpt($data_get->t_idspt); //$ar_sspd = $this->getSSPD()->getviewcetakssp($data_get->t_idspt);
                    $data_sspd = $this->getSSPD()->getviewcetakssp($data_get->t_idspt);
                    // var_dump($ar_sspd); die;

                    $dataidsptsebelum = $this->getSSPD()->getdataidsptsebelumnya($data_get->t_idspt);

                    if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) { // jika ada spt sebelumnya yang dikoreksi bpn
                        $ar_sebelum = $this->getSSPD()->getdatassspdsebelumnya($dataidsptsebelum['t_idsptsebelumnya']);
                    }
                    $ar_tglcetak = date('d-m-Y');
                } else { // nyetak sspd yang dari pop up modal
                    $ar_sspd = $this->getSSPD()->getdatasspd($base);
                    $data_sspd = $this->getSSPD()->getdatasspd($base);
                    $ar_tglcetak = $base->tgl_cetak;
                }
                $ar_pemda = $this->getPemda()->getdata();

                $ar_Mengetahui = $this->getTblPejabat()->getdataid((int)$data_get->mengetahuibphtbsspd);
                // var_dump($ar_Mengetahui);die;
            }
        }

        // $data_sspd_array = $data_sspd->current();
        // $kodebayar = $data_sspd_array["t_kodebayarbanksppt"];
        // $detail_nop_array = array();
        // if($data_sspd_array['t_jenispendaftaran']==4){
        //     // nop gabungan
        //     $detail_nop = $this->getSSPD()->getDetailNOP($data_sspd_array['t_iddetailsptbphtb']);
        //     foreach ($detail_nop as $row):
        //         $detail_nop_array[] = $row;
        //     endforeach;
        // }

        // return $this->create_sppd($ar_pemda, $ar_sspd, $dataidsptsebelum, $ar_sebelum, $detail_nop_array, $kodebayar, $data_get->cetakversi, "D", $ar_Mengetahui, null, $this->cekurl(), null);

        $view = new ViewModel(array(
            'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $ar_tglcetak,
            'ar_sebelum' => $ar_sebelum,
            'dataidsptsebelum' => $dataidsptsebelum,
            'data_mengetahui' => $ar_Mengetahui,
            'cetakversi' => $data_get->cetakversi,
            // 'detail_nop' => $detail_nop_array,
        ));
        $data = array(
            'nilai' => '3', //no layout
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakntpdAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            $ar_sebelum = "";
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();
                $ar_sspd = $this->getSSPD()->getviewcetakssp($data_get->t_idspt);
                $ar_tglcetak = date('d-m-Y');
                $ar_pemda = $this->getPemda()->getdata();

                if (!empty($data_get->mengetahuibphtbsspd)) {
                    if ($data_get->mengetahuibphtbsspd == 'null') {
                        $ar_Mengetahui = array();
                    } else {
                        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtbsspd);
                    }
                } else {
                    $ar_Mengetahui = array();
                }
            }
        }

        $view = new ViewModel(array(
            'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $ar_tglcetak,
            'ar_sebelum' => $ar_sebelum,

            'data_mengetahui' => $ar_Mengetahui,
            'cetakversi' => $data_get->cetakversi,
            'lembar' => $data_get->lembar,
        ));
        $data = array(
            'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetakbuktipenerimaanAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                // $app_path = $this->getServiceLocator()->get('app_path') . "/";
                $data_get = $req->getQuery();
                // if ($data_get->action == 'cetakbuktipenerimaan') {
                // $ar_sspd = $this->getSSPD()->getdatassspd($data_get->t_idspt);
                // $ar_tglcetak = date('d-m-Y');
                // } else {
                //=============== Model\Pencetakan\SSPDTable
                $ar_sspd = $this->getSSPD()->getdatasspdbuktipenerimaan($base, $session['s_iduser'], $session['s_tipe_pejabat']); //getdatasspd
                $ar_tglcetak = $base->tgl_cetak_bukti;
                // $hasildata = $this->getSSPD()->getdatasspd($base);
                // }
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        /*
         * $testarray = array();
         * foreach ($hasildata as $row) {
         * $combosyarat = $this->getTblPersyaratan()->comboBox($row['t_idjenistransaksi']);
         * array_push($testarray, $combosyarat);
         * }
         */
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'BuktiPenerimaan.pdf');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            // 'app_path' => $app_path,
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda
        ));
        // 'testarray' => $testarray,
        // 'combosyarat' => $combosyarat,

        return $pdf;
    }

    //======================= upload file syarat
    public function uploadfilesyaratAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');

            //=========== persyaratan
            $combosyarat = $this->getTblPersyaratan()->syaratfileupload($data['t_idjenistransaksi'], $data['t_idspt']);
        }
        $view = new ViewModel(array(
            'datasspd' => $data,
            'combosyarat' => $combosyarat
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function uploadjenisyaratAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');

            //=========== persyaratan
            $combosyarat = $this->getTblPersyaratan()->syaratfileupload($data['t_idjenistransaksi'], $data['t_idspt']);
            $idyyarat = $req->getQuery()->get('syarat');
            $namasyarat = $this->getTblPersyaratan()->getDataId($idyyarat);
        }
        $view = new ViewModel(array(
            'datasspd' => $data,
            //'combosyarat' => $combosyarat,
            'idspt' => $req->getQuery()->get('t_idspt'),
            'idsyarat' => $req->getQuery()->get('syarat'),
            'namasyarat' => $namasyarat,
        ));


        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
            //'nilai' => '3' //no layout
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function uploadfilesyarat2Action()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {
            $show_hide_combo_notaris = "display:inherit;";
            $notaris = 2;
        } elseif ($session['s_tipe_pejabat'] == 2) {
            //if ($session['s_namauserrole'] == "Notaris") {
            $show_hide_combo_notaris = "display:none;";
            $notaris = 1;
        }
        $string = Rand::getString(6, 'abcdefghijklmnopqrstuvwxyz-1234567890_ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $frm = new \Bphtb\Form\Pendataan\SSPDFrmUpload($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris());
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId($id);
            $datahistory = $this->getTblSSPDBphtb()->gethistorybphtb($data->t_nikwppembeli, $data->t_idspt, $data->t_periodespt);
            if ($data->t_idjenistransaksi == 5) {
                $show_hide_penerima_waris = "display:inherit;";
            } else {
                $show_hide_penerima_waris = "display:none;";
            }
            $data_penerimawaris = $this->getTblWaris()->CariPenerimaWaris($data->t_idspt);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $t_luastanah = str_ireplace('.', '', $data->t_luastanah);
            $t_luasbangunan = str_ireplace('.', '', $data->t_luasbangunan);
            $data->t_luastanah = number_format(($t_luastanah / 100), 0, ',', '.');
            $data->t_luasbangunan = number_format(($t_luasbangunan / 100), 0, ',', '.');
            $frm2 = new \Bphtb\Form\Pendataan\SSPDFrmUpload($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), $string, $this->populateComboNotaris(), $this->populatePersyaratanId($data->t_idjenistransaksi));
            $frm2->bind($data);
            $frm2->get("t_terbukti")->setValue(\Zend\Json\Json::decode($data->t_terbukti));
            $frm2->get("t_persyaratan")->setValue(\Zend\Json\Json::decode($data->t_persyaratan));

            $data_tarif = $this->getTblSSPDBphtb()->temukanDataTarifBphtb_tahun($data->t_periodespt);


            $combosyarat = $this->getTblPersyaratan()->syaratfileupload($data->t_idjenistransaksi, $data->t_idspt);
        }




        //========== panggil Model\Pendataan\SSPDBphtbTable


        $view = new ViewModel(array(
            'frm' => $frm2,
            'data' => $data->t_terbukti,
            'show_hide_combo_notaris' => $show_hide_combo_notaris,
            'datahistory' => $datahistory,
            'show_hide_penerima_waris' => $show_hide_penerima_waris,
            'data_penerimawaris' => $data_penerimawaris,
            'idtarifbphtb' => $data_tarif['s_idtarifbphtb'],
            'notaris' => $notaris,
            'datasspd' => $data,
            'combosyarat' => $combosyarat
        ));

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function uploadsyaratpostAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $req = $this->getRequest();
        $helper = new ValidationHelper();
        $validasi = $helper->validateAndUploadFile($req->getFiles()['files'][0]);

        if ($validasi['status'] == false) {
            $this->simpanfilepost(null, null, null, null, null, true, null, 1);
            // echo $validasi['message'];
            // exit;
        }

        $id = (int) $req->getQuery()->get('idspt');
        $idsyarat = (int) $req->getQuery()->get('idsyarat');
        if (!empty($id)) {

            $datacari = $this->getTblSSPDBphtb()->getDataId_all($id);
            $letak_dir1 = "public";
            $letak_dir2 = "upload";
            $letak_dir3 = "filetransaksi";
            $letak_dir4 = $datacari['t_periodespt'];
            $letak_dir5 = str_replace(' ', '', strtolower($datacari['s_namajenistransaksi']));
            $letak_dir6 = $datacari['t_idspt'];
            $letak_dir7 = $req->getQuery()->get('idsyarat');

            $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/' . $letak_dir6 . '/' . $letak_dir7 . '/';
            $view = new ViewModel(array(
                'get' => $req,
                'letakfile' => $letak_dir,
                'idspt' => $id,
                'idsyarat' => $idsyarat,
                'datasspd' => $datacari
            ));

            $options2 = array(
                'delete_type' => 'POST',
            );

            $this->simpanfilepost($options2, $letak_dir, $id, $idsyarat, $datacari);
        } else {
            $view = new ViewModel(array(
                'get' => $req,
            ));
        }

        $data = array(
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'nilai' => '3', //no layout
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function uploadsyaratpost2Action()
    {
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $req = $this->getRequest();
        $newFile = "";


        $iddetailspt = $req->getPost('t_iddetailsptbphtb');

        if (!empty($iddetailspt)) {
            $post = array_merge_recursive($req->getPost()->toArray(), $req->getFiles()->toArray());
            $httpadapter = new \Zend\File\Transfer\Adapter\Http();
            $httpadapter->setDestination('public/upload/');
            //if ($httpadapter->receive($post["uploadsyarat"]["name"])) {
            //    $newFile = $httpadapter->getFileName();
            //}

            $sm = $this->getServiceLocator();
            $tbl_upload = $sm->get("UploadTable");

            //var_dump($newFile);
            //exit();
            //$this->tbl_upload->savedata($base, $newFile);

            for ($i = 1; $i <= (int) ($req->getPost('hdnLine')); $i++) {
                if ($req->getPost("fileUpload" . $i) != "") { //["name"]
                    $letak_dir1 = "public";
                    $letak_dir2 = "upload";
                    $letak_dir3 = "filetransaksi";
                    $letak_dir4 = $req->getPost('foldertahun');
                    $letak_dir5 = $req->getPost('foldertransaksi');
                    $letak_dir6 = $req->getPost('folderidspt');

                    $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/' . $letak_dir6 . '/';
                    //mkdir("kampret/module/asem/src/anjir/Controller", 0777, true);

                    if (is_dir($letak_dir1) == false) {
                        mkdir("" . $letak_dir1 . "", 0777, true);  // Create directory if it does not exist
                    }
                    if (is_dir($letak_dir1 . '/' . $letak_dir2) == false) {
                        mkdir("" . $letak_dir1 . "/" . $letak_dir2, 0777, true);  // Create directory if it does not exist
                    }
                    if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3) == false) {
                        mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "", 0777, true);  // Create directory if it does not exist
                    }

                    if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                        mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                    }

                    if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                        mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                    }

                    if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5) == false) {
                        mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5, 0777, true);  // Create directory if it does not exist
                    }

                    if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/' . $letak_dir6) == false) {
                        mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5 . "/" . $letak_dir6, 0777, true);  // Create directory if it does not exist
                    }

                    if (move_uploaded_file($_FILES["fileUpload" . $i]["tmp_name"], "" . $letak_dir . "" . $_FILES["fileUpload" . $i]["name"])) {
                        echo $req->getPost("fileUpload" . $i);
                        chmod($letak_dir . "" . $_FILES["fileUpload" . $i]["name"], 0777, true);
                        $tbl_upload->savedata_syarat($req->getPost('idjenistransaksi'), $req->getPost('id_detailspt' . $i), $_FILES["fileUpload" . $i]["name"], $req->getPost('keterangan_file' . $i), $req->getPost('id_syarat' . $i), $letak_dir, $req->getPost('folderidspt'), $req->getPost('folderiddetailspt'));
                        //$strSQL = "INSERT INTO gallery ";
                        //$strSQL .="(GalleryName,Picture) VALUES ('".$_POST["txtGalleryName".$i]."','".$_FILES["fileUpload".$i]["name"]."')";
                        //mysql_query($strSQL);
                        //echo "Copy/Upload ".$_FILES["fileUpload".$i]["name"]." completed.<br>";
                    }
                }
            }



            //return $this->redirect()->toRoute('pendataan_sspd');
        }
    }

    //======================= end upload file syarat


    function populatePersyaratanId($id)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($id);
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idpersyaratan] = $row->s_namapersyaratan;
        }
        return $selectData;
    }

    public function populateComboJenisTransaksi()
    {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idjenistransaksi] = str_pad($row->s_kodejenistransaksi, 2, '0', STR_PAD_LEFT) . " | " . $row->s_namajenistransaksi;
        }
        return $selectData;
    }

    public function populateComboHakTanah()
    {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    public function comboPenguranganTarif()
    {
        $data = $this->getPengurnaganTarif()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idpengurangan] = $row->s_namapengurangan;
        }
        return $selectData;
    }

    public function populateComboDokTanah()
    {
        $data = $this->getTblDokTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_iddoktanah] = $row->s_namadoktanah;
        }
        return $selectData;
    }

    private function populateComboNotaris()
    {
        $data = $this->getNotaris()->getdataCombo();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row['s_iduser']] = $row['s_namanotaris'];
        }
        return $selectData;
    }

    public function getSSPD()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblSSPDBphtb()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDBphtbTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblWaris()
    {
        if (!$this->tbl_sspdwaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspdwaris = $sm->get("SPTWarisTable");
        }
        return $this->tbl_sspdwaris;
    }

    public function getTblSpt()
    {
        if (!$this->tbl_spt) {
            $sm = $this->getServiceLocator();
            $this->tbl_spt = $sm->get("SPTTable");
        }
        return $this->tbl_spt;
    }

    public function getTblSPPT()
    {
        if (!$this->tbl_nop) {
            $sm = $this->getServiceLocator();
            $this->tbl_nop = $sm->get('SPPTTable');
        }
        return $this->tbl_nop;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getTblPejabat()
    {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getNotaris()
    {
        if (!$this->tbl_notaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_notaris = $sm->get('NotarisBphtbTable');
        }
        return $this->tbl_notaris;
    }

    public function getTblPersyaratan()
    {
        if (!$this->tbl_persyaratan) {
            $sm = $this->getServiceLocator();
            $this->tbl_persyaratan = $sm->get('PersyaratanTable');
        }
        return $this->tbl_persyaratan;
    }

    public function getTblJenTran()
    {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan()
    {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getTblDokTan()
    {
        if (!$this->tbl_doktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_doktanah = $sm->get('DokTanahTable');
        }
        return $this->tbl_doktanah;
    }

    public function getPengurnaganTarif()
    {
        if (!$this->tbl_pengurangantarif) {
            $sm = $this->getServiceLocator();
            $this->tbl_pengurangantarif = $sm->get('PenguranganTarifTable');
        }
        return $this->tbl_pengurangantarif;
    }

    //==================================================================================== class upload
    //public function __construct($options = null, $lokasifile = null, $idspt = null, $idsyarat = null, $datasspd = null, $initialize = true, $error_messages = null) {
    public function simpanfilepost($options = null, $lokasifile = null, $idspt = null, $idsyarat = null, $datasspd = null, $initialize = true, $error_messages = null, $asd = null)
    {

        //global $lokasifile
        //echo kampret();
        //$this->MenuHelper()->SimpanFileUpload('5', '12886', 'DSC03494.jpg', 'waris','26','public/upload/filetransaksi/2016/waris/13053/26/','13053','12886');

        $this->lokasifile = $lokasifile;
        $this->idspt = $idspt;
        $this->idsyarat = $idsyarat;
        $this->idjenistransaksi = $datasspd['t_idjenistransaksi'];
        $this->t_iddetailsptbphtb = $datasspd['t_iddetailsptbphtb'];
        $this->foldertahun = $datasspd['t_periodespt'];
        $this->s_namajenistransaksi = str_replace(' ', '', strtolower($datasspd['s_namajenistransaksi']));
        $this->folderidspt = $idspt;
        $this->folderiddetailspt = $datasspd['t_iddaetailsptbphtb'];

        $this->response = array();
        $this->options = array(
            'script_url' => $this->get_full_url() . '/' . $this->basename($this->get_server_var('SCRIPT_NAME')),
            'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')) . '/' . $lokasifile . '', //'.$this->letakfile.' 
            'upload_url' => $this->get_full_url() . '/' . $lokasifile . '', //
            'input_stream' => 'php://input',
            'user_dirs' => false,
            'mkdir_mode' => 0755,
            'param_name' => 'files',
            // Set the following option to 'POST', if your server does not support
            // DELETE requests. This is a parameter sent to the client:
            'delete_type' => 'DELETE',
            'access_control_allow_origin' => '*',
            'access_control_allow_credentials' => false,
            'access_control_allow_methods' => array(
                'OPTIONS',
                'HEAD',
                'GET',
                'POST',
                'PUT',
                'PATCH',
                'DELETE'
            ),
            'access_control_allow_headers' => array(
                'Content-Type',
                'Content-Range',
                'Content-Disposition'
            ),
            // By default, allow redirects to the referer protocol+host:
            'redirect_allow_target' => '/^' . preg_quote(
                parse_url($this->get_server_var('HTTP_REFERER'), PHP_URL_SCHEME)
                    . '://'
                    . parse_url($this->get_server_var('HTTP_REFERER'), PHP_URL_HOST)
                    . '/', // Trailing slash to not match subdomains by mistake
                '/' // preg_quote delimiter param
            ) . '/',
            // Enable to provide file downloads via GET requests to the PHP script:
            //     1. Set to 1 to download files via readfile method through PHP
            //     2. Set to 2 to send a X-Sendfile header for lighttpd/Apache
            //     3. Set to 3 to send a X-Accel-Redirect header for nginx
            // If set to 2 or 3, adjust the upload_url option to the base path of
            // the redirect parameter, e.g. '/files/'.
            'download_via_php' => false,
            // Read files in chunks to avoid memory limits when download_via_php
            // is enabled, set to 0 to disable chunked reading of files:
            'readfile_chunk_size' => 10 * 1024 * 1024, // 10 MiB
            // Defines which files can be displayed inline when downloaded:
            'inline_file_types' => '/\.(gif|jpe?g|png|pdf)$/i',
            // Defines which files (based on their names) are accepted for upload:
            //'accept_file_types' => '/.+$/i',
            'accept_file_types' => '/\.(gif|jpe?g|png|pdf)$/i',
            // The php.ini settings upload_max_filesize and post_max_size
            // take precedence over the following max_file_size setting:
            //'max_file_size' => null,
            'max_file_size' => 2000000,
            'min_file_size' => 1,
            // The maximum number of files for the upload directory:
            'max_number_of_files' => null,
            // Defines which files are handled as image files:
            'image_file_types' => '/\.(gif|jpe?g|png)$/i',
            // Use exif_imagetype on all files to correct file extensions:
            'correct_image_extensions' => false,
            // Image resolution restrictions:
            'max_width' => null,
            'max_height' => null,
            'min_width' => 1,
            'min_height' => 1,
            // Set the following option to false to enable resumable uploads:
            'discard_aborted_uploads' => true,
            // Set to 0 to use the GD library to scale and orient images,
            // set to 1 to use imagick (if installed, falls back to GD),
            // set to 2 to use the ImageMagick convert binary directly:
            'image_library' => 1,
            // Uncomment the following to define an array of resource limits
            // for imagick:
            // Command or path for to the ImageMagick convert binary:
            'convert_bin' => 'convert',
            // Uncomment the following to add parameters in front of each
            // ImageMagick convert call (the limit constraints seem only
            // to have an effect if put in front):
            // Command or path for to the ImageMagick identify binary:
            'identify_bin' => 'identify',
            'image_versions' => array(
                // The empty image version key defines options for the original image:
                '' => array(
                    // Automatically rotate images based on EXIF meta data:
                    'auto_orient' => true
                ),
                // Uncomment the following to create medium sized images:
                'thumbnail' => array(
                    // Uncomment the following to use a defined directory for the thumbnails
                    // instead of a subdirectory based on the version identifier.
                    // Make sure that this directory doesn't allow execution of files if you
                    // don't pose any restrictions on the type of uploaded files, e.g. by
                    // copying the .htaccess file from the files directory for Apache:
                    //'upload_dir' => dirname($this->get_server_var('SCRIPT_FILENAME')).'/thumb/',
                    //'upload_url' => $this->get_full_url().'/thumb/',
                    // Uncomment the following to force the max
                    // dimensions and e.g. create square thumbnails:
                    //'crop' => true,
                    'max_width' => 80,
                    'max_height' => 80
                )
            ),
            'print_response' => true
        );

        if ($options || $asd == 1) {
            // Debug::dump("asu");
            // exit;
            $this->options = $options + $this->options;
        }
        if ($error_messages) {
            // Debug::dump("asu2");
            // exit;
            $this->error_messages = $error_messages + $this->error_messages;
        }
        if ($initialize) {
            // Debug::dump("asu3");
            // exit;
            $this->initialize();
        }
    }

    protected function initialize()
    {
        /* $this->db = new mysqli(
          $this->options['db_host'],
          $this->options['db_user'],
          $this->options['db_pass'],
          $this->options['db_name']
          ); */
        //parent::initialize();
        //============ pindah kene
        switch ($this->get_server_var('REQUEST_METHOD')) {
            case 'OPTIONS':
            case 'HEAD':
                $this->head();
                break;
            case 'GET':
                $this->get($this->options['print_response']);
                break;
            case 'PATCH':
            case 'PUT':
            case 'POST':
                $this->post($this->options['print_response']);
                break;
            case 'DELETE':
                $this->delete($this->options['print_response']);
                break;
            default:
                $this->header('HTTP/1.1 405 Method Not Allowed');
        }

        //============ end pindah kene
        //$this->db->close();
    }

    protected function get_file_object($file_name)
    {
        if ($this->is_valid_file_object($file_name)) {
            $file = new \stdClass();
            $file->name = $file_name;
            $file->size = $this->get_file_size(
                $this->get_upload_path($file_name)
            );
            $file->url = $this->get_download_url($file->name);
            foreach ($this->options['image_versions'] as $version => $options) {
                if (!empty($version)) {
                    if (is_file($this->get_upload_path($file_name, $version))) {
                        $file->{$version . 'Url'} = $this->get_download_url(
                            $file->name,
                            $version
                        );
                    }
                }
            }
            $this->set_additional_file_properties($file);
            return $file;
        }
        return null;
    }

    protected function get_file_objects($iteration_method = 'get_file_object')
    {
        $upload_dir = $this->get_upload_path();
        if (!is_dir($upload_dir)) {
            return array();
        }
        return array_values(array_filter(array_map(
            array($this, $iteration_method),
            scandir($upload_dir)
        )));
    }

    protected function count_file_objects()
    {
        return count($this->get_file_objects('is_valid_file_object'));
    }

    protected function get_error_message($error)
    {
        return isset($this->error_messages[$error]) ?
            $this->error_messages[$error] : $error;
    }

    public function get_config_bytes($val)
    {
        $val = trim($val);
        $last = strtolower($val[strlen($val) - 1]);
        $val = (int) $val;
        switch ($last) {
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }
        return $this->fix_integer_overflow($val);
    }

    protected function validate($uploaded_file, $file, $error, $index)
    {
        if ($error) {
            $file->error = $this->get_error_message($error);
            return false;
        }
        $content_length = $this->fix_integer_overflow(
            (int) $this->get_server_var('CONTENT_LENGTH')
        );
        $post_max_size = $this->get_config_bytes(ini_get('post_max_size'));
        if ($post_max_size && ($content_length > $post_max_size)) {
            $file->error = $this->get_error_message('post_max_size');
            return false;
        }
        if (!preg_match($this->options['accept_file_types'], $file->name)) {
            $file->error = $this->get_error_message('accept_file_types');
            return false;
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = $this->get_file_size($uploaded_file);
        } else {
            $file_size = $content_length;
        }
        if (
            $this->options['max_file_size'] && (
                $file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
        ) {
            $file->error = $this->get_error_message('max_file_size');
            return false;
        }
        if (
            $this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']
        ) {
            $file->error = $this->get_error_message('min_file_size');
            return false;
        }
        if (
            is_int($this->options['max_number_of_files']) &&
            ($this->count_file_objects() >= $this->options['max_number_of_files']) &&
            // Ignore additional chunks of existing files:
            !is_file($this->get_upload_path($file->name))
        ) {
            $file->error = $this->get_error_message('max_number_of_files');
            return false;
        }
        $max_width = @$this->options['max_width'];
        $max_height = @$this->options['max_height'];
        $min_width = @$this->options['min_width'];
        $min_height = @$this->options['min_height'];
        if (($max_width || $max_height || $min_width || $min_height) && preg_match($this->options['image_file_types'], $file->name)) {
            list($img_width, $img_height) = $this->get_image_size($uploaded_file);

            // If we are auto rotating the image by default, do the checks on
            // the correct orientation
            if (
                @$this->options['image_versions']['']['auto_orient'] &&
                function_exists('exif_read_data') &&
                ($exif = @exif_read_data($uploaded_file)) &&
                (((int) @$exif['Orientation']) >= 5)
            ) {
                $tmp = $img_width;
                $img_width = $img_height;
                $img_height = $tmp;
                unset($tmp);
            }
        }
        if (!empty($img_width)) {
            if ($max_width && $img_width > $max_width) {
                $file->error = $this->get_error_message('max_width');
                return false;
            }
            if ($max_height && $img_height > $max_height) {
                $file->error = $this->get_error_message('max_height');
                return false;
            }
            if ($min_width && $img_width < $min_width) {
                $file->error = $this->get_error_message('min_width');
                return false;
            }
            if ($min_height && $img_height < $min_height) {
                $file->error = $this->get_error_message('min_height');
                return false;
            }
        }
        return true;
    }

    protected function upcount_name_callback($matches)
    {
        $index = isset($matches[1]) ? ((int) $matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return ' (' . $index . ')' . $ext;
    }

    protected function upcount_name($name)
    {
        return preg_replace_callback(
            '/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
            array($this, 'upcount_name_callback'),
            $name,
            1
        );
    }

    protected function get_unique_filename($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        while (is_dir($this->get_upload_path($name))) {
            $name = $this->upcount_name($name);
        }
        // Keep an existing filename if this is part of a chunked upload:
        $uploaded_bytes = $this->fix_integer_overflow((int) $content_range[1]);
        while (is_file($this->get_upload_path($name))) {
            if ($uploaded_bytes === $this->get_file_size(
                $this->get_upload_path($name)
            )) {
                break;
            }
            $name = $this->upcount_name($name);
        }
        return $name;
    }

    protected function fix_file_extension($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        // Add missing file extension for known image types:
        if (
            strpos($name, '.') === false &&
            preg_match('/^image\/(gif|jpe?g|png)/', $type, $matches)
        ) {
            $name .= '.' . $matches[1];
        }
        if (
            $this->options['correct_image_extensions'] &&
            function_exists('exif_imagetype')
        ) {
            switch (@exif_imagetype($file_path)) {
                case IMAGETYPE_JPEG:
                    $extensions = array('jpg', 'jpeg');
                    break;
                case IMAGETYPE_PNG:
                    $extensions = array('png');
                    break;
                case IMAGETYPE_GIF:
                    $extensions = array('gif');
                    break;
            }
            // Adjust incorrect image file extensions:
            if (!empty($extensions)) {
                $parts = explode('.', $name);
                $extIndex = count($parts) - 1;
                $ext = strtolower(@$parts[$extIndex]);
                if (!in_array($ext, $extensions)) {
                    $parts[$extIndex] = $extensions[0];
                    $name = implode('.', $parts);
                }
            }
        }
        return $name;
    }

    //=========================== pindah kene

    protected function get_full_url()
    {
        $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0 ||
            !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
        return ($https ? 'https://' : 'http://') .
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'] . '@' : '') .
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'] .
                ($https && $_SERVER['SERVER_PORT'] === 443 ||
                    $_SERVER['SERVER_PORT'] === 80 ? '' : ':' . $_SERVER['SERVER_PORT']))) .
            substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
    }

    protected function get_upload_path($file_name = null, $version = null)
    {
        $file_name = $file_name ? $file_name : '';
        if (empty($version)) {
            $version_path = '';
        } else {
            $version_dir = @$this->options['image_versions'][$version]['upload_dir'];
            if ($version_dir) {
                return $version_dir . $this->get_user_path() . $file_name;
            }
            $version_path = $version . '/';
        }
        return $this->options['upload_dir'] . $this->get_user_path()
            . $version_path . $file_name;
    }

    protected function get_user_id()
    {
        @session_start();
        return session_id();
    }

    protected function get_user_path()
    {
        if ($this->options['user_dirs']) {
            return $this->get_user_id() . '/';
        }
        return '';
    }

    protected function get_query_separator($url)
    {
        return strpos($url, '?') === false ? '?' : '&';
    }

    protected function get_download_url($file_name, $version = null, $direct = false)
    {
        if (!$direct && $this->options['download_via_php']) {
            $url = $this->options['script_url']
                . $this->get_query_separator($this->options['script_url'])
                . $this->get_singular_param_name()
                . '=' . rawurlencode($file_name);
            if ($version) {
                $url .= '&version=' . rawurlencode($version);
            }
            return $url . '&download=1';
        }
        if (empty($version)) {
            $version_path = '';
        } else {
            $version_url = @$this->options['image_versions'][$version]['upload_url'];
            if ($version_url) {
                return $version_url . $this->get_user_path() . rawurlencode($file_name);
            }
            $version_path = rawurlencode($version) . '/';
        }
        return $this->options['upload_url'] . $this->get_user_path()
            . $version_path . rawurlencode($file_name);
    }

    protected function is_valid_file_object($file_name)
    {
        $file_path = $this->get_upload_path($file_name);
        if (is_file($file_path) && $file_name[0] !== '.') {
            return true;
        }
        return false;
    }

    protected function trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        // Remove path information and dots around the filename, to prevent uploading
        // into different directories or replacing hidden system files.
        // Also remove control characters and spaces (\x00..\x20) around the filename:
        $name = trim($this->basename(stripslashes($name)), ".\x00..\x20");
        // Use a timestamp for empty filenames:
        if (!$name) {
            $name = str_replace('.', '-', microtime(true));
        }
        return $name;
    }

    protected function get_file_name($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        $name = $this->trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range);
        return $this->get_unique_filename(
            $file_path,
            $this->fix_file_extension($file_path, $name, $size, $type, $error, $index, $content_range),
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
    }

    protected function get_scaled_image_file_paths($file_name, $version)
    {
        $file_path = $this->get_upload_path($file_name);
        if (!empty($version)) {
            $version_dir = $this->get_upload_path(null, $version);
            if (!is_dir($version_dir)) {
                mkdir($version_dir, $this->options['mkdir_mode'], true);
            }
            $new_file_path = $version_dir . '/' . $file_name;
        } else {
            $new_file_path = $file_path;
        }
        return array($file_path, $new_file_path);
    }

    // Fix for overflowing signed 32 bit integers,
    // works for sizes up to 2^32-1 bytes (4 GiB - 1):
    protected function fix_integer_overflow($size)
    {
        if ($size < 0) {
            $size += 2.0 * (PHP_INT_MAX + 1);
        }
        return $size;
    }

    protected function get_file_size($file_path, $clear_stat_cache = false)
    {
        if ($clear_stat_cache) {
            if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
                clearstatcache(true, $file_path);
            } else {
                clearstatcache();
            }
        }
        return $this->fix_integer_overflow(filesize($file_path));
    }

    protected function gd_get_image_object($file_path, $func, $no_cache = false)
    {
        if (empty($this->image_objects[$file_path]) || $no_cache) {
            $this->gd_destroy_image_object($file_path);
            $this->image_objects[$file_path] = $func($file_path);
        }
        return $this->image_objects[$file_path];
    }

    protected function gd_set_image_object($file_path, $image)
    {
        $this->gd_destroy_image_object($file_path);
        $this->image_objects[$file_path] = $image;
    }

    protected function gd_destroy_image_object($file_path)
    {
        $image = (isset($this->image_objects[$file_path])) ? $this->image_objects[$file_path] : null;
        return $image && imagedestroy($image);
    }

    protected function gd_imageflip($image, $mode)
    {
        if (function_exists('imageflip')) {
            return imageflip($image, $mode);
        }
        $new_width = $src_width = imagesx($image);
        $new_height = $src_height = imagesy($image);
        $new_img = imagecreatetruecolor($new_width, $new_height);
        $src_x = 0;
        $src_y = 0;
        switch ($mode) {
            case '1': // flip on the horizontal axis
                $src_y = $new_height - 1;
                $src_height = -$new_height;
                break;
            case '2': // flip on the vertical axis
                $src_x = $new_width - 1;
                $src_width = -$new_width;
                break;
            case '3': // flip on both axes
                $src_y = $new_height - 1;
                $src_height = -$new_height;
                $src_x = $new_width - 1;
                $src_width = -$new_width;
                break;
            default:
                return $image;
        }
        imagecopyresampled(
            $new_img,
            $image,
            0,
            0,
            $src_x,
            $src_y,
            $new_width,
            $new_height,
            $src_width,
            $src_height
        );
        return $new_img;
    }

    protected function gd_orient_image($file_path, $src_img)
    {
        if (!function_exists('exif_read_data')) {
            return false;
        }
        $exif = @exif_read_data($file_path);
        if ($exif === false) {
            return false;
        }
        $orientation = (int) @$exif['Orientation'];
        if ($orientation < 2 || $orientation > 8) {
            return false;
        }
        switch ($orientation) {
            case 2:
                $new_img = $this->gd_imageflip(
                    $src_img,
                    defined('IMG_FLIP_VERTICAL') ? IMG_FLIP_VERTICAL : 2
                );
                break;
            case 3:
                $new_img = imagerotate($src_img, 180, 0);
                break;
            case 4:
                $new_img = $this->gd_imageflip(
                    $src_img,
                    defined('IMG_FLIP_HORIZONTAL') ? IMG_FLIP_HORIZONTAL : 1
                );
                break;
            case 5:
                $tmp_img = $this->gd_imageflip(
                    $src_img,
                    defined('IMG_FLIP_HORIZONTAL') ? IMG_FLIP_HORIZONTAL : 1
                );
                $new_img = imagerotate($tmp_img, 270, 0);
                imagedestroy($tmp_img);
                break;
            case 6:
                $new_img = imagerotate($src_img, 270, 0);
                break;
            case 7:
                $tmp_img = $this->gd_imageflip(
                    $src_img,
                    defined('IMG_FLIP_VERTICAL') ? IMG_FLIP_VERTICAL : 2
                );
                $new_img = imagerotate($tmp_img, 270, 0);
                imagedestroy($tmp_img);
                break;
            case 8:
                $new_img = imagerotate($src_img, 90, 0);
                break;
            default:
                return false;
        }
        $this->gd_set_image_object($file_path, $new_img);
        return true;
    }

    protected function gd_create_scaled_image($file_name, $version, $options)
    {
        if (!function_exists('imagecreatetruecolor')) {
            error_log('Function not found: imagecreatetruecolor');
            return false;
        }
        list($file_path, $new_file_path) = $this->get_scaled_image_file_paths($file_name, $version);
        $type = strtolower(substr(strrchr($file_name, '.'), 1));
        switch ($type) {
            case 'jpg':
            case 'jpeg':
                $src_func = 'imagecreatefromjpeg';
                $write_func = 'imagejpeg';
                $image_quality = isset($options['jpeg_quality']) ?
                    $options['jpeg_quality'] : 75;
                break;
            case 'gif':
                $src_func = 'imagecreatefromgif';
                $write_func = 'imagegif';
                $image_quality = null;
                break;
            case 'png':
                $src_func = 'imagecreatefrompng';
                $write_func = 'imagepng';
                $image_quality = isset($options['png_quality']) ?
                    $options['png_quality'] : 9;
                break;
            default:
                return false;
        }
        $src_img = $this->gd_get_image_object(
            $file_path,
            $src_func,
            !empty($options['no_cache'])
        );
        $image_oriented = false;
        if (!empty($options['auto_orient']) && $this->gd_orient_image(
            $file_path,
            $src_img
        )) {
            $image_oriented = true;
            $src_img = $this->gd_get_image_object(
                $file_path,
                $src_func
            );
        }
        $max_width = $img_width = imagesx($src_img);
        $max_height = $img_height = imagesy($src_img);
        if (!empty($options['max_width'])) {
            $max_width = $options['max_width'];
        }
        if (!empty($options['max_height'])) {
            $max_height = $options['max_height'];
        }
        $scale = min(
            $max_width / $img_width,
            $max_height / $img_height
        );
        if ($scale >= 1) {
            if ($image_oriented) {
                return $write_func($src_img, $new_file_path, $image_quality);
            }
            if ($file_path !== $new_file_path) {
                return copy($file_path, $new_file_path);
            }
            return true;
        }
        if (empty($options['crop'])) {
            $new_width = $img_width * $scale;
            $new_height = $img_height * $scale;
            $dst_x = 0;
            $dst_y = 0;
            $new_img = imagecreatetruecolor($new_width, $new_height);
        } else {
            if (($img_width / $img_height) >= ($max_width / $max_height)) {
                $new_width = $img_width / ($img_height / $max_height);
                $new_height = $max_height;
            } else {
                $new_width = $max_width;
                $new_height = $img_height / ($img_width / $max_width);
            }
            $dst_x = 0 - ($new_width - $max_width) / 2;
            $dst_y = 0 - ($new_height - $max_height) / 2;
            $new_img = imagecreatetruecolor($max_width, $max_height);
        }
        // Handle transparency in GIF and PNG images:
        switch ($type) {
            case 'gif':
            case 'png':
                imagecolortransparent($new_img, imagecolorallocate($new_img, 0, 0, 0));
            case 'png':
                imagealphablending($new_img, false);
                imagesavealpha($new_img, true);
                break;
        }
        $success = imagecopyresampled(
            $new_img,
            $src_img,
            $dst_x,
            $dst_y,
            0,
            0,
            $new_width,
            $new_height,
            $img_width,
            $img_height
        ) && $write_func($new_img, $new_file_path, $image_quality);
        $this->gd_set_image_object($file_path, $new_img);
        return $success;
    }

    protected function imagick_get_image_object($file_path, $no_cache = false)
    {
        if (empty($this->image_objects[$file_path]) || $no_cache) {
            $this->imagick_destroy_image_object($file_path);
            $image = new \Imagick();
            if (!empty($this->options['imagick_resource_limits'])) {
                foreach ($this->options['imagick_resource_limits'] as $type => $limit) {
                    $image->setResourceLimit($type, $limit);
                }
            }
            $image->readImage($file_path);
            $this->image_objects[$file_path] = $image;
        }
        return $this->image_objects[$file_path];
    }

    protected function imagick_set_image_object($file_path, $image)
    {
        $this->imagick_destroy_image_object($file_path);
        $this->image_objects[$file_path] = $image;
    }

    protected function imagick_destroy_image_object($file_path)
    {
        $image = (isset($this->image_objects[$file_path])) ? $this->image_objects[$file_path] : null;
        return $image && $image->destroy();
    }

    protected function imagick_orient_image($image)
    {
        $orientation = $image->getImageOrientation();
        $background = new \ImagickPixel('none');
        switch ($orientation) {
            case \imagick::ORIENTATION_TOPRIGHT: // 2
                $image->flopImage(); // horizontal flop around y-axis
                break;
            case \imagick::ORIENTATION_BOTTOMRIGHT: // 3
                $image->rotateImage($background, 180);
                break;
            case \imagick::ORIENTATION_BOTTOMLEFT: // 4
                $image->flipImage(); // vertical flip around x-axis
                break;
            case \imagick::ORIENTATION_LEFTTOP: // 5
                $image->flopImage(); // horizontal flop around y-axis
                $image->rotateImage($background, 270);
                break;
            case \imagick::ORIENTATION_RIGHTTOP: // 6
                $image->rotateImage($background, 90);
                break;
            case \imagick::ORIENTATION_RIGHTBOTTOM: // 7
                $image->flipImage(); // vertical flip around x-axis
                $image->rotateImage($background, 270);
                break;
            case \imagick::ORIENTATION_LEFTBOTTOM: // 8
                $image->rotateImage($background, 270);
                break;
            default:
                return false;
        }
        $image->setImageOrientation(\imagick::ORIENTATION_TOPLEFT); // 1
        return true;
    }

    protected function imagick_create_scaled_image($file_name, $version, $options)
    {
        list($file_path, $new_file_path) = $this->get_scaled_image_file_paths($file_name, $version);
        $image = $this->imagick_get_image_object(
            $file_path,
            !empty($options['crop']) || !empty($options['no_cache'])
        );
        if ($image->getImageFormat() === 'GIF') {
            // Handle animated GIFs:
            $images = $image->coalesceImages();
            foreach ($images as $frame) {
                $image = $frame;
                $this->imagick_set_image_object($file_name, $image);
                break;
            }
        }
        $image_oriented = false;
        if (!empty($options['auto_orient'])) {
            $image_oriented = $this->imagick_orient_image($image);
        }
        $new_width = $max_width = $img_width = $image->getImageWidth();
        $new_height = $max_height = $img_height = $image->getImageHeight();
        if (!empty($options['max_width'])) {
            $new_width = $max_width = $options['max_width'];
        }
        if (!empty($options['max_height'])) {
            $new_height = $max_height = $options['max_height'];
        }
        if (!($image_oriented || $max_width < $img_width || $max_height < $img_height)) {
            if ($file_path !== $new_file_path) {
                return copy($file_path, $new_file_path);
            }
            return true;
        }
        $crop = !empty($options['crop']);
        if ($crop) {
            $x = 0;
            $y = 0;
            if (($img_width / $img_height) >= ($max_width / $max_height)) {
                $new_width = 0; // Enables proportional scaling based on max_height
                $x = ($img_width / ($img_height / $max_height) - $max_width) / 2;
            } else {
                $new_height = 0; // Enables proportional scaling based on max_width
                $y = ($img_height / ($img_width / $max_width) - $max_height) / 2;
            }
        }
        $success = $image->resizeImage(
            $new_width,
            $new_height,
            isset($options['filter']) ? $options['filter'] : \imagick::FILTER_LANCZOS,
            isset($options['blur']) ? $options['blur'] : 1,
            $new_width && $new_height // fit image into constraints if not to be cropped
        );
        if ($success && $crop) {
            $success = $image->cropImage(
                $max_width,
                $max_height,
                $x,
                $y
            );
            if ($success) {
                $success = $image->setImagePage($max_width, $max_height, 0, 0);
            }
        }
        $type = strtolower(substr(strrchr($file_name, '.'), 1));
        switch ($type) {
            case 'jpg':
            case 'jpeg':
                if (!empty($options['jpeg_quality'])) {
                    $image->setImageCompression(\imagick::COMPRESSION_JPEG);
                    $image->setImageCompressionQuality($options['jpeg_quality']);
                }
                break;
        }
        if (!empty($options['strip'])) {
            $image->stripImage();
        }
        return $success && $image->writeImage($new_file_path);
    }

    protected function imagemagick_create_scaled_image($file_name, $version, $options)
    {
        list($file_path, $new_file_path) = $this->get_scaled_image_file_paths($file_name, $version);
        $resize = @$options['max_width']
            . (empty($options['max_height']) ? '' : 'X' . $options['max_height']);
        if (!$resize && empty($options['auto_orient'])) {
            if ($file_path !== $new_file_path) {
                return copy($file_path, $new_file_path);
            }
            return true;
        }
        $cmd = $this->options['convert_bin'];
        if (!empty($this->options['convert_params'])) {
            $cmd .= ' ' . $this->options['convert_params'];
        }
        $cmd .= ' ' . escapeshellarg($file_path);
        if (!empty($options['auto_orient'])) {
            $cmd .= ' -auto-orient';
        }
        if ($resize) {
            // Handle animated GIFs:
            $cmd .= ' -coalesce';
            if (empty($options['crop'])) {
                $cmd .= ' -resize ' . escapeshellarg($resize . '>');
            } else {
                $cmd .= ' -resize ' . escapeshellarg($resize . '^');
                $cmd .= ' -gravity center';
                $cmd .= ' -crop ' . escapeshellarg($resize . '+0+0');
            }
            // Make sure the page dimensions are correct (fixes offsets of animated GIFs):
            $cmd .= ' +repage';
        }
        if (!empty($options['convert_params'])) {
            $cmd .= ' ' . $options['convert_params'];
        }
        $cmd .= ' ' . escapeshellarg($new_file_path);
        exec($cmd, $output, $error);
        if ($error) {
            error_log(implode('\n', $output));
            return false;
        }
        return true;
    }

    protected function get_image_size($file_path)
    {
        if ($this->options['image_library']) {
            if (extension_loaded('imagick')) {
                $image = new \Imagick();
                try {
                    if (@$image->pingImage($file_path)) {
                        $dimensions = array($image->getImageWidth(), $image->getImageHeight());
                        $image->destroy();
                        return $dimensions;
                    }
                    return false;
                } catch (\Exception $e) {
                    error_log($e->getMessage());
                }
            }
            if ($this->options['image_library'] === 2) {
                $cmd = $this->options['identify_bin'];
                $cmd .= ' -ping ' . escapeshellarg($file_path);
                exec($cmd, $output, $error);
                if (!$error && !empty($output)) {
                    // image.jpg JPEG 1920x1080 1920x1080+0+0 8-bit sRGB 465KB 0.000u 0:00.000
                    $infos = preg_split('/\s+/', substr($output[0], strlen($file_path)));
                    $dimensions = preg_split('/x/', $infos[2]);
                    return $dimensions;
                }
                return false;
            }
        }
        if (!function_exists('getimagesize')) {
            error_log('Function not found: getimagesize');
            return false;
        }
        return @getimagesize($file_path);
    }

    protected function create_scaled_image($file_name, $version, $options)
    {
        if ($this->options['image_library'] === 2) {
            return $this->imagemagick_create_scaled_image($file_name, $version, $options);
        }
        if ($this->options['image_library'] && extension_loaded('imagick')) {
            return $this->imagick_create_scaled_image($file_name, $version, $options);
        }
        return $this->gd_create_scaled_image($file_name, $version, $options);
    }

    protected function destroy_image_object($file_path)
    {
        if ($this->options['image_library'] && extension_loaded('imagick')) {
            return $this->imagick_destroy_image_object($file_path);
        }
    }

    protected function is_valid_image_file($file_path)
    {
        if (!preg_match($this->options['image_file_types'], $file_path)) {
            return false;
        }
        if (function_exists('exif_imagetype')) {
            return @exif_imagetype($file_path);
        }
        $image_info = $this->get_image_size($file_path);
        return $image_info && $image_info[0] && $image_info[1];
    }

    protected function handle_image_file($file_path, $file)
    {
        $failed_versions = array();
        foreach ($this->options['image_versions'] as $version => $options) {
            if ($this->create_scaled_image($file->name, $version, $options)) {
                if (!empty($version)) {
                    $file->{$version . 'Url'} = $this->get_download_url(
                        $file->name,
                        $version
                    );
                } else {
                    $file->size = $this->get_file_size($file_path, true);
                }
            } else {
                $failed_versions[] = $version ? $version : 'original';
            }
        }
        if (count($failed_versions)) {
            // $file->error = $this->get_error_message('image_resize')
            //         . ' (' . implode($failed_versions, ', ') . ')';
            $file->error = $this->get_error_message('image_resize')
                . ' (' . implode(', ', $failed_versions) . ')';
        }
        // Free memory:
        $this->destroy_image_object($file_path);
    }

    protected function readfile($file_path)
    {
        $file_size = $this->get_file_size($file_path);
        $chunk_size = $this->options['readfile_chunk_size'];
        if ($chunk_size && $file_size > $chunk_size) {
            $handle = fopen($file_path, 'rb');
            while (!feof($handle)) {
                echo fread($handle, $chunk_size);
                @ob_flush();
                @flush();
            }
            fclose($handle);
            return $file_size;
        }
        return readfile($file_path);
    }

    protected function body($str)
    {
        echo $str;
    }

    protected function header($str)
    {
        header($str);
    }

    protected function get_upload_data($id)
    {
        return @$_FILES[$id];
    }

    protected function get_post_param($id)
    {
        return @$_POST[$id];
    }

    protected function get_query_param($id)
    {
        return @$_GET[$id];
    }

    protected function get_server_var($id)
    {
        return @$_SERVER[$id];
    }

    protected function get_version_param()
    {
        return $this->basename(stripslashes($this->get_query_param('version')));
    }

    protected function get_singular_param_name()
    {
        return substr($this->options['param_name'], 0, -1);
    }

    protected function get_file_name_param()
    {
        $name = $this->get_singular_param_name();
        return $this->basename(stripslashes($this->get_query_param($name)));
    }

    protected function get_file_names_params()
    {
        $params = $this->get_query_param($this->options['param_name']);
        if (!$params) {
            return null;
        }
        foreach ($params as $key => $value) {
            $params[$key] = $this->basename(stripslashes($value));
        }
        return $params;
    }

    protected function get_file_type($file_path)
    {
        switch (strtolower(pathinfo($file_path, PATHINFO_EXTENSION))) {
            case 'jpeg':
            case 'jpg':
                return 'image/jpeg';
            case 'png':
                return 'image/png';
            case 'gif':
                return 'image/gif';
            default:
                return '';
        }
    }

    protected function download()
    {
        switch ($this->options['download_via_php']) {
            case 1:
                $redirect_header = null;
                break;
            case 2:
                $redirect_header = 'X-Sendfile';
                break;
            case 3:
                $redirect_header = 'X-Accel-Redirect';
                break;
            default:
                return $this->header('HTTP/1.1 403 Forbidden');
        }
        $file_name = $this->get_file_name_param();
        if (!$this->is_valid_file_object($file_name)) {
            return $this->header('HTTP/1.1 404 Not Found');
        }
        if ($redirect_header) {
            return $this->header(
                $redirect_header . ': ' . $this->get_download_url(
                    $file_name,
                    $this->get_version_param(),
                    true
                )
            );
        }
        $file_path = $this->get_upload_path($file_name, $this->get_version_param());
        // Prevent browsers from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if (!preg_match($this->options['inline_file_types'], $file_name)) {
            $this->header('Content-Type: application/octet-stream');
            $this->header('Content-Disposition: attachment; filename="' . $file_name . '"');
        } else {
            $this->header('Content-Type: ' . $this->get_file_type($file_path));
            $this->header('Content-Disposition: inline; filename="' . $file_name . '"');
        }
        $this->header('Content-Length: ' . $this->get_file_size($file_path));
        $this->header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', filemtime($file_path)));
        $this->readfile($file_path);
    }

    protected function send_content_type_header()
    {
        $this->header('Vary: Accept');
        if (strpos($this->get_server_var('HTTP_ACCEPT'), 'application/json') !== false) {
            $this->header('Content-type: application/json');
        } else {
            $this->header('Content-type: text/plain');
        }
    }

    protected function send_access_control_headers()
    {
        $this->header('Access-Control-Allow-Origin: ' . $this->options['access_control_allow_origin']);
        $this->header('Access-Control-Allow-Credentials: '
            . ($this->options['access_control_allow_credentials'] ? 'true' : 'false'));
        $this->header('Access-Control-Allow-Methods: '
            . implode(', ', $this->options['access_control_allow_methods']));
        $this->header('Access-Control-Allow-Headers: '
            . implode(', ', $this->options['access_control_allow_headers']));
    }

    public function generate_response($content, $print_response = true)
    {
        $this->response = $content;
        if ($print_response) {
            $json = json_encode($content);
            $redirect = stripslashes($this->get_post_param('redirect'));
            if ($redirect && preg_match($this->options['redirect_allow_target'], $redirect)) {
                $this->header('Location: ' . sprintf($redirect, rawurlencode($json)));
                return;
            }
            $this->head();
            if ($this->get_server_var('HTTP_CONTENT_RANGE')) {
                $files = isset($content[$this->options['param_name']]) ?
                    $content[$this->options['param_name']] : null;
                if ($files && is_array($files) && is_object($files[0]) && $files[0]->size) {
                    $this->header('Range: 0-' . (
                        $this->fix_integer_overflow((int) $files[0]->size) - 1
                    ));
                }
            }
            $this->body($json);
        }
        return $content;
    }

    public function get_response()
    {
        return $this->response;
    }

    public function head()
    {
        $this->header('Pragma: no-cache');
        $this->header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->header('Content-Disposition: inline; filename="files.json"');
        // Prevent Internet Explorer from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if ($this->options['access_control_allow_origin']) {
            $this->send_access_control_headers();
        }
        $this->send_content_type_header();
    }

    public function get($print_response = true)
    {
        if ($print_response && $this->get_query_param('download')) {
            return $this->download();
        }
        $file_name = $this->get_file_name_param();
        if ($file_name) {
            $response = array(
                $this->get_singular_param_name() => $this->get_file_object($file_name)
            );
        } else {
            $response = array(
                $this->options['param_name'] => $this->get_file_objects()
            );
        }
        return $this->generate_response($response, $print_response);
    }

    public function post($print_response = true)
    {
        if ($this->get_query_param('_method') === 'DELETE') {
            return $this->delete($print_response);
        }
        $upload = $this->get_upload_data($this->options['param_name']);
        // Parse the Content-Disposition header, if available:
        $content_disposition_header = $this->get_server_var('HTTP_CONTENT_DISPOSITION');
        $file_name = $content_disposition_header ?
            rawurldecode(preg_replace(
                '/(^[^"]+")|("$)/',
                '',
                $content_disposition_header
            )) : null;
        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
        $content_range_header = $this->get_server_var('HTTP_CONTENT_RANGE');
        $content_range = $content_range_header ?
            preg_split('/[^0-9]+/', $content_range_header) : null;
        $size = $content_range ? $content_range[3] : null;
        $files = array();
        if ($upload) {
            if (is_array($upload['tmp_name'])) {
                // param_name is an array identifier like "files[]",
                // $upload is a multi-dimensional array:
                foreach ($upload['tmp_name'] as $index => $value) {
                    $files[] = $this->handle_file_upload(
                        $upload['tmp_name'][$index],
                        $file_name ? $file_name : $upload['name'][$index],
                        $size ? $size : $upload['size'][$index],
                        $upload['type'][$index],
                        $upload['error'][$index],
                        $index,
                        $content_range
                    );
                }
            } else {
                // param_name is a single object identifier like "file",
                // $upload is a one-dimensional array:
                $files[] = $this->handle_file_upload(
                    isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
                    $file_name ? $file_name : (isset($upload['name']) ?
                        $upload['name'] : null),
                    $size ? $size : (isset($upload['size']) ?
                        $upload['size'] : $this->get_server_var('CONTENT_LENGTH')),
                    isset($upload['type']) ?
                        $upload['type'] : $this->get_server_var('CONTENT_TYPE'),
                    isset($upload['error']) ? $upload['error'] : null,
                    null,
                    $content_range
                );
            }
        }
        $response = array($this->options['param_name'] => $files);
        return $this->generate_response($response, $print_response);
    }

    protected function basename($filepath, $suffix = null)
    {
        $splited = preg_split('/\//', rtrim($filepath, '/ '));
        return substr(basename('X' . $splited[count($splited) - 1], $suffix), 1);
    }

    //=========================== end pindah kene

    protected function handle_form_data($file, $index)
    {
        $file->title = @$_REQUEST['title'][$index];
        $file->description = @$_REQUEST['nama_transaksi'][$index];
    }

    protected function handle_file_upload_ori($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null)
    {
        $file = new \stdClass();
        $file->name = $this->get_file_name($uploaded_file, $name, $size, $type, $error, $index, $content_range);
        $file->size = $this->fix_integer_overflow((int) $size);
        $file->type = $type;
        if ($this->validate($uploaded_file, $file, $error, $index)) {
            $this->handle_form_data($file, $index);
            $upload_dir = $this->get_upload_path();
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, $this->options['mkdir_mode'], true);
            }
            $file_path = $this->get_upload_path($file->name);
            $append_file = $content_range && is_file($file_path) &&
                $file->size > $this->get_file_size($file_path);
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen($this->options['input_stream'], 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }
            $file_size = $this->get_file_size($file_path, $append_file);
            if ($file_size === $file->size) {
                $file->url = $this->get_download_url($file->name);
                if ($this->is_valid_image_file($file_path)) {
                    $this->handle_image_file($file_path, $file);
                }
            } else {
                $file->size = $file_size;
                if (!$content_range && $this->options['discard_aborted_uploads']) {
                    unlink($file_path);
                    $file->error = $this->get_error_message('abort');
                }
            }
            $this->set_additional_file_properties($file);
        }
        return $file;
    }

    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null)
    {
        //$file = parent::handle_file_upload(
        //    $uploaded_file, $name, $size, $type, $error, $index, $content_range
        //);

        $sm = $this->getServiceLocator();
        $tbl_upload = $sm->get("UploadTable");

        $file = $this->handle_file_upload_ori($uploaded_file, $name, $size, $type, $error, $index, $content_range);
        if (empty($file->error)) {

            /* $sql = 'INSERT INTO `'.$this->options['db_table']
              .'` (`name`, `size`, `type`, `title`, `description`)'
              .' VALUES (?, ?, ?, ?, ?)';
              $query = $this->db->prepare($sql);
              $query->bind_param(
              'sisss',
              $file->name,
              $file->size,
              $file->type,
              $file->title,
              $file->description
              );
              $query->execute();
              $file->id = $this->db->insert_id; */
            //echo $this->idjenistransaksi.' <br>'.$this->t_iddetailsptbphtb.' <br>'.$name.' <br>'.$this->s_namajenistransaksi.' <br>'.$this->idsyarat.' <br>'.$this->lokasifile.' <br>'.$this->idspt.' <br>'.$this->t_iddetailsptbphtb;
            //$file->id = 1;
            //lemparpostsimpan($this->idjenistransaksi, $this->t_iddetailsptbphtb, $name, $this->s_namajenistransaksi, $this->idsyarat, $this->lokasifile, $this->idspt, $this->t_iddetailsptbphtb);
            $tbl_upload->savedata_syarat($this->idjenistransaksi, $this->t_iddetailsptbphtb, $name, $this->s_namajenistransaksi, $this->idsyarat, $this->lokasifile, $this->idspt, $this->t_iddetailsptbphtb);
        }
        return $file;
    }

    protected function set_additional_file_properties($file)
    {
        //parent::set_additional_file_properties($file);
        //================== delete e pindah kene
        //$file->deleteUrl = $this->options['script_url']
        //    .$this->get_query_separator($this->options['script_url'])
        //    .$this->get_singular_param_name()
        //    .'='.rawurlencode($file->name);
        $file->deleteUrl = 'uploadsyaratpost?' . $this->get_singular_param_name() . '=' . rawurlencode($file->name);
        $file->deleteType = $this->options['delete_type'];
        if ($file->deleteType !== 'DELETE') {
            $file->deleteUrl .= '&_method=DELETE&idspt=' . $this->idspt . '&idsyarat=' . $this->idsyarat . '';
        }
        if ($this->options['access_control_allow_credentials']) {
            $file->deleteWithCredentials = true;
        }
        //================== end delete e pindah kene


        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            /* $sql = 'SELECT `id`, `type`, `title`, `description` FROM `'
              .$this->options['db_table'].'` WHERE `name`=? ';
              $query = $this->db->prepare($sql);
              $query->bind_param('s', $file->name);
              $query->execute();
              $query->bind_result(
              $id,
              $type,
              $title,
              $description
              );
              while ($query->fetch()) {
              $file->id = $id;
              $file->type = $type;
              $file->title = $title;
              $file->description = $description;
              } */
        }
    }

    public function delete_ori($print_response = true)
    {
        //echo $this->lokasifile;
        $file_names = $this->get_file_names_params();
        if (empty($file_names)) {
            $file_names = array($this->get_file_name_param());
        }
        $response = array();
        foreach ($file_names as $file_name) {
            $file_path = $this->get_upload_path($file_name); //'public/upload/filetransaksi/2016/jualbeli/13051/1/'
            //public/upload/filetransaksi/2016/jualbeli/13051/1/
            $success = is_file($file_path) && $file_name[0] !== '.' && unlink($file_path);
            //$success = unlink($file_path);
            if ($success) {
                foreach ($this->options['image_versions'] as $version => $options) {
                    if (!empty($version)) {
                        $file = $this->get_upload_path($file_name, $version);
                        if (is_file($file)) {
                            unlink($file);
                        }
                    }
                }
            }
            $response[$file_name] = $success;
        }
        return $this->generate_response($response, $print_response);
    }

    public function delete($print_response = true)
    {
        $sm = $this->getServiceLocator();
        $tbl_upload = $sm->get("UploadTable");

        //$response = parent::delete(false);
        $response = $this->delete_ori(false); //parent::delete(false);
        //unlink('public/upload/filetransaksi/2016/jualbeli/13051/1/kampret2.pdf');
        foreach ($response as $name => $deleted) {
            if ($deleted) {
                /* sql = 'DELETE FROM `'
                  .$this->options['db_table'].'` WHERE `name`=?';
                  $query = $this->db->prepare($sql);
                  $query->bind_param('s', $name);
                  $query->execute(); */
                $tbl_upload->hapusdata_syarat($name, $this->idspt, $this->t_iddetailsptbphtb, $this->idsyarat);
            }
        }
        return $this->generate_response($response, $print_response);
    }

    public function cektunggakanpbbAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $base = new \Bphtb\Model\Pendataan\SPPTBase();
            $frm->setData($req->getPost());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getPost();
                $base->t_nopbphtbspptinfoop = $data_get['t_nopbphtbsppt'];
                $data_tunggakan = $this->getTblSPPT()->temukanDataTunggakanop($base);
                $html = "<div class='row'>
                          <div class='col-md-12'>
                          <div class='panel panel-primary'>
                          <div class='panel-heading'><strong>Tunggakan SPPT-PBB</strong></div>
                          <table class='table table-striped'>";
                $html .= "<tr>";
                $html .= "<th>No.</th>";
                $html .= "<th>Tahun</th>";
                $html .= "<th>Tunggakan (Rp.)</th>";
                $html .= "<th>Jatuh Tempo</th>";
                $html .= "<th>Denda (Rp.)</th>";
                $html .= "</tr>";
                $i = 1;
                $jumlahdenda = 0;
                $PBB_YG_HARUS_DIBAYAR_SPPT = 0;
                foreach ($data_tunggakan as $row) {
                    $html .= "<tr>";
                    $html .= "<td> " . $i . " </td>";
                    $html .= "<td> " . $row['THN_PAJAK_SPPT'] . " </td>";
                    $html .= "<td> " . number_format($row['PBB_YG_HARUS_DIBAYAR_SPPT'], 0, ',', '.') . " </td>";
                    $html .= "<td> " . $row['JATUH_TEMPO'] . " </td>";
                    //                    $html .= "<td> " . date('d-m-Y', strtotime($row['JATUH_TEMPO'])) . " </td>";
                    $dat1 = date('Y-m-d', strtotime($row['JATUH_TEMPO']));
                    $dat2 = date('Y-m-d');
                    /* $date1 = new \DateTime($dat1);
                      $date2 = new \DateTime($dat2);
                      $interval = $date1->diff($date2);
                      $bedanya = $interval->m + ($interval->y * 12);
                      if ($bedanya > 24) {
                      $beda = 24;
                      } else {
                      $beda = $bedanya;
                      } */

                    $tgl_bayar = explode("-", $dat2);
                    $tgl_tempo = explode("-", $dat1);

                    $tahun = $tgl_bayar[0] - $tgl_tempo[0];
                    $bulan = $tgl_bayar[1] - $tgl_tempo[1];
                    $hari = $tgl_bayar[2] - $tgl_tempo[2];


                    if (($tahun == 0) || ($tahun < 1)) {
                        if (($bulan == 0) || ($bulan < 1)) {
                            if ($bulan < 0) {
                                $months = 0;
                            } else {
                                if (($hari == 0) || ($hari < 1)) {
                                    $months = 0;
                                } else {
                                    $months = 1;
                                }
                            }
                        } else {
                            if (($hari == 0) || ($hari < 1)) {
                                $months = $bulan;
                            } else {
                                $months = $bulan + 1;
                            }
                        }
                    } else {
                        $jmltahun = $tahun * 12;
                        if ($bulan == 0) {
                            $months = $jmltahun;
                        } elseif ($bulan < 1) {
                            $months = $jmltahun + $bulan;
                        } else {
                            $months = $bulan + $jmltahun;
                        }
                    }


                    if ($months > 24) {
                        $beda = 24;
                    } else {
                        $beda = $months;
                    }

                    $denda = $beda * $row['PBB_YG_HARUS_DIBAYAR_SPPT'] * 2 / 100;
                    $html .= "<td> " . number_format($denda, 0, ',', '.') . " </td>";
                    $html .= "</tr>";
                    $i++;
                    $PBB_YG_HARUS_DIBAYAR_SPPT = $PBB_YG_HARUS_DIBAYAR_SPPT + $row['PBB_YG_HARUS_DIBAYAR_SPPT'];
                    $jumlahdenda = $jumlahdenda + $denda;
                }
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='2'><center>Jumlah Tunggakan</center></td>";
                $html .= "<td> " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT, 0, ',', '.') . " </td>";
                $html .= "<td>Jumlah Denda</td>";
                $html .= "<td> " . number_format($jumlahdenda, 0, ',', '.') . " </td>";
                $html .= "</tr>";
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='3'><center>Jumlah Seluruh Tunggakan</center></td>";
                $html .= "<td colspan='2'><div style='text-align:center; color:red' > Rp. " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT + $jumlahdenda, 0, ',', '.') . " </div></td>";
                $html .= "</tr>";
                $html .= "</table>
                        </div></div></div>";
                $data['datatunggakan'] = $html;
                $data['PBB_YG_HARUS_DIBAYAR_SPPT'] = $PBB_YG_HARUS_DIBAYAR_SPPT;
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    //==================================================================================== end class upload

    public function addNOPAction()
    {
        $post = $this->getRequest()->getPost()->toArray();
        $sessionArray = $_SESSION['eBPHTBSESSIONKABTANAHBUMBU'];
        var_dump($sessionArray);
        //        $sessionArray['NOP_GABUNGAN'] = array();
        if (!is_array($sessionArray['NOP_GABUNGAN'])) {
            $sessionArray['NOP_GABUNGAN'] = array();
        }
        foreach ($post['data'] as $col => $row) :
            if ($row == 'null') {
                $post['data'][$col] = NULL;
            }
        endforeach;
        $nopGabungan = [
            't_nopbphtbsppt' => $post['nop'],
            't_thnsppt' => $post['tahun'],
            't_namasppt' => $post['data']['NM_WP_SPPT'],
            't_alamatop' => $post['data']['JALAN_OP'],
            't_rtop' => $post['data']['RT_OP'],
            't_rwop' => $post['data']['RW_OP'],
            't_kelurahanop' => $post['data']['NM_KELURAHAN'],
            't_kecamatanop' => $post['data']['NM_KECAMATAN'],
            't_luastanah' => $post['data']['LUAS_BUMI_SPPT'],
            't_luastanah_sismiop' => $post['data']['LUAS_BUMI_SPPT'],
            't_luasbangunan_sismiop' => $post['data']['LUAS_BNG_SPPT'],
            't_njoptanah_sismiop' => $post['data']['NILAI_PER_M2_TANAH'],
            't_njopbangunan_sismiop' => $post['data']['NILAI_PER_M2_BNG'],
            't_njoptanah' => $post['data']['NILAI_PER_M2_TANAH'] * 1000,
            't_totalnjoptanah' => $post['data']['NJOP_BUMI_SPPT'],
            't_luasbangunan' => $post['data']['LUAS_BNG_SPPT'],
            't_njopbangunan' => $post['data']['NILAI_PER_M2_BNG'] * 1000,
            't_totalnjopbangunan' => $post['data']['NJOP_BNG_SPPT'],
            't_grandtotalnjop' => $post['data']['NJOP_SPPT'],
            't_namawppenjual' => $post['data']['NM_WP_SPPT'],
            't_alamatwppenjual' => "Jl. " . $post['data']['JLN_WP_SPPT'],
            't_rtwppenjual' => $post['data']['RT_WP_SPPT'],
            't_rwwppenjual' => $post['data']['RW_WP_SPPT'],
            't_kelurahanwppenjual' => $post['data']['KELURAHAN_WP_SPPT'],
            't_kabkotawppenjual' => $post['data']['KOTA_WP_SPPT'],
            't_kodeposwppenjual' => $post['data']['KD_POS_WP_SPPT'],
        ];
        array_push($sessionArray['NOP_GABUNGAN'], $nopGabungan);
        $_SESSION['eBPHTBSESSIONKABTANAHBUMBU']['NOP_GABUNGAN'] = $sessionArray['NOP_GABUNGAN'];
        exit();
    }

    public function removeNOPAction()
    {
        $post = $this->getRequest()->getPost()->toArray();
        $sessionArray = $_SESSION['eBPHTBSESSIONKABTANAHBUMBU'];
        foreach ($sessionArray['NOP_GABUNGAN'] as $col => $row) :
            if ($row['t_nopbphtbsppt'] == $post['nop']) {
                unset($sessionArray['NOP_GABUNGAN'][$col]);
            }
        endforeach;
        $sessionArray['NOP_GABUNGAN'] = array_values($sessionArray['NOP_GABUNGAN']);
        $_SESSION['eBPHTBSESSIONKABTANAHBUMBU']['NOP_GABUNGAN'] = $sessionArray['NOP_GABUNGAN'];
        exit();
    }

    public function getNOPAction()
    {
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($_SESSION['eBPHTBSESSIONKABTANAHBUMBU']['NOP_GABUNGAN']));
    }

    public function datanotifikasivalidasiAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        //$session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();


        $view = new ViewModel(
            array(

                'menu_pendataan' => 'active',
                'role_id' => $session->s_akses,
                'data_pemda' => $ar_pemda,
                'aturgambar' => 1,
                'username' => $session->s_username,
                'session' => $session
            )
        );

        return $view;
    }

    public function dataGridnotifvalidasiAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $input = $this->getRequest();
        $aColumns = array('id_psnvalidasi', 'fr_pembayaran_v5.t_kohirspt', 's_pejabat.s_namapejabat', 'fr_pembayaran_v5.t_tglverifikasispt',  'fr_pembayaran_v5.t_keteranganvalidasi', 't_idspt');
        $panggildata = $this->getTblSSPDBphtb();
        //$rResult = $panggildata->semuadatapendaftaran($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());    
        $rResult = $panggildata->semuadatanotifvalidasi($input, $aColumns, $session, $this->cekurl(), $allParams);
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    //=============================== LIHAT DETAIL NOTIFIKASI VALIDASI
    public function detailnotifAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        $idpsn = "";
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $idpsn = (int) $req->getQuery()->get('idpsn');
            //$data = $this->getTblSSPDBphtb()->get_fr_pembayaran_v5($id);
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);

            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = $t_luastanah; //number_format(($T_LUASTANAH / 100), 0, ',', '.');
            $data['t_luasbangunan'] = $t_luasbangunan; //number_format(($T_LUASBANGUNAN / 100), 0, ',', '.');
            $pesan = '';
        }
        if ($req->isPost()) {
            $id = (int) $req->getPost('t_idspt');
            $data = $this->getTblSSPDBphtb()->get_fr_pembayaran_v5($id);

            $isi_pesan = $req->getPost('t_kirimpsnoperator');
            $this->getTblSSPDBphtb()->kirimpesankepetugasvalidasi($data, $isi_pesan, $session, $ar_pemda, $this->cekurl());
            $pesan = 'PESAN BERHASIL DI KIRIM';
        }
        //var_dump($data);exit();
        $view = new ViewModel(array(
            'datasspd' => $data,
            'pesan' => $pesan,
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
            'idpsn' => $idpsn
        ));
        $layout_data = [
            'pesan' => $pesan,
            'menu_pendataan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session
        ];
        $this->layout()->setVariables($layout_data);

        return $view;
    }

    //=============================== end LIHAT DETAIL NOTIFIKASI VALIDASI


    //====CREATE SSPD
    function kekata($x)
    {
        $x = abs($x);
        $angka = array(
            "",
            "Satu",
            "Dua",
            "Tiga",
            "Empat",
            "Lima",
            "Enam",
            "Tujuh",
            "Delapan",
            "Sembilan",
            "Sepuluh",
            "Sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    function terbilang($x, $style = 4)
    {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    public function create_sppd($data_pemda = array(), $ar_sspd = array(), $dataidsptsebelum = array(), $ar_sebelum = array(), $detail_nop = array(), $kodebayar = null, $cetakversi = null, $tipesimpan = null, $data_mengetahui = array(), $esign = null, $app_path = null, $basePath = null)
    {
        if ($esign == 1) {
            include_once 'public/barcodecoy/qrlib.php';
            if (empty($kodebayar)) {
                $tampilbarcode = '';
            } else {
                $PNG_TEMP_DIR = $_SERVER['DOCUMENT_ROOT'] . '/' . $basePath . '/public/img_barcode/' . $kodebayar . '/';
                //ofcourse we need rights to create temp dir
                if (!file_exists($PNG_TEMP_DIR)) {
                    mkdir($_SERVER['DOCUMENT_ROOT'] . '/' . $basePath . '/public/img_barcode/' . $kodebayar . '/', 0777, true);
                    chmod($_SERVER['DOCUMENT_ROOT'] . '/' . $basePath . '/public/img_barcode/' . $kodebayar, 0777);
                }
                //processing form input
                //remember to sanitize user input in real-life solution !!!
                $errorCorrectionLevel = 'L';
                $matrixPointSize = 3;
                $barcode = $kodebayar;

                $filename = $PNG_TEMP_DIR . '' . $kodebayar . '_' . md5($barcode . '|' . $errorCorrectionLevel . '|' . $matrixPointSize) . '.png';
                QRcode::png($barcode, $filename, $errorCorrectionLevel, $matrixPointSize, 2, FALSE, 1);

                $tampilbarcode = '<img src="' . $app_path . '/public/img_barcode/' . $kodebayar . '/' . basename($filename) . '" />';
            }
        } else {
            $tampilbarcode = "";
        }

        include_once 'public/cetakan/MPDF57/mpdf.php';
        $mpdf = new mPDF('utf-8', array(215, 330), 0, 5, 2.5, 10, 2.5, 1, 1, 'P');

        $html = '
        <html>
        <head>
        <style>
        @page {
            size: auto;
        }

        </style>
        </head>
        <body>
        ';

        foreach ($ar_sspd as $row) {
            if ($cetakversi == 1 || $cetakversi == 2) {
                if ($cetakversi == 2) {
                    $jmlh_lmbr = 6;
                } else {
                    $jmlh_lmbr = 1;
                }

                for ($lmbr = 1; $lmbr <= $jmlh_lmbr; $lmbr++) {
                    $html .= '
                <div style="height: 100%; ">
                    <table width="100%" style="padding:0; border-spacing:0px; border: 1px solid black; font-size: 11px;">
                        <tr>
                            <td style=" text-align: center; padding-top: 5px; width: 200px; border-right: 1px solid black; ">';
                    $gambar = "" . $app_path . "/" . $data_pemda->s_logo;
                    $html .= '
                                <img src="' . $gambar . '" style="width:90px; height:95px; " />
                            </td>
                            <td style=" text-align: center; width: 360px; font-size: 15px; border-right: 1px solid black; border-bottom: 1px solid black; ">
                                SURAT SETORAN
                                <br/>
                                BEA PEROLEHAN HAK ATAS TANAH DAN BANGUNAN
                                <br/>
                                <strong style=" font-style:  normal;">( S S B )</strong>
                            </td>
                            <td rowspan="2" class="font_sembilan">
                                    <center>
                                        <br>';
                    if ($cetakversi == 2) {
                        if ($lmbr == 1) {
                            $html .= '<b>Lembar <font style="font-size: 22px; ">1</font></b><br />Untuk Wajib Pajak sebagai bukti pembayaran';
                        } elseif ($lmbr == 2) {
                            $html .= '<b>Lembar <font style="font-size: 22px; ">2</font></b><br />Untuk PPAT/Notaris/Kepala Kantor Lelang/Pejabat Lelang';
                        } elseif ($lmbr == 3) {
                            $html .= '<b>Lembar <font style="font-size: 22px; ">3</font></b><br />Untuk Kepala Kantor Pertanahan Kabupaten ' . $data_pemda->s_namakabkot . '';
                        } elseif ($lmbr == 4) {
                            $html .= '<b>Lembar <font style="font-size: 22px; ">4</font></b><br />' . ucwords($data_pemda->s_namainstansi) . '';
                        } elseif ($lmbr == 5) {
                            $html .= '<b>Lembar <font style="font-size: 22px; ">5</font></b><br />Untuk Bank Yang Ditunjuk/Bendahara Penerimaan';
                        } elseif ($lmbr == 6) {
                            $html .= '<b>Lembar <font style="font-size: 22px; ">6</font></b><br />Kantor Pelayanan Pajak (KPP) Pratama ' . $data_pemda->s_namaibukotakabkot . '';
                        }
                    }
                    $html .= '</center>
                                    <br><center>
                                        No. Daftar : ' . $row['t_kohirspt'] . '<br>
                                        Kode Bayar : ' . $row['t_kodebayarbanksppt'] . '</center>
                            </td>
                    </tr>
                    <tr>
                        <td style=" text-align: center; border-right: 1px solid black;">
                            <font style=" font-size: 9px; ">PEMERINTAH ' . strtoupper($data_pemda->s_namakabkot) . '</font>
                            <br />
                            <b style=" font-size: 11px; ">' . strtoupper($data_pemda->s_namainstansi) . '</b>
                        </td>
                        <td align="center" style="border-right: 1px solid black; font-size: 9pt; ">
                            BERFUNGSI SEBAGAI SURAT PEMBERITAHUAN OBJEK PAJAK
                            <br/>
                            PAJAK BUMI DAN BANGUNAN (SPOP PBB)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-size: 8px; padding: 5px; border-top: 1px solid black;">
                            PERHATIAN : Bacalah petunjuk pada halaman belakang lembar ini terlebih dahulu
                        </td>
                    </tr>
                </table>';
                    $html .= '
                <table border="0" width="100%" style="padding:0; border-spacing:0px; font-size: 11px; border-left: 1px solid black; border-right: 1px solid black;">
                    <tr>
                        <td align="center" style="padding-left: 10px; padding-top: 10px;  width: 5px;">
                            A.
                        </td>
                        <td style="width: 150px; padding-top: 10px;">
                            1. Nama Wajib Pajak :
                        </td>
                        <td colspan="3" style="padding-top: 10px;">
                            ' . strtoupper($row['t_namawppembeli']) . '
                        </td>
                        <td style="width: 10px;"></td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            2. NPWP :
                        </td>
                        <td colspan="3">
                            ' . $row['t_npwpwppembeli'] . '
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="border-bottom: 1px solid black;">
                            3. Alamat Wajib Pajak : 
                        </td>
                        <td style="border-bottom: 1px solid black;" colspan="3">
                            ' . strtoupper($row["t_alamatwppembeli"]) . '
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="border-bottom: 1px solid black;">
                            4. Kelurahan/Desa :
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            ' . strtoupper($row["t_kelurahanwppembeli"]) . '
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            5. RT/RW :
                            ' . strtoupper($row["t_rtwppembeli"]) . ' / ' . strtoupper($row["t_rwwppembeli"]) . '
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            6. Kecamatan : ' . strtoupper($row["t_kecamatanwppembeli"]) . '
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="border-bottom: 1px solid black;">
                            7. Kabupaten/Kota :
                        </td>
                        <td style="border-bottom: 1px solid black;" colspan="2">
                            ' . strtoupper($row["t_kabkotawppembeli"]) . '
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            8. Kode Pos : ' . $row["t_kodeposwppembeli"] . '
                        </td>
                        <td></td>
                    </tr>
                </table>';
                    $html .= '
                <table width="100%" style="padding:0;border-spacing:0px; border-right: 1px solid black; border-left: 1px solid black; font-size: 11px; " border="0">
                <tr>
                    <td style="width: 5px; padding-left: 10px; padding-top: 10px; vertical-align: top;">
                        B.
                    </td>
                    <td style="padding-top: 10px; ">
                        <table width="100%" style="border-spacing: 0px;" border="0">';


                    if ($detail_nop != NULL) {
                        foreach ($detail_nop as $r) :
                            $ix++;

                            $html .= '
                            <tr>
                            <td>
                            ' . $ix . '.1. Nomor Objek Pajak (NOP) PBB :
                            </td>
                            <td style=" width: 1%" class="">:&nbsp;</td>
                            <td style="width: 10px;">&nbsp;</td>';
                            $html .= '
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($r['t_nopbphtbsppt'], 0, 1) . '
                                </td>
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($r['t_nopbphtbsppt'], 1, 1) . '
                                </td>';
                            $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                            $html .= '<td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($r['t_nopbphtbsppt'], 3, 1) . '
                            </td>
                            <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($r['t_nopbphtbsppt'], 4, 1) . '
                            </td>';
                            $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                            $html .= '
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($r['t_nopbphtbsppt'], 6, 1) . '
                                </td>
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($r['t_nopbphtbsppt'], 7, 1) . '
                                </td>
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($r['t_nopbphtbsppt'], 8, 1) . '
                                </td>';
                            $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                            $html .= '<td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 10, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 11, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 12, 1) . '
                                    </td>';
                            $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                            $html .= '<td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 14, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 15, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 16, 1) . '
                                    </td>';
                            $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                            $html .= '<td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 18, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 19, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 20, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($r['t_nopbphtbsppt'], 21, 1) . '
                                    </td>';
                            $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>
                            <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                            ' . substr($r['t_nopbphtbsppt'], 23, 1) . '
                            </td>
                            <td style="border-left: 1px solid black; padding-left: 20px; ">
                            </td>
                            <td style="width: 10px"></td>
                        </tr>
                            <tr>
                                <td style="border-bottom: 1px solid black;">
                                ' . $ix . '.2. Letak tanah dan atau bangunan :
                                </td>
                                <td style="border-bottom: 1px solid black;" colspan="27">
                                    ' . strtoupper($R["t_alamatop"]) . '
                                </td>
                                <!--<td colspan="15" style="border-bottom: 1px solid black;">
                                    Blok/Kav/Nomor : 
                                </td>-->
                                <td></td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid black;" colspan="14">
                                ' . $ix . '.3. Kelurahan/Desa : ' . $r["t_kelurahanop"] . '
                                </td>
                                <td style="border-bottom: 1px solid black;" colspan="14">
                                    ' . $ix . '.4. RT/RW :
                                    ' . strtoupper($r["t_rtop"]) . ' / ' . strtoupper($r["t_rwop"]) . '
                                </td>
                                <!--<td colspan="15" style="border-bottom: 1px solid black;">
                                    5. Kecamatan : ' . $r["t_kecamatanop"] . '
                                </td>-->
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="14" style="border-bottom: 1px solid black;">
                                    <!--6. Kabupaten/Kota : ' . $data_pemda->s_namakabkot . '-->
                                    ' . $ix . '.5. Kecamatan : ' . $r["t_kecamatanop"] . '
                                </td>
                                <td colspan="14" style="border-bottom: 1px solid black;">
                                    <!--Kode Pos : ' . $data_pemda->s_kodepos . '-->
                                    ' . $ix . '.6. Kabupaten/Kota : ' . $data_pemda->s_namakabkot . '
                                </td>
                                <td></td>
                            </tr>';

                        endforeach;
                    } else {
                        $html .= '
                            <tr>
                            <td>
                                1. Nomor Objek Pajak (NOP) PBB 
                            </td>
                            <td style=" width: 1%" class="">:&nbsp;</td>
                            <td style="width: 10px;">&nbsp;</td>';
                        $html .= '
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($row['t_nopbphtbsppt'], 0, 1) . '
                                </td>
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($row['t_nopbphtbsppt'], 1, 1) . '
                                </td>';
                        $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                        $html .= '<td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($row['t_nopbphtbsppt'], 3, 1) . '
                            </td>
                            <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($row['t_nopbphtbsppt'], 4, 1) . '
                            </td>';
                        $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                        $html .= '
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($row['t_nopbphtbsppt'], 6, 1) . '
                                </td>
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($row['t_nopbphtbsppt'], 7, 1) . '
                                </td>
                                <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                ' . substr($row['t_nopbphtbsppt'], 8, 1) . '
                                </td>';
                        $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                        $html .= '<td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 10, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 11, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 12, 1) . '
                                    </td>';
                        $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                        $html .= '<td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 14, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 15, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 16, 1) . '
                                    </td>';
                        $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>';
                        $html .= '<td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 18, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 19, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 20, 1) . '
                                    </td>
                                    <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                    ' . substr($row['t_nopbphtbsppt'], 21, 1) . '
                                    </td>';
                        $html .= '
                            <td style="border-left: 1px solid black;">
                            </td>
                            <td style="height: 8px; width: 15px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                            ' . substr($row['t_nopbphtbsppt'], 23, 1) . '
                            </td>
                            <td style="border-left: 1px solid black; padding-left: 20px; ">
                            </td>
                            <td style="width: 10px"></td>
                        </tr>
                            <tr>
                                <td style="border-bottom: 1px solid black;">
                                    2. Letak tanah dan atau bangunan :
                                </td>
                                <td style="border-bottom: 1px solid black;" colspan="27">
                                    ' . strtoupper($row["t_alamatop"]) . '
                                </td>
                                <!--<td colspan="15" style="border-bottom: 1px solid black;">
                                    Blok/Kav/Nomor : 
                                </td>-->
                                <td></td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid black;" colspan="14">
                                    3. Kelurahan/Desa : ' . $row["t_kelurahanop"] . '
                                </td>
                                <td style="border-bottom: 1px solid black;" colspan="14">
                                    4. RT/RW :
                                    ' . strtoupper($row["t_rtop"]) . ' / ' . strtoupper($row["t_rwop"]) . '
                                </td>
                                <!--<td colspan="15" style="border-bottom: 1px solid black;">
                                    5. Kecamatan : ' . $row["t_kecamatanop"] . '
                                </td>-->
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="14" style="border-bottom: 1px solid black;">
                                    <!--6. Kabupaten/Kota : ' . $data_pemda->s_namakabkot . '-->
                                    5. Kecamatan : ' . $row["t_kecamatanop"] . '
                                </td>
                                <td colspan="14" style="border-bottom: 1px solid black;">
                                    <!--Kode Pos : ' . $data_pemda->s_kodepos . '-->
                                    6. Kabupaten/Kota : ' . $data_pemda->s_namakabkot . '
                                </td>
                                <td></td>
                            </tr>';
                    }

                    $html .= '
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    Penghitungan NJOP PBB
                </td>
            </tr>
            <tr>
                <td> 
                </td>
                <td>
                    <table width="100%" style="border-spacing: 0px;" border="0">';
                    if ($detail_nop != NULL) {
                        foreach ($detail_nop as $r) :
                            $iy++;
                            $html .= '
                                <tr>
                                <td style="text-align: center; border: 1px solid black; width: 150px; ">
                                    Uraian
                                </td>
                                <td colspan="2" style="text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                                    <p style="size: 2px; margin: 0;"><b>Luas</b><br><span style="font-size:8px;">(Diisi luas tanah dan atau bangunan yang haknya diperoleh)</span></p>
                                </td>
                                <td colspan="2" style="text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                                    <p style="size: 2px; margin: 0;"><b>NJOP PBB / m <sup>2</sup></b><br><span style="font-size:8px;">(Diisi berdasarkan SPPT PBB tahun terjadinya perolehan hak/Tahun......)</span></p>
                                </td>
                                <td colspan="3" style="text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                                    Luas x NJOP PBB / m<sup>2</sup>
                                </td>
                                <td style="width: 10px; "></td>
                            </tr>
                            <tr>
                                <td style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    Tanah (Bumi)
                                </td>
                                <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black; width: 40px; ">
                                    ' . $iy . '.7.
                                </td>
                                <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                            if (!empty($r['p_luastanah'])) {
                                $luastanah = $r['p_luastanah'];
                            } else {
                                $luastanah = $r["t_luastanah"];
                            }
                            $html .= '
                                    ' . number_format($luastanah, 0, ',', '.') . ' m<sup>2</sup>
                                </td>
                                <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black; width: 40px; ">
                                ' . $iy . '.9.
                                </td>
                                <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                            if (!empty($r['p_njoptanah'])) {
                                $njoptanah = $r['p_njoptanah'];
                            } else {
                                $njoptanah = $r["t_njoptanah"];
                            }
                            $html .= '
                                    ' . number_format($njoptanah, 0, ',', '.') . '
                                </td>
                                <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black; width: 40px; ">
                                ' . $iy . '.11.
                                </td>
                                <td colspan="2" style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black; width: 180px;">';
                            if (!empty($r['p_totalnjoptanah'])) {
                                $totalnjoptanah = $r['p_totalnjoptanah'];
                            } else {
                                $totalnjoptanah = $r["t_totalnjoptanah"];
                            }
                            $html .= '
                                    ' . number_format($totalnjoptanah, 2, ',', '.') . '
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    Bangunan
                                </td>
                                <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    ' . $iy . '.8.
                                </td>
                                <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                            if (!empty($r['p_luasbangunan'])) {
                                $luasbangunan = $r['p_luasbangunan'];
                            } else {
                                $luasbangunan = $r["t_luasbangunan"];
                            }
                            $html .= '
                                    ' . number_format($luasbangunan, 0, ',', '.') . ' m<sup>2</sup>
                                </td>
                                <td  style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black;">
                                ' . $iy . '.10.
                                </td>
                                <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                            if (!empty($r['p_njopbangunan'])) {
                                $njopbangunan = $r['p_njopbangunan'];
                            } else {
                                $njopbangunan = $r["t_njopbangunan"];
                            }
                            $html .= '
                                    ' . number_format($njopbangunan, 0, ',', '.') . '
                                </td>
                                <td  style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black;">
                                ' . $iy . '.12.
                                </td>
                                <td colspan="2" style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                            if (!empty($r['p_totalnjopbangunan'])) {
                                $totalnjopbangunan = $r['p_totalnjopbangunan'];
                            } else {
                                $totalnjopbangunan = $r["t_totalnjopbangunan"];
                            }
                            $html .= '
                                    ' . number_format($totalnjopbangunan, 2, ',', '.') . '
                                </td>
                                <td></td>
                            </tr>';

                        endforeach;
                    } else {
                        $html .= '
                            <tr>
                                <td style="text-align: center; border: 1px solid black; width: 150px; ">
                                    Uraian
                                </td>
                                <td colspan="2" style="text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                                    <p style="size: 2px; margin: 0;"><b>Luas</b><br><span style="font-size:8px;">(Diisi luas tanah dan atau bangunan yang haknya diperoleh)</span></p>
                                </td>
                                <td colspan="2" style="text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                                    <p style="size: 2px; margin: 0;"><b>NJOP PBB / m <sup>2</sup></b><br><span style="font-size:8px;">(Diisi berdasarkan SPPT PBB tahun terjadinya perolehan hak/Tahun......)</span></p>
                                </td>
                                <td colspan="3" style="text-align: center; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black;">
                                    Luas x NJOP PBB / m<sup>2</sup>
                                </td>
                                <td style="width: 10px; "></td>
                            </tr>
                            <tr>
                                <td style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    Tanah (Bumi)
                                </td>
                                <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black; width: 40px; ">
                                    7
                                </td>
                                <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                        if (!empty($row['p_luastanah'])) {
                            $luastanah = $row['p_luastanah'];
                        } else {
                            $luastanah = $row["t_luastanah"];
                        }
                        $html .= '
                                    ' . number_format($luastanah, 0, ',', '.') . ' m<sup>2</sup>
                                </td>
                                <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black; width: 40px; ">
                                    9
                                </td>
                                <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                        if (!empty($row['p_njoptanah'])) {
                            $njoptanah = $row['p_njoptanah'];
                        } else {
                            $njoptanah = $row["t_njoptanah"];
                        }
                        $html .= '
                                    ' . number_format($njoptanah, 0, ',', '.') . '
                                </td>
                                <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black; width: 40px; ">
                                    11
                                </td>
                                <td colspan="2" style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black; width: 180px;">';
                        if (!empty($row['p_totalnjoptanah'])) {
                            $totalnjoptanah = $row['p_totalnjoptanah'];
                        } else {
                            $totalnjoptanah = $row["t_totalnjoptanah"];
                        }
                        $html .= '
                                    ' . number_format($totalnjoptanah, 2, ',', '.') . '
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    Bangunan
                                </td>
                                <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    8
                                </td>
                                <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                        if (!empty($row['p_luasbangunan'])) {
                            $luasbangunan = $row['p_luasbangunan'];
                        } else {
                            $luasbangunan = $row["t_luasbangunan"];
                        }
                        $html .= '
                                    ' . number_format($luasbangunan, 0, ',', '.') . ' m<sup>2</sup>
                                </td>
                                <td  style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    10
                                </td>
                                <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                        if (!empty($row['p_njopbangunan'])) {
                            $njopbangunan = $row['p_njopbangunan'];
                        } else {
                            $njopbangunan = $row["t_njopbangunan"];
                        }
                        $html .= '
                                    ' . number_format($njopbangunan, 0, ',', '.') . '
                                </td>
                                <td  style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    12
                                </td>
                                <td colspan="2" style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">';
                        if (!empty($row['p_totalnjopbangunan'])) {
                            $totalnjopbangunan = $row['p_totalnjopbangunan'];
                        } else {
                            $totalnjopbangunan = $row["t_totalnjopbangunan"];
                        }
                        $html .= '
                                    ' . number_format($totalnjopbangunan, 2, ',', '.') . '
                                </td>
                                <td></td>
                            </tr>';
                    }

                    $html .= '
                                    <tr>
                                        <td colspan="5" align="right" style="border-right: 1px solid balck; ">
                                            NJOP PBB : &nbsp; &nbsp; &nbsp;
                                        </td>
                                        <td style="text-align: center; border-bottom: 1px solid black; border-right: 1px solid black;">
                                            13
                                        </td>
                                        <td colspan="2" style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black; ">';
                    if (!empty($row['p_grandtotalnjop'])) {
                        $grandnjop = $row['p_grandtotalnjop'];
                    } else {
                        $grandnjop = $row["t_grandtotalnjop"];
                    }
                    $html .= '' . number_format($grandnjop, 2, ',', '.') . '
                                        </td>
                                        <td></td>
                                    </tr>';

                    if (($row['t_tarif_pembagian_aphb_kali'] == null) || ($row['t_tarif_pembagian_aphb_kali'] == '') || ($row['t_tarif_pembagian_aphb_kali'] == 0) || ($row['t_tarif_pembagian_aphb_bagi'] == null) || ($row['t_tarif_pembagian_aphb_bagi'] == '') || ($row['t_tarif_pembagian_aphb_bagi'] == 0)) {

                        $ket_aphb_pembagian = '';
                        $grandnjop_aphb = 0;
                    } else {
                        $ket_aphb_pembagian = ' ' . $row['t_tarif_pembagian_aphb_kali'] . '/' . $row['t_tarif_pembagian_aphb_bagi'];
                        if (!empty($row['p_grandtotalnjop_aphb'])) {
                            $grandnjop_aphb = $row['p_grandtotalnjop_aphb'];
                        } else {
                            $grandnjop_aphb = $row["t_grandtotalnjop_aphb"];
                        }
                        $html .= '
                                        <tr>
                                            <td colspan="5" align="right" style="border-right: 1px solid black; ">
                                                &nbsp; &nbsp; &nbsp;
                                            </td>
                                            <td colspan="3" style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black; ">
                                                ' . $ket_aphb_pembagian . '
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" align="right" style="border-right: 1px solid black; ">
                                                NJOP PBB APHB: &nbsp; &nbsp; &nbsp;
                                            </td>
                                            <td colspan="3" style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black; ">
                                                ' . number_format($grandnjop_aphb, 2, ',', '.') . '
                                            </td>
                                            <td></td>
                                        </tr>';
                    }

                    $html .= '
                                    <tr>
                                        <td colspan="9" >
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    </table>
                                    <table width="100%" style="border-spacing: 0px;" border="0">
                                    <tr>
                                        <td colspan="3" align="left" style="width: 290px; ">
                                            15. Jenis perolehan hak atas tanah dan/atau bangunan :
                                        </td>
                                        <td>
                                        <table align="center" style="margin-top: -4px; border-collapse: collapse;" border="0">
                                            <tr>
                                                <td style="padding-left: 5px; padding-right: 5px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; ">
                                                    ' . substr(str_pad($row["s_kodejenistransaksi"], 2, '0', STR_PAD_LEFT), 0, 1) . '
                                                </td>
        
                                                <td style="padding-left: 5px; padding-right: 5px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                    ' . substr(str_pad($row["s_kodejenistransaksi"], 2, '0', STR_PAD_LEFT), 1, 1) . '
                                                </td>
        
                                            </tr>
                                        </table>
                                        </td>
                                        <td align="left" style="width:181px;">
                                            &nbsp; &nbsp;14. Harga transaksi / Nilai pasar : 
                                        </td>
                                        <td align="left" style="border-bottom: 1px solid black; border-top: 1px solid black; border-left: 1px solid black; ">Rp.</td>
                                        <td colspan="2" align="right" style="border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black; width: 202px;">';

                    if (!empty($row['p_nilaitransaksispt'])) {
                        $nilaitransaksispt = $row['p_nilaitransaksispt'];
                    } else {
                        $nilaitransaksispt = $row["t_nilaitransaksispt"];
                    }
                    $html .= '' . number_format($nilaitransaksispt, 2, ',', '.') . '
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="padding-bottom: 10px; ">
                                            16. Nomor Sertifikat Tanah : ' . $row["t_nosertifikathaktanah"] . '
                                        </td>
                                    </tr>
                                </table>
                            </td>
                </tr>
                </table>';
                    $html .= '
                <table width="100%" style="padding:0; border-spacing:0px; font-size: 11px; border-right: 1px solid black; border-left: 1px solid black; ">
                <tr>
                    <td style="padding-left: 10px; border-top: 1px solid black; border-bottom: 1px solid black;" colspan="5">
                        C. PENGHITUNGAN BPHTB (Hanya diisi berdasarkan penghitungan Wajib Pajak)';

                    // Untuk Perhitungan NPOP
                    $aphb_kali = $row['t_tarif_pembagian_aphb_kali'];
                    $aphb_bagi = $row['t_tarif_pembagian_aphb_kali'];
                    if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

                        if ($grandnjop > $nilaitransaksispt) {
                            $npop = $grandnjop;
                        } else {
                            $npop = $nilaitransaksispt;
                        }
                    } else {
                        if ($grandnjop_aphb > $nilaitransaksispt) {
                            $npop = $grandnjop_aphb;
                        } else {
                            $npop = $nilaitransaksispt;
                        }
                    }


                    /* if ($nilaitransaksispt >= $grandnjop) {
                    $npop = $nilaitransaksispt;
                    } else {
                    $npop = $grandnjop;
                    } */

                    // Untuk Potongan (NPOPTKP)
                    $potonganspt = $row["t_potonganspt"];

                    if (($row['t_potongan_waris_hibahwasiat'] == null) || ($row['t_potongan_waris_hibahwasiat'] == 0)) {

                        $t_potongan_waris_hibahwasiat = 0;
                        $hitung_potonganwaris = 1;
                    } else {

                        $t_potongan_waris_hibahwasiat = $row['t_potongan_waris_hibahwasiat'];
                        $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
                    }

                    // Untuk Perhitungan NPOPKP
                    if ($npop < $row["t_potonganspt"]) {
                        $npopkp = 0;
                    } else {
                        $npopkp = $npop - $row["t_potonganspt"];
                    }

                    // Untuk Perhitungan Pajak BPHTB
                    if ($npopkp == 0) {
                        $totalpajak = 0;
                        $totalpajak_ori = 0;
                    } else {
                        $totalpajak_ori = ceil($npopkp * $row['t_persenbphtb'] / 100);
                        $totalpajak = ceil($npopkp * $row['t_persenbphtb'] / 100 * $hitung_potonganwaris);
                    }

                    $html .= '
                        </td>
                    </tr>
                    <tr>

                        <td colspan="2" style=" border-bottom: 1px solid black; ">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nilai Perolehan Objek Pajak (NPOP) <span style="font-size:10px"></span>
                        </td>
                        <td style="width: 20px; text-align: center; border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                            1
                        </td>
                        <td style="width: 10px; border-bottom: 1px solid black; ">
                            Rp.
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid black;">
                            ' . number_format($npop, 2, ',', '.') . '
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-bottom: 1px solid black;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nilai Perolehan Objek Pajak Tidak Kena Pajak (NPOPTKP)
                        </td>
                        <td style="text-align: center; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                            2
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            Rp.
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid black;">
                            ' . number_format($potonganspt, 2, ',', '.') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 450px; border-bottom: 1px solid black;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nilai Perolehan Objek Pajak Kena Pajak (NPOPKP)
                        </td>
                        <td style="width: 100px; text-align: center; border-bottom: 1px solid black; border-left: 1px solid black;">
                            angka 1-angka 2
                        </td>
                        <td style="text-align: center; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                            3
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            Rp. 
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid black;">
                            ' . number_format($npopkp, 2, ',', '.') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px solid black;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bea Perolehan Hak atas Tanah dan Bangunan yang terutang
                        </td>
                        <td style="width: 80px; text-align: center; border-bottom: 1px solid black; border-left: 1px solid black;">
                            ' . $row['t_persenbphtb'] . '% x angka 3
                        </td>
                        <td style="text-align: center; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                            4
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            Rp.
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid black;">
                            ' . number_format($totalpajak_ori, 2, ',', '.') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px solid black;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengenaan 50% karena waris / Hibah Wasiat / pemberian hak pengelolaan*)
                        </td>
                        <td style="width: 80px; text-align: center; border-bottom: 1px solid black; border-left: 1px solid black;">
                    ' . $row['t_potongan_waris_hibahwasiat'] . '% x angka 4
                        </td>
                        <td style="text-align: center; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                            5
                        </td>
                        <td style="border-bottom: 1px solid black;">
                            Rp.
                        </td>
                        <td style="text-align: right; border-bottom: 1px solid black;">';

                    if (($row['t_potongan_waris_hibahwasiat'] == null) || ($row['t_potongan_waris_hibahwasiat'] == 0)) {
                        $html .= '' . number_format(0, 2, ',', '.') . '';
                    } else {
                        $html .= '' . number_format($totalpajak, 2, ',', '.') . '';
                    }
                    $html .= '
                        </td>
                    </tr>';

                    // Untuk Nilai Pembayaran Yang Pernah Dibayarkan
                    if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) {
                        $total_pembayaran_sspd = $totalpajak - $ar_sebelum['t_nilaipembayaranspt'];
                        //$nilaipembayaranspt = $ar_sebelum['t_nilaipembayaranspt'] + $row['t_nilaipembayaranspt'];
                    } else {
                        $total_pembayaran_sspd = $totalpajak;

                        //$nilaipembayaranspt = $row['t_nilaipembayaranspt'];
                    }

                    // Untuk Total Yang Masih Harus Dibayarkan 
                    //$harus_dibayar = $totalpajak - $nilaipembayaranspt;

                    $html .= '
                <tr>
                    <td style="border-bottom: 1px solid black;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bea Perolehan Hak atas Tanah dan Bangunan yang harus dibayar
                    </td>
                    <td style="width: 80px; text-align: center;" style="border-bottom: 1px solid black;">
                        ';
                    //number_format($ar_sebelum['t_nilaipembayaranspt'], 0, ',', '.')       
                    $html .= '
                    </td>
                    <td style="text-align: center; border-bottom: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                        6
                    </td>
                    <td style="border-bottom: 1px solid black;">
                        Rp. 
                    </td>
                    <td style="text-align: right; border-bottom: 1px solid black;">
                        ' . number_format($total_pembayaran_sspd, 2, ',', '.') . '
                    </td>
                </tr>
                </table>';
                    $html .= '
                <table border="0" width="100%" style="padding:0; border-spacing:0px; font-size: 11px; border-left: 1px solid black; border-right: 1px solid black; ">
                    <tr>
                        <td colspan="6" style="padding-left: 10px; padding-top: 10px; ">
                            D. Jumlah Setoran berdasarkan : (Beri tanda silang "X" pda kotak yang sesuai)
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10px;">
                        </td>
                        <td style="width: 20px;">
                            <table>
                                <tr>
                                    <td style=" width: 20px; border-left: 1px solid black; border-right: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="4" style="padding-left: 10px;">
                            a. Penghitungan Wajib Pajak
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <td style=" width: 20px; border-left: 1px solid black; border-right: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                                     
                            ';
                    if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) {
                        $html .= 'X';
                    } else {
                        $html .= '&nbsp;';
                    }
                    $html .= '
                                </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="4" style="padding-left: 10px; ">
                            b. SPTD/ SKPDKB/ SKPDKBT *)  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nomor: ............................... &nbsp;&nbsp;&nbsp;&nbsp; Tanggal: .......................
                        </td>
                        <!--<td>
                            <table width="100%" style="border-spacing: 0;">
                                <tr>
                                    <td class="border_bawah" style="width: 50%;">
                                        Nomor.
                                    </td>
                                    <td class="border_bawah">
                                        Tanggal
                                    </td>
                                </tr>
                            </table>
                        </td>-->
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <td style=" width: 20px; border-left: 1px solid black; border-right: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" style="padding-left: 10px; width: 230px;">
                            c. Pengurangan dihitung sendiri menjadi :
                        </td>
                        <td style="width: 40px; ">
                            <table style="border-collapse: collaspe;">
                                <tr>
                                    <td style="width: 15px; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">&nbsp;</td>
                                    <td style="width: 15px; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">
                            % berdasarkan Peraturan Bupati Nomor ..................................
                        </td>
                    </tr>
                    <tr>
                    <td></td>
                        <td>
                            <table>
                                <tr>
                                    <td style=" width: 20px; border-left: 1px solid black; border-right: 1px solid black; border-top: 1px solid black; border-bottom: 1px solid black;">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="4" style="padding-left: 10px; ">
                            d. ..........................
                        </td>
                    </tr>
                    <tr>
                        <td style=" border-bottom: 1px solid black;"></td>
                        <td colspan="5" style="border-bottom: 1px solid black; ">
                            <i>*) Coret yang tidak perlu</i>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="border-spacing: 10px; height: 50px; border-bottom: 1px solid black; ">
        
                            <table align="left" border="0" width="100%">
                                <tr>
                                    <td width="250px" style="text-align: center;" align="center">JUMLAH YANG DISETOR (dengan angka): </td>
                                    <!--<td>
                                        &nbsp;&nbsp;&nbsp; 
                                    </td>-->
                                    <td style="text-align: left;" rowspan="2">
                                        (dengan huruf) : ';

                    /* if ($npopkp == 0) {
                                        //$totalpajak = 0;
                                        echo"Nihil";
                                        } else {
                                        $totalpajak = $npopkp * 5 / 100;
                                        echo ucwords(strtolower(terbilang($totalpajak) . " Rupiah"));
                                        } */


                    //if ($nilaipembayaranspt == 0) {
                    //    echo"Nihil";
                    //} else {
                    //echo ucwords(strtolower(terbilang($nilaipembayaranspt) . " Rupiah"));

                    if ($totalpajak != 0) {
                        $html .= '' . ucwords(strtolower($this->terbilang($total_pembayaran_sspd) . " Rupiah"));
                    } else {
                        $html .= 'Nihil';
                    }

                    //}


                    //$data_pemda->s_norekbank       
                    $html .= '
                                    </td>
                                </tr>
                                <tr>
        
                                    <td style="height: 20px; text-align: center; border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; " align="center" width="120">
                                        Rp. ' . number_format($total_pembayaran_sspd, 2, ',', '.'); // $nilaipembayaranspt  $totalpajak                       
                    $html .= '</td>
                                    <!--<td> &nbsp;&nbsp;&nbsp;</td>-->
                                    <!--<td>
                                        &nbsp;&nbsp;&nbsp;
                                    </td>-->
                                </tr>
                                <!--<tr>
                                    <td> &nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>-->
                            </table>
        
                        </td>
        
                    </tr>
                </table>';
                    $html .= '
                <table border="0" width="100%" style="padding:0;border-spacing:0px; font-size: 10px; border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black; ">
                <tr>
                    <td style="text-align: center; border-right: 1px solid black; width: 24%; padding: 5px; vertical-align: top; ">
                        ' . $data_pemda->s_namaibukotakabkot . ', tgl ........................
                        <br />
                        WAJIB PAJAK / PENYETOR
                    </td>
                    <td style="text-align: center; border-right: 1px solid black; width: 26%; padding: 5px; vertical-align: top;">
                        MENGETAHUI :
                        <br />
                        PPAT/NOTARIS/KEPALA KANTOR<br/> LELANG/
                        PEJABAT LELANG/KEPALA<br/>KANTOR
                        PERTANAHAN KABUPATEN
                    </td>
        
                    <td style="text-align: center; border-right: 1px solid black; width: 23%; padding: 5px; vertical-align: top;">
                        DITERIMA OLEH :
                        <br>
                        TEMPAT PEMBAYARAN BPHTB
                        <br>
                        Tanggal : 
                        <b>';
                    if ($row['t_tanggalpembayaran'] != null) {
                        $html .= '' . date('d-m-Y', strtotime($row['t_tanggalpembayaran']));
                    } else {
                        $html .= '';
                    }
                    $html .= '
                        </b>';
                    $html .= '
                    </td>
                    
                    <td style="text-align: center; width: 27%; padding: 5px; vertical-align: top;" rowspan="2">
                        telah diverifikasi : 
                        <b>';
                    if ($row['t_tglverifikasispt_kabid'] != null) {
                        $html .= '' . date('d-m-Y', strtotime($row['t_tglverifikasispt_kabid'])) . '';
                    } else {
                        $html .= '';
                    }
                    $html .= '
                        </b>
                        <br>
                        ' . strtoupper($data_pemda->s_namainstansi) . '<br>';
                    $html .= '' . $tampilbarcode . ' 
                    </td>
                </tr>
                <tr>
                    <td style="border-right: 1px solid black;"><br/><br/><br/><br/><br/></td>
                    <td style="border-right: 1px solid black;"><br/><br/><br/><br/><br/></td>
                    <td style="border-right: 1px solid black;"><br/><br/><br/><br/><br/></td>
                </tr>
                <tr>
                    <td style="text-align: center; border-right: 1px solid black; ">' . strtoupper($row['t_namawppembeli']) . '</td>
                    <td style="text-align: center; border-right: 1px solid black;">' . strtoupper($row['s_namanotaris']) . '</td>
                    <td style="text-align: center; border-right: 1px solid black; ">';
                    if ($row['t_idpenerimasetoran'] == 3) {
                        if ($row['t_nilaipembayaranspt'] == 0) {
                            $html .= '';
                        } else {
                            $html .= 'BPD KALSEL';
                        }
                    } else {
                        // $html .=''. strtoupper($row['namapenerimasetoran']);
                        $html .= '';
                    }
                    $html .= '</td>
                    <td style="text-align: center; ">' . strtoupper($data_mengetahui->s_namapejabat) . '</td>
                </tr>
                <tr>
                    <td style="font-size: 10px; text-align: center; border-spacing: 10px; border-right: 1px solid black; border-top: 1px solid black;">
                        Nama Lengkap dan tanda tangan
                    </td>
                    <td style="font-size: 10px; text-align: center; border-right: 1px solid black; border-top: 1px solid black;">
                        Nama Lengkap, Stempel dan tanda tangan
                    </td>
                    <td style="font-size: 10px; text-align: center; border-right: 1px solid black; border-top: 1px solid black;">
                        Nama Lengkap, Stempel dan tanda tangan
                    </td>
                    <td style="font-size: 10px; text-align: center; border-top: 1px solid black;">
                        Nama Lengkap, Stempel dan tanda tangan
                    </td>   
                </tr>
                </table>';
                    $html .= '
                <table border="0" width="100%" style="padding:0; border-spacing:0px; font-size: 11px; border-right: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black;">
                <tr>
                    <td style="text-align: center; width: 110px; border-right: 1px solid black;">
                        Hanya diisi oleh <br>petugas ' . $data_pemda->s_namasingkatinstansi . '
                    </td>
                    <td>
                        <table width="100%" style="border-spacing: 0; padding: 5px; " border="0">
                            <tr>
                                <td style="width: 120px; ">
                                    Nomor Dokumen
                                </td>
                                <td style="width: 20px; ">
                                    :
                                </td>
                                <td style="height: 8px; text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">';
                    $tgl = explode('-', $row['t_tglprosesspt']);
                    $tahun = $tgl[0];
                    $bulan = $tgl[1];
                    $hari = $tgl[2];
                    $html .= '
                                    ' . $hari[0] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                    ' . $hari[1] . '
                                </td>
                                <td>&nbsp;
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                    ' . $bulan[0] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                                    ' . $bulan[1] . '
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    ' . $tahun[0] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    ' . $tahun[1] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    ' . $tahun[2] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;"> 
                                    ' . $tahun[3] . '
                                </td>
                                <td>&nbsp;
                                </td>';

                    $nosspd = str_pad($row['t_kohirketetapanspt'], 7, '0', STR_PAD_LEFT);

                    $html .= '
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    ' . $nosspd[0] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    ' . $nosspd[1] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    ' . $nosspd[2] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;" width="12">
                                    ' . $nosspd[3] . '
                                </td>
                                <td>&nbsp;
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    ' . $nosspd[4] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                    ' . $nosspd[5] . '
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black; ">
                                    ' . $nosspd[6] . '
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="26">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    NOP PBB Baru
                                </td>
                                <td width="5">
                                    :
                                </td>
                                <td style="width: 20px; height: 8px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black">&nbsp;
                                </td>
                                <td style="width: 20px;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-right: 1px solid black; border-left: 1px solid black; ">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; ">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-left: 1px solid blakc;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;" >&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black; " >&nbsp;
                                </td>
                                <td style="width: 20px;">&nbsp;
                                </td>
                                <td style="width: 20px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black; ">&nbsp;
                                </td>
        
                            </tr>
        
                        </table>
                    </td>
                </tr>
                </table>';
                    if ($esign == 1) {
                        $html .= '
                    <table style="width: 100%; border-collapse: collapse; font-size: 7px;" border="0">
                        <tr>
                            <td style="text-align: right; "><b>Dokumen ini ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh BSrE.</b></td>
                        </tr>
                    </table>';
                    }
                    if ($cetakversi != 2) {
                        $html .= '
                    <table style=" border-collapse: collapse; font-size: 7px;" border="0">
                        <tr>
                            <td colspan="2">Keterangan :</td>
                        </tr>
                        <tr>
                            <td colspan="2">Lembar SSPD ini dinyatakan SAH setelah di validasi, tandatangan dan stempel oleh bank tempat pembayaran (apabila berstatus bayar)</td>
                        </tr>
                        <tr>
                            <td style=" padding-right: 5px; ">Lembar 1 : Untuk Wajib Pajak sebagai bukti pembayaran</td>
                            <td>Lembar 4 : Untuk ' . ucwords($data_pemda->s_namainstansi) . '</td>
                        </tr>
                        <tr>
                            <td style=" padding-right: 5px; ">Lembar 2 : Untuk PPAT/Notaris/Kepala Kantor Lelang/Pejabat Lelang</td>
                            <td>Lembar 5 : Untuk Bank yang ditunjuk Bendahara penerimaan</td>
                        </tr>
                        <tr>
                            <td style=" padding-right: 5px; ">Lembar 3 : Untuk Kepala Kantor Pertanahan ' . $data_pemda->s_namakabkot . '</td>
                            <td>Lembar 6 : Kantor Pelayanan Pajak (KPP) Pratama ' . $data_pemda->s_namaibukotakabkot . '</td>
                        </tr>
                    </table>';
                    }
                    $html .= '</div>';
                }
            } else {

                //============================ cetakan kurang bayar
                if (!empty($row['p_nilaitransaksispt'])) {
                    $nilaitransaksispt = $row['p_nilaitransaksispt'];
                } else {
                    $nilaitransaksispt = $row["t_nilaitransaksispt"];
                }


                if (!empty($row['p_grandtotalnjop'])) {
                    $grandnjop = $row['p_grandtotalnjop'];
                } else {
                    $grandnjop = $row["t_grandtotalnjop"];
                }

                if (($row['t_tarif_pembagian_aphb_kali'] == null) || ($row['t_tarif_pembagian_aphb_kali'] == '') || ($row['t_tarif_pembagian_aphb_kali'] == 0) || ($row['t_tarif_pembagian_aphb_bagi'] == null) || ($row['t_tarif_pembagian_aphb_bagi'] == '') || ($row['t_tarif_pembagian_aphb_bagi'] == 0)) {


                    $grandnjop_aphb = 0;
                } else {

                    if (!empty($row['p_grandtotalnjop_aphb'])) {
                        $grandnjop_aphb = $row['p_grandtotalnjop_aphb'];
                    } else {
                        $grandnjop_aphb = $row["t_grandtotalnjop_aphb"];
                    }
                }


                // Untuk Perhitungan NPOP
                $aphb_kali = $row['t_tarif_pembagian_aphb_kali'];
                $aphb_bagi = $row['t_tarif_pembagian_aphb_kali'];
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

                    if ($grandnjop > $nilaitransaksispt) {
                        $npop = $grandnjop;
                    } else {
                        $npop = $nilaitransaksispt;
                    }
                } else {
                    if ($grandnjop_aphb > $nilaitransaksispt) {
                        $npop = $grandnjop_aphb;
                    } else {
                        $npop = $nilaitransaksispt;
                    }
                }
                // Untuk Potongan (NPOPTKP)
                $potonganspt = $row["t_potonganspt"];

                if (($row['t_potongan_waris_hibahwasiat'] == null) || ($row['t_potongan_waris_hibahwasiat'] == 0)) {

                    $t_potongan_waris_hibahwasiat = 0;
                    $hitung_potonganwaris = 1;
                } else {

                    $t_potongan_waris_hibahwasiat = $row['t_potongan_waris_hibahwasiat'];
                    $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
                }

                // Untuk Perhitungan NPOPKP
                if ($npop < $row["t_potonganspt"]) {
                    $npopkp = 0;
                } else {
                    $npopkp = $npop - $row["t_potonganspt"];
                }

                // Untuk Perhitungan Pajak BPHTB
                if ($npopkp == 0) {
                    $totalpajak = 0;
                    $totalpajak_ori = 0;
                } else {
                    $totalpajak_ori = ceil($npopkp * $row['t_persenbphtb'] / 100);
                    $totalpajak = ceil($npopkp * $row['t_persenbphtb'] / 100 * $hitung_potonganwaris);
                }

                if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) {

                    $sudahdibayar = $ar_sebelum['t_nilaipembayaranspt'];

                    $total_pembayaran_sspd = $totalpajak - $ar_sebelum['t_nilaipembayaranspt'];
                    //$nilaipembayaranspt = $ar_sebelum['t_nilaipembayaranspt'] + $row['t_nilaipembayaranspt'];
                } else {
                    $sudahdibayar = " ";
                    $total_pembayaran_sspd = $totalpajak;

                    //$nilaipembayaranspt = $row['t_nilaipembayaranspt'];
                }
                $html .= '
                <div style="height: 100%;">
        
                    <table style="border-spacing: 0; font-size:9pt; width: 100%;" border="1">
                        <tr>
                            <td colspan="2">
                                <table style="border-spacing: 0;">
                                    <tr>
                                        <td width = "50" class="border_bawah">
                                    <center><img src="' . $app_path . '/' . $data_pemda->s_logo . '" alt="" width="60" style="float:left;margin-top:10px; " title="LOGO ' . $data_pemda->s_namakabkot . '"/></center>
                            </td>
                            <td class="border_bawah">
                        <center>
                            <b style="font-size:14pt;">PEMERINTAH ' . strtoupper($data_pemda->s_namakabkot) . '.' . strtoupper($data_pemda->s_namaprov) . '</b>
                            <br>
                            <b style="font-size:11pt;">' . strtoupper($data_pemda->s_namainstansi) . ' (' . strtoupper($data_pemda->s_namasingkatinstansi) . ')</b>
                            <br>
                            <b style="font-size:10pt;">' . strtoupper($data_pemda->s_alamatinstansi) . ' Telp/Fax ' . $data_pemda->s_notelinstansi . ', ' . $data_pemda->s_namaibukotakabkot . ' ' . $data_pemda->s_kodepos . '</b>
                        </center>
                        </td>
                        </tr>
                    </table>
                </td>
                <tr>
                    <td colspan="2">
                <center><b style="font-size:10pt;">SURAT KETETAPAN PAJAK DAERAH KURANG BAYAR</b></center><br><br>
                </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width:100%">
                            <tr>
                                <td style="width: 350px;">
                                    &nbsp;
                                </td>
                                <td>
                                    Kepada Yth : ' . strtoupper($row['t_namawppembeli']) . '  <br>
                                    Di ' . strtoupper($row["t_alamatwppembeli"]) . '
        
                                    RT/RW ' . strtoupper($row["t_rtwppembeli"]) . ' / ' . strtoupper($row["t_rwwppembeli"]) . '<br>
                                    Kel. ' . strtoupper($row["t_kelurahanwppembeli"]) . ' Kec. ' . strtoupper($row["t_kecamatanwppembeli"]) . '
                                    Kab. ' . strtoupper($row["t_kabkotawppembeli"]) . '<br>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>Nomor</td>
                                <td>:</td>
                                <td>' . $row['t_kohirspt'] . '</td>
                            </tr>
                            <tr>
                                <td>Tanggal penerbitan</td>
                                <td>:</td>
                                <td>' . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . '</td>
                            </tr>
        
                            <tr>
                                <td>Tanggal jatuh tempo</td>
                                <td>:</td>
                                <td>' . date('d-m-Y', strtotime(date('Y-m-d', strtotime($row['t_tglprosesspt'])) . "+1 months")) . '</td>
                            </tr>
                        </table>
                    </td>
        
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0">
                            <tr>
                                <td style="vertical-align:top; width: 10px;">I.</td>
                                <td colspan="3">Berdasarkan Peraturan Bupati MINAHASA SELATAN Nomor 32 Tahun 2012 
                                    tentang Bea Perolehan Hak Atas Tanah dan Bangunan telah dilakukan
                                    pemeriksaan atau berdasarkan keterangan lain mengenai pelaksanaan
                                    kewajiban Bea Perolehan Hak atas Tanah dan Bangunan terhadap :</td>
        
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="width: 120px; vertical-align: top;">Nama</td>
                                <td style="width: 3px; vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row['t_namawppembeli']) . '</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="vertical-align: top;">Alamat</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row["t_alamatwppembeli"]) . '
                                    RT/RW ' . strtoupper($row["t_rtwppembeli"]) . ' / ' . strtoupper($row["t_rwwppembeli"]) . '
                                    Kel. ' . strtoupper($row["t_kelurahanwppembeli"]) . '<br>
                                    Kec. ' . strtoupper($row["t_kecamatanwppembeli"]) . '
                                    Kab. ' . strtoupper($row["t_kabkotawppembeli"]) . '</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="3">Atas perolehan hak atas tanah dan atau bangunannnya dengan : <br>
                                    Akta. Risalah Lelang/Pendaftaran Hak *)</td>
        
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="vertical-align: top;">Nama</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row["t_namasppt"]) . '</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="vertical-align: top;">NOP</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row["t_nopbphtbsppt"]) . '</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="vertical-align: top;">Alamat</td>
                                <td style="vertical-align: top;">:</td>
                                <td style="vertical-align: top;">' . strtoupper($row["t_alamatop"]) . '
                                    ' . strtoupper($row["t_rtop"]) . ' / ' . strtoupper($row["t_rwop"]) . '
                                    Kel. ' . $row["t_kelurahanop"] . ' Kec. ' . $row["t_kecamatanop"] . '<br><br>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align:top; width: 10px;">II.</td>
                                <td colspan="3">Dari Pemeriksaan tersebut di atas, jumlah yang masih harus dibayar adalah sebagai berikut :
                                </td>
        
                            </tr>
                            <tr>
                                <td style="vertical-align:top; width: 10px;">&nbsp;</td>
                                <td colspan="3">
                                    <table width="100%" style="padding:0;border-spacing:0px;" border="0">
                                        <tr>
                                            <td style="width: 10px; vertical-align: middle; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                1.
                                            </td>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; width: 600px; ">
                                                Nilai Perolehan Objek Pajak (NPOP)
                                            </td>
                                            <td style="width: 15px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black;">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-top: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($npop, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                2.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Nilai Perolehan Objek Pajak Tidak Kena Pajak (NPOPTKP)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black;">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($row["t_potonganspt"], 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                3.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Nilai Perolehan Objek Pajak Kena Pajak (1-2)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($npopkp, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                4.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Pajak yang seharusnya terutang : 5% X Rp ' . number_format($npopkp, 0, ',', '.') . '(3)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($totalpajak_ori, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                5.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Pengenaan Hak Pengelolaan/Hibah Wasiat/Waris : ' . $t_potongan_waris_hibahwasiat . '% X Rp. ' . number_format($totalpajak_ori, 0, ',', '.') . ' (4)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($totalpajak, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                6. 
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Pajak yang seharusnya dibayar ( 4 atau 5 )
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($totalpajak, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                7.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Pajak yang telah dibayar
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($sudahdibayar, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                8.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Diperhitungkan :
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style=" border-bottom: 1px solid black; border-left: 1px solid black;">
        
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                8.a Pokok STPD
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style=" border-bottom: 1px solid black; border-left: 1px solid black;">
        
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                8.b. Pengurangan
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style=" border-bottom: 1px solid black; border-left: 1px solid black;">
        
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                8.c. Jumlah (8.a. + 8.b)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style=" border-bottom: 1px solid black; border-left: 1px solid black;">
        
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                8.d. Dikurangi pokok SKPDLB
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                        <td style=" border-bottom: 1px solid black; border-left: 1px solid black;">
        
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                8.e. Jumlah (8.c - 8.d.)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                9.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Jumlah yang dapat diperhitungkan (7 + 8.e.)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($sudahdibayar, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                10.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Pajak yang kurang dibayar (6-9)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp. 
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($total_pembayaran_sspd, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                11.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Sanksi administrasi berupa bunga (Pasal 84 Perda Pajak Daerah No 1 Tahun 2012):
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp.
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                0
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                                                12.
                                            </td>
                                            <td style="border-bottom: 1px solid black;">
                                                Jumlah yang masih harus dibayar (10+11)
                                            </td>
                                            <td style="border-bottom: 1px solid black; border-left: 1px solid black; ">
                                                Rp. 
                                            </td>
                                            <td style="text-align: right; width: 100px; border-bottom: 1px solid black; border-right: 1px solid black;">
                                                ' . number_format($total_pembayaran_sspd, 0, ',', '.') . '
                                            </td>
                                        </tr>
                                    </table>
        
                                </td>
        
                            </tr>
                        </table>
                    </td>
        
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width:100%">
                            <tr>
                                <td style="width: 400px;">
                                    &nbsp;
                                </td>
                                <td>
                                <center>' . $data_pemda->s_namaibukotakabkot . ', &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                // var_dump(strpos(strtolower($data_mengetahui->s_jabatanpejabat), "kepala badan"));exit();
                if (strpos(strtolower($data_mengetahui->s_jabatanpejabat), "kepala badan") !== FALSE) {
                    $atasnamakepalabadan = "";
                } else {
                    $atasnamakepalabadan = "<br />A.n Kepala " . $data_pemda->s_namainstansi . "";
                }
                $html .= '' .
                    $atasnamakepalabadan . '<br>
                                    ' . $data_mengetahui->s_jabatanpejabat . '<br><br><br><br><br>
        
                                    ( ' . strtoupper($data_mengetahui->s_namapejabat) . ' )</center>
                                </td>
                            </tr>
                        </table>
                </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 5px; ">*) coret yang tidak perlu<br>
                        NIP ................................
                    </td>
                </tr>
        
        
                </table>
        
                <table width="100%" style="margin-top: 10px; border-spacing:0px; font-size: 11px; border-top: 1px solid black; border-bottom: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
                    <tr>
                        <td style="width: 75%;">
                            <table style="border-spacing:0px;">
                                <tr>
                                    <td style="width: 150px; ">Nama Wajib Pajak</td>
                                    <td style="width: 3px; text-align: center;">:</td>
                                    <td>' . strtoupper($row['t_namawppembeli']) . '</td>
                                </tr>
                                <tr>
                                    <td colspan="3">Atas perolehan Hak atas tanah dan bangunan dengan,</td>
        
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td style="width: 3px; text-align: center;">:</td>
                                    <td>' . strtoupper($row["t_alamatop"]) . ' 
                                        ' . strtoupper($row["t_rtop"]) . ' / ' . strtoupper($row["t_rwop"]) . '
                                        Kel. ' . $row["t_kelurahanop"] . ' Kec. ' . $row["t_kecamatanop"] . '</td>
                                </tr>
                                <tr>
                                    <td>NOP</td>
                                    <td style="width: 3px; text-align: center;">:</td>
                                    <td>' . $row['t_nopbphtbsppt'] . '</td>
                                </tr>
                                <tr>
                                    <td>Nomor SPTPD</td>
                                    <td style="width: 3px; text-align: center;">:</td>
                                    <td>' . $row['t_kohirspt'] . '</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Penerbitan</td>
                                    <td style="width: 3px; text-align: center;">:</td>
                                    <td>' . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . '</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            Diterima tanggal, ........................ <br>
                            Oleh : <br>
                            <br><br><br>
        
                            (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)<br>
        
        
                        </td>
        
                    </tr>
                </table>
        
                </div>';
            }
        }

        $html .= '</body></html>';
        $mpdf->WriteHTML($html);
        if ($tipesimpan == "D" || $tipesimpan == "I") {
            if ($cetakversi == 1) {
                $filename_save = "cetaksppd_" . $kodebayar . "_1.pdf";
            } elseif ($cetakversi == 2) {
                $filename_save = "cetaksppd_" . $kodebayar . "_2.pdf";
            } else {
                $filename_save = "cetakskpdkb_" . $kodebayar . ".pdf";
            }
            $mpdf->Output($filename_save, $tipesimpan);
            exit();
        } elseif ($tipesimpan == "F") {
            if ($cetakversi == 1) {
                $filename_save = "./public/sspd_signed/cetaksppd_" . $kodebayar . "_1.pdf";
            } elseif ($cetakversi == 2) {
                $filename_save = "./public/sspd_signed/cetaksppd_" . $kodebayar . "_2.pdf";
            } else {
                $filename_save = "./public/sspd_signed/cetakskpdkb_" . $kodebayar . ".pdf";
            }
            $mpdf->Output($filename_save, $tipesimpan);
        }
        return;
    }
    //====CREATE SSPD
}
