<?php

// Modul Verifikasi

namespace Bphtb\Controller\Verifikasi;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Math\Rand;
use Bphtb\Form\Pendataan\SSPDFrm;
use Bphtb\Model\Pendataan\SSPDBphtbBase;

//PHP MAILER ================
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//PHP MAILER ================

class VerifikasiSPT extends AbstractActionController
{

    protected $tbl_pemda, $tbl_pembayaran, $tbl_verifikasi, $tbl_jenistransaksi, $tbl_haktanah, $tbl_pendataan, $tbl_persyaratan, $tbl_notifikasi, $tbl_pejabat, $tbl_sspd, $tbl_nop;

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
    }

    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $form = new \Bphtb\Form\Verifikasi\VerifikasiSPTFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $Mengetahuibphtb = $this->getTblPejabat()->getdata();
        $Mengetahuilengkap = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian1 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian2 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian3 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian4 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian5 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian6 = $this->getTblPejabat()->getdata();

        $data_mengetahuilengkap_sspd = $this->getTblPejabat()->getdata();
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $view = new ViewModel(array(
            "form" => $form,
            'data_mengetahuibphtb' => $Mengetahuibphtb,
            'data_mengetahuilengkap' => $Mengetahuilengkap,
            'data_mengetahuipenelitian1' => $Mengetahuipenelitian1,
            'data_mengetahuipenelitian2' => $Mengetahuipenelitian2,
            'data_mengetahuipenelitian3' => $Mengetahuipenelitian3,
            'data_mengetahuipenelitian4' => $Mengetahuipenelitian4,
            'data_mengetahuipenelitian5' => $Mengetahuipenelitian5,
            'data_mengetahuipenelitian6' => $Mengetahuipenelitian6,
            'data_mengetahuilengkap_sspd' => $data_mengetahuilengkap_sspd,
            'datajenistransaksi' => $datajenistransaksi
        ));
        $data = array(
            'menu_verifikasi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'fr_pendaftaran_v5';
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_idspt DESC";
        $aColumns = array(
            't_idspt',
            't_kohirspt',
            't_kohirketetapanspt',
            's_idjenistransaksi',
            't_tglverifikasispt_kabid', //'t_tglverifikasispt', 
            't_nopbphtbsppt',
            't_namawppembeli',
            't_totalspt',
            't_kodebayarbanksppt',
            't_statusbayarspt',
            'status_pendaftaran',
            'status_validasi',
            's_namapejabat',
            's_namajenistransaksi',
            't_persyaratan',
            't_verifikasispt',
            't_idjenistransaksi',
            't_inputbpn',
            't_periodespt',
            't_idpembayaranspt',
            'p_totalspt',
            'p_idpemeriksaan',
            'fr_tervalidasidua',
            'fr_validasidua',
            't_idsptsebelumnya',
            't_statusspt_kasubid',
            't_statusspt_kabid',
            't_tgljatuhtempokodebayarspt',
            't_waktuawaldaftar',
            't_waktuawalproses'
        );


        $panggildata = $this->getServiceLocator()->get("VerifikasiSPTTable");
        $rResult = $panggildata->semuadatavalidasi($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGrid2Action()
    {
        $allParams = (array)$this->getEvent()->getRouteMatch()->getParams();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $base = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->getTblPembayaran()->getGridCountVerifikasi($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->getTblPembayaran()->getGridDataVerifikasi($base, $start);
        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            if ($row['t_inputbpn'] == true) {
                $s .= "<td> <span class='badge' style='background-color:#CC0000;'>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . " </span></td>";
            } else {
                $s .= "<td>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . "</td>";
            }
            $s .= "<td>" . $row['t_periodespt'] . "</td>";
            $s .= "<td>" . date('d-m-Y', strtotime($row['t_tglverifikasispt'])) . "</td>";
            $s .= "<td>" . $row['t_namawppembeli'] . "</td>";
            $s .= "<td>" . $row['s_namajenistransaksi'] . "</td>";
            if ($row['t_statusbayarspt'] == true) {
                $status_bayar = "Sudah Dibayar";
            } else {
                $status_bayar = "Belum Dibayar";
            }

            if (!empty($row['p_idpemeriksaan'])) {
                $cetaksuratpenelitian = "<a href='#' onclick='openCetakPenelitian(" . $row['t_kohirspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:100px'>Surat Penelitian</a>";
            } else {
                $cetaksuratpenelitian = "";
            }

            $result_array_syarat = \Zend\Json\Json::decode($row['t_persyaratan']);
            $jml_syarat = count($result_array_syarat);
            $result_array_syarat_verifikasi = \Zend\Json\Json::decode($row['t_verifikasispt']);
            $jml_syarat_verifikasi = count($result_array_syarat_verifikasi);
            $cektabelpersyaratan = $this->getTblJenTran()->jumlahsyarat($row['t_idjenistransaksi']);
            if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {
                $status_verifikasi = "Tervalidasi";
                $cetaksurat = "<a href='#' onclick='openCetak(" . $row['t_kohirspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:80px'>Surat Bukti</a>";
                // $cetaksurat = "<a href='cetak_sspd/cetakbuktipenerimaanvalidasi?&action=cetakpenerimaanvalidasi&t_idspt=$row[t_idspt]' target='_blank'>Surat Bukti</a>";
                //$cetaksspd_kodebayar = "<td>" . $cetaksurat . " <a href='pendataan_sspd/cetaksspdbphtb?&action=cetaksspd&t_idspt=$row[t_idspt]' target='_blank' class='btn btn-success btn-sm btn-flat' style='width:80px'>SSPD</a> " . $cetaksuratpenelitian . " </td>";
                $cetaksurat_sspd = "<a href='#' onclick='openCetakSSPD(" . $row['t_idspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:80px'>SSPD</a>";
                $cetaksspd_kodebayar = "<td>" . $cetaksurat . " " . $cetaksurat_sspd . " " . $cetaksuratpenelitian . " </td>";
                if ($row['t_statusbayarspt'] == true) {
                    $edit = "<td><a href='verifikasi_spt/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat'>Lihat</a></td>";
                } else {
                    $edit = "<td><a href='verifikasi_spt/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat'>Lihat</a> <a href='verifikasi_spt/edit?t_idpembayaranspt=" . $row['t_idpembayaranspt'] . "' class='btn btn-warning btn-sm btn-flat'>Edit</a></td>";
                }
            } else {
                $status_verifikasi = "Belum Lengkap";
                $cetaksurat_sspd = "<a href='#' onclick='openCetakSSPD(" . $row['t_idspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:80px'>SSPD</a>";
                //<a href='pendataan_sspd/cetaksspdbphtb?&action=cetaksspd&t_idspt=$row[t_idspt]' target='_blank' class='btn btn-success btn-sm btn-flat' style='width:130px'>SSPD</a>
                $cetaksurat = "<a href='#' onclick='openCetakBukti(" . $row['t_kohirspt'] . ");return false;' class='btn btn-success btn-sm btn-flat' style='width:130px'>Surat Pemberitahuan</a> " . $cetaksurat_sspd . "";
                // $cetaksurat = "<a href='cetak_sspd/cetakbuktipenerimaanvalidasi?&action=cetakpenerimaanvalidasi&t_idspt=$row[t_idspt]' target='_blank'>Surat Pemberitahuan</a> || <a href='pendataan_sspd/cetaksspdbphtb?&action=cetaksspd&t_idspt=$row[t_idspt]' target='_blank'>SSPD</a>";
                $cetaksspd_kodebayar = "<td>" . $cetaksurat . " " . $cetaksuratpenelitian . "</td>";
                if ($row['t_inputbpn'] == true) {
                    $edit = "<td><a href='verifikasi_spt/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat'>Lihat</a> <a href='verifikasi_spt/edit?t_idpembayaranspt=" . $row['t_idpembayaranspt'] . "' class='btn btn-warning btn-sm btn-flat'>Edit</a></td>";
                } else {
                    $edit = "<td><a href='verifikasi_spt/viewdata?t_idspt=$row[t_idspt]' class='btn btn-primary btn-sm btn-flat'>Lihat</a> <a href='verifikasi_spt/edit?t_idpembayaranspt=" . $row['t_idpembayaranspt'] . "' class='btn btn-warning btn-sm btn-flat'>Edit</a> <a href='#' onclick='hapus(" . $row['t_idpembayaranspt'] . ");return false;' class='btn btn-danger btn-sm btn-flat'>Batal</a></td>";
                }
            }
            $s .= "<td>" . $status_verifikasi . "</td>";
            $s .= "<td>" . $row['t_kodebayarbanksppt'] . "</td>";
            $s .= "<td>" . $status_bayar . "</td>";
            $s .= $cetaksspd_kodebayar;
            $s .= "" . $edit . "";

            if (($session['s_namauserrole'] == "Administrator")) {
                $s .= "<td><a href='#' onclick='hapus(" . $row['t_idpembayaranspt'] . ");return false;' class='btn btn-danger btn-sm btn-flat'>Hapus Verifikasi</a></td>";
            } else {
            }

            $s .= "<tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function tambahAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $ar_pemda = $this->getPemda()->getdata();
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $frm = new \Bphtb\Form\Verifikasi\VerifikasiSPTFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Verifikasi\VerifikasiSPTBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                // var_dump($kb);exit();
                $t_pejabatverifikasi = $session['s_iduser'];
                // mengecek jumlah persyaratan di pendaftaran dan verifikasi
                $dataPendaftaran = $this->getTblVerifikasi()->getViewPendaftaran($kb->t_idspt);
                $result_array_syarat = \Zend\Json\Json::decode($dataPendaftaran['t_persyaratan']);
                $jml_syarat = count($result_array_syarat);
                $jml_syarat_verifikasi = count($kb->t_persyaratanverifikasi);
                $cektabelpersyaratan = $this->getTblJenTran()->jumlahsyarat($dataPendaftaran['t_idjenistransaksi']);
                usleep(rand(500000, 2000000));
                if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {

                    //=============== Model\Verifikasi\VerifikasiSPTTable
                    $datamax = $this->getTblVerifikasi()->getmaxkohir($dataPendaftaran['t_periodespt']);
                    if (!empty($dataPendaftaran['t_kohirketetapanspt'])) {
                    } else {
                        $this->getTblVerifikasi()->updatenosspd($kb, $datamax['t_kohirketetapanspt']);
                    }

                    if (empty($kb->t_kodebayarbanksppt)) {
                        $kb->t_kodebayarbanksppt = '1002' . date('y') . str_pad($datamax['t_kohirketetapanspt'], 6, "0", STR_PAD_LEFT); //str_replace('-', '', date('Y-m-d')).''; //$string; //$dataPendaftaran['t_kohirspt'] //$dataPendaftaran['t_tglprosesspt']
                    } else {
                        $kb->t_kodebayarbanksppt = $kb->t_kodebayarbanksppt;
                    }
                } else {
                    $kb->t_kodebayarbanksppt = null;
                }

                $data_get = $req->getPost();
                if ($data_get->t_pemeriksaanop == 1) {
                    $kb->t_totalspt = (int)str_ireplace(".", "", $kb->p_totalspt);
                } else {
                    $kb->t_totalspt = (int)str_ireplace(".", "", $kb->t_totalspt);
                }

                // simpan validasi default
                //if ($kb->t_totalspt > 0) {
                //    $this->getTblVerifikasi()->savedata($kb, $t_pejabatverifikasi);
                //} else {
                // jika pembayaran nihil maka otomatis terbayar (jika sudah valid)
                //    if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {
                //        $this->getTblVerifikasi()->savedataverifikasipembayaran($kb, $t_pejabatverifikasi);
                //    } else { // jika belum valid maka belum dianggap bayar
                //        $this->getTblVerifikasi()->savedata($kb, $t_pejabatverifikasi);
                //    }
                //}

                //simpan validasi kasubid
                $dataspt = $this->getTblSSPDBphtb()->getDataId_all($kb->t_idspt);
                // var_dump($dataspt);exit();
                // OPSI POTONGAN WARIS_HIBAH_WASIAT
                if ($dataspt["t_idjenistransaksi"] == 4 || $dataspt["t_idjenistransaksi"] == 5) {
                    if ($data_get['opsi_potongan_waris_hibahwasiat'] == 1) {
                        $dataspt["t_potongan_waris_hibahwasiat"] = 0;
                        $dataPendaftaran['t_potongan_waris_hibahwasiat'] = 0;
                        $potongan_hibahwasiat_waris = 1;
                    } elseif ($data_get['opsi_potongan_waris_hibahwasiat'] == 2) {
                        $dataspt["t_potongan_waris_hibahwasiat"] = 50;
                        $dataPendaftaran['t_potongan_waris_hibahwasiat'] = 50;
                        $potongan_hibahwasiat_waris = $dataspt["t_potongan_waris_hibahwasiat"] / 100;
                    }
                    $dataspt["t_totalspt"] = str_ireplace(".", "", $data_get['t_npopkpspt']) * $data_get['t_persenbphtb'] / 100 * $potongan_hibahwasiat_waris;
                    $dataspt["t_totalspt"] = $dataspt["t_totalspt"] - str_ireplace(".", "", $dataspt["t_pengurangan"]);
                    // var_dump($dataspt);exit();
                }

                $this->getTblVerifikasi()->savedataVerifikasiKasubid($kb, $t_pejabatverifikasi, $ar_pemda, $cektabelpersyaratan, $jml_syarat, $jml_syarat_verifikasi, $this->cekurl(), $dataspt);

                // simpan pemeriksaan jika ada pemeriksaan
                if ($data_get->t_pemeriksaanop == 1) {
                    $ex = $req->getPost();
                    $p_luastanah = str_ireplace(".", "", $ex->p_luastanah);
                    $p_njoptanah = str_ireplace(".", "", $ex->p_njoptanah);

                    $p_luasbangunan = str_ireplace(".", "", $ex->p_luasbangunan);
                    $p_njopbangunan = str_ireplace(".", "", $ex->p_njopbangunan);

                    $input = $this->getRequest();
                    $aphb_kali = $ex->p_t_tarif_pembagian_aphb_kali;
                    $aphb_bagi = $ex->p_t_tarif_pembagian_aphb_bagi;

                    if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                        $p_totalnjoptanah = $p_luastanah * $p_njoptanah;
                        $p_totalnjopbangunan = $p_luasbangunan * $p_njopbangunan;
                        $p_grandtotalnjop = $p_totalnjoptanah + $p_totalnjopbangunan;
                        $p_grandtotalnjop_aphb = $p_grandtotalnjop;
                    } else {
                        $aphb = $aphb_kali / $aphb_bagi;
                        $p_totalnjoptanah = $p_luastanah * $p_njoptanah;
                        $p_totalnjopbangunan = $p_luasbangunan * $p_njopbangunan;
                        $p_grandtotalnjop = $p_totalnjoptanah + $p_totalnjopbangunan;

                        $p_grandtotalnjop_aphb = ceil($p_grandtotalnjop * $aphb);
                    }

                    $p_nilaitransaksispt = str_ireplace(".", "", $ex->p_nilaitransaksispt);
                    if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                        if ($p_nilaitransaksispt >= $p_grandtotalnjop * 1) {
                            $p_npop = $p_nilaitransaksispt;
                        } else {
                            $p_npop = $p_grandtotalnjop;
                        }
                    } else {
                        if ($p_nilaitransaksispt >= $p_grandtotalnjop_aphb * 1) {
                            $p_npop = $p_nilaitransaksispt;
                        } else {
                            $p_npop = $p_grandtotalnjop_aphb;
                        }
                    }

                    if (($dataPendaftaran['t_potongan_waris_hibahwasiat'] == null) || ($dataPendaftaran['t_potongan_waris_hibahwasiat'] == 0)) {
                        $t_potongan_waris_hibahwasiat = 0;
                        $hitung_potonganwaris = 1;
                    } else {
                        $t_potongan_waris_hibahwasiat = $dataPendaftaran['t_potongan_waris_hibahwasiat'];
                        $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
                    }


                    $p_potonganspt = str_ireplace(".", "", $ex->p_potonganspt);
                    $p_npopkp = $p_npop - $p_potonganspt;
                    if ($p_npopkp <= 0) {
                        $p_npopkp = 0;
                        $p_totalspt = 0;
                    } else {
                        $p_persenbphtb = str_ireplace(".", "", $ex->p_persenbphtb);
                        $p_totalspt = ceil($p_npopkp * $p_persenbphtb / 100 * $hitung_potonganwaris);
                    }

                    $this->getTblVerifikasi()->savedatapemeriksaan($req->getPost(), $p_totalnjoptanah, $p_totalnjopbangunan, $p_grandtotalnjop, $p_grandtotalnjop_aphb, $p_potonganspt, $p_totalspt);
                } else {
                    $this->getTblVerifikasi()->savedatapemeriksaan_hapus($req->getPost());
                }

                //CEK PEMBAYARAN NIHIL =================
                $dataspt = $this->getTblSSPDBphtb()->getDataId_all($kb->t_idspt);
                if (!empty($dataspt['p_idpemeriksaan']) && $dataspt['p_idpemeriksaan'] != "") {
                    $totalspt = $dataspt['p_totalspt'];
                } else {
                    $totalspt = $dataspt['t_totalspt'];
                }

                // CEK APABILA SYARAT LENGKAP
                if (($cektabelpersyaratan == $jml_syarat) && ($cektabelpersyaratan == $jml_syarat_verifikasi)) {
                    if ((int)$totalspt <= 0) {
                        // jika pembayaran nihil maka otomatis terbayar (jika sudah valid)
                        $kb->t_idpembayaranspt = $dataspt['t_idpembayaranspt'];
                        $this->getTblVerifikasi()->savepembayarannihil($kb, $t_pejabatverifikasi);
                    }

                    //KIRIM EMAIL SSPD TELAH TERBIT
                    $setnotifemail = $this->getServiceLocator()->get("VerifikasiSPTTable")->getsetnotifemail();
                    if ($setnotifemail["s_status"] == 1) {
                        $datanotaris = $this->getServiceLocator()->get("VerifikasiSPTTable")->getDataNotarisIdUser($kb->t_idnotarisspt);
                        //PHP MAILER ===========================  
                        require 'public/PHPMailer/src/PHPMailer.php';
                        require 'public/PHPMailer/src/Exception.php';
                        require 'public/PHPMailer/src/SMTP.php';

                        $mail = new PHPMailer();
                        $mail->IsSMTP();

                        //GMAIL config
                        $mail->SMTPAuth   = true;                  // enable SMTP authentication
                        //$mail->SMTPSecure = "ssl";                 // sets the prefix to the server
                        //$mail->Host = "smtp.gmail.com";
                        //$mail->Port = 465 ;
                        $mail->Host = 'tls://smtp.gmail.com:587';

                        $mail->Username   = "bpprd.tanahbumbukab@gmail.com";  // GMAIL username
                        $mail->Password   = "b1p2p3r4d51";
                        $mail->SMTPDebug = 0;
                        //=== untuk keterangan debug server dan client = 2
                        //=== untuk client = 1
                        //=== unutk tidak muncul = 0

                        //End Gmail
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true
                            )
                        );

                        $mail->From       = "bpprd.tanahbumbukab@gmail.com";
                        $mail->FromName   = "e-Bphtb " . str_ireplace("Kabupaten ", "", $ar_pemda->s_namakabkot) . "";
                        $mail->Subject    = "SSPD TELAH TERBIT";

                        $mail->AddEmbeddedImage('./' . $ar_pemda->s_logo, 'logo_1');
                        $html = '
                        <table style="font-family:Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif; max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
                        <tbody>
                        <tr style="margin: 0px; padding: 0px;">
                            <td style="margin: 0px; padding: 0px;"></td>
                            <td bgcolor="#FFFFFF" style="margin: 0px auto; padding: 0px; display: block; max-width: 600px; clear: both;">
            
                                <div style="max-width: 600px; display: block; border-collapse: collapse; margin: 0px auto; padding: 30px 15px; border: 1px solid rgb(231, 231, 231);">
                                    <table style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif; max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
                                        <tbody>
                                        <tr>
                                                <td style="padding: 15px 20px 10px;" bgcolor="#2069b3">
                                        <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; color: rgb(255, 255, 255);" border="0">
                                            <tbody><tr>
                                                <td width="280">
                                                    <img src="cid:logo_1" style="border: 0px; min-height: auto; width: 41px; outline: 0px;">
                                                </td>
                                                <td width="280" align="right" style="font-size: 20px;">
                                                    SSPD TELAH TERBIT
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                            </tr>
                                        <tr style="margin: 0px; padding: 0px;">
                                            <td style="margin: 0px; padding: 0px;">
                                                <h4 style="font-size: 20px; padding: 30px 0px 10px; font-weight: bold; color: rgb(78, 78, 78);">
                                                Pendaftaran No. ' . $dataspt['t_kohirspt'] . ' Periode ' . $dataspt['t_periodespt'] . '
            </h4>
                                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">Hi, <b>' . $datanotaris['s_namanotaris'] . '</b>.</p>
                                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">
                                                    Kami informasikan untuk berkas dengan informasi dibawah ini :
                                                </p>
                                                <div style="text-align: left; margin: 20px; padding: 0px;" align="center">
                                                    <table width="100%" border="0" style=" border-collapse: collapse;">
                                                        <tr>
                                                            <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                            No. Daftar</td>
                                                            <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                            <td style="padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['t_kohirspt'] . '</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                            Periode</td>
                                                            <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                            <td style=" padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['t_periodespt'] . '</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                            Nama WP</td>
                                                            <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                            <td style=" padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['t_namawppembeli'] . '</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                            Jenis Transaksi</td>
                                                            <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                            <td style="padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['s_namajenistransaksi'] . '</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 150px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">
                                                            NOP</td>
                                                            <td style="width: 5px; padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">:</td>
                                                            <td style="padding: 5px; font-weight: bold; border-bottom: 1px solid rgb(217, 217, 217);">' . $dataspt['t_nopbphtbsppt'] . '</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" style="width: 150px; padding: 5px; font-weight: bold; color: red; border-bottom: 1px solid rgb(217, 217, 217); text-align: center;">
                                                            SSPD telah terbit, silahkan lakukan pembayaran apabila terdapat tagihan.</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0px 0px 20px; padding: 0px;">
                                                    Mohon untuk mengecek berkas tersebut. Terima Kasih.
                                                </p>
                                                <div style="text-align: center; margin: 20px; padding: 0px;" align="center">
                                                    <a href="' . $this->cekurl() . '/pendataan_sspd" style="color: rgb(255, 255, 255); text-decoration: none; display: inline-block; vertical-align: middle; line-height: 20px; font-size: 13px; font-weight: 600; text-align: center; white-space: nowrap; border-radius: 3px; background-color: #2069b3; margin: 0px; padding: 9px 14px; border: 1px solid #2e5cb8" target="_blank" data-saferedirecturl="' . $this->cekurl() . '/pendataan_sspd">
                                                        LINK E-BPHTB
                                                    </a>
                                                </div>
                                                <p style="font-weight: normal; font-size: 14px; line-height: 1.6; border-top: 3px solid rgb(208, 208, 208); margin: 40px 0px 0px; padding: 10px 0px 0px;">
                                                    <small style="color: rgb(153, 153, 153); margin: 0px; padding: 0px;">
                                                        Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                                                    </small>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </div>
                            </td>
                            <td style="margin: 0px; padding: 0px;"></td>
                        </tr>
                    </tbody></table>
                    <table style="max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px 0px 60px; padding: 0px; clear: both;" bgcolor="transparent">
                <tbody>
                    <tr style="margin: 0px; padding: 0px;">
                        <tr style="margin: 0px; padding: 0px;">
                            <td style="margin: 0px; padding: 0px;"></td>
                            <td style="margin: 0px auto; padding: 0px; display: block; max-width: 600px; clear: both;">
                                <div style="max-width: 600px; display: block; border-collapse: collapse; background-color: rgb(247, 247, 247); margin: 0px auto; padding: 20px 15px; border-color: rgb(231, 231, 231); border-style: solid; border-width: 0px 1px 1px;">
                                <table width="100%" style="max-width: 100%; border-collapse: collapse; border-spacing: 0px; width: 100%; background-color: transparent; margin: 0px; padding: 0px;" bgcolor="transparent">
                                    <tbody style="margin: 0px; padding: 0px;">
                                        <tr style="margin: 0px; padding: 0px;">
                                            <td valign="middle" style="margin: 0px; padding: 0px; width: 7%;">
                                                <img src="cid:logo_1" style="border: 0px; min-height: auto; width: 41px; outline: 0px;">
                                            </td>
                                            <td valign="middle" style="margin: 0px; padding: 0px; width: 53%;">
                                                <p style="color: rgb(145, 144, 142); font-size: 10px; line-height: 150%; font-weight: normal; margin: 0px; padding: 0px;">
                                                    Jika butuh bantuan, gunakan halaman <a href="http://bpprd.tanahbumbukab.go.id/?page_id=344" style="color: #2069b3; text-decoration: none; margin: 0px; padding: 0px;" target="_blank" data-saferedirecturl="http://bpprd.tanahbumbukab.go.id/?page_id=344">Kontak Kami</a>.
                                                    <br style="margin: 0px; padding: 0px;">
                                                    © 2019, ' . $ar_pemda->s_namasingkatinstansi . ' ' . $ar_pemda->s_namakabkot . '
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    <td style="margin: 0px; padding: 0px;"></td>
                </tr>
            </tbody>
            </table>';
                        $mail->MsgHTML($html);

                        $mail->AddReplyTo("bpprd.tanahbumbukab@gmail.com", "BPPRD MINAHASA SELATAN"); //optional
                        $mail->AddAddress($datanotaris['s_email'], $datanotaris['s_namanotaris']);
                        $mail->IsHTML(true); // send as HTML

                        if (!$mail->Send()) { //to see if we return a message or a value bolean
                            echo "Mailer Error: " . $mail->ErrorInfo;
                        };
                        //PHP MAILER ===========================
                    }
                }
                //CEK PEMBAYARAN NIHIL =================

                return $this->redirect()->toRoute('verifikasi_spt');
            }
        }

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();

        $view = new ViewModel(array(
            'frm' => $frm,
            'datajenistransaksi' => $datajenistransaksi,
            'datanotaris' => $ar_notaris
        ));
        $data = array(
            'menu_verifikasi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function editAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $req = $this->getRequest();
        if ($req->isGet()) {
            //get data t_pembayaranspt => object
            $data = $this->getTblPembayaran()->getDataId($req->getQuery()->get('t_idpembayaranspt'));

            //get data t_pembayaranspt => array
            $dataArray = $this->getTblPembayaran()->getDataIdArray($req->getQuery()->get('t_idpembayaranspt'));

            $data->t_kohirpembayaran = $data->t_kohirspt;
            $data->t_persyaratanverifikasi = $data->t_verifikasispt;
            $frm = new \Bphtb\Form\Verifikasi\VerifikasiSPTFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $string, null, $this->populateCheckBoxverifikasi($data->t_idjenistransaksi));
            // cek apakah pernah diperiksa lapangan atau belum
            $data1 = $this->getTblPembayaran()->getDataPemeriksaanId($req->getQuery()->get('t_idpembayaranspt'));
            $data->p_idpemeriksaan = $data1['p_idpemeriksaan'];
            $data->p_luastanah = number_format($data1['p_luastanah'], 0, ',', '.');
            $data->p_luasbangunan = number_format($data1['p_luasbangunan'], 0, ',', '.');
            $data->p_njoptanah = number_format($data1['p_njoptanah'], 0, ',', '.');
            $data->p_njopbangunan = number_format($data1['p_njopbangunan'], 0, ',', '.');
            $data->p_totalnjoptanah = number_format($data1['p_totalnjoptanah'], 0, ',', '.');
            $data->p_totalnjopbangunan = number_format($data1['p_totalnjopbangunan'], 0, ',', '.');
            $data->p_grandtotalnjop = number_format($data1['p_grandtotalnjop'], 0, ',', '.');
            $data->p_nilaitransaksispt = number_format($data1['p_nilaitransaksispt'], 0, ',', '.');
            $data->p_potonganspt = number_format($data1['p_potonganspt'], 0, ',', '.');
            $data->p_totalspt = number_format($data1['p_totalspt'], 0, ',', '.');

            $aphb_kali = $data1['t_tarif_pembagian_aphb_kali'];
            $aphb_bagi = $data1['t_tarif_pembagian_aphb_bagi'];
            if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                if ($data1['p_grandtotalnjop'] > $data1['p_nilaitransaksispt']) {
                    $p_npop = $data1['p_grandtotalnjop'];
                } else {
                    $p_npop = $data1['p_nilaitransaksispt'];
                }
            } else {
                if ($data1['p_grandtotalnjop_aphb'] > $data1['p_nilaitransaksispt']) {
                    $p_npop = $data1['p_grandtotalnjop_aphb'];
                } else {
                    $p_npop = $data1['p_nilaitransaksispt'];
                }
            }


            $data->p_npop = number_format($p_npop, 0, ',', '.');
            $p_npopkp = $p_npop - $data1['p_potonganspt'];
            if ($p_npopkp < 0) {
                $data->p_npopkp = 0;
            } else {
                $data->p_npopkp = number_format($p_npopkp, 0, ',', '.');
            }
            $data->p_persenbphtb = 5;
            if (!empty($data1)) {
                $data->t_pemeriksaanop = 1;
                $t_pemeriksaanop = 1;
            } else {
                $data->t_pemeriksaanop = 0;
                $t_pemeriksaanop = 0;
            }
            $data->p_namajenistransaksi = $data->s_namajenistransaksi;
            $data->p_namahaktanah = $data->s_namahaktanah;

            // cari harga acuan pada tabel master s_acuan
            $nop = explode('.', $data->t_nopbphtbsppt);
            $s_kd_propinsi = $nop[0];
            $s_kd_dati2 = $nop[1];
            $s_kd_kecamatan = $nop[2];
            $s_kd_kelurahan = $nop[3];
            $s_kd_blok = $nop[4];
            $datahargaacuan = $this->getTblVerifikasi()->getHargaAcuan($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok);

            $data->s_permetertanah = number_format($datahargaacuan['s_permetertanah'], 0, ',', '.');
            $njop_tanah = ($data->t_nilaitransaksispt - $data->t_totalnjopbangunan) / $data->t_luastanah;
            $analisis_a = 0;
            if ($datahargaacuan) {
                $analisis_a = $njop_tanah / $datahargaacuan['s_permetertanah'] * 100;
            }

            if ((int)$analisis_a < 0) {
                $harga_a = 0;
                $hasil_a = $this->getTblVerifikasi()->getPresentase($harga_a);
            } elseif ((int)$analisis_a > 100) {
                $harga_a = 100;
                $hasil_a = $this->getTblVerifikasi()->getPresentase($harga_a);
            } else {
                $hasil_a = $this->getTblVerifikasi()->getPresentase($analisis_a);
            }
            $warna = $hasil_a['warna'];

            $html = "<div class='col-md-7'>";

            if ($analisis_a == 0) {
            } else {
                $html .= "<left>NJOP Tanah (Transaksi) / Harga Acuan x 100 % = <span class='badge' style='background-color:$warna;'>" . round($analisis_a, 2) . " % </span> <strong>" . $hasil_a['s_keterangan'] . "</strong> </left>";
            }
            $html .= "</div>";
            $data->analisishargaacuan = $html;

            $idpembayaran = (int)$req->getQuery()->get('t_idpembayaranspt');
            $datahistorynjoptanah = $this->getTblVerifikasi()->getHargaHistoryNJOPTanah($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $idpembayaran);

            $njoptanahtransaksi = (int)$datahistorynjoptanah['njoptanahtransaksi'];
            $njoptanah = (int)$datahistorynjoptanah['njoptanah'];
            if ($njoptanahtransaksi >= $njoptanah) {
                $data->historynjoptanah = $njoptanahtransaksi;
            } else {
                $data->historynjoptanah = $njoptanah;
            }

            $analisis_b = 0;
            if ($data->historynjoptanah) {
                $analisis_b = $njop_tanah / $data->historynjoptanah * 100;
            }

            if ((int)$analisis_b < 0) {
                $harga_b = 0;
                $hasil_b = $this->getTblVerifikasi()->getPresentase($harga_b);
            } elseif ((int)$analisis_b > 100) {
                $harga_b = 100;
                $hasil_b = $this->getTblVerifikasi()->getPresentase($harga_b);
            } else {
                $hasil_b = $this->getTblVerifikasi()->getPresentase($analisis_b);
            }

            $warna_b = $hasil_b['warna'];

            if ($analisis_b == '0') {
                $html1 = "";
            } else {
                $html1 = "<div class='col-md-7'>";
                $html1 .= "<left>NJOP Tanah (Transaksi) / Harga History x 100 % = <span class='badge' style='background-color:$warna_b;'>" . round($analisis_b, 3) . " % </span> <strong>" . $hasil_b['s_keterangan'] . "</strong> </left>";
                $html1 .= "</div>";
            }

            $html1 .= "</div>";
            $data->analisisharganjoptanah = $html1;
            $data->historynjoptanah = number_format($data->historynjoptanah, 0, ',', '.');

            //$data->t_tglverifikasispt = date('d-m-Y', strtotime($data->t_tglverifikasispt));

            $data->t_tglverifikasispt = date('d-m-Y', strtotime($dataArray['t_tglverifikasispt_kasubid']));

            $frm->bind($data);
            $frm->get("t_persyaratanverifikasi")->setValue(\Zend\Json\Json::decode($data->t_persyaratanverifikasi));
        }

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();

        $datahistorynjoptanah2 = $this->getTblVerifikasi()->getHargaHistoryNJOPTanahpilih2($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $data->t_idspt);

        $view = new ViewModel(array(
            'frm' => $frm,
            't_pemeriksaanop' => $t_pemeriksaanop,
            'datajenistransaksi' => $datajenistransaksi,
            'datanotaris' => $ar_notaris,
            'datahistorynjoptanah2' => $datahistorynjoptanah2,
            'data' => $data,
        ));

        $data = array(
            'menu_verifikasi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function viewdataAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int)$req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_verifikasi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    //untuk menghapus data t_pemabayaranspt
    public function hapusAction()
    {
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ret = $this->getTblVerifikasi()->batalVerifikasi($this->params('page'));
            $this->getTblVerifikasi()->hapusDataPemeriksaan($this->params('page'));
            $res->setContent(\Zend\Json\Json::encode($ret));
        }
        return $res;
    }

    //untuk update t_pembayaranspt, mengahpus validasi kasubid dan kabid
    public function hapusVerifikasiKasubidAction()
    {
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ret = $this->getTblVerifikasi()->batalverifikasikasubid($this->params('page'));
            $this->getTblVerifikasi()->hapusDataPemeriksaan($this->params('page'));
            $res->setContent(\Zend\Json\Json::encode($ret));
        }
        return $res;
    }

    public function dataGridPendataanBphtbAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'fr_pendaftaran_v5';

        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
        $aColumns = array('t_idspt', 't_kohirspt', 't_tglprosesspt', 't_nopbphtbsppt', 't_namawppembeli', 't_namawppenjual', 's_namanotaris', 't_idjenistransaksi', 't_idnotarisspt', 's_namajenistransaksi');

        $panggildata = $this->getServiceLocator()->get("VerifikasiSPTTable");
        $rResult = $panggildata->semuadataselesaiverifikasiberkas($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGridPendataanBphtb2Action()
    {
        $res = $this->getResponse();
        $page = $this->getRequest()->getPost('page') ? $this->getRequest()->getPost('page') : 1;
        $rp = $this->getRequest()->getPost('rp') ? $this->getRequest()->getPost('rp') : 10;
        $sortname = $this->getRequest()->getPost('sortname') ? $this->getRequest()->getPost('sortname') : 't_idspt';
        $sortorder = $this->getRequest()->getPost('sortorder') ? $this->getRequest()->getPost('sortorder') : 'desc';
        $query = $this->getRequest()->getPost('query') ? $this->getRequest()->getPost('query') : false;
        $qtype = $this->getRequest()->getPost('qtype') ? $this->getRequest()->getPost('qtype') : false;

        //============= Model\Pendataan\SSPDBphtbTable
        $count = $this->getTblSSPDBphtb()->getGridCountBlmVerifikasi($query, $qtype);
        $start = (($page - 1) * $rp);
        $res->getHeaders()->addheaders(array(
            'Content-type' => 'text/xml'
        ));
        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>" . $page . "</page>";
        $s .= "<total>" . $count . "</total>";
        $s .= "<records>" . $count . "</records>";

        //=========== Model\Pendataan\SSPDBphtbTable
        $data = $this->getTblSSPDBphtb()->databelumverifikasiBphtb($sortname, $sortorder, $query, $qtype, $start, $rp); //getGridDataBlmVerifikasi
        foreach ($data as $row) {
            $s .= "<row id='" . $row['t_idspt'] . "'>";
            $s .= "<cell>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . "</cell>";
            $s .= "<cell>" . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . "</cell>";
            $s .= "<cell>" . $row['t_nopbphtbsppt'] . "</cell>";
            //$s .= "<cell>" . $row['t_namawppembeli'] . "</cell>";
            //$s .= "<cell>" . $row['t_namawppenjual'] . "</cell>";
            $s .= "<cell>" . addslashes(str_replace('\', &amp;', ' ', htmlspecialchars($row['t_namawppembeli']))) . "</cell>";
            $s .= "<cell>" . addslashes(str_replace('\', &amp;', ' ', htmlspecialchars($row['t_namawppenjual']))) . "</cell>";
            $s .= "<cell><![CDATA[<a href='#' class='btn btn-xs btn-warning' onclick='pilihPendataanSspdBphtb(" . $row['t_idspt'] . ");return false;' >PILIH</a>]]></cell>";
            $s .= "</row>";
        }
        $s .= "</rows>";
        $res->setContent($s);
        return $res;
    }

    public function inputvalidasikeduaAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int)$req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            //$data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            //$data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_verifikasi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function simpanvalidasikeduaAction()
    {
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $input = $this->getRequest();
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $t_idspt = $this->getRequest()->getPost('t_idspt');
        $t_iddetailsptbphtb = str_replace("'", "\'", $this->getRequest()->getPost('t_iddetailsptbphtb'));
        $t_idpembayaranspt = str_replace("'", "\'", $this->getRequest()->getPost('t_idpembayaranspt'));
        //var_dump($t_idspt); exit();

        $panggildata = $this->getServiceLocator()->get("SPTTable");
        $data = $panggildata->getSptid($t_idspt);
        $datadetail = $this->getTblSSPDBphtb()->getSemuaData($t_iddetailsptbphtb);
        $dataverifikasi = $this->getTblVerifikasi()->getSptid($t_idpembayaranspt);
        //var_dump($dataverifikasi);exit();

        //=========================== hitung lagi data yang masuk
        if (($session['s_namauserrole'] == "Administrator") || ($session['s_namauserrole'] == "Pegawai") || ($session['s_tipe_pejabat'] == 1)) {

            $idpendaftar = $session['s_iduser'];
        } elseif ($session['s_tipe_pejabat'] == 2) {

            $idpendaftar = $session['s_iduser'];
        }



        sleep(3);
        $t_luastanah = str_ireplace(".", "", $input->getPost('p_luastanah'));
        $t_njoptanah = str_ireplace(".", "", $input->getPost('p_njoptanah'));

        $t_luasbangunan = str_ireplace(".", "", $input->getPost('p_luasbangunan'));
        $t_njopbangunan = str_ireplace(".", "", $input->getPost('p_njopbangunan'));

        $aphb_kali = $datadetail->t_tarif_pembagian_aphb_kali;
        $aphb_bagi = $datadetail->t_tarif_pembagian_aphb_bagi;

        $t_nilaitransaksispt = str_ireplace(".", "", $input->getPost('p_nilaitransaksispt'));

        if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
            $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
            $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
            $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

            $t_grandtotalnjop_aphb = $t_grandtotalnjop;
        } else {
            $aphb = $aphb_kali / $aphb_bagi;

            $t_totalnjoptanah = $t_luastanah * $t_njoptanah;
            $t_totalnjopbangunan = $t_luasbangunan * $t_njopbangunan;
            $t_grandtotalnjop = $t_totalnjoptanah + $t_totalnjopbangunan;

            $t_grandtotalnjop_hitung = $t_grandtotalnjop * $aphb;
            $t_grandtotalnjop_aphb = ceil($t_grandtotalnjop_hitung);
        }

        if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {

            if ($t_grandtotalnjop > $t_nilaitransaksispt) {
                $t_npopspt = $t_grandtotalnjop;
            } else {
                $t_npopspt = $t_nilaitransaksispt;
            }
        } else {
            if ($t_grandtotalnjop_aphb > $t_nilaitransaksispt) {
                $t_npopspt = $t_grandtotalnjop_aphb;
            } else {
                $t_npopspt = $t_nilaitransaksispt;
            }
        }



        if (!empty($datadetail->p_idpemeriksaan)) {
            $t_potonganspt = $datadetail->p_potonganspt;
        } else {
            $t_potonganspt = $datadetail->t_potonganspt;
        }

        if (($datadetail->t_potongan_waris_hibahwasiat == null) || ($datadetail->t_potongan_waris_hibahwasiat == 0)) {
            $t_potongan_waris_hibahwasiat = 0;
            $hitung_potonganwaris = 1;
        } else {
            $t_potongan_waris_hibahwasiat = $datadetail->t_potongan_waris_hibahwasiat;
            $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
        }


        $t_persenbphtb = $datadetail->t_persenbphtb; //str_ireplace(".", "", $ex->t_persenbphtb);
        if ($t_potonganspt == '0') {
            $t_potonganspt = 0;
            $t_npopkpspt = $t_npopspt;
            $t_totalspt = ceil($t_npopkpspt * $t_persenbphtb / 100 * $hitung_potonganwaris);
        } else {

            $npop = $t_npopspt;
            $npopkp = $npop - $t_potonganspt;
            if ($npopkp <= 0) {
                $t_npopkpspt = 0;
                $t_totalspt = 0;
            } else {
                $t_npopkpspt = $npopkp;
                $t_totalspt = ceil($t_npopkpspt * $t_persenbphtb / 100 * $hitung_potonganwaris);
            }
        }

        //=========================== end hitung lagi data yang masuk


        // nilai pembayaran sebelumnya
        $nilaibayarsebelumnya = $datadetail->t_nilaipembayaranspt;

        if ($t_totalspt > $nilaibayarsebelumnya) {
            $fr_statusvalidasi = 1; //============= iki kurang bayar
        } else {
            $fr_statusvalidasi = 2; //============= iki lebih bayar
        }

        //========= simpan ke tabel t_spt
        $dataspt = $panggildata->savedatavalidasikedua($data, $idpendaftar, $input, $t_potonganspt, $t_totalspt, $aphb_kali, $aphb_bagi, $fr_statusvalidasi, $t_potongan_waris_hibahwasiat);

        $this->getTblSSPDBphtb()->savedatadetail_validasikedua($datadetail, $dataspt->t_idspt, $input, $session, $t_luastanah, $t_njoptanah, $t_luasbangunan, $t_njopbangunan, $t_totalnjoptanah, $t_totalnjopbangunan, $t_grandtotalnjop, $t_grandtotalnjop_aphb);
        //$this->getTblSSPDBphtb()->savedatadetailbpn($datadetail, $dataspt->t_idspt, $input);

        $datamax = $this->getTblVerifikasi()->getmaxkohir();
        $this->getTblVerifikasi()->updatenosspd_validasikedua($dataspt->t_idspt, $datamax['t_kohirketetapanspt']);

        //if ($t_totalspt > 0 && $t_totalspt > $nilaibayarsebelumnya) {
        // jika ada nominal pembayaran prosesnya : daftar dan validasi


        $ar_pemda = $this->getPemda()->getdata();
        $this->getTblVerifikasi()->savedataverifikasi_validasikedua($dataverifikasi, $dataspt->t_idspt, $datamax['t_kohirketetapanspt'], str_replace('-', '', date('Y-m-d')), $ar_pemda); //$dataspt->t_kohirspt
        //} else {
        // jika nihil maka prosesnya : daftar, validasi dan verifikasi otomatis
        //    $this->getTblVerifikasi()->savedataverbayarbpn($dataverifikasi, $dataspt->t_idspt, $string);
        //}


        return $this->redirect()->toRoute('verifikasi_spt');
    }

    public function pilihPendataanSspdBphtbAction()
    {
        $frm = new SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $data_get = $req->getPost();
                $ex->exchangeArray($frm->getData());
                $data = $this->getTblSSPDBphtb()->getPendataanSspdBphtb($ex);
                // var_dump($data);exit();

                $aphb_kali = $data['t_tarif_pembagian_aphb_kali'];
                $aphb_bagi = $data['t_tarif_pembagian_aphb_bagi'];
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    if ($data['t_nilaitransaksispt'] >= $data['t_grandtotalnjop']) {
                        $data['npop'] = $data['t_nilaitransaksispt'];
                    } else {
                        $data['npop'] = $data['t_grandtotalnjop'];
                    }
                } else {
                    if ($data['t_nilaitransaksispt'] >= $data['t_grandtotalnjop_aphb']) {
                        $data['npop'] = $data['t_nilaitransaksispt'];
                    } else {
                        $data['npop'] = $data['t_grandtotalnjop_aphb'];
                    }
                }

                $data['t_luastanah'] = str_ireplace('.', '', $data['t_luastanah']) / 100;
                $data['t_luasbangunan'] = str_ireplace('.', '', $data['t_luasbangunan']) / 100;

                // cari harga acuan pada tabel master s_acuan
                $nop = explode('.', $data['t_nopbphtbsppt']);
                $s_kd_propinsi = $nop[0];
                $s_kd_dati2 = $nop[1];
                $s_kd_kecamatan = $nop[2];
                $s_kd_kelurahan = $nop[3];
                $s_kd_blok = $nop[4];
                $datahargaacuan = $this->getTblVerifikasi()->getHargaAcuan($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok);
                $data['s_permetertanah'] = number_format($datahargaacuan['s_permetertanah'], 0, ',', '.');
                $njop_tanah = round(($data['t_nilaitransaksispt'] - $data['t_totalnjopbangunan']) / $data['t_luastanah']);
                // $njop_tanah = round(($data['npop'] - $data['t_totalnjopbangunan']) / $data['t_luastanah']);

                $analisis_a = 0;
                if ($datahargaacuan) {
                    $analisis_a = $njop_tanah / $datahargaacuan['s_permetertanah'] * 100;
                }

                if ((int)$analisis_a < 0) {
                    $harga_a = 0;
                    $hasil_a = $this->getTblVerifikasi()->getPresentase($harga_a);
                } elseif ((int)$analisis_a > 100) {
                    $harga_a = 100;
                    $hasil_a = $this->getTblVerifikasi()->getPresentase($harga_a);
                } else {
                    $hasil_a = $this->getTblVerifikasi()->getPresentase($analisis_a);
                }
                $warna = $hasil_a['warna'];

                $html = "<div class='col-md-7'>";

                if ((int)$analisis_a == 0) {
                } else {
                    $html .= "<left>NJOP Tanah (Transaksi) / Harga Acuan x 100 % = <span class='badge' style='background-color:$warna;'>" . round($analisis_a, 2) . " % </span> <strong>" . $hasil_a['s_keterangan'] . "</strong> </left>  ";
                }
                $html .= "</div>";
                $data['analisishargaacuan'] = $html;

                $idspt = (int)$data['t_idspt'];
                $datahistorynjoptanah = $this->getTblVerifikasi()->getHargaHistoryNJOPTanahpilih($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $idspt);
                //var_dump($datahistorynjoptanah);exit();
                $njoptanahtransaksi = (int)$datahistorynjoptanah['njoptanahtransaksi'];
                $njoptanah = (int)$datahistorynjoptanah['njoptanah'];

                if ($njoptanahtransaksi >= $njoptanah) {
                    $data['historynjoptanah'] = $njoptanahtransaksi;
                } else {
                    $data['historynjoptanah'] = $njoptanah;
                }

                $analisis_b = 0;
                if ($data['historynjoptanah']) {
                    $analisis_b = $njop_tanah / $data['historynjoptanah'] * 100;
                }
                if ((int)$analisis_b < 0) {
                    $harga_b = 0;
                    $hasil_b = $this->getTblVerifikasi()->getPresentase($harga_b);
                } elseif ((int)$analisis_b > 100) {
                    $harga_b = 100;
                    $hasil_b = $this->getTblVerifikasi()->getPresentase($harga_b);
                } else {
                    $hasil_b = $this->getTblVerifikasi()->getPresentase($analisis_b);
                }

                $warna_b = $hasil_b['warna'];
                $html1 = "<div class='col-md-7'>";
                if ((int)$analisis_b == 0) {
                    $html1 .= "";
                } else {
                    $html1 .= "<left>NJOP Tanah (Transaksi) / Harga History x 100 % = <span class='badge' style='background-color:$warna_b;'>" . round($analisis_b, 2) . " % </span> <strong>" . $hasil_b['s_keterangan'] . "</strong> </left>";
                }

                $html1 .= "</div>";
                $data['analisisharganjoptanah'] = $html1;
                $data['historynjoptanah'] = number_format($data['historynjoptanah'], 0, ',', '.');

                $data['t_luastanah'] = number_format($data['t_luastanah'], 0, ',', '.');
                $data['t_njoptanah'] = number_format($data['t_njoptanah'], 0, ',', '.');
                $data['t_luasbangunan'] = number_format($data['t_luasbangunan'], 0, ',', '.');
                $data['t_njopbangunan'] = number_format($data['t_njopbangunan'], 0, ',', '.');
                $data['t_totalnjoptanah'] = number_format($data['t_totalnjoptanah'], 0, ',', '.');
                $data['t_totalnjopbangunan'] = number_format($data['t_totalnjopbangunan'], 0, ',', '.');
                $data['t_grandtotalnjop'] = number_format($data['t_grandtotalnjop'], 0, ',', '.');
                $data['t_nilaitransaksispt'] = number_format($data['t_nilaitransaksispt'], 0, ',', '.');
                $data['npop'] = number_format($data['npop'], 0, ',', '.');
                $data['t_potonganspt'] = number_format($data['t_potonganspt'], 0, ',', '.');

                $data['t_tarif_pembagian_aphb_kali'] = $data['t_tarif_pembagian_aphb_kali'];
                $data['t_tarif_pembagian_aphb_bagi'] = $data['t_tarif_pembagian_aphb_bagi'];
                $data['t_grandtotalnjop_aphb'] = number_format($data['t_grandtotalnjop_aphb'], 0, ',', '.');
                $data['p_grandtotalnjop_aphb'] = number_format($data['p_grandtotalnjop_aphb'], 0, ',', '.');
                $data['t_persenbphtb'] = $data['t_persenbphtb'];
                $data['t_potongan_waris_hibahwasiat'] = $data['t_potongan_waris_hibahwasiat'];
                $data['t_idjenistransaksi'] = $data['t_idjenistransaksi'];

                $npopkp = (str_replace('.', '', $data['npop']) - str_replace('.', '', $data['t_potonganspt']));
                if ($npopkp <= 0) {
                    $nilai_npopkp = 0;
                } else {
                    $nilai_npopkp = $npopkp;
                }
                $data['npopkp'] = number_format($nilai_npopkp, 0, ',', '.');

                //$npopkp = $data['t_totalspt'] / 5 * 100;
                //$data['npopkp'] = number_format($npopkp, 0, ',', '.');

                // OPSI TAMBAHAN PERSETUJUAN POTONGAN HIBAH_WASIAT
                $html_opsi_potongan_waris_hibahwasiat = '<option value="">Pilih</option>';
                if ($data["t_idjenistransaksi"] == 4 || $data["t_idjenistransaksi"] == 5) {
                    if ($data_get["opsi_potongan_waris_hibahwasiat"] == "") {
                        if ($data["t_potongan_waris_hibahwasiat"] == 0) {
                            $select1 = "selected";
                        } elseif ($data["t_potongan_waris_hibahwasiat"] == 50) {
                            $select2 = "selected";
                        }
                    } elseif ($data_get["opsi_potongan_waris_hibahwasiat"] == 1) {
                        $select1 = "selected";
                        $data['t_potongan_waris_hibahwasiat'] = 0;
                        $data['t_totalspt'] = str_ireplace('.', '', $data['npopkp']) * $data['t_persenbphtb'] / 100 * 1;
                    } elseif ($data_get["opsi_potongan_waris_hibahwasiat"] == 2) {
                        $select2 = "selected";
                        $data['t_potongan_waris_hibahwasiat'] = 50;
                        $data['t_totalspt'] = str_ireplace('.', '', $data['npopkp']) * $data['t_persenbphtb'] / 100 * 50 / 100;
                    }
                    $html_opsi_potongan_waris_hibahwasiat = '<option value="1" ' . $select1 . '>Tidak</option><option value="2" ' . $select2 . '>Ya</option>';
                }
                $data['html_opsi_potongan_waris_hibahwasiat'] = $html_opsi_potongan_waris_hibahwasiat;

                $data['t_bphtbterhutang'] = number_format($data['t_totalspt'], 0, ',', '.');

                $pengurangan = $this->getTblSSPDBphtb()->getPengurangan($ex->t_idspt);
                $data['t_pengurangan'] = number_format($pengurangan['t_pengurangan'], 0, ',', '.');
                $data['t_totalspt'] = number_format(str_ireplace(".", "", $data['t_bphtbterhutang']) + $pengurangan['t_pengurangan'], 0, ',', '.');

                // Persyaratan Pendaftaran dan Validasi
                $data['t_persyaratan'] = $this->populatePersyaratanId($data['t_idjenistransaksi'], $data['t_idspt']);
                $data['t_persyaratanverifikasi'] = $this->populatePersyaratanVerifikasi($data['t_idjenistransaksi']);

                $panggildata = $this->getServiceLocator()->get("PersyaratanTable");
                $datafileupload = $panggildata->syaratfileupload($data['t_idjenistransaksi'], $data['t_idspt']);

                $no = 1;
                $namasyarat = '';
                $nomer = 1;
                $fileuploadnya = '';
                foreach ($datafileupload as $v) {

                    if ($namasyarat <> $v['s_namapersyaratan']) {

                        if ($namasyarat <> '') {
                        }
                        //$fileuploadnya .= $no;

                        $fileuploadnya .= $no . ". " . $v['s_namapersyaratan'] . "<br>";
                        /*$fileuploadnya .= ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$this->url().'/uploadjenisyarat?t_idspt='.$datasspd['t_idspt'].'&syarat='.$v['s_idpersyaratan'].'" class="btn bg-purple btn-sm btn-flat">
                                    <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Upload File
                                </a><br>';
                         */
                        $namasyarat = $v['s_namapersyaratan'];
                        $clast_rek_name = $v['s_namapersyaratan'];
                        //$no = 1;
                        $no++;
                        $nomer = 1;
                    }

                    if (!empty($v['nama_file'])) {
                        //$fileuploadnya .= $v['nama_file']."<br>";
                        $fileuploadnya .= '<a style="display:none;" data-ng-href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '" title="' . $v['nama_file'] . '" download="' . $v['nama_file'] . '" data-gallery="" href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '"><img data-ng-src="' . $this->cekurl() . '/' . $v['letak_file'] . 'thumbnail/' . $v['nama_file'] . '" alt="" src="' . $this->cekurl() . '/' . $v['letak_file'] . 'thumbnail/' . $v['nama_file'] . '"></a>';


                        $extension = pathinfo($v['nama_file'], PATHINFO_EXTENSION);
                        if ($extension == "pdf") {
                            $fileuploadnya .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '" title="' . $v['nama_file'] . '" target="_blank">' . $nomer . '. ' . $v['nama_file'] . '</a><br>';
                        } else {
                            $fileuploadnya .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-ng-switch-when="true" data-ng-href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '" title="' . $v['nama_file'] . '" download="' . $v['nama_file'] . '" data-gallery="" class="ng-binding ng-scope" href="' . $this->cekurl() . '/' . $v['letak_file'] . '' . $v['nama_file'] . '">' . $nomer . '. ' . $v['nama_file'] . '</a><br>';
                        }
                    }

                    $nomer++;
                }

                $data['fileuploadcoy'] = $fileuploadnya;

                $datahistorynjoptanah2 = $this->getTblVerifikasi()->getHargaHistoryNJOPTanahpilih2($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $idspt);
                $html_history = '';
                $html_history .= '<table class="table table-bordered table-striped">
                        <thead>
                            <tr class="info">
                                <th style="text-align: center;">NOP</th>
                                <th style="text-align: center;">LOKASI OP</th>
                                <th style="text-align: center;">NILAI TRANSAKSI</th>
                                <th style="text-align: center;">NJOP TANAH TRANSAKSI</th>
                            </tr>
                        </thead>
                        <tbody>';
                foreach ($datahistorynjoptanah2 as $d) {
                    //if($d['njoptanah'] < 0) $d['njoptanah']=0;
                    if ($d['njoptanahtransaksi'] > $d['njoptanah']) {
                        $njoptanah_history = (int) $d['njoptanahtransaksi'];
                    } else {
                        $njoptanah_history = (int) $d['njoptanah'];
                    }
                    $html_history .= '<tr><td>' . $d['t_nopbphtbsppt'] . '</td>'
                        . '<td>' . $d['t_alamatop'] . ' Rt ' . $d['t_rtop'] . ' Rw ' . $d['t_rwop'] . '<br>Kel. ' . $d['t_kelurahanop'] . ' Kec. ' . $d['t_kecamatanop'] . ' Kab ' . $d['t_kabupatenop'] . '</td>'
                        . '<td>' . number_format($d['t_nilaitransaksispt'], 0, ',', '.') . '</td>'
                        . '<td>' . number_format($njoptanah_history, 0, ',', '.') . '</td>'
                        . '</tr>';
                }
                $html_history .= '</tbody>
                                    </table>';
                $data['datahistorynjoptanah'] = $html_history;

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function datapembayaranAction()
    {
        $frm = new \Bphtb\Form\Verifikasi\VerifikasiSPTFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Verifikasi\VerifikasiSPTBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getTblVerifikasi()->temukanDataPembayaran($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    // Menghitung nilai njop pbb
    public function hitungnjopAction()
    {
        $frm = new \Bphtb\Form\Verifikasi\VerifikasiSPTFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $p_luastanah = str_ireplace(".", "", $ex->p_luastanah);
                $p_njoptanah = str_ireplace(".", "", $ex->p_njoptanah);

                $p_luasbangunan = str_ireplace(".", "", $ex->p_luasbangunan);
                $p_njopbangunan = str_ireplace(".", "", $ex->p_njopbangunan);

                $input = $this->getRequest();
                $aphb_kali = $input->getPost('p_t_tarif_pembagian_aphb_kali');
                $aphb_bagi = $input->getPost('p_t_tarif_pembagian_aphb_bagi');

                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    $p_totalnjoptanah = $p_luastanah * $p_njoptanah;
                    $p_totalnjopbangunan = $p_luasbangunan * $p_njopbangunan;
                    $p_grandtotalnjop = $p_totalnjoptanah + $p_totalnjopbangunan;
                    $p_grandtotalnjop_aphb = $p_grandtotalnjop;
                } else {
                    $aphb = $aphb_kali / $aphb_bagi;
                    $p_totalnjoptanah = $p_luastanah * $p_njoptanah;
                    $p_totalnjopbangunan = $p_luasbangunan * $p_njopbangunan;
                    $p_grandtotalnjop = $p_totalnjoptanah + $p_totalnjopbangunan;

                    $p_grandtotalnjop_aphb = ceil($p_grandtotalnjop * $aphb);
                }


                $p_nilaitransaksispt = str_ireplace(".", "", $ex->p_nilaitransaksispt);
                if (($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)) {
                    if ($p_nilaitransaksispt >= $p_grandtotalnjop * 1) {
                        $p_npop = $p_nilaitransaksispt;
                    } else {
                        $p_npop = $p_grandtotalnjop;
                    }
                } else {
                    if ($p_nilaitransaksispt >= $p_grandtotalnjop_aphb * 1) {
                        $p_npop = $p_nilaitransaksispt;
                    } else {
                        $p_npop = $p_grandtotalnjop_aphb;
                    }
                }

                if (!empty($input->getPost('t_potongan_waris_hibahwasiat'))) {
                    $t_potongan_waris_hibahwasiat = $input->getPost('t_potongan_waris_hibahwasiat');
                    $hitung_potonganwaris = $t_potongan_waris_hibahwasiat / 100;
                } else {
                    $t_potongan_waris_hibahwasiat = 0;
                    $hitung_potonganwaris = 1;
                }


                $p_potonganspt = str_ireplace(".", "", $ex->p_potonganspt);
                $p_npopkp = $p_npop - $p_potonganspt;
                if ($p_npopkp <= 0) {
                    $p_npopkp = 0;
                    $p_totalspt = 0;
                } else {
                    $p_persenbphtb = str_ireplace(".", "", $ex->p_persenbphtb);
                    $p_totalspt = ceil($p_npopkp * $p_persenbphtb / 100 * $hitung_potonganwaris);
                }
                $data = array(
                    "p_totalnjoptanah" => $p_totalnjoptanah,
                    "p_totalnjopbangunan" => $p_totalnjopbangunan,
                    "p_grandtotalnjop" => $p_grandtotalnjop,
                    "p_t_grandtotalnjop_aphb" => $p_grandtotalnjop_aphb,
                    "p_npop" => $p_npop,
                    "p_potonganspt" => $p_potonganspt,
                    "p_npopkp" => $p_npopkp,
                    "p_totalspt" => $p_totalspt
                );
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    function populatePersyaratanId($idtransaksi, $idspt)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($idtransaksi);
        $html = "<div>";
        $html .= "<label>Persyaratan Pendaftaran : </label>";
        foreach ($data as $row) {
            $html .= "<div class='col-sm-12'>
                      <div class='form-group'>";
            $html .= "<div class='col-sm-12'>";
            $dataa = $this->getTblPersyaratan()->getDataSyaratSPT($row->s_idpersyaratan, $idspt);
            if ($dataa == false) {
                $html .= "<input id='t_persyaratan' name='t_persyaratan[]' type='checkbox' value='" . $row->s_idpersyaratan . "'>";
            } else {
                $html .= "<input id='t_persyaratan' name='t_persyaratan[]' type='checkbox' value='" . $row->s_idpersyaratan . "' checked='checked' disabled > ";
            }
            $html .= $row->s_namapersyaratan;
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</div>";
        }
        $html .= "</div>";
        return $html;
    }

    function populatePersyaratanVerifikasi($id)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($id);
        $html = "<div>";
        $html .= "<label>Persyaratan Validasi : </label>";
        foreach ($data as $row) {
            $html .= "<div class='col-sm-12'>
                      <div class='form-group'>";
            $html .= "<div class='col-sm-12'>";
            $html .= "<input id='t_persyaratanverifikasi' name='t_persyaratanverifikasi[]' type='checkbox' value='" . $row->s_idpersyaratan . "'> ";
            $html .= $row->s_namapersyaratan;
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</div>";
        }
        $html .= "</div>";
        return $html;
    }

    public function populateComboJenisTransaksi()
    {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idjenistransaksi] = $row->s_namajenistransaksi;
        }
        return $selectData;
    }

    // Pengecekan Tunggakan PBB
    // Lokasi : Tambah Pembayaran
    public function cektunggakanpbbc()
    {
        $frm = new \Bphtb\Form\Pembayaran\PembayaranSptFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                var_dump($ex);
                exit();
                $datahari = $this->getTblPembayaran()->cekJumlahhari();
                $data = $this->getTblPembayaran()->cekValiditasSSPD($ex, $datahari['s_jumlahhari']);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function cetaksuratpenelitianAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $ar_sspd = $this->getSSPD()->getdatasurathasilpenelitian($data_get);

                $ar_Mengetahui1 = $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian1);
                $ar_Mengetahui2 = $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian2);
                $ar_Mengetahui3 = $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian3);
                $ar_Mengetahui4 = $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian4);
                $ar_Mengetahui5 = $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian5);
                $ar_Mengetahui6 = $this->getTblPejabat()->getdataid($data_get->mengetahuipenelitian6);
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'SuratHasilPenelitian');
        $pdf->setOption('paperSize', 'A4');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'data_pemda' => $ar_pemda,
            'tgl_cetak' => $data_get->tgl_cetak_penelitian,
            'data_mengetahui1' => $ar_Mengetahui1,
            'data_mengetahui2' => $ar_Mengetahui2,
            'data_mengetahui3' => $ar_Mengetahui3,
            'data_mengetahui4' => $ar_Mengetahui4,
            'data_mengetahui5' => $ar_Mengetahui5,
            'data_mengetahui6' => $ar_Mengetahui6
        ));
        return $pdf;
    }

    public function cektunggakanpbbAction()
    {
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $base = new \Bphtb\Model\Pendataan\SPPTBase();
            $frm->setData($req->getPost());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getPost();
                $base->t_nopbphtbspptinfoop = $data_get['t_nopbphtbsppt'];
                $base->t_thnsppt = $data_get['t_thnsppt'];
                $data_tunggakan = $this->getTblSPPT()->temukanDataTunggakanop($base);
                $html = "<div class='row'>
                          <div class='col-md-12'>
                          <div class='panel panel-primary'>
                          <div class='panel-heading'><strong>Tunggakan SPPT-PBB</strong></div>
                          <table class='table table-striped'>";
                $html .= "<tr>";
                $html .= "<th>No.</th>";
                $html .= "<th>Tahun</th>";
                $html .= "<th>Tunggakan (Rp.)</th>";
                $html .= "<th>Jatuh Tempo</th>";
                $html .= "<th>Denda (Rp.)</th>";
                $html .= "</tr>";
                $i = 1;
                $jumlahdenda = 0;
                $PBB_YG_HARUS_DIBAYAR_SPPT = 0;
                foreach ($data_tunggakan as $row) {
                    $html .= "<tr>";
                    $html .= "<td> " . $i . " </td>";
                    $html .= "<td> " . $row['THN_PAJAK_SPPT'] . " </td>";
                    $html .= "<td> " . number_format($row['PBB_YG_HARUS_DIBAYAR_SPPT'], 0, ',', '.') . " </td>";
                    $html .= "<td> " . $row['JATUH_TEMPO'] . " </td>";
                    //                    $html .= "<td> " . date('d-m-Y', strtotime($row['JATUH_TEMPO'])) . " </td>";
                    $dat1 = date('Y-m-d', strtotime($row['JATUH_TEMPO']));
                    $dat2 = date('Y-m-d');
                    /*$date1 = new \DateTime($dat1);
                    $date2 = new \DateTime($dat2);
                    $interval = $date1->diff($date2);
                    $bedanya = $interval->m + ($interval->y * 12);
                    if ($bedanya > 24) {
                        $beda = 24;
                    } else {
                        $beda = $bedanya;
                    }*/

                    $tgl_bayar = explode("-", $dat2);
                    $tgl_tempo = explode("-", $dat1);

                    $tahun = $tgl_bayar[0] - $tgl_tempo[0];
                    $bulan = $tgl_bayar[1] - $tgl_tempo[1];
                    $hari = $tgl_bayar[2] - $tgl_tempo[2];


                    if (($tahun == 0) || ($tahun < 1)) {
                        if (($bulan == 0) || ($bulan < 1)) {
                            if ($bulan < 0) {
                                $months = 0;
                            } else {
                                if (($hari == 0) || ($hari < 1)) {
                                    $months = 0;
                                } else {
                                    $months = 1;
                                }
                            }
                        } else {
                            if (($hari == 0) || ($hari < 1)) {
                                $months = $bulan;
                            } else {
                                $months = $bulan + 1;
                            }
                        }
                    } else {
                        $jmltahun = $tahun * 12;
                        if ($bulan == 0) {
                            $months = $jmltahun;
                        } elseif ($bulan < 1) {
                            $months = $jmltahun + $bulan;
                        } else {
                            $months = $bulan + $jmltahun;
                        }
                    }


                    if ($months > 24) {
                        $beda = 24;
                    } else {
                        $beda = $months;
                    }

                    $denda = $beda * $row['PBB_YG_HARUS_DIBAYAR_SPPT'] * 2 / 100;
                    $html .= "<td> " . number_format($denda, 0, ',', '.') . " </td>";
                    $html .= "</tr>";
                    $i++;
                    $PBB_YG_HARUS_DIBAYAR_SPPT = $PBB_YG_HARUS_DIBAYAR_SPPT + $row['PBB_YG_HARUS_DIBAYAR_SPPT'];
                    $jumlahdenda = $jumlahdenda + $denda;
                }
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='2'><center>Jumlah Tunggakan</center></td>";
                $html .= "<td> " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT, 0, ',', '.') . " </td>";
                $html .= "<td>Jumlah Denda</td>";
                $html .= "<td> " . number_format($jumlahdenda, 0, ',', '.') . " </td>";
                $html .= "</tr>";
                $html .= "<tr style='font-size:16px; font-weight:bold;'>";
                $html .= "<td colspan='3'><center>Jumlah Seluruh Tunggakan</center></td>";
                $html .= "<td colspan='2'><div style='text-align:center; color:red' > Rp. " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT + $jumlahdenda, 0, ',', '.') . " </div></td>";
                $html .= "</tr>";
                $html .= "</table>
                        </div></div></div>";
                $data['datatunggakan'] = $html;
                $data['PBB_YG_HARUS_DIBAYAR_SPPT'] = $PBB_YG_HARUS_DIBAYAR_SPPT;
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function cetakkodebayarAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();

                //=============== Model\Pencetakan\SSPDTable
                $ar_sspd = $this->getSSPD()->ambildatakodebayar($data_get);
                $ar_tglcetak = $base->tgl_cetak;
                $ar_pemda = $this->getPemda()->getdata();
            }
        }
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'KodeBayar');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'tgl_cetak' => $ar_tglcetak,
            'data_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    public function cetakbuktipenerimaanvalidasiAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $frm = new \Bphtb\Form\Pencetakan\SSPDFrm();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $base = new \Bphtb\Model\Pencetakan\SSPDBase();
            $frm->setData($req->getQuery());
            if ($frm->isValid()) {
                $base->exchangeArray($frm->getData());
                $data_get = $req->getQuery();
                $datahari = $this->getTblPembayaran()->cekJumlahhari();
                if ($data_get->action == 'cetakpenerimaanvalidasi') {
                    $ar_sspd = $this->getSSPD()->cetakbuktipenerimaanvalidasi($data_get->t_idspt);
                    // $ar_sspd = $this->getSSPD()->ambildatainsptvalidasi($data_get->t_idspt);
                    $ar_tglcetak = date('d-m-Y');
                    $hasildata = $this->getSSPD()->cetakbuktipenerimaanvalidasi($data_get->t_idspt);
                } else {
                    //=============== Model\Pencetakan\SSPDTable
                    $ar_sspd = $this->getSSPD()->cetakbuktipenerimaanvalidasi($base);
                    // $ar_sspd = $this->getSSPD()->ambildatainsptvalidasi(@$data_tidspt);
                    $ar_tglcetak = $base->tgl_cetak;
                    $hasildata = $this->getSSPD()->cetakbuktipenerimaanvalidasi($base);
                }
                $ar_pemda = $this->getPemda()->getdata();
                $testarray = array();
                foreach ($hasildata as $row) {
                    $result_array_syarat = \Zend\Json\Json::decode($row['t_persyaratan']);
                    $jml_syarat = count($result_array_syarat);
                    $result_array_syarat_verifikasi = \Zend\Json\Json::decode($row['t_verifikasispt']);
                    $jml_syarat_verifikasi = count($result_array_syarat_verifikasi);
                    if ($jml_syarat == $jml_syarat_verifikasi) {
                    } else {
                        $combosyarat = $this->getTblPersyaratan()->comboBox($row['t_idjenistransaksi']);
                        array_push($testarray, $combosyarat);
                    }
                }
            }
        }

        //var_dump($ar_sspd);
        //exit();

        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtb);
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'BuktiPenerimaan');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_sspd' => $ar_sspd,
            'tgl_cetak' => $ar_tglcetak,
            'data_pemda' => $ar_pemda,
            'testarray' => $testarray,
            'datahari' => $datahari,
            'ar_Pejabat' => $ar_Pejabat
        ));
        return $pdf;
    }

    public function cetakdaftarverifikasiAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        //========================== Model\Pencetakan\SSPDTable
        $ar_DataVerifikasi = $this->getSSPD()->getDataVerifikasi($data_get->periode_spt, $data_get->tgl_verifikasi1, $data_get->tgl_verifikasi2);
        $ar_tglcetak = $data_get->tgl_cetak;
        $ar_periodespt = $data_get->periode_spt;
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Daftar Verifikasi');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'landscape');
        $pdf->setVariables(array(
            'data_Verifikasi' => $ar_DataVerifikasi,
            'tgl_cetak' => $ar_tglcetak,
            'periode_spt' => $ar_periodespt,
            'tgl_verifikasi1' => $data_get->tgl_verifikasi1,
            'tgl_verifikasi2' => $data_get->tgl_verifikasi2,
            'nama_login' => $session['s_namauserrole']
        ));
        return $pdf;
    }

    public function populateComboHakTanah()
    {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    private function populateCheckBoxverifikasi($idtransaksi)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($idtransaksi);
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idpersyaratan] = ' ' . $row->s_namapersyaratan;
        }
        return $selectData;
    }

    public function getTblVerifikasi()
    {
        if (!$this->tbl_verifikasi) {
            $sm = $this->getServiceLocator();
            $this->tbl_verifikasi = $sm->get('VerifikasiSPTTable');
        }
        return $this->tbl_verifikasi;
    }

    public function getTblPembayaran()
    {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function getTblJenTran()
    {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan()
    {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getTblSSPDBphtb()
    {
        if (!$this->tbl_pendataan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pendataan = $sm->get('SSPDBphtbTable');
        }
        return $this->tbl_pendataan;
    }

    public function getTblPersyaratan()
    {
        if (!$this->tbl_persyaratan) {
            $sm = $this->getServiceLocator();
            $this->tbl_persyaratan = $sm->get('PersyaratanTable');
        }
        return $this->tbl_persyaratan;
    }

    public function getTblPejabat()
    {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getSSPD()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblSPPT()
    {
        if (!$this->tbl_nop) {
            $sm = $this->getServiceLocator();
            $this->tbl_nop = $sm->get('SPPTTable');
        }
        return $this->tbl_nop;
    }

    // private function getTblNotifikasi(){
    // if (!$this->tbl_notifikasi) {
    // $sm = $this->getServiceLocator();
    // $this->tbl_notifikasi = $sm->get('NotifikasiTable');
    // }
    // return $this->tbl_notifikasi;
    // }

    public function validasikabidAction()
    {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
            ->get('EtaxService')
            ->getStorage()
            ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int)$req->getQuery()->get('t_idspt');
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
        }

        if ($req->isPost()) {
            //sleep(3);
            $post = $req->getPost();
            $id = $post['t_idspt'];
            $data = $this->getTblSSPDBphtb()->getDataId_all($id);
            //var_dump($data);exit();
            $this->getServiceLocator()->get('VerifikasiSPTTable')->validasikabid($post, $session, $data, $post['tolak'], $ar_pemda);

            return $this->redirect()->toRoute('verifikasi_spt');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_verifikasi' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }
}
