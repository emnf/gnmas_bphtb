<?php

// Modul Verifikasi

namespace Bphtb\Controller\Verifikasi;

use Bphtb\Controller\Pendataan\PendataanSSPD;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use BSrE_PDF_Signer_Cli;
use mPDF;

class VerifikasiESignature extends AbstractActionController {

    protected $tbl_pemda,
            $tbl_pembayaran,
            $tbl_jenistransaksi,
            $tbl_haktanah,
            $tbl_pendataan,
            $tbl_pejabat,
            $tbl_sspd;

    public function cekurl() {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . ':' . $_SERVER['SERVER_PORT'] . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
    }

    public function indexAction() {
        // $session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()
                ->get('EtaxService')
                ->getStorage()
                ->read();
        $ar_pemda = $this->getPemda()->getdata();

        $data_mengetahui = $this->getTblPejabat()->getdata();
        $data_mengetahui_periksa = $this->getTblPejabat()->getdata();
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $view = new ViewModel(array(
            'data_mengetahui' => $data_mengetahui,
            'data_mengetahui_periksa' => $data_mengetahui_periksa,
            'datajenistransaksi' => $datajenistransaksi
        ));
        $data = array(
            'menu_verifikasi_esignature' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();

        $sTable = 'fr_pembayaran_v5';
        $count = 't_idspt';

        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
        $aColumns = array(
            't_idspt', 
            't_kohirspt', 
            't_kohirketetapanspt', 
            's_namajenistransaksi', 
            't_tglverifikasispt', 
            't_namawppembeli', 
            't_nilaipembayaranspt', 
            't_kodebayarbanksppt', 
            't_statusbayarspt',
            't_status_esignature',
            't_idspt',
            't_inputbpn'
        );
        $panggildata = $this->getServiceLocator()->get("VerifikasiESignatureTable");
        $rResult = $panggildata->semuadatapembayaran($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function viewdataAction() {
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()
                ->get('EtaxService')
                ->getStorage()
                ->read();
        $req = $this->getRequest();
        if ($req->isGet()) {
            $id = (int) $req->getQuery()->get('t_idspt');
            $data = $this->getPendataanBphtb()->getDataId_all($id);
            $data['t_tglprosesspt'] = date('d-m-Y', strtotime($data['t_tglprosesspt']));
            $data['t_tglajb'] = date('d-m-Y', strtotime($data['t_tglajb']));
            $t_luastanah = str_ireplace('.', '', $data['t_luastanah']);
            $t_luasbangunan = str_ireplace('.', '', $data['t_luasbangunan']);
            $data['t_luastanah'] = number_format(($t_luastanah / 100), 0, ',', '.');
            $data['t_luasbangunan'] = number_format(($t_luasbangunan / 100), 0, ',', '.');
        }
        $view = new ViewModel(array(
            'datasspd' => $data
        ));

        $data = array(
            'menu_verifikasi_esignature' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function prosestandatanganAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        
        $rResult = array();
        if($req->isPost()){
            $data_get = $req->getPost();

            // CREATE SSPD
            $data_pemda = $this->getPemda()->getdata();
            $record_ar_sspd = array();
            $ar_sspd = $this->getSSPD()->getDataSSPDArrayIdSpt($data_get["t_idspt"]);
            foreach ($ar_sspd as $ar_sspd) {
                $record_ar_sspd[] = $ar_sspd;
            }
            $data_sspd = $this->getSSPD()->getviewcetakssp($data_get["t_idspt"]);
            $dataidsptsebelum = $this->getSSPD()->getdataidsptsebelumnya($data_get["t_idspt"]);
            $ar_sebelum = array();
            if (!empty($dataidsptsebelum['t_idsptsebelumnya'])) {
                $ar_sebelum = $this->getSSPD()->getdatassspdsebelumnya($dataidsptsebelum['t_idsptsebelumnya']);
            }
            $data_sspd_array = $data_sspd->current();
            $kodebayar = $data_sspd_array["t_kodebayarbanksppt"];
            $detail_nop_array = array();
            if($data_sspd_array['t_jenispendaftaran']==4){
                // nop gabungan
                $detail_nop = $this->getSSPD()->getDetailNOP($data_sspd_array['t_iddetailsptbphtb']);
                foreach ($detail_nop as $row):
                    $detail_nop_array[] = $row;
                endforeach;
            }

            $basePath = $this->getRequest()->getBasePath();
            $pendataansspd = new PendataanSSPD;
            $sspd1 = $pendataansspd->create_sppd($data_pemda, $record_ar_sspd, $dataidsptsebelum, $ar_sebelum, $detail_nop_array, $kodebayar, 1, "F", null, 1, $this->cekurl(), $basePath);
            $sspd2 = $pendataansspd->create_sppd($data_pemda, $record_ar_sspd, $dataidsptsebelum, $ar_sebelum, $detail_nop_array, $kodebayar, 2, "F", null, 1, $this->cekurl(), $basePath);
            
            // SIGNED SSPD
            $res = $this->signed_sspd_all($kodebayar, $data_get["passphrase"]);

            // $res = $this->signed_sspd($data_get["t_idspt"], $data_get["passphrase"]);
            $log = $this->getServiceLocator()->get("VerifikasiESignatureTable")->updateESignature($data_get["t_idspt"], $session["s_iduser"], $res["code"], $res["message"]);
            if($res["code"] == 1){
                $colorfont= 'green';
            }else{
                $colorfont ='red';
            }
            $rResult = array(
                'code' => $res['code'],
                'response_messages' => "<b style='color:".$colorfont.";'>".$res["message"]."</b>",
                // 'filename' => $res["filename"],
                'kodebayar' => $kodebayar
            );
        }
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }
    
    public function signed_sspd_all($kodebayar=null, $passphrase=null)
    {
        require_once 'public/bsre-pdf-sign-cli/src/pdf-sign-cli.php';

        $pdfSigner = new BSrE_PDF_Signer_Cli();

        $pdfs = array(
            './public/sspd_signed/cetaksppd_'.$kodebayar.'_1.pdf',
            './public/sspd_signed/cetaksppd_'.$kodebayar.'_2.pdf'
        );
        
        $pdfSigner->setDocument($pdfs);

        $pdfSigner->readCertificateFromFile(
            './public/bsre-pdf-sign-cli/example/cert/devel-agustus2019.p12', 
            // 'D:/PHP56/cert_bsre/devel-agustus2019.p12', 
            '' . $passphrase . ''
        );

        $pdfSigner->setCertificationLevel(0);

        $tsa_url = 'http://tsa-bsre.bssn.go.id';
        $tsa_uname = 'coba';
        $tsa_pass = '1234';
        $pdfSigner->setTimeStamp($tsa_url, $tsa_uname, $tsa_pass);

        $pdfSigner->setOCSP('cvs-bsre.bssn.go.id/ocsp');

        $pdfSigner->setLocation('Jakarta');

        // $pdfSigner->setAppearance(
        //         $position = array(
        //     'llx' => '431.1159',
        //     'lly' => '313.54327',
        //     'urx' => '573.08984',
        //     'ury' => '239.05511'
        //         ), $page = 1, $spesimen = null
        // );

        if (!$pdfSigner->sign()) {
            $response_code = 0;
            $response_messages = $pdfSigner->getError();
        } else {
            $response_code = 1;
            $response_messages = 'Sukses';
        }

        $unlik1 = unlink('./public/sspd_signed/cetaksppd_'.$kodebayar.'_1.pdf');
        $unlik2 = unlink('./public/sspd_signed/cetaksppd_'.$kodebayar.'_2.pdf');

        return array(
            'code' => $response_code,
            'message' => $response_messages,
        );
    }

    public function getTblPejabat() {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getPemda() {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getSSPD() {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

}
