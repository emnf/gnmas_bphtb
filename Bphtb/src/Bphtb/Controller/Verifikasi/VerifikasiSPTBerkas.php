<?php

// Modul Verifikasi

namespace Bphtb\Controller\Verifikasi;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Bphtb\Form\Pendataan\SSPDFrm;
use Bphtb\Model\Pendataan\SSPDBphtbBase;

class VerifikasiSPTBerkas extends AbstractActionController {

    protected $tbl_pemda,
              $tbl_pembayaran, 
              $tbl_verifikasi, 
              $tbl_jenistransaksi, 
              $tbl_haktanah, 
              $tbl_pendataan, 
              $tbl_persyaratan, 
              $tbl_notifikasi, 
              $tbl_pejabat, 
              $tbl_sspd, 
              $tbl_nop;

    public function cekurl()
     {
        $basePath = $this->getRequest()->getBasePath();
            $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
            $uri->setPath($basePath);
            $uri->setQuery(array());
            $uri->setFragment('');
            
            return $uri->getScheme() . '://' . $uri->getHost() . ':' . $_SERVER['SERVER_PORT'] . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'
    
     }
    
    public function indexAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $form = new \Bphtb\Form\Verifikasi\VerifikasiSPTFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $Mengetahuibphtb = $this->getTblPejabat()->getdata();
        $Mengetahuilengkap = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian1 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian2 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian3 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian4 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian5 = $this->getTblPejabat()->getdata();
        $Mengetahuipenelitian6 = $this->getTblPejabat()->getdata();
        
        $data_mengetahuilengkap_sspd = $this->getTblPejabat()->getdata();
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        
        $view = new ViewModel(array(
            "form" => $form,
            'data_mengetahuibphtb' => $Mengetahuibphtb,
            'data_mengetahuilengkap' => $Mengetahuilengkap,
            'data_mengetahuipenelitian1' => $Mengetahuipenelitian1,
            'data_mengetahuipenelitian2' => $Mengetahuipenelitian2,
            'data_mengetahuipenelitian3' => $Mengetahuipenelitian3,
            'data_mengetahuipenelitian4' => $Mengetahuipenelitian4,
            'data_mengetahuipenelitian5' => $Mengetahuipenelitian5,
            'data_mengetahuipenelitian6' => $Mengetahuipenelitian6,
            'data_mengetahuilengkap_sspd' => $data_mengetahuilengkap_sspd,
            'datajenistransaksi' => $datajenistransaksi  
        ));
        $data = array(
            'menu_verifikasi_berkas' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function dataGridAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser']; 
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        
        $sTable = 'fr_pendaftaran_v5';
        $count = 't_idspt';
        
        $input = $this->getRequest();
        $order_default = " t_tglverifikasispt DESC";
        $aColumns = array('t_idspt', 't_kohirspt', 't_kohirketetapanspt', 's_idjenistransaksi', 't_tglverifikasispt', 't_namawppembeli', 't_totalspt', 't_kodebayarbanksppt', 't_statusbayarspt', 'status_pendaftaran', 'status_validasi', 's_namajenistransaksi', 't_persyaratan', 't_verifikasispt', 't_idjenistransaksi', 't_inputbpn', 't_periodespt', 't_idpembayaranspt', 'p_totalspt', 'p_idpemeriksaan', 'fr_tervalidasidua', 'fr_validasidua', 't_idsptsebelumnya'); 
        
        
        $panggildata = $this->getServiceLocator()->get("VerifikasiSPTTable");
        $rResult = $panggildata->semuadataverifikasiberkas($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());    
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }
    
    public function tambahAction() {
        $session = $this->getServiceLocator()
                ->get('EtaxService')
                ->getStorage()
                ->read();
        $ar_pemda = $this->getPemda()->getdata();
        $frm = new \Bphtb\Form\Verifikasi\VerifikasiSPTFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        if ($req->isPost()) {
            $kb = new \Bphtb\Model\Verifikasi\VerifikasiSPTBase();
            $frm->setInputFilter($kb->getInputFilter());
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $kb->exchangeArray($frm->getData());
                $t_pejabatverifikasi = $session['s_iduser'];
                sleep(4);
                $this->getTblVerifikasi()->savedataVerifikasiBerkas($kb, $t_pejabatverifikasi);
                return $this->redirect()->toRoute('verifikasi_spt_berkas');
                
            }
        }
        
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();
        
        $view = new ViewModel(array(
            'frm' => $frm,
            'datajenistransaksi' => $datajenistransaksi,
            'datanotaris' => $ar_notaris
        ));
        $data = array(
            'menu_verifikasi_berkas' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    public function dataGridPendataanBphtbAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser']; 
        $s_tipe_pejabat = $session['s_tipe_pejabat'];
        
        $sTable = 'fr_databelumdivalidasi'; //'fr_pendaftaran_v5'; 
        $count = 't_idspt';
        
        $input = $this->getRequest();
        $order_default = " t_kohirspt DESC";
        $aColumns = array('t_idspt', 't_kohirspt', 't_tglprosesspt', 't_nopbphtbsppt', 't_namawppembeli', 't_namawppenjual', 's_namanotaris', 't_idjenistransaksi', 't_idnotarisspt',  's_namajenistransaksi'); 
        
        $panggildata = $this->getServiceLocator()->get("VerifikasiSPTTable");
        $rResult = $panggildata->semuadatapendaftaran($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());    
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }
    
    public function populateComboJenisTransaksi() {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idjenistransaksi] = $row->s_namajenistransaksi;
        }
        return $selectData;
    }
    
    public function populateComboHakTanah() {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }
    
    public function pilihPendataanSspdBphtbAction() {
        $frm = new SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getTblSSPDBphtb()->getPendataanSspdBphtb($ex);
                $pengurangan = $this->getTblSSPDBphtb()->getPengurangan($ex->t_idspt);
                $data['t_pengurangan'] = number_format($pengurangan['t_pengurangan'],0,',','.');
                $data['t_bphtbterhutang'] = number_format($data['t_totalspt']+$pengurangan['t_pengurangan'],0,',','.');
                $aphb_kali = $data['t_tarif_pembagian_aphb_kali'];
                $aphb_bagi = $data['t_tarif_pembagian_aphb_bagi'];
                if(($aphb_kali == null) || ($aphb_kali == '') || ($aphb_kali == 0) || ($aphb_bagi == null) || ($aphb_bagi == '') || ($aphb_bagi == 0)){
                    if ($data['t_nilaitransaksispt'] >= $data['t_grandtotalnjop']) {
                        $data['npop'] = $data['t_nilaitransaksispt'];
                    } else {
                        $data['npop'] = $data['t_grandtotalnjop'];
                    }
                }else{
                    if ($data['t_nilaitransaksispt'] >= $data['t_grandtotalnjop_aphb']) {
                        $data['npop'] = $data['t_nilaitransaksispt'];
                    } else {
                        $data['npop'] = $data['t_grandtotalnjop_aphb'];
                    }
                }
                
                
                $data['t_luastanah'] = str_ireplace('.', '', $data['t_luastanah']) / 100;
                $data['t_luasbangunan'] = str_ireplace('.', '', $data['t_luasbangunan']) / 100;

                // cari harga acuan pada tabel master s_acuan
                $nop = explode('.', $data['t_nopbphtbsppt']);
                $s_kd_propinsi = $nop[0];
                $s_kd_dati2 = $nop[1];
                $s_kd_kecamatan = $nop[2];
                $s_kd_kelurahan = $nop[3];
                $s_kd_blok = $nop[4];
                $datahargaacuan = $this->getTblVerifikasi()->getHargaAcuan($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok);
                $data['s_permetertanah'] = number_format($datahargaacuan['s_permetertanah'], 0, ',', '.');
                $njop_tanah = ($data['t_nilaitransaksispt'] - $data['t_totalnjopbangunan']) / $data['t_luastanah'];
                $analisis_a = 0;
                if ($datahargaacuan) {
                    $analisis_a = $njop_tanah / $datahargaacuan['s_permetertanah'] * 100;
                }

                if ((int) $analisis_a < 0) {
                    $harga_a = 0;
                    $hasil_a = $this->getTblVerifikasi()->getPresentase($harga_a);
                } elseif ((int) $analisis_a > 100) {
                    $harga_a = 100;
                    $hasil_a = $this->getTblVerifikasi()->getPresentase($harga_a);
                } else {
                    $hasil_a = $this->getTblVerifikasi()->getPresentase($analisis_a);
                }
                $warna = $hasil_a['warna'];

                $html = "<div class='col-md-7'>";
                
                if($analisis_a == 0){
                    
                }else{
                    $html .= "<left>NJOP Tanah (Transaksi) / Harga Acuan x 100 % = <span class='badge' style='background-color:$warna;'>" . round($analisis_a,3). " % </span> <strong>" . $hasil_a['s_keterangan'] . "</strong> </left>  ";
                }
                $html .= "</div>";
                $data['analisishargaacuan'] = $html;

                $idspt = (int) $data['t_idspt'];
                $datahistorynjoptanah = $this->getTblVerifikasi()->getHargaHistoryNJOPTanahpilih($s_kd_propinsi, $s_kd_dati2, $s_kd_kecamatan, $s_kd_kelurahan, $s_kd_blok, $idspt);

                $njoptanahtransaksi = (int) $datahistorynjoptanah['njoptanahtransaksi'];
                $njoptanah = (int) $datahistorynjoptanah['njoptanah'];
                if ($njoptanahtransaksi >= $njoptanah) {
                    $data['historynjoptanah'] = $njoptanahtransaksi;
                } else {
                    $data['historynjoptanah'] = $njoptanah;
                }

                $analisis_b = 0;
                if ($data['historynjoptanah']) {
                    $analisis_b = $njop_tanah / $data['historynjoptanah'] * 100;
                }
                if ((int) $analisis_b < 0) {
                    $harga_b = 0;
                    $hasil_b = $this->getTblVerifikasi()->getPresentase($harga_b);
                } elseif ((int) $analisis_b > 100) {
                    $harga_b = 100;
                    $hasil_b = $this->getTblVerifikasi()->getPresentase($harga_b);
                } else {
                    $hasil_b = $this->getTblVerifikasi()->getPresentase($analisis_b);
                }

                $warna_b = $hasil_b['warna'];
                $html1 = "<div class='col-md-7'>";
                if($analisis_b == '0'){
                    $html1 .= "";
                }else{
                    $html1 .= "<left>NJOP Tanah (Transaksi) / Harga History x 100 % = <span class='badge' style='background-color:$warna_b;'>" . round($analisis_b,3) . " % </span> <strong>" . $hasil_b['s_keterangan'] . "</strong> </left>";
                }
                $html1 .= "</div>";
                $data['analisisharganjoptanah'] = $html1;
                $data['historynjoptanah'] = number_format($data['historynjoptanah'], 0, ',', '.');

                $data['t_luastanah'] = number_format($data['t_luastanah'], 0, ',', '.');
                $data['t_njoptanah'] = number_format($data['t_njoptanah'], 0, ',', '.');
                $data['t_luasbangunan'] = number_format($data['t_luasbangunan'], 0, ',', '.');
                $data['t_njopbangunan'] = number_format($data['t_njopbangunan'], 0, ',', '.');
                $data['t_totalnjoptanah'] = number_format($data['t_totalnjoptanah'], 0, ',', '.');
                $data['t_totalnjopbangunan'] = number_format($data['t_totalnjopbangunan'], 0, ',', '.');
                $data['t_grandtotalnjop'] = number_format($data['t_grandtotalnjop'], 0, ',', '.');
                $data['t_nilaitransaksispt'] = number_format($data['t_nilaitransaksispt'], 0, ',', '.');
                $data['npop'] = number_format($data['npop'], 0, ',', '.');
                $data['t_potonganspt'] = number_format($data['t_potonganspt'], 0, ',', '.');
                
                $data['t_tarif_pembagian_aphb_kali'] = $data['t_tarif_pembagian_aphb_kali'];
                $data['t_tarif_pembagian_aphb_bagi'] = $data['t_tarif_pembagian_aphb_bagi'];
                $data['t_grandtotalnjop_aphb'] = number_format($data['t_grandtotalnjop_aphb'], 0, ',', '.');
                $data['p_grandtotalnjop_aphb'] = number_format($data['p_grandtotalnjop_aphb'], 0, ',', '.');
                $data['t_persenbphtb'] = $data['t_persenbphtb'];
                $data['t_potongan_waris_hibahwasiat'] = $data['t_potongan_waris_hibahwasiat'];
                $data['t_idjenistransaksi'] = $data['t_idjenistransaksi'];
                
                $npopkp = (str_replace('.', '', $data['npop']) -  str_replace('.', '', $data['t_potonganspt']));
                if($npopkp <= 0){
                    $nilai_npopkp = 0;
                }else{
                    $nilai_npopkp = $npopkp;
                }
                $data['npopkp'] = number_format($nilai_npopkp, 0, ',', '.');
                
                //$npopkp = $data['t_totalspt'] / 5 * 100;
                //$data['npopkp'] = number_format($npopkp, 0, ',', '.');
                $data['t_totalspt'] = number_format($data['t_totalspt'], 0, ',', '.');
                // Persyaratan Pendaftaran dan Validasi
                $data['t_persyaratan'] = $this->populatePersyaratanId($data['t_idjenistransaksi'], $data['t_idspt']);
                $data['t_persyaratanverifikasi'] = $this->populatePersyaratanVerifikasi($data['t_idjenistransaksi']);
                
                $panggildata = $this->getServiceLocator()->get("PersyaratanTable");
                $datafileupload = $panggildata->syaratfileupload($data['t_idjenistransaksi'], $data['t_idspt']);
                
                    $no = 1;
                    $namasyarat = '';
                    $nomer = 1;
                    $fileuploadnya = '';
                    foreach ($datafileupload as $v) {

                        if ($namasyarat <> $v['s_namapersyaratan']) {

                                if ($namasyarat <> '') {

                                }
                                //$fileuploadnya .= $no;

                                $fileuploadnya .= $no . ". ".$v['s_namapersyaratan'] . "<br>";
                                /*$fileuploadnya .= ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$this->url().'/uploadjenisyarat?t_idspt='.$datasspd['t_idspt'].'&syarat='.$v['s_idpersyaratan'].'" class="btn bg-purple btn-sm btn-flat">
                                    <span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;Upload File
                                </a><br>';
                                */
                                $namasyarat = $v['s_namapersyaratan'];
                                $clast_rek_name = $v['s_namapersyaratan'];
                                //$no = 1;
                                $no++;
                                $nomer = 1; 
                        }        

                        if(!empty($v['nama_file'])){
                            //$fileuploadnya .= $v['nama_file']."<br>";
                            $fileuploadnya .= '<a style="display:none;" data-ng-href="'.$this->cekurl().'/'.$v['letak_file'].''.$v['nama_file'].'" title="'.$v['nama_file'].'" download="'.$v['nama_file'].'" data-gallery="" href="'.$this->cekurl().'/'.$v['letak_file'].''.$v['nama_file'].'"><img data-ng-src="'.$this->cekurl() .'/'.$v['letak_file'].'thumbnail/'.$v['nama_file'].'" alt="" src="'.$this->cekurl().'/'.$v['letak_file'].'thumbnail/'.$v['nama_file'].'"></a>';
                            $fileuploadnya .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-ng-switch-when="true" data-ng-href="'.$this->cekurl().'/'.$v['letak_file'].''.$v['nama_file'].'" title="'.$v['nama_file'].'" download="'.$v['nama_file'].'" data-gallery="" class="ng-binding ng-scope" href="'.$this->cekurl() .'/'.$v['letak_file'].''.$v['nama_file'].'">'.$nomer.'. '.$v['nama_file'].'</a><br>';
                        } 
                         
                        $nomer++;
                    } 
                    
                    $data['fileuploadcoy'] = $fileuploadnya;

                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }
    
    //untuk menghapus data t_pemabayaranspt
    public function hapusAction() {
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ret = $this->getTblVerifikasi()->batalVerifikasi($this->params('page'));
            $this->getTblVerifikasi()->hapusDataPemeriksaan($this->params('page'));
            $res->setContent(\Zend\Json\Json::encode($ret));
        }
        return $res;
    }
    
    function populatePersyaratanId($idtransaksi, $idspt) {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($idtransaksi);
        $html = "<div>";
        $html .= "<label>Persyaratan Pendaftaran : </label>";
        foreach ($data as $row) {
            $html .= "<div class='col-sm-12'>
                      <div class='form-group'>";
            $html .= "<div class='col-sm-12'>";
            $dataa = $this->getTblPersyaratan()->getDataSyaratSPT($row->s_idpersyaratan, $idspt);
            if ($dataa == false) {
                $html .= "<input id='t_persyaratan' name='t_persyaratan[]' type='checkbox' value='" . $row->s_idpersyaratan . "'>";
            } else {
                $html .= "<input id='t_persyaratan' name='t_persyaratan[]' type='checkbox' value='" . $row->s_idpersyaratan . "' checked='checked' disabled > ";
            }
            $html .= $row->s_namapersyaratan;
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</div>";
        }
        $html .= "</div>";
        return $html;
    }
    
    function populatePersyaratanVerifikasi($id) {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($id);
        $html = "<div>";
        $html .= "<label>Persyaratan Validasi : </label>";
        foreach ($data as $row) {
            $html .= "<div class='col-sm-12'>
                      <div class='form-group'>";
            $html .= "<div class='col-sm-12'>";
            $html .= "<input id='t_persyaratanverifikasi' name='t_persyaratanverifikasi[]' type='checkbox' value='" . $row->s_idpersyaratan . "'> ";
            $html .= $row->s_namapersyaratan;
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</div>";
        }
        $html .= "</div>";
        return $html;
    }
    
    public function editAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $req = $this->getRequest();
        
        if ($req->isGet()) {
            
            $data = $this->getTblPembayaran()->getDataId($req->getQuery()->get('t_idpembayaranspt'));
            $dataArray = $this->getTblPembayaran()->getDataIdArray($req->getQuery()->get('t_idpembayaranspt'));
            
            $data->t_persyaratanverifikasi = $data->t_verifikasispt;
            $frm = new \Bphtb\Form\Verifikasi\VerifikasiSPTFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $string, null, $this->populateCheckBoxverifikasi($data->t_idjenistransaksi));
            $data->t_tglverifikasispt = date('d-m-Y', strtotime($data->t_tglverifikasispt));
            $data->t_keteranganvalidasi = $dataArray['t_keteranganvalidasi'];
            $frm->bind($data);
            $frm->get("t_persyaratanverifikasi")->setValue(\Zend\Json\Json::decode($data->t_persyaratanverifikasi));
        }
        
        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();
        
        $view = new ViewModel(array(
            'frm' => $frm,
            't_pemeriksaanop' => $t_pemeriksaanop,
            'datajenistransaksi' => $datajenistransaksi,
            'datanotaris' => $ar_notaris
        ));
        $data = array(
            'menu_verifikasi_berkas' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username'],
            'session' => $session,
        );
        $this->layout()->setVariables($data);
        return $view;
    }
    
    private function populateCheckBoxverifikasi($idtransaksi) {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($idtransaksi);
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idpersyaratan] = ' ' . $row->s_namapersyaratan;
        }
        return $selectData;
    }

    public function getTblVerifikasi() {
        if (!$this->tbl_verifikasi) {
            $sm = $this->getServiceLocator();
            $this->tbl_verifikasi = $sm->get('VerifikasiSPTTable');
        }
        return $this->tbl_verifikasi;
    }

    public function getTblPembayaran() {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function getTblJenTran() {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan() {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getTblSSPDBphtb() {
        if (!$this->tbl_pendataan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pendataan = $sm->get('SSPDBphtbTable');
        }
        return $this->tbl_pendataan;
    }

    public function getTblPersyaratan() {
        if (!$this->tbl_persyaratan) {
            $sm = $this->getServiceLocator();
            $this->tbl_persyaratan = $sm->get('PersyaratanTable');
        }
        return $this->tbl_persyaratan;
    }

    public function getTblPejabat() {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getPemda() {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getSSPD() {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblSPPT() {
        if (!$this->tbl_nop) {
            $sm = $this->getServiceLocator();
            $this->tbl_nop = $sm->get('SPPTTable');
        }
        return $this->tbl_nop;
    }

}
