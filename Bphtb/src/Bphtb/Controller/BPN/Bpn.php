<?php

namespace Bphtb\Controller\BPN;

use Bphtb\Model\BPN\BPNTable;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Math\Rand;

class Bpn extends AbstractActionController
{

    protected $tbl_pemda, $tbl_pembayaran, $tbl_pendataan, $tbl_jenistransaksi, $tbl_haktanah, $tbl_doktanah, 
            $tbl_sspdbphtb, $tbl_sspd, $tbl_notaris, $tbl_persyaratan, $tbl_spt, $tbl_verifikasi;

    public function cekurl()
    {
        $basePath = $this->getRequest()->getBasePath();
        $uri = new \Zend\Uri\Uri($this->getRequest()->getUri());
        $uri->setPath($basePath);
        $uri->setQuery(array());
        $uri->setFragment('');

        return $uri->getScheme() . '://' . $uri->getHost() . '' . $uri->getPath(); //:'.$_SERVER['SERVER_PORT'].'

    }

    public function indexAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();

        $view = new ViewModel(array(
            'datajenistransaksi' => $datajenistransaksi
        ));
        $data = array(
            'menu_bpn' => 'active',
            'data_bpn' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function dataGridAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $s_iduser = $session['s_iduser'];
        $s_tipe_pejabat = $session['s_tipe_pejabat'];

        $sTable = 'bpn';
        $count = 'bpn_id';

        $input = $this->getRequest();
        $order_default = " bpn_id DESC";
        $aColumns = array('npwp', 'bpn_id', 'tgl_transaksi', 'nomor_akta', 'nama_ppat', 'nop', 'jenis_hak', 'nama_wp', 'kelurahan_op', 'kecamatan_op', 'kab_kota_op',    'nomor_induk_bidang', 'koordinat_x', 'koordinat_y', 'ntpd', 'nik', 'luastanah_op', 'tgl_akta');


        $panggildata = $this->getServiceLocator()->get("PembayaranSptTable");
        $rResult = $panggildata->semuadatabpn($sTable, $count, $input, $order_default, $aColumns, $session, $this->cekurl());
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($rResult));
    }

    public function dataGrid2Action()
    {
        $allParams = (array) $this->getEvent()->getRouteMatch()->getParams();
        $base = new \Bphtb\Model\Pembayaran\PembayaranSptBase();
        $base->exchangeArray($allParams);
        if ($base->direction == 2)
            $base->page = $base->page + 1;
        if ($base->direction == 1)
            $base->page = $base->page - 1;
        if ($base->page <= 0)
            $base->page = 1;
        $page = $base->page;
        $limit = $base->rows;
        $count = $this->getTblPembayaran()->getGridCountBpn($base);
        if ($count > 0 && $limit > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit;
        if ($start < 0)
            $start = 0;
        $data = $this->getTblPembayaran()->getGridDataBpn($base, $start);
        $s = "";
        foreach ($data as $row) {
            $s .= "<tr>";
            $s .= "<td>" . str_pad($row['t_kohirspt'], 4, '0', STR_PAD_LEFT) . "</td>";
            $s .= "<td>" . $row['t_periodepembayaran'] . "</td>";
            $s .= "<td>" . date('d-m-Y', strtotime($row['t_tglprosesspt'])) . "</td>";
            $s .= "<td>" . $row['t_namawppembeli'] . "</td>";
            if ($row['t_verifikasispt'] == true) {
                $status_verifikasi = "Tervalidasi / " . date('d-m-Y', strtotime($row['t_tglverifikasispt']));
            } else {
                $status_verifikasi = "Status : Belum Tervalidasi";
            }
            $s .= "<td>" . $status_verifikasi . "</td>";
            if ($row['t_statusbayarspt'] == true) {
                $status_bayar = "Dibayar / " . number_format($row['t_nilaipembayaranspt'], 0, ',', '.') . " / " . date('d-m-Y', strtotime($row['t_tanggalpembayaran'])) . "";
            } else {
                $status_bayar = "Belum Dibayar";
            }
            $s .= "<td>" . $status_bayar . "</td>";
            if (!empty($row['p_idpemeriksaan'])) {
                $s .= "<td colspan='2'>" . number_format($row['p_luastanah'], 0, ',', '.') . " / " . number_format($row['p_luasbangunan'], 0, ',', '.') . "</td>";
            } else {
                $s .= "<td colspan='2'>" . number_format($row['t_luastanah'], 0, ',', '.') . " / " . number_format($row['t_luasbangunan'], 0, ',', '.') . "</td>";
            }
            $s .= "<td colspan='2'>" . number_format($row['t_luastanahbpn'], 0, ',', '.') . " / " . number_format($row['t_luasbangunanbpn'], 0, ',', '.') . "</td>";
            if (empty($row['t_tglsertifikatbaru'])) {
                $s .= "<td>-</td>";
            } else {
                $s .= "<td>" . $row['t_nosertifikatbaru'] . " / " . date('d-m-Y', strtotime($row['t_tglsertifikatbaru'])) . "</td>";
            }
            $s .= "<td><form method='post' action='bpn_lihatdata/edit' id='formtambah'><input type='hidden' name='t_idspt' value='" . $row['t_iddetailsptbphtb'] . "'/> <input type='hidden' name='status' value='1'/><input value='Cek Detail' class='btn btn-primary btn-sm btn-flat' type='submit'>  </form></td>";
            $s .= "</tr>";
        }
        $data_render = array(
            "grid" => $s,
            "rows" => $base->rows,
            "count" => $count,
            "page" => $page,
            "start" => $start,
            "total_halaman" => $total_pages
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function editAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();
        $id = (int) $this->getRequest()->getPost()->get('t_idspt');
        $status = (int) $this->getRequest()->getPost()->get('status');
        $req = $this->getRequest();
        if ($req->isPost()) {
            $idcari = $this->getTblSSPDBphtb()->getSptid($id);
            $hasil_cariidspt = $idcari->t_idspt;
            $data = $this->getTblSSPDBphtb()->getDataIdBpn($hasil_cariidspt);
            if (!empty($data->p_idpemeriksaan)) {
                $data->t_luastanah = $data->p_luastanah;
                $data->t_njoptanah = $data->p_njoptanah;
                $data->t_luasbangunan = $data->p_luasbangunan;
                $data->t_njopbangunan = $data->p_njopbangunan;
            }
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $data->t_tanggalpembayaran = date('d-m-Y', strtotime($data->t_tanggalpembayaran));
            $data->t_tglverifikasispt = date('d-m-Y', strtotime($data->t_tglverifikasispt));

            $luastanahbpn = $data->fr_luas_tanah_bpn;
            $luasbangunanbpn = $data->fr_luas_bangunan_bpn;

            if (empty($data->t_tglsertifikatbaru)) {
                $data->t_tglsertifikatbaru = '';
            } else {
                $data->t_tglsertifikatbaru = date('d-m-Y', strtotime($data->t_tglsertifikatbaru));
            }
            $frm2 = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), null, $this->populateComboNotaris(), $this->populatePersyaratanId($data->t_idjenistransaksi));
            $frm2->bind($data);
        } else {
            return $this->redirect()->toRoute('bpn_lihatdata');
        }
        $view = new ViewModel(array(
            'frm' => $frm2,
            'status' => $status,
            'id_spt' => $id,
            'luas_tanahbpn' => $luastanahbpn,
            'luas_bangunanbpn' => $luasbangunanbpn
        ));
        $data = array(
            'menu_bpn' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 2,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function simpandatabpnAction()
    {
        $input = $this->getRequest();
        $panggildata = $this->getServiceLocator()->get("TarifBphtbTable");
        $t_luastanahbpn = $input->getPost('t_luastanahbpn');
        $t_luasbangunanbpn = $input->getPost('t_luasbangunanbpn');

        if (!empty($t_luastanahbpn)) {
            $luastanahbpn = $t_luastanahbpn;
        } else {
            $luastanahbpn = 0;
        }

        if (!empty($t_luasbangunanbpn)) {
            $luasbangunanbpn = $t_luasbangunanbpn;
        } else {
            $luasbangunanbpn = 0;
        }

        $iddetailspt = $input->getPost('t_iddetailsptbphtb');

        if (!empty($iddetailspt)) {
            $query = "UPDATE t_detailsptbphtb SET (fr_luas_tanah_bpn, fr_luas_bangunan_bpn) = (" . $luastanahbpn . "," . $luasbangunanbpn . ") WHERE t_iddetailsptbphtb=" . $iddetailspt . " ";
            $panggildata->simpandata($query);
            return $this->redirect()->toRoute('bpn_lihatdata');
        } else {
            return $this->redirect()->toRoute('bpn_lihatdata');
        }
    }

    public function updateAction()
    {
        $string = Rand::getString(6, '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', true);
        $t_idspt = str_replace("'", "\'", $this->getRequest()->getPost('t_idspt'));
        $t_iddetailsptbphtb = str_replace("'", "\'", $this->getRequest()->getPost('t_iddetailsptbphtb'));
        $t_idpembayaranspt = str_replace("'", "\'", $this->getRequest()->getPost('t_idpembayaranspt'));
        $t_luastanahbpn1 = str_replace("'", "\'", $this->getRequest()->getPost('t_luastanahbpn'));
        $t_luasbangunanbpn1 = str_replace("'", "\'", $this->getRequest()->getPost('t_luasbangunanbpn'));

        $t_luastanahbpn = str_ireplace('.', '', $t_luastanahbpn1);
        $t_luasbangunanbpn = str_ireplace('.', '', $t_luasbangunanbpn1);

        $data = $this->getTbl()->getSptid($t_idspt);
        $datadetail = $this->getTblSSPDBphtb()->getSemuaData($t_iddetailsptbphtb);
        $dataverifikasi = $this->getTblVerifikasi()->getSptid($t_idpembayaranspt);

        // cek apakah ada pemeriksaan atau tidak
        if (!empty($datadetail->p_idpemeriksaan)) {
            $datadetail->t_totalnjoptanah = $datadetail->p_njoptanah * $t_luastanahbpn;
            $datadetail->t_totalnjopbangunan = $datadetail->p_njopbangunan * $t_luasbangunanbpn;
            $datadetail->t_grandtotalnjop = $datadetail->t_totalnjoptanah + $datadetail->t_totalnjopbangunan;
            if ($datadetail->p_nilaitransaksispt > $datadetail->t_grandtotalnjop) {
                $npopyangdiambil = $datadetail->p_nilaitransaksispt;
            } else {
                $npopyangdiambil = $datadetail->t_grandtotalnjop;
            }

            if ($data->t_idjenistransaksi == 4 || $data->t_idjenistransaksi == 5) {
                // hibah wasiat dan waris dibagi 50%
                $t_totalspt = ($npopyangdiambil - $datadetail->p_potonganspt) * 5 / 200;
            } else {
                // selain hibah wasiat dan waris
                $t_totalspt = ($npopyangdiambil - $datadetail->p_potonganspt) * 5 / 100;
            }

            if ($t_totalspt >= 0) {
                $data->t_totalspt = $t_totalspt;
            } else {
                $data->t_totalspt = 0;
            }
            $t_luastanah = $datadetail->p_luastanah;
            $t_luasbangunan = $datadetail->p_luasbangunan;
        } else {
            $datadetail->t_totalnjoptanah = $datadetail->t_njoptanah * $t_luastanahbpn;
            $datadetail->t_totalnjopbangunan = $datadetail->t_njopbangunan * $t_luasbangunanbpn;
            $datadetail->t_grandtotalnjop = $datadetail->t_totalnjoptanah + $datadetail->t_totalnjopbangunan;
            if ($data->t_nilaitransaksispt > $datadetail->t_grandtotalnjop) {
                $npopyangdiambil = $data->t_nilaitransaksispt;
            } else {
                $npopyangdiambil = $datadetail->t_grandtotalnjop;
            }

            if ($data->t_idjenistransaksi == 4 || $data->t_idjenistransaksi == 5) {
                // hibah wasiat dan waris dibagi 50%
                $t_totalspt = ($npopyangdiambil - $data->t_potonganspt) * 5 / 200;
            } else {
                // selain hibah wasiat dan waris
                $t_totalspt = ($npopyangdiambil - $data->t_potonganspt) * 5 / 100;
            }

            if ($t_totalspt >= 0) {
                $data->t_totalspt = $t_totalspt;
            } else {
                $data->t_totalspt = 0;
            }
            $t_luastanah = $datadetail->t_luastanah;
            $t_luasbangunan = $datadetail->t_luasbangunan;
        }

        // nilai pembayaran sebelumnya
        $nilaibayarsebelumnya = $datadetail->t_nilaipembayaranspt;

        if ($t_luastanah < $t_luastanahbpn || $t_luasbangunan < $t_luasbangunanbpn) {
            // simpan di pendaftaran
            $dataspt = $this->getTbl()->savedatabpn($data);
            // simpan luas tanah dan bangunan versi bpn
            $this->getTblSSPDBphtb()->savedatadetailbpn($datadetail, $dataspt->t_idspt, $t_luastanahbpn, $t_luasbangunanbpn);
            if ($data->t_totalspt > 0 && $data->t_totalspt > $nilaibayarsebelumnya) {
                // jika ada nominal pembayaran prosesnya : daftar dan validasi
                $this->getTblVerifikasi()->savedataverifikasibpn($dataverifikasi, $dataspt->t_idspt, $string);
            } else {
                // jika nihil maka prosesnya : daftar, validasi dan verifikasi otomatis
                $this->getTblVerifikasi()->savedataverbayarbpn($dataverifikasi, $dataspt->t_idspt, $string);
            }
        }
        // update t_detailsptbphtb untuk luas tanah dan bangunan bpn
        $this->getTblSSPDBphtb()->savedata_koreksiluas($t_iddetailsptbphtb, $t_luastanahbpn, $t_luasbangunanbpn);
        return $this->redirect()->toRoute('bpn_lihatdata');
    }

    public function inputsertifikatAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $id = (int) $this->getRequest()->getQuery()->get('t_idspt');
        $idcari = $this->getTblSSPDBphtb()->getSptid($id);
        $hasil_cariidspt = $idcari->t_idspt;
        $req = $this->getRequest();
        if ($req->isGet()) {
            $data = $this->getTblSSPDBphtb()->getDataIdBpn($hasil_cariidspt);
            $data->t_tglprosesspt = date('d-m-Y', strtotime($data->t_tglprosesspt));
            $data->t_tglajb = date('d-m-Y', strtotime($data->t_tglajb));
            $frm2 = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah(), $this->populateComboDokTanah(), null, $this->populateComboNotaris(), $this->populatePersyaratanId($data->t_idjenistransaksi));
            $frm2->bind($data);
        }
        $view = new ViewModel(array(
            'frm' => $frm2
        ));
        $menu = new ViewModel(array('menu_bpn' => 'active', 'role_id' => $session['s_akses']));
        $menu->setTemplate('bphtb/menu.phtml');
        $view->addChild($menu, 'menu');
        return $view;
    }

    public function updatesertifikatAction()
    {
        $t_iddetailsptbphtb = str_replace("'", "\'", $this->getRequest()->getPost('t_iddetailsptbphtb'));
        $t_nosertifikatbaru = str_replace("'", "\'", $this->getRequest()->getPost('t_nosertifikatbaru'));
        $t_tglsertifikatbaru = str_replace("'", "\'", $this->getRequest()->getPost('t_tglsertifikatbaru'));
        $this->getTblSSPDBphtb()->savedata_sertifikatbaru($t_iddetailsptbphtb, $t_nosertifikatbaru, $t_tglsertifikatbaru);
        return $this->redirect()->toRoute('bpn_lihatdata');
    }

    public function cetakhasilbpnAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $tgl_awal = date('Y-m-d', strtotime($data_get->tgl_hasilbpn1));
        $tgl_akhir = date('Y-m-d', strtotime($data_get->tgl_hasilbpn2));
        $data_hasilbpn = $this->getServiceLocator()->get("BPNTable")->getDataByDate($tgl_awal, $tgl_akhir);
        
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $sm = $this->getServiceLocator();
        $this->tbl_pemda = $sm->get("PemdaTable");
        $ar_pemda = $this->tbl_pemda->getdata();

        $pdf->setOption('filename', 'daftardatabpn');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'landscape');
        $pdf->setOption('enable_html5_parser', TRUE);
        $pdf->setVariables(array(
            'data_hasilbpn' => $data_hasilbpn,
            'tgl_hasilbpn1' => $data_get->tgl_hasilbpn1,
            'tgl_hasilbpn2' => $data_get->tgl_hasilbpn2,
            'ar_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    public function gantiPasswordBpnAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();

        // $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        // $datajenistransaksi = $panggildata->comboBox();

        $akunbpn = $this->getServiceLocator()->get("BPNTable");
        $data_bpn = $akunbpn->getAkunBpn();

        $view = new ViewModel(array(
            'data_bpn' => $data_bpn,
            // 'datajenistransaksi' => $datajenistransaksi
        ));
        $data = array(
            'menu_bpn' => 'active',
            'ubah_password_bpn' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function updatePassBpnAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $old_pass = $request->getPost('pass_lama');
            $new_pass = $request->getPost('pass_baru');
            $update_pass = $this->getServiceLocator()->get('GetDataBPN')->changePassword($old_pass, $new_pass);

            return $this->getResponse()->setContent(Json::encode(['result' => $update_pass]));
        }
        return false;
    }

    public function getDetileDataAction()
    {
        $request = $this->getRequest();
        $bpn_id = $request->getPost('bpn_id');
        $bpn_table = $this->getServiceLocator()->get("BPNTable");
        $data_bpn = $bpn_table->getDataById($bpn_id);

        return $this->getResponse()->setContent(Json::encode($data_bpn));
    }

    public function lokasibpnAction()
    {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $ar_pemda = $this->getPemda()->getdata();

        $akunbpn = $this->getServiceLocator()->get("BPNTable");

        $view = new ViewModel(array(
            // 'data_bpn' => $data_bpn,
            // 'datajenistransaksi' => $datajenistransaksi
        ));
        $data = array(
            'menu_bpn' => 'active',
            'ubah_password_bpn' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function loadDataBPNAction()
    {
        $request = $this->getRequest();
        $tgl_proses = $request->getPost('tgl_proses');
        $data_bpn = $this->getServiceLocator()->get("BPNTable")->getDataByTglProses(date('Y-m-d', strtotime($tgl_proses)));
        $result = array();
        
        foreach ($data_bpn as $val) {
            $result[] = $val;
        }

        return $this->getResponse()->setContent(Json::encode($result));
    }

    public function sinkronisasiAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tgl = $request->getPost('tgl');
            $sinkron = $this->getServiceLocator()->get('GetDataBPN')->sync($tgl);

            return $this->getResponse()->setContent(Json::encode(['result' => $sinkron]));
        }
        return false;
    }

    public function getdatabpnAction()
    { // get data bpn
        $ar_pemda = $this->getPemda()->getdata();
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $form = new \Bphtb\Form\Pendataan\SSPDFrm();

        $panggildata = $this->getServiceLocator()->get("JenisTransaksiBphtbTable");
        $datajenistransaksi = $panggildata->comboBox();
        $ar_notaris = $this->getServiceLocator()->get('NotarisBphtbTable')->getdataCombo();

        $view = new ViewModel(array(
            "form" => $form,
            'datajenistransaksi' => $datajenistransaksi,
            'role_id' => $session['s_akses'],
            'datanotaris' => $ar_notaris
        ));

        $data = array(
            'side_getdatabpn' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'session' => $session,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );

        $this->layout()->setVariables($data);
        return $view;
    }

    public function cekdatabpnAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        $tanggal = date('j/m/Y', strtotime($data_get->tanggal));

        $params = array(
            // 'USERNAME' => "testbphtb",
            'USERNAME' => "bapendasingkawang",
            'PASSWORD' => "a",
            'TANGGAL' => $tanggal,
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            //   CURLOPT_URL => "http://103.49.37.84:8080/BPNDevApi/Api/BPHTB/getDataBPN",
            CURLOPT_URL => "http://103.49.37.84:8080/BPNApiService/Api/BPHTB/getDataBPN",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                // "Postman-Token: a2fc5293-c0bf-4246-ba9a-55ccf94bcee1"
            ),
        ));

        $response = curl_exec($curl);
        echo json_encode($response);
        die;
        @$decode = json_decode($response, true);
        $grid = "";
        if (!empty($decode['result'])) {
            $counter = 1;
            foreach ($decode['result'] as $row) {
                $grid .= '<tr>';
                $grid .= '<td>' . $counter++ . '.</td>
                    <td>' . strtoupper($row['AKTAID']) . '</td>
                    <td>' . strtoupper($row['TGL_AKTA']) . '</td>
                    <td>' . strtoupper($row['NOP']) . '</td>
                    <td>' . strtoupper($row['NIB']) . '</td>
                    <td>' . strtoupper($row['NIK']) . '</td>
                    <td>' . strtoupper($row['NPWP']) . '</td>
                    <td>' . strtoupper($row['NAMA_WP']) . '</td>
                    <td>' . strtoupper($row['ALAMAT_OP']) . '</td>
                    <td>' . strtoupper($row['KELURAHAN_OP']) . '</td>
                    <td>' . strtoupper($row['KECAMATAN_OP']) . '</td>
                    <td>' . strtoupper($row['KOTA_OP']) . '</td>
                    <td align="right">' . number_format($row['LUASTANAH_OP'], 2, ",", ".") . '</td>
                    <td align="right">' . number_format($row['LUASBANGUNAN_OP'], 2, ",", ".") . '</td>
                    <td>' . strtoupper($row['PPAT']) . '</td>
                    <td>' . strtoupper($row['NO_SERTIPIKAT']) . '</td>
                    <td>' . strtoupper($row['NO_AKTA']) . '</td>';
                $grid .= '</tr>';
            };
        } else {
            $grid .= '<tr><td colspan="17" style="color:red">DATA TIDAK ADA!</td><tr>';
        }

        $data_render = array(
            "grid" => $grid
        );
        return $this->getResponse()->setContent(\Zend\Json\Json::encode($data_render));
    }

    public function storedatabpnAction()
    {
        $req = $this->getRequest();
        $data_get = $req->getPost();
        // $tanggal = date('j/m/Y', strtotime($data_get->tanggal));
        $tanggal = date('d/m/Y');

        $params = array(
            // 'USERNAME' => "testbphtb",
            'USERNAME' => "bapendasingkawang",
            'PASSWORD' => "a",
            'TANGGAL' => $tanggal,
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            //   CURLOPT_URL => "http://103.49.37.84:8080/BPNDevApi/Api/BPHTB/getDataBPN",
            CURLOPT_URL => "http://103.49.37.84:8080/BPNApiService/Api/BPHTB/getDataBPN",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($params),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                // "Postman-Token: a2fc5293-c0bf-4246-ba9a-55ccf94bcee1"
            ),
        ));

        $response = curl_exec($curl);
    }

    public function populateComboJenisTransaksi()
    {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idjenistransaksi] = $row->s_namajenistransaksi;
        }
        return $selectData;
    }

    public function getTblJenTran()
    {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblPembayaran()
    {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function populateComboHakTanah()
    {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    public function populateComboDokTanah()
    {
        $data = $this->getTblDokTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_iddoktanah] = $row->s_namadoktanah;
        }
        return $selectData;
    }

    private function populateComboNotaris()
    {
        $data = $this->getNotaris()->getdataCombo();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row['s_iduser']] = $row['s_namanotaris'];
        }
        return $selectData;
    }

    function populatePersyaratanId($id)
    {
        $data = $this->getTblPersyaratan()->getDataIdTransaksis($id);
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idpersyaratan] = $row->s_namapersyaratan;
        }
        return $selectData;
    }

    public function getTblHakTan()
    {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getNotaris()
    {
        if (!$this->tbl_notaris) {
            $sm = $this->getServiceLocator();
            $this->tbl_notaris = $sm->get('NotarisBphtbTable');
        }
        return $this->tbl_notaris;
    }

    public function getTblSSPDBphtb()
    {
        if (!$this->tbl_sspdbphtb) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspdbphtb = $sm->get("SSPDBphtbTable");
        }
        return $this->tbl_sspdbphtb;
    }

    public function getTblVerifikasi()
    {
        if (!$this->tbl_verifikasi) {
            $sm = $this->getServiceLocator();
            $this->tbl_verifikasi = $sm->get("VerifikasiSptTable");
        }
        return $this->tbl_verifikasi;
    }

    public function getTblPersyaratan()
    {
        if (!$this->tbl_persyaratan) {
            $sm = $this->getServiceLocator();
            $this->tbl_persyaratan = $sm->get('PersyaratanTable');
        }
        return $this->tbl_persyaratan;
    }

    public function getTbl()
    {
        if (!$this->tbl_spt) {
            $sm = $this->getServiceLocator();
            $this->tbl_spt = $sm->get("SPTTable");
        }
        return $this->tbl_spt;
    }

    public function getTblDokTan()
    {
        if (!$this->tbl_doktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_doktanah = $sm->get('DokTanahTable');
        }
        return $this->tbl_doktanah;
    }

    public function getPemda()
    {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getTableSSPD()
    {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }
}
