<?php

namespace Bphtb\Controller\Pencetakan;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CetakLaporan extends AbstractActionController {

    protected $tbl_pemda, $tbl_sspd, $tbl_pejabat, $tbl_pendataan, $tbl_jenistransaksi, $tbl_haktanah, $tbl_pembayaran, $tbl_pelaporan;

    public function indexAction() {
        $ar_pemda = $this->getPemda()->getdata();
        $Pejabat = $this->getTblPejabat()->getdata();
        $Mengetahui = $this->getTblPejabat()->getdata();
        $PejabatRealisasi = $this->getTblPejabat()->getdata();
        $MengetahuiRealisasi = $this->getTblPejabat()->getdata();
        $Mengetahuibphtb = $this->getTblPejabat()->getdata();
        
        $PejabatSkpdkb = $this->getTblPejabat()->getdata();
        
                    //========= Model\Setting\PejabatBphtbTable
        $notaris = $this->getTblPejabat()->getdataNotaris();
        $notaris_harian = $this->getTblPejabat()->getdataNotaris();
        
        $PejabatRealisasi_perdesa = $this->getTblPejabat()->getdata();
        $MengetahuiRealisasi_perdesa = $this->getTblPejabat()->getdata();
        
        $data_kecamatan = $this->getTblPejabat()->getdataKecamatan();
        $notaris_perdesa = $this->getTblPejabat()->getdataNotaris();
        $view = new ViewModel(array(
            'data_pejabat' => $Pejabat,
            'data_mengetahui' => $Mengetahui,
            'data_pejabatrealisasi' => $PejabatRealisasi,
            'data_mengetahuirealisasi' => $MengetahuiRealisasi,
            'data_mengetahuibphtb' => $Mengetahuibphtb,
            'data_pejabatskpdkb' => $PejabatSkpdkb,
            'data_notaris' => $notaris,
            'data_notaris_harian' => $notaris_harian,
            'data_pejabatrealisasi_perdesa' => $PejabatRealisasi_perdesa,
            'data_mengetahuirealisasi_perdesa' => $MengetahuiRealisasi_perdesa,
            'data_kecamatan' => $data_kecamatan,
            'data_notaris_perdesa' => $notaris_perdesa,
        ));
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $data = array(
            'menu_cetaklaporan' => 'active',
            'role_id' => $session['s_akses'],
            'data_pemda' => $ar_pemda,
            'aturgambar' => 1,
            'username' => $session['s_username']
        );
        $this->layout()->setVariables($data);
        return $view;
    }

    public function cetaklapbphtbAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        
        //=================== Model\Pencetakan\SSPDTable
        $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan($data_get->tgl_cetakbphtb1, $data_get->tgl_cetakbphtb2, $data_get->idnotaris);
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahuibphtb);
        $ar_pemda = $this->getPemda()->getdata();
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Daftar Laporan Harian BPHTB');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');

        $pdf->setVariables(array(
            'data_lapBphtb' => $ar_LapBphtb,
            'data_mengetahui' => $ar_Mengetahui,
            'tgl_cetakbphtb1' => $data_get->tgl_cetakbphtb1,
            'tgl_cetakbphtb2' => $data_get->tgl_cetakbphtb2,
            'data_pemda' => $ar_pemda,
            'nama_login' => $session['s_namauserrole'],
            'tgl_cetakan' => $data_get->tgl_cetakan
        ));
        return $pdf;
    }

    public function cetaklapharianAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        
        //=================== Model\Pencetakan\SSPDTable
        $ar_LapHarian = $this->getTableSSPD()->getDataLapHarianSetor($data_get->tgl_cetak, $data_get->idnotaris_harian); //getDataLapHarian
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui);
        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabat);
        $ar_pemda = $this->getPemda()->getdata();
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Daftar Laporan Harian BPHTB');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');

        $pdf->setVariables(array(
            'data_lapharian' => $ar_LapHarian,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetak,
            'data_pemda' => $ar_pemda,
            'nama_login' => $session['s_iduser'],
            'tgl_cetakan_harian' => $data_get->tgl_cetakan_harian,
        ));
        return $pdf;
    }

    public function cetaklapbulananbphtbAction() {
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        /*if ($session['s_namauserrole'] == "Administrator" || $session['s_namauserrole'] == "Pegawai") {
            $idnotaris = $data_get->t_idnotarisspt;
        } else if ($session['s_namauserrole'] == "Notaris") {
            $idnotaris = $session['s_iduser'];
        }*/
        //========================== Model\Pencetakan\SSPDTable
        $ar_LapbulananBphtb = $this->getTableSSPD()->getDataLapBulananBphtbNotaris($data_get->bulanpelaporan, $data_get->periode_spt, $data_get->t_idnotarisspt1, $session['s_iduser'], $session['s_tipe_pejabat']);
        //if (!empty($idnotaris)) {
            $data_notaris = $this->getTableSSPD()->getNotaris($session['s_iduser']);
        //}
        $ar_pemda = $this->getPemda()->getdata();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Daftar Laporan Bulanan BPHTB');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'landscape');

        $pdf->setVariables(array(
            'data_lapBulananBphtb' => $ar_LapbulananBphtb,
            'tgl_cetak' => $data_get->tgl_cetaklapbulanan,
            'bulanpelaporan' => $data_get->bulanpelaporan,
            'data_pemda' => $ar_pemda,
            'data_notaris' => $data_notaris,
            'periode_spt' => $data_get->periode_spt    
        ));
        return $pdf;
    }

    public function cetakrealisasibphtbAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        
        //=================== Model\Pencetakan\SSPDTable
        $ar_DataRealisasi = $this->getTableSSPD()->getDataRealisasi($data_get->periode_sptrealisasi, $data_get->tgl_cetakrealisasi);
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui);
        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabat);
        $ar_pemda = $this->getPemda()->getdata();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Daftar Realisasi');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_Realisasi' => $ar_DataRealisasi,
            'data_mengetahui' => $ar_Mengetahui,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetakrealisasi,
            'periode_spt' => $data_get->periode_sptrealisasi,
            'data_pemda' => $ar_pemda,
            'tgl_cetakan_realisasi' => $data_get->tgl_cetakan_realisasi
        ));
        return $pdf;
    }

    public function cetakskpdkbAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        $ar_DataSkpdkb = $this->getTableSSPD()->getDataValidasiSkpdkb($data_get->no_sspdbphtb1, $data_get->no_sspdbphtb2);
        $ar_Pejabat = $this->getTblPejabat()->getdataid($data_get->pejabatskpdkb);
        $ar_pemda = $this->getPemda()->getdata();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'Data SKPDKB');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');
        $pdf->setVariables(array(
            'data_Skpdkb' => $ar_DataSkpdkb,
            'data_pejabat' => $ar_Pejabat,
            'tgl_cetak' => $data_get->tgl_cetakskpdkb,
            'periode_skpdkb' => $data_get->periode_skpdkb,
            'data_pemda' => $ar_pemda
        ));
        return $pdf;
    }

    public function dataGridSkpdkbAction() {
        $res = $this->getResponse();
        $page = $this->getRequest()->getPost('page') ? $this->getRequest()->getPost('page') : 1;
        $rp = $this->getRequest()->getPost('rp') ? $this->getRequest()->getPost('rp') : 10;
        $sortname = $this->getRequest()->getPost('sortname') ? $this->getRequest()->getPost('sortname') : 't_kohirspt';
        $sortorder = $this->getRequest()->getPost('sortorder') ? $this->getRequest()->getPost('sortorder') : 'desc';
        $query = $this->getRequest()->getPost('query') ? $this->getRequest()->getPost('query') : false;
        $qtype = $this->getRequest()->getPost('qtype') ? $this->getRequest()->getPost('qtype') : false;
        $count = $this->getTblPembayaran()->getGridCountValidasi($query, $qtype);
        $start = (($page - 1) * $rp);
        $res->getHeaders()->addheaders(array('Content-type' => 'text/xml'));
        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>" . $page . "</page>";
        $s .= "<total>" . $count . "</total>";
        $s .= "<records>" . $count . "</records>";
        $data = $this->getTblPembayaran()->getGridDataValidasi($sortname, $sortorder, $query, $qtype, $start, $rp);
        foreach ($data as $row) {
            $s .= "<row id='" . $row['t_idspt'] . "'>";
            $s .= "<cell>" . $row['t_kohirspt'] . "</cell>";
            $s .= "<cell>" . $row['t_periodespt'] . "</cell>";
            $s .= "<cell>" . $row['t_tglprosesspt'] . "</cell>";
            $s .= "<cell>" . $row['t_namawppembeli'] . "</cell>";
            $s .= "<cell><![CDATA[<a href='#' onclick='pilihSkpdkbBphtb(" . $row['t_idspt'] . ");return false;' >Pilih</a>]]></cell>";
            $s .= "</row>";
        }
        $s .= "</rows>";
        $res->setContent($s);
        return $res;
    }

    public function pilihSkpdkbBphtbAction() {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getPendataanBphtb()->getPendataanSspdBphtb($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }

    public function dataGridSkpdkb2Action() {
        $res = $this->getResponse();
        $page = $this->getRequest()->getPost('page') ? $this->getRequest()->getPost('page') : 1;
        $rp = $this->getRequest()->getPost('rp') ? $this->getRequest()->getPost('rp') : 10;
        $sortname = $this->getRequest()->getPost('sortname') ? $this->getRequest()->getPost('sortname') : 't_kohirspt';
        $sortorder = $this->getRequest()->getPost('sortorder') ? $this->getRequest()->getPost('sortorder') : 'desc';
        $query = $this->getRequest()->getPost('query') ? $this->getRequest()->getPost('query') : false;
        $qtype = $this->getRequest()->getPost('qtype') ? $this->getRequest()->getPost('qtype') : false;
        $count = $this->getTblPembayaran()->getGridCountValidasi($query, $qtype);
        $start = (($page - 1) * $rp);
        $res->getHeaders()->addheaders(array('Content-type' => 'text/xml'));
        $s = "<?xml version='1.0' encoding='utf-8'?>";
        $s .= "<rows>";
        $s .= "<page>" . $page . "</page>";
        $s .= "<total>" . $count . "</total>";
        $s .= "<records>" . $count . "</records>";
        $data = $this->getTblPembayaran()->getGridDataValidasi($sortname, $sortorder, $query, $qtype, $start, $rp);
        foreach ($data as $row) {
            $s .= "<row id='" . $row['t_idspt'] . "'>";
            $s .= "<cell>" . $row['t_kohirspt'] . "</cell>";
            $s .= "<cell>" . $row['t_periodespt'] . "</cell>";
            $s .= "<cell>" . $row['t_tglprosesspt'] . "</cell>";
            $s .= "<cell>" . $row['t_namawppembeli'] . "</cell>";
            $s .= "<cell><![CDATA[<a href='#' onclick='pilihSkpdkbBphtb2(" . $row['t_idspt'] . ");return false;' >Pilih</a>]]></cell>";
            $s .= "</row>";
        }
        $s .= "</rows>";
        $res->setContent($s);
        return $res;
    }

    public function pilihSkpdkbBphtb2Action() {
        $frm = new \Bphtb\Form\Pendataan\SSPDFrm($this->populateComboJenisTransaksi(), $this->populateComboHakTanah());
        $req = $this->getRequest();
        $res = $this->getResponse();
        if ($req->isPost()) {
            $ex = new \Bphtb\Model\Pendataan\SSPDBphtbBase();
            $frm->setData($req->getPost());
            if (!$frm->isValid()) {
                $ex->exchangeArray($frm->getData());
                $data = $this->getPendataanBphtb()->getPendataanSspdBphtb($ex);
                $res->setContent(\Zend\Json\Json::encode($data));
            }
        }
        return $res;
    }
    
    public function cetaklapbphtbperdesaAction() {
        $req = $this->getRequest();
        $data_get = $req->getQuery();
        
        //=================== Model\Pencetakan\SSPDTable
        $ar_LapBphtb = $this->getTableSSPD()->getDataLapBphtbBulanan_perdesa($data_get->tgl_cetakbphtb_perdesa, $data_get->tgl_cetakbphtb2_perdesa, $data_get->idnotaris_perdesa, $data_get->kecamatan_perdesa);
        $ar_Mengetahui = $this->getTblPejabat()->getdataid($data_get->mengetahui_perdesa);
        $ar_pemda = $this->getPemda()->getdata();
        //$session = new \Zend\Session\Container('user_session');
        $session = $this->getServiceLocator()->get('EtaxService')->getStorage()->read();
        $pdf = new \DOMPDFModule\View\Model\PdfModel();
        $pdf->setOption('filename', 'daftarlaporanperdesa.pdf');
        $pdf->setOption('paperSize', 'legal');
        $pdf->setOption('paperOrientation', 'potrait');

        $pdf->setVariables(array(
            'data_lapBphtb' => $ar_LapBphtb,
            'data_mengetahui' => $ar_Mengetahui,
            'tgl_cetakbphtb1' => $data_get->tgl_cetakbphtb_perdesa,
            'tgl_cetakbphtb2' => $data_get->tgl_cetakbphtb2_perdesa,
            'data_pemda' => $ar_pemda,
            'nama_login' => $session['s_namauserrole'],
            'tgl_cetakan' => $data_get->tgl_cetakan_perdesa
        ));
        return $pdf;
    }

    public function getTableSSPD() {
        if (!$this->tbl_sspd) {
            $sm = $this->getServiceLocator();
            $this->tbl_sspd = $sm->get("SSPDTable");
        }
        return $this->tbl_sspd;
    }

    public function getTblPejabat() {
        if (!$this->tbl_pejabat) {
            $sm = $this->getServiceLocator();
            $this->tbl_pejabat = $sm->get("PejabatBphtbTable");
        }
        return $this->tbl_pejabat;
    }

    public function getPemda() {
        if (!$this->tbl_pemda) {
            $sm = $this->getServiceLocator();
            $this->tbl_pemda = $sm->get("PemdaTable");
        }
        return $this->tbl_pemda;
    }

    public function getPendataanBphtb() {
        if (!$this->tbl_pendataan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pendataan = $sm->get('SSPDBphtbTable');
        }
        return $this->tbl_pendataan;
    }

    public function populateComboJenisTransaksi() {
        $data = $this->getTblJenTran()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idjenistransaksi] = $row->s_namajenistransaksi;
        }
        return $selectData;
    }

    public function populateComboHakTanah() {
        $data = $this->getTblHakTan()->comboBox();
        $selectData = array();
        foreach ($data as $row) {
            $selectData[$row->s_idhaktanah] = $row->s_namahaktanah;
        }
        return $selectData;
    }

    public function getTblJenTran() {
        if (!$this->tbl_jenistransaksi) {
            $sm = $this->getServiceLocator();
            $this->tbl_jenistransaksi = $sm->get('JenisTransaksiBphtbTable');
        }
        return $this->tbl_jenistransaksi;
    }

    public function getTblHakTan() {
        if (!$this->tbl_haktanah) {
            $sm = $this->getServiceLocator();
            $this->tbl_haktanah = $sm->get('HakTanahTable');
        }
        return $this->tbl_haktanah;
    }

    public function getTblPembayaran() {
        if (!$this->tbl_pembayaran) {
            $sm = $this->getServiceLocator();
            $this->tbl_pembayaran = $sm->get('PembayaranSptTable');
        }
        return $this->tbl_pembayaran;
    }

    public function getTblPelaporan() {
        if (!$this->tbl_pelaporan) {
            $sm = $this->getServiceLocator();
            $this->tbl_pelaporan = $sm->get('PelaporanTable');
        }
        return $this->tbl_pelaporan;
    }

}
